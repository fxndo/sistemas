
-- -----------------------------------------------------
-- Table `cotizaciondb`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cotizaciondb`.`usuario` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(70) NULL,
  `email` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  `status` VARCHAR(45) NULL,
  `type` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cotizaciondb`.`proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cotizaciondb`.`proveedor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cotizaciondb`.`producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cotizaciondb`.`producto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NULL,
  `codigoInterno` VARCHAR(45) NULL,
  `codigoProveedor` VARCHAR(45) NULL,
  `catalogo` VARCHAR(100) NULL,
  `marca` VARCHAR(100) NULL,
  `status` VARCHAR(45) NULL,
  `type` INT NULL,
  `proveedor_id` INT NULL,
  `descripcion` VARCHAR(600) NULL,
  `productocol` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_producto_proveedor1_idx` (`proveedor_id` ASC),
  CONSTRAINT `fk_producto_proveedor1`
    FOREIGN KEY (`proveedor_id`)
    REFERENCES `cotizaciondb`.`proveedor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cotizaciondb`.`tipoCliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cotizaciondb`.`tipoCliente` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cotizaciondb`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cotizaciondb`.`Cliente` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(70) NULL,
  `status` VARCHAR(45) NULL,
  `type` INT NULL,
  `tipoCliente_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Cliente_tipoCliente_idx` (`tipoCliente_id` ASC),
  CONSTRAINT `fk_Cliente_tipoCliente`
    FOREIGN KEY (`tipoCliente_id`)
    REFERENCES `cotizaciondb`.`tipoCliente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cotizaciondb`.`cotizacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cotizaciondb`.`cotizacion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATETIME NULL,
  `folio` VARCHAR(45) NULL,
  `total` DOUBLE NULL,
  `status` VARCHAR(45) NULL,
  `type` INT NULL,
  `Cliente_id` INT NOT NULL,
  `usuario_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cotizacion_Cliente1_idx` (`Cliente_id` ASC),
  INDEX `fk_cotizacion_usuario1_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_cotizacion_Cliente1`
    FOREIGN KEY (`Cliente_id`)
    REFERENCES `cotizaciondb`.`Cliente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cotizacion_usuario1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `cotizaciondb`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cotizaciondb`.`edicion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cotizaciondb`.`edicion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATETIME NULL,
  `cotizacion_id` INT NOT NULL,
  `usuario_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_edicion_cotizacion1_idx` (`cotizacion_id` ASC),
  INDEX `fk_edicion_usuario1_idx` (`usuario_id` ASC),
  CONSTRAINT `fk_edicion_cotizacion1`
    FOREIGN KEY (`cotizacion_id`)
    REFERENCES `cotizaciondb`.`cotizacion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_edicion_usuario1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `cotizaciondb`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cotizaciondb`.`cotizacionParte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cotizaciondb`.`cotizacionParte` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `precio` DOUBLE NULL,
  `status` VARCHAR(45) NULL,
  `type` INT NULL,
  `cotizacion_id` INT NOT NULL,
  `producto_id` INT NOT NULL,
  `cantidad` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cotizacionParte_cotizacion1_idx` (`cotizacion_id` ASC),
  INDEX `fk_cotizacionParte_producto1_idx` (`producto_id` ASC),
  CONSTRAINT `fk_cotizacionParte_cotizacion1`
    FOREIGN KEY (`cotizacion_id`)
    REFERENCES `cotizaciondb`.`cotizacion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cotizacionParte_producto1`
    FOREIGN KEY (`producto_id`)
    REFERENCES `cotizaciondb`.`producto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cotizaciondb`.`precio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cotizaciondb`.`precio` (
  `id` INT NOT NULL,
  `precio` DOUBLE NULL,
  `producto_id` INT NOT NULL,
  `tipoCliente_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_precio_producto1_idx` (`producto_id` ASC),
  INDEX `fk_precio_tipoCliente1_idx` (`tipoCliente_id` ASC),
  CONSTRAINT `fk_precio_producto1`
    FOREIGN KEY (`producto_id`)
    REFERENCES `cotizaciondb`.`producto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_precio_tipoCliente1`
    FOREIGN KEY (`tipoCliente_id`)
    REFERENCES `cotizaciondb`.`tipoCliente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


