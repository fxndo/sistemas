/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author fernando
 */
@Entity
@Table(name = "producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p")
    , @NamedQuery(name = "Producto.findById", query = "SELECT p FROM Producto p WHERE p.id = :id")
    , @NamedQuery(name = "Producto.findByIsbn", query = "SELECT p FROM Producto p WHERE p.isbn = :isbn")
    , @NamedQuery(name = "Producto.findByTitulo", query = "SELECT p FROM Producto p WHERE p.titulo = :titulo")
    , @NamedQuery(name = "Producto.findBySerie", query = "SELECT p FROM Producto p WHERE p.serie = :serie")
    , @NamedQuery(name = "Producto.findByAutor", query = "SELECT p FROM Producto p WHERE p.autor = :autor")
    , @NamedQuery(name = "Producto.findByPreciocompra", query = "SELECT p FROM Producto p WHERE p.preciocompra = :preciocompra")
    , @NamedQuery(name = "Producto.findByPrecioventa", query = "SELECT p FROM Producto p WHERE p.precioventa = :precioventa")
    , @NamedQuery(name = "Producto.findByImagen", query = "SELECT p FROM Producto p WHERE p.imagen = :imagen")
    , @NamedQuery(name = "Producto.findByTextlit", query = "SELECT p FROM Producto p WHERE p.textlit = :textlit")
    , @NamedQuery(name = "Producto.findByEstado", query = "SELECT p FROM Producto p WHERE p.estado = :estado")})
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 15)
    @Column(name = "isbn")
    private String isbn;
    @Size(max = 250)
    @Column(name = "titulo")
    private String titulo;
    @Size(max = 45)
    @Column(name = "serie")
    private String serie;
    @Size(max = 100)
    @Column(name = "autor")
    private String autor;
    @Size(max = 45)
    @Column(name = "preciocompra")
    private String preciocompra;
    @Size(max = 45)
    @Column(name = "precioventa")
    private String precioventa;
    @Size(max = 45)
    @Column(name = "imagen")
    private String imagen;
    @Size(max = 15)
    @Column(name = "textlit")
    private String textlit;
    @Size(max = 2)
    @Column(name = "estado")
    private String estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productoId")
    private List<Pedidoparte> pedidoparteList;
    @JoinColumn(name = "editorial_id1", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Editorial editorialId1;
    @JoinColumn(name = "nivel_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Nivel nivelId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private List<VentasHasProducto> ventasHasProductoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "productoId")
    private List<Almacen> almacenList;

    public Producto() {
    }

    public Producto(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getPreciocompra() {
        return preciocompra;
    }

    public void setPreciocompra(String preciocompra) {
        this.preciocompra = preciocompra;
    }

    public String getPrecioventa() {
        return precioventa;
    }

    public void setPrecioventa(String precioventa) {
        this.precioventa = precioventa;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getTextlit() {
        return textlit;
    }

    public void setTextlit(String textlit) {
        this.textlit = textlit;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Pedidoparte> getPedidoparteList() {
        return pedidoparteList;
    }

    public void setPedidoparteList(List<Pedidoparte> pedidoparteList) {
        this.pedidoparteList = pedidoparteList;
    }

    public Editorial getEditorialId1() {
        return editorialId1;
    }

    public void setEditorialId1(Editorial editorialId1) {
        this.editorialId1 = editorialId1;
    }

    public Nivel getNivelId() {
        return nivelId;
    }

    public void setNivelId(Nivel nivelId) {
        this.nivelId = nivelId;
    }

    @XmlTransient
    public List<VentasHasProducto> getVentasHasProductoList() {
        return ventasHasProductoList;
    }

    public void setVentasHasProductoList(List<VentasHasProducto> ventasHasProductoList) {
        this.ventasHasProductoList = ventasHasProductoList;
    }

    @XmlTransient
    public List<Almacen> getAlmacenList() {
        return almacenList;
    }

    public void setAlmacenList(List<Almacen> almacenList) {
        this.almacenList = almacenList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Producto[ id=" + id + " ]";
    }
    
}
