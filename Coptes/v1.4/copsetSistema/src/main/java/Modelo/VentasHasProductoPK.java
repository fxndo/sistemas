/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author fernando
 */
@Embeddable
public class VentasHasProductoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "ventas_id")
    private int ventasId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_id")
    private int productoId;

    public VentasHasProductoPK() {
    }

    public VentasHasProductoPK(int ventasId, int productoId) {
        this.ventasId = ventasId;
        this.productoId = productoId;
    }

    public int getVentasId() {
        return ventasId;
    }

    public void setVentasId(int ventasId) {
        this.ventasId = ventasId;
    }

    public int getProductoId() {
        return productoId;
    }

    public void setProductoId(int productoId) {
        this.productoId = productoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) ventasId;
        hash += (int) productoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentasHasProductoPK)) {
            return false;
        }
        VentasHasProductoPK other = (VentasHasProductoPK) object;
        if (this.ventasId != other.ventasId) {
            return false;
        }
        if (this.productoId != other.productoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.VentasHasProductoPK[ ventasId=" + ventasId + ", productoId=" + productoId + " ]";
    }
    
}
