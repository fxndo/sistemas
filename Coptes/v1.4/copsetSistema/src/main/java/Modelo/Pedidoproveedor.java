/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author fernando
 */
@Entity
@Table(name = "pedidoproveedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pedidoproveedor.findAll", query = "SELECT p FROM Pedidoproveedor p")
    , @NamedQuery(name = "Pedidoproveedor.findById", query = "SELECT p FROM Pedidoproveedor p WHERE p.id = :id")
    , @NamedQuery(name = "Pedidoproveedor.findByEstado", query = "SELECT p FROM Pedidoproveedor p WHERE p.estado = :estado")
    , @NamedQuery(name = "Pedidoproveedor.findByTipo", query = "SELECT p FROM Pedidoproveedor p WHERE p.tipo = :tipo")})
public class Pedidoproveedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "tipo")
    private Integer tipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pedidoProveedorid")
    private List<Pedidoparte> pedidoparteList;
    @JoinColumn(name = "pedido_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pedido pedidoId;
    @JoinColumn(name = "proveedor_id", referencedColumnName = "id")
    @ManyToOne
    private Proveedor proveedorId;

    public Pedidoproveedor() {
    }

    public Pedidoproveedor(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public List<Pedidoparte> getPedidoparteList() {
        return pedidoparteList;
    }

    public void setPedidoparteList(List<Pedidoparte> pedidoparteList) {
        this.pedidoparteList = pedidoparteList;
    }

    public Pedido getPedidoId() {
        return pedidoId;
    }

    public void setPedidoId(Pedido pedidoId) {
        this.pedidoId = pedidoId;
    }

    public Proveedor getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(Proveedor proveedorId) {
        this.proveedorId = proveedorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pedidoproveedor)) {
            return false;
        }
        Pedidoproveedor other = (Pedidoproveedor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Pedidoproveedor[ id=" + id + " ]";
    }
    
}
