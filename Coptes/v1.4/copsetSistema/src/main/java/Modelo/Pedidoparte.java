/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fernando
 */
@Entity
@Table(name = "pedidoparte")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pedidoparte.findAll", query = "SELECT p FROM Pedidoparte p")
    , @NamedQuery(name = "Pedidoparte.findById", query = "SELECT p FROM Pedidoparte p WHERE p.id = :id")
    , @NamedQuery(name = "Pedidoparte.findByCantidad", query = "SELECT p FROM Pedidoparte p WHERE p.cantidad = :cantidad")
    , @NamedQuery(name = "Pedidoparte.findByDevolucion", query = "SELECT p FROM Pedidoparte p WHERE p.devolucion = :devolucion")
    , @NamedQuery(name = "Pedidoparte.findByFaltante", query = "SELECT p FROM Pedidoparte p WHERE p.faltante = :faltante")
    , @NamedQuery(name = "Pedidoparte.findByTipo", query = "SELECT p FROM Pedidoparte p WHERE p.tipo = :tipo")
    , @NamedQuery(name = "Pedidoparte.findByEstado", query = "SELECT p FROM Pedidoparte p WHERE p.estado = :estado")
    , @NamedQuery(name = "Pedidoparte.findByDescuento", query = "SELECT p FROM Pedidoparte p WHERE p.descuento = :descuento")
    , @NamedQuery(name = "Pedidoparte.findByPrecio", query = "SELECT p FROM Pedidoparte p WHERE p.precio = :precio")
    , @NamedQuery(name = "Pedidoparte.findByDescuentoProveedor", query = "SELECT p FROM Pedidoparte p WHERE p.descuentoProveedor = :descuentoProveedor")})
public class Pedidoparte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "cantidad")
    private Integer cantidad;
    @Column(name = "devolucion")
    private Integer devolucion;
    @Column(name = "faltante")
    private Integer faltante;
    @Column(name = "tipo")
    private Integer tipo;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "descuento")
    private Integer descuento;
    @Size(max = 45)
    @Column(name = "precio")
    private String precio;
    @Column(name = "descuentoProveedor")
    private Integer descuentoProveedor;
    @JoinColumn(name = "pedidoProveedor_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pedidoproveedor pedidoProveedorid;
    @JoinColumn(name = "producto_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Producto productoId;
    @JoinColumn(name = "sucursal_id", referencedColumnName = "id")
    @ManyToOne
    private Sucursal sucursalId;

    public Pedidoparte() {
    }

    public Pedidoparte(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getDevolucion() {
        return devolucion;
    }

    public void setDevolucion(Integer devolucion) {
        this.devolucion = devolucion;
    }

    public Integer getFaltante() {
        return faltante;
    }

    public void setFaltante(Integer faltante) {
        this.faltante = faltante;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public Integer getDescuentoProveedor() {
        return descuentoProveedor;
    }

    public void setDescuentoProveedor(Integer descuentoProveedor) {
        this.descuentoProveedor = descuentoProveedor;
    }

    public Pedidoproveedor getPedidoProveedorid() {
        return pedidoProveedorid;
    }

    public void setPedidoProveedorid(Pedidoproveedor pedidoProveedorid) {
        this.pedidoProveedorid = pedidoProveedorid;
    }

    public Producto getProductoId() {
        return productoId;
    }

    public void setProductoId(Producto productoId) {
        this.productoId = productoId;
    }

    public Sucursal getSucursalId() {
        return sucursalId;
    }

    public void setSucursalId(Sucursal sucursalId) {
        this.sucursalId = sucursalId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pedidoparte)) {
            return false;
        }
        Pedidoparte other = (Pedidoparte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Pedidoparte[ id=" + id + " ]";
    }
    
}
