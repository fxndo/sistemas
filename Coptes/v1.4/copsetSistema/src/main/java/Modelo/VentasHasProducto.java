/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fernando
 */
@Entity
@Table(name = "ventas_has_producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VentasHasProducto.findAll", query = "SELECT v FROM VentasHasProducto v")
    , @NamedQuery(name = "VentasHasProducto.findByVentasId", query = "SELECT v FROM VentasHasProducto v WHERE v.ventasHasProductoPK.ventasId = :ventasId")
    , @NamedQuery(name = "VentasHasProducto.findByProductoId", query = "SELECT v FROM VentasHasProducto v WHERE v.ventasHasProductoPK.productoId = :productoId")
    , @NamedQuery(name = "VentasHasProducto.findByCantidad", query = "SELECT v FROM VentasHasProducto v WHERE v.cantidad = :cantidad")
    , @NamedQuery(name = "VentasHasProducto.findByDescuento", query = "SELECT v FROM VentasHasProducto v WHERE v.descuento = :descuento")
    , @NamedQuery(name = "VentasHasProducto.findByEstado", query = "SELECT v FROM VentasHasProducto v WHERE v.estado = :estado")
    , @NamedQuery(name = "VentasHasProducto.findByDevolucion", query = "SELECT v FROM VentasHasProducto v WHERE v.devolucion = :devolucion")
    , @NamedQuery(name = "VentasHasProducto.findByPrecio", query = "SELECT v FROM VentasHasProducto v WHERE v.precio = :precio")})
public class VentasHasProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VentasHasProductoPK ventasHasProductoPK;
    @Column(name = "cantidad")
    private Integer cantidad;
    @Column(name = "descuento")
    private Integer descuento;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "devolucion")
    private Integer devolucion;
    @Size(max = 45)
    @Column(name = "precio")
    private String precio;
    @JoinColumn(name = "producto_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "sucursal_id", referencedColumnName = "id")
    @ManyToOne
    private Sucursal sucursalId;
    @JoinColumn(name = "ventas_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ventas ventas;

    public VentasHasProducto() {
    }

    public VentasHasProducto(VentasHasProductoPK ventasHasProductoPK) {
        this.ventasHasProductoPK = ventasHasProductoPK;
    }

    public VentasHasProducto(int ventasId, int productoId) {
        this.ventasHasProductoPK = new VentasHasProductoPK(ventasId, productoId);
    }

    public VentasHasProductoPK getVentasHasProductoPK() {
        return ventasHasProductoPK;
    }

    public void setVentasHasProductoPK(VentasHasProductoPK ventasHasProductoPK) {
        this.ventasHasProductoPK = ventasHasProductoPK;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getDevolucion() {
        return devolucion;
    }

    public void setDevolucion(Integer devolucion) {
        this.devolucion = devolucion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Sucursal getSucursalId() {
        return sucursalId;
    }

    public void setSucursalId(Sucursal sucursalId) {
        this.sucursalId = sucursalId;
    }

    public Ventas getVentas() {
        return ventas;
    }

    public void setVentas(Ventas ventas) {
        this.ventas = ventas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ventasHasProductoPK != null ? ventasHasProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentasHasProducto)) {
            return false;
        }
        VentasHasProducto other = (VentasHasProducto) object;
        if ((this.ventasHasProductoPK == null && other.ventasHasProductoPK != null) || (this.ventasHasProductoPK != null && !this.ventasHasProductoPK.equals(other.ventasHasProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.VentasHasProducto[ ventasHasProductoPK=" + ventasHasProductoPK + " ]";
    }
    
}
