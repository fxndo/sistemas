/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fernando
 */
@Entity
@Table(name = "permisos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Permisos.findAll", query = "SELECT p FROM Permisos p")
    , @NamedQuery(name = "Permisos.findById", query = "SELECT p FROM Permisos p WHERE p.id = :id")
    , @NamedQuery(name = "Permisos.findByTpvp", query = "SELECT p FROM Permisos p WHERE p.tpvp = :tpvp")
    , @NamedQuery(name = "Permisos.findByComprasp", query = "SELECT p FROM Permisos p WHERE p.comprasp = :comprasp")
    , @NamedQuery(name = "Permisos.findByAlmacenp", query = "SELECT p FROM Permisos p WHERE p.almacenp = :almacenp")
    , @NamedQuery(name = "Permisos.findByPagosp", query = "SELECT p FROM Permisos p WHERE p.pagosp = :pagosp")
    , @NamedQuery(name = "Permisos.findByRemisionp", query = "SELECT p FROM Permisos p WHERE p.remisionp = :remisionp")
    , @NamedQuery(name = "Permisos.findByAdministracionp", query = "SELECT p FROM Permisos p WHERE p.administracionp = :administracionp")
    , @NamedQuery(name = "Permisos.findByProveedoresp", query = "SELECT p FROM Permisos p WHERE p.proveedoresp = :proveedoresp")
    , @NamedQuery(name = "Permisos.findByPedidosp", query = "SELECT p FROM Permisos p WHERE p.pedidosp = :pedidosp")
    , @NamedQuery(name = "Permisos.findByVentap", query = "SELECT p FROM Permisos p WHERE p.ventap = :ventap")
    , @NamedQuery(name = "Permisos.findByProductosp", query = "SELECT p FROM Permisos p WHERE p.productosp = :productosp")
    , @NamedQuery(name = "Permisos.findByTraslados", query = "SELECT p FROM Permisos p WHERE p.traslados = :traslados")
    , @NamedQuery(name = "Permisos.findBySuperusuario", query = "SELECT p FROM Permisos p WHERE p.superusuario = :superusuario")
    , @NamedQuery(name = "Permisos.findByDevolucion", query = "SELECT p FROM Permisos p WHERE p.devolucion = :devolucion")})
public class Permisos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "tpvp")
    private Integer tpvp;
    @Column(name = "comprasp")
    private Integer comprasp;
    @Column(name = "almacenp")
    private Integer almacenp;
    @Column(name = "pagosp")
    private Integer pagosp;
    @Column(name = "remisionp")
    private Integer remisionp;
    @Column(name = "administracionp")
    private Integer administracionp;
    @Column(name = "proveedoresp")
    private Integer proveedoresp;
    @Column(name = "pedidosp")
    private Integer pedidosp;
    @Column(name = "ventap")
    private Integer ventap;
    @Column(name = "productosp")
    private Integer productosp;
    @Column(name = "traslados")
    private Integer traslados;
    @Column(name = "superusuario")
    private Integer superusuario;
    @Column(name = "devolucion")
    private Integer devolucion;
    @JoinColumn(name = "usuario_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario usuarioId;

    public Permisos() {
    }

    public Permisos(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTpvp() {
        return tpvp;
    }

    public void setTpvp(Integer tpvp) {
        this.tpvp = tpvp;
    }

    public Integer getComprasp() {
        return comprasp;
    }

    public void setComprasp(Integer comprasp) {
        this.comprasp = comprasp;
    }

    public Integer getAlmacenp() {
        return almacenp;
    }

    public void setAlmacenp(Integer almacenp) {
        this.almacenp = almacenp;
    }

    public Integer getPagosp() {
        return pagosp;
    }

    public void setPagosp(Integer pagosp) {
        this.pagosp = pagosp;
    }

    public Integer getRemisionp() {
        return remisionp;
    }

    public void setRemisionp(Integer remisionp) {
        this.remisionp = remisionp;
    }

    public Integer getAdministracionp() {
        return administracionp;
    }

    public void setAdministracionp(Integer administracionp) {
        this.administracionp = administracionp;
    }

    public Integer getProveedoresp() {
        return proveedoresp;
    }

    public void setProveedoresp(Integer proveedoresp) {
        this.proveedoresp = proveedoresp;
    }

    public Integer getPedidosp() {
        return pedidosp;
    }

    public void setPedidosp(Integer pedidosp) {
        this.pedidosp = pedidosp;
    }

    public Integer getVentap() {
        return ventap;
    }

    public void setVentap(Integer ventap) {
        this.ventap = ventap;
    }

    public Integer getProductosp() {
        return productosp;
    }

    public void setProductosp(Integer productosp) {
        this.productosp = productosp;
    }

    public Integer getTraslados() {
        return traslados;
    }

    public void setTraslados(Integer traslados) {
        this.traslados = traslados;
    }

    public Integer getSuperusuario() {
        return superusuario;
    }

    public void setSuperusuario(Integer superusuario) {
        this.superusuario = superusuario;
    }

    public Integer getDevolucion() {
        return devolucion;
    }

    public void setDevolucion(Integer devolucion) {
        this.devolucion = devolucion;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permisos)) {
            return false;
        }
        Permisos other = (Permisos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Permisos[ id=" + id + " ]";
    }
    
}
