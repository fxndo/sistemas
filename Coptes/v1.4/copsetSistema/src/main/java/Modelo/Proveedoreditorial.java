/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fernando
 */
@Entity
@Table(name = "proveedoreditorial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proveedoreditorial.findAll", query = "SELECT p FROM Proveedoreditorial p")
    , @NamedQuery(name = "Proveedoreditorial.findById", query = "SELECT p FROM Proveedoreditorial p WHERE p.id = :id")})
public class Proveedoreditorial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "descuento_id", referencedColumnName = "id")
    @ManyToOne
    private Descuento descuentoId;
    @JoinColumn(name = "editorial_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Editorial editorialId;
    @JoinColumn(name = "proveedor_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Proveedor proveedorId;

    public Proveedoreditorial() {
    }

    public Proveedoreditorial(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Descuento getDescuentoId() {
        return descuentoId;
    }

    public void setDescuentoId(Descuento descuentoId) {
        this.descuentoId = descuentoId;
    }

    public Editorial getEditorialId() {
        return editorialId;
    }

    public void setEditorialId(Editorial editorialId) {
        this.editorialId = editorialId;
    }

    public Proveedor getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(Proveedor proveedorId) {
        this.proveedorId = proveedorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedoreditorial)) {
            return false;
        }
        Proveedoreditorial other = (Proveedoreditorial) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Proveedoreditorial[ id=" + id + " ]";
    }
    
}
