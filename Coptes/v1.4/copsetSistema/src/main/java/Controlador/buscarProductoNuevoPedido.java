/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Producto;
import Modelo.Usuario;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.ProductoJpaController;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "buscarProductoNuevoPedido", urlPatterns = {"/buscarProductoNuevoPedido"})
public class buscarProductoNuevoPedido extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        String objeto = request.getParameter("objeto");
        int idPedido = Integer.parseInt(request.getParameter("id"));
        PedidoJpaController pj = new PedidoJpaController(emf);
        Pedido pedido = new Pedido();
        if (idPedido != 0) {
            pedido = pj.findPedido(idPedido);
        }
        ProductoJpaController p = new ProductoJpaController(emf);
        List<Producto> productos = p.buscarProductos(objeto, 0);
        Usuario informacion = (Usuario) sesion.getAttribute("informacion");
        String cadena = "vacio";
        if (productos.size() > 0) {
            cadena = "<tr>\n"
                    + "                            <th>ISBN</th>\n"
                    + "                            <th>Tutulo</th>\n"
                    + "                            <th>Serie</th>\n"
                    + "                            <th>Editorial</th>\n"
                    + "                            <th>$</th>\n"
                    + "                            <th style='width:50px;'></th>\n";
            cadena += "</tr>";
            System.out.println("---:" + objeto);
            int renglon=1;
            for (Producto pd : productos) {
                System.out.println("/-" + pd.getTitulo());
                cadena += "<tr class='renglon"+renglon%2+"'><td id='isbnS" + pd.getId() + "'>" + pd.getIsbn() + "</td>";
                cadena += "<td id='tituloS" + pd.getId() + "'>" + pd.getTitulo() + "</td>";
                if (pd.getSerie() != null) {
                    cadena += "<td id='serieS" + pd.getId() + "'>" + pd.getSerie();
                } else {
                    cadena += "<td><span id='serieS" + pd.getId() + "' class='noInformacion'>Sin Informacion</span>";
                }
                cadena += "</td> <td id='editorialS"+pd.getId()+"'><input type='hidden' id='idEditorialS"+pd.getId()+"' name='idEditorialS"+pd.getId()+"'  value='"+pd.getEditorialId1().getId()+"'>" + pd.getEditorialId1().getNombre()
                        + "</td><td id='precioS"+pd.getId()+"'>"+pd.getPrecioventa()+"</td><td style='padding:0px;'>"
                        + "<div style='padding:6px;display:inline-block;'>";
                cadena += "<button onclick=\"insertarProducto('" + pd.getId() + "')\"; class='btnAgregar quitarBoton'><img class='iconoNormal' src='images/suma2.png'></button></div></td></tr>";
            renglon++;
            }
            cadena = cadena.replace("ñ", "&#241;");
            cadena = cadena.replace("Ñ", "&#209;");
            cadena = cadena.replace("á", "&#225;");
            cadena = cadena.replace("é", "&#233;");
            cadena = cadena.replace("í", "&#237;");
            cadena = cadena.replace("ó", "&#243;");
            cadena = cadena.replace("ú", "&#250;");
            cadena = cadena.replace("Á", "&#193;");
            cadena = cadena.replace("É", "&#201;");
            cadena = cadena.replace("Í", "&#205;");
            cadena = cadena.replace("Ó", "&#211;");
            cadena = cadena.replace("Ú", "&#218;");
            cadena = cadena.replace("¿", "&#191;");
            cadena = cadena.replace("¡", "&#33;");
        }

        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType("application/x-json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        outputStream.print(new Gson().toJson(cadena));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
