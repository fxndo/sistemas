/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Cliente;
import Modelo.Direccion;
import ModeloJSP.ClienteJpaController;
import ModeloJSP.DireccionJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "editarCliente", urlPatterns = {"/editarCliente"})
public class editarCliente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        
        int idCliente =Integer.parseInt(request.getParameter("idCliente"));
        ClienteJpaController c=new ClienteJpaController(emf);
        Cliente cliente=c.findCliente(idCliente);
        
        String nombre=request.getParameter("nombre");
        String escuela=request.getParameter("escuela");
        String numTarjeta=request.getParameter("tarjeta");
        String telefono=request.getParameter("telefono");
        String celular=request.getParameter("celular");
        String email=request.getParameter("email");
        
        String calle=request.getParameter("calle");
        String colonia=request.getParameter("colonia");
        String delegacion=request.getParameter("delegacion");
        String cp=request.getParameter("cp");
        String ciudad=request.getParameter("ciudad");
        String estado=request.getParameter("estado");
        String tarjeta=request.getParameter("tarjeta");
        
        DireccionJpaController d=new DireccionJpaController(emf);
        Direccion direccion=d.findDireccion(cliente.getId());
        
        direccion.setCalle(calle);
        direccion.setColonia(colonia);
        direccion.setDelegacion(delegacion);
        direccion.setCp(cp);
        direccion.setCiudad(ciudad);
        direccion.setEstado(estado);
        
        try {
            d.edit(direccion);
        }  catch (Exception ex) {
            Logger.getLogger(editarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        cliente.setDireccionId(direccion);
        cliente.setNombre(nombre);
        cliente.setEscuela(escuela);
        cliente.setNumerotarjeta(numTarjeta);
        cliente.setCelular(celular);
        cliente.setTelefono(telefono);
        cliente.setEmail(email);
        cliente.setNumerotarjeta(tarjeta);
        
        try {
            c.edit(cliente);
        } catch (Exception ex) {
            Logger.getLogger(editarCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.sendRedirect("clientes.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
