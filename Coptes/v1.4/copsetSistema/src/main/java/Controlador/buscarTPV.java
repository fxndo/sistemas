/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Producto;
import Modelo.Sucursal;
import Modelo.Usuario;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.ProductoJpaController;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "buscarTPV", urlPatterns = {"/buscarTPV"})
public class buscarTPV extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        String objeto = request.getParameter("objeto");
        ProductoJpaController p = new ProductoJpaController(emf);
        List<Producto> productos = p.buscarProductos(objeto, 0);
        AlmacenJpaController a = new AlmacenJpaController(emf);
        Usuario informacion = (Usuario) sesion.getAttribute("informacion");
        Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursalTPV");

        String cadena = "vacio";
        if (productos.size() > 0) {
            cadena = "<tr>\n"
                    + "                            <th>Tutulo</th>\n"
                    + "                            <th>Autor</th>\n"
                    + "                            <th>Editorial</th>\n"
                    + "                            <th>Existencia</th>\n"
                    + "                            <th>Opciones</th>\n"
                    + "                            </tr>";

            for (Producto pd : productos) {
                int existencia = 0;
                if (pd.getAlmacenList().size() > 0) {
                    existencia = a.productosAlmacen(pd.getId(), sucursal.getId());
                }
                if (existencia > 0) {
                    cadena += "<tr class='noEmpty'>";
                } else {
                    cadena += "<tr class='empty'>";
                }
                cadena += "<td>" + pd.getTitulo() + "</td> <td>" + pd.getAutor()
                        + "</td> <td>" + pd.getEditorialId1().getNombre()
                        + "</td><td style='text-align: center;'><strong>" + existencia
                        + "</strong></td><td class='puntero' style='text-align: center;'>";
                if (existencia > 0) {
                    cadena += "<img onclick=\"incluirProducto('" + pd.getIsbn() + "')\" class='iconoNormalGrande' src='images/suma2.png'>";
                }
                cadena += "</td></tr>";
            }
            cadena = cadena.replace("ñ", "n");
            cadena = cadena.replace("Ñ", "N");
        }

        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType("application/x-json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        outputStream.print(new Gson().toJson(cadena));

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
