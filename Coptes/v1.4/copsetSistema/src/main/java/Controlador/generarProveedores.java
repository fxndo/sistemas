/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
PedidoParte: 1.-Proveedor 2.-Sucursal 
 */
package Controlador;

import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoparteJpaController;
import ModeloJSP.PedidoproveedorJpaController;
import ModeloJSP.ProveedorJpaController;
import ModeloJSP.ProveedoreditorialJpaController;
import ModeloJSP.SucursalJpaController;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "generarProveedores", urlPatterns = {"/generarProveedores"})
public class generarProveedores extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        PedidoparteJpaController ppaj = new PedidoparteJpaController(emf);
        PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
        SucursalJpaController scj = new SucursalJpaController(emf);
        int idPedido = Integer.parseInt(request.getParameter("idPedido"));
        PedidoJpaController pj = new PedidoJpaController(emf);
        Pedido pedido = pj.findPedido(idPedido);
        ProveedorJpaController prj = new ProveedorJpaController(emf);
        ProveedoreditorialJpaController pej=new ProveedoreditorialJpaController(emf);

//            ---------------------------------------------------------------------------------------------------------
        int cantidad = Integer.parseInt(request.getParameter("cantidad"));
        int pendiente = 0;
        
        for (int c = 1; c < cantidad; c++) {

            Pedidoparte parte = ppaj.findPedidoparte(Integer.parseInt(request.getParameter("idPedidoParte" + c)));

            int idProveedor = 0;
            if (request.getParameter("idProveedor" + c) != null) {
                idProveedor = Integer.parseInt(request.getParameter("idProveedor" + c));
            }
            int descuento = Integer.parseInt(request.getParameter("descuento" + c));
            int origen = 0;
            if(request.getParameter("origen"+c)!=null){
                origen = Integer.parseInt(request.getParameter("origen" + c));
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Pedidoproveedor pedidoPro = new Pedidoproveedor();
            if (parte.getPedidoProveedorid().getProveedorId() != null) {
                if (idProveedor == 0) {
//-----------------------------------------------------------------------------------------------------------------
                    int idPedidoPro = buscarPedioproNull(pedido);
                    if (idPedidoPro == 0) {
                        pedidoPro.setEstado(1);
                        pedidoPro.setPedidoId(pedido);
                        pedidoPro.setProveedorId(null);
                        pprj.create(pedidoPro);
                        idPedidoPro = pedidoPro.getId();
                    }
                    parte.setDescuento(descuento);
                    parte.setPedidoProveedorid(pprj.findPedidoproveedor(idPedidoPro));
                    parte.setEstado(2);
                    parte.setSucursalId(scj.findSucursal(origen));
                    pendiente = 1;
//-----------------------------------------------------------------------------------------------------------------
                } else {
                    if (pprj.pedidoP(parte.getPedidoProveedorid().getPedidoId().getId(), idProveedor) > 0) {
                        int idPedidoPro = pprj.pedidoP(parte.getPedidoProveedorid().getPedidoId().getId(), idProveedor);
                        pedidoPro = pprj.findPedidoproveedor(idPedidoPro);
                    } else {
                        pedidoPro.setEstado(1);
                        pedidoPro.setPedidoId(pedido);
                        pedidoPro.setProveedorId(prj.findProveedor(idProveedor));
                        pprj.create(pedidoPro);
                    }

                    parte.setDescuento(descuento);
                    parte.setPedidoProveedorid(pedidoPro);
                    parte.setDescuentoProveedor(pej.proveedorDescuento(parte.getProductoId().getEditorialId1().getId(), idProveedor));
                    parte.setDescuentoProveedor(pej.proveedorDescuento(parte.getProductoId().getEditorialId1().getId(), idProveedor));
//-----------------------------------------------------------------------------------------------------------------
                }
            } else {
                if (parte.getSucursalId() != null) {
                    if (idProveedor != 0) {
                        if (pprj.pedidoP(parte.getPedidoProveedorid().getPedidoId().getId(), idProveedor) > 0) {
                            int idPedidoPro = pprj.pedidoP(parte.getPedidoProveedorid().getPedidoId().getId(), idProveedor);
                            pedidoPro = pprj.findPedidoproveedor(idPedidoPro);
                        } else {
                            pedidoPro.setPedidoId(pedido);
                            pedidoPro.setProveedorId(prj.findProveedor(idProveedor));
                            pedidoPro.setEstado(1);
                            pprj.create(pedidoPro);
                        }
                        parte.setPedidoProveedorid(pedidoPro);
                        parte.setSucursalId(null);
                        parte.setDescuento(descuento);
                        parte.setDescuentoProveedor(pej.proveedorDescuento(parte.getProductoId().getEditorialId1().getId(), idProveedor));
                        parte.setEstado(1);
                    } else {
//-----------------------------------------------------------------------------------------------------------------
                        parte.setDescuento(descuento);
                        parte.setDescuentoProveedor(pej.proveedorDescuento(parte.getProductoId().getEditorialId1().getId(), idProveedor));
                        parte.setSucursalId(scj.findSucursal(origen));
                        pendiente = 1;
//-----------------------------------------------------------------------------------------------------------------
                    }
                } else {
                    if (idProveedor != 0) {

                        if (pprj.pedidoP(parte.getPedidoProveedorid().getPedidoId().getId(), idProveedor) > 0) {
                            int idPedidoPro = pprj.pedidoP(parte.getPedidoProveedorid().getPedidoId().getId(), idProveedor);
                            pedidoPro = pprj.findPedidoproveedor(idPedidoPro);
                        } else {
                            pedidoPro.setPedidoId(pedido);
                            pedidoPro.setProveedorId(prj.findProveedor(idProveedor));
                            pedidoPro.setEstado(1);
                            pprj.create(pedidoPro);
                        }
                        parte.setDescuento(descuento);
                        parte.setDescuentoProveedor(pej.proveedorDescuento(parte.getProductoId().getEditorialId1().getId(), idProveedor));
                        parte.setPedidoProveedorid(pedidoPro);
                    } else {
//-----------------------------------------------------------------------------------------------------------------
                        parte.setSucursalId(scj.findSucursal(origen));
                        parte.setDescuento(descuento);
                        parte.setDescuentoProveedor(pej.proveedorDescuento(parte.getProductoId().getEditorialId1().getId(), idProveedor));
                        
                        parte.setEstado(2);
                        pendiente = 1;
//-----------------------------------------------------------------------------------------------------------------
                    }
                }
            }
            
            parte.setPrecio(parte.getProductoId().getPrecioventa());
            System.out.println("Precio: "+parte.getProductoId().getPrecioventa());
            try {
            System.out.println("Editado");
                ppaj.edit(parte);
            } catch (Exception ex) {
                Logger.getLogger(generarProveedores.class.getName()).log(Level.SEVERE, null, ex);
            }

            pedido = pj.findPedido(pedido.getId());
            for (Pedidoproveedor pprm : pedido.getPedidoproveedorList()) {
                if (pprm.getPedidoparteList().isEmpty()) {
                    try {
                        pprj.destroy(pprm.getId());
                    } catch (IllegalOrphanException ex) {
                        Logger.getLogger(generarProveedores.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(generarProveedores.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        }
//            ---------------------------------------------------------------------------------------------------------------

        pedido = pj.findPedido(pedido.getId());
        double total = 0;
        for (Pedidoproveedor pprf : pedido.getPedidoproveedorList()) {
            for (Pedidoparte ppaf : pprf.getPedidoparteList()) {
                double descuento = Double.parseDouble(ppaf.getPrecio()) * ppaf.getDescuento() / 100;
                double semi = Double.parseDouble(ppaf.getProductoId().getPrecioventa()) - descuento;
                total += ppaf.getCantidad() * semi;
            }
        }
        total= new BigDecimal(total).setScale(0, RoundingMode.HALF_EVEN).doubleValue();
        pedido.setEstado("2");
        pedido.setTotal(total);
        try {
            pj.edit(pedido);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(generarProveedores.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(generarProveedores.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("pedido.jsp?id=" + pedido.getId() + "&list=proveedores&noti=10");
    }

    public int buscarPedioproNull(Pedido pedido) {
        int idPp = 0;
        for (Pedidoproveedor pprm : pedido.getPedidoproveedorList()) {
            if (pprm.getProveedorId() == null) {
                idPp = pprm.getId();
            }
        }
        return idPp;
    }

    public int buscarPedidoproVacio(Pedido pedido) {
        int id = 0;
        for (Pedidoproveedor pprm : pedido.getPedidoproveedorList()) {
            if (pprm.getPedidoparteList().size() == 0) {
                id = pprm.getId();
            }
        }
        return id;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
