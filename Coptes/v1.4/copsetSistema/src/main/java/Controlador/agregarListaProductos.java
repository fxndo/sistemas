/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Editorial;
import Modelo.Nivel;
import Modelo.Pedido;
import Modelo.Producto;
import ModeloJSP.EditorialJpaController;
import ModeloJSP.NivelJpaController;
import ModeloJSP.ProductoJpaController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.Thread.sleep;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarListaProductos", urlPatterns = {"/agregarListaProductos"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB 
        maxFileSize = 1024 * 1024 * 50, // 50 MB
        maxRequestSize = 1024 * 1024 * 100)
public class agregarListaProductos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        String tipo = request.getParameter("tipo");
        int idEditorial = Integer.parseInt(request.getParameter("editorial"));
        int idNivel = Integer.parseInt(request.getParameter("nivel"));
        int pagina = Integer.parseInt(request.getParameter("hoja"));
        Part part = request.getPart("archivo");
        int agregados = 0;
        int pendientes = 0;
        int actualizar = Integer.parseInt(request.getParameter("actualizar"));
        int editados = 0;
        int validar = 100;
        String valor = "2";

        if (idEditorial != 0 || idNivel != 0 || pagina != 0) {
            ProductoJpaController pj = new ProductoJpaController(emf);
            ProductoJpaController ptj = new ProductoJpaController(emf);
            EditorialJpaController edj = new EditorialJpaController(emf);
            NivelJpaController nj = new NivelJpaController(emf);
            Editorial editorial = edj.findEditorial(idEditorial);
            Nivel nivel = nj.findNivel(idNivel);
            try {
                String applicationPath = request.getServletContext().getRealPath("") + "/reportes/";
                String uploadFilePath = applicationPath;
                String fileName = null;
                if (!part.getSubmittedFileName().substring(part.getSubmittedFileName().lastIndexOf(".") + 1).equals("xlsx")) {
                    validar = 101;
                } else {
                    fileName = "listaProductos" + "." + part.getSubmittedFileName().substring(part.getSubmittedFileName().lastIndexOf(".") + 1);
                    File imagen = new File(applicationPath + fileName);
                    if (imagen.exists()) {
                        imagen.delete();
                    }
                    part.write(uploadFilePath + fileName);
                    try {
                        sleep(3000);
                    } catch (Exception ex) {
                    }

                    FileInputStream file = new FileInputStream(new File(uploadFilePath + fileName));
                    XSSFWorkbook worbook = new XSSFWorkbook(file);
                    XSSFSheet sheet = worbook.getSheetAt(pagina - 1);
                    Iterator<Row> rowIterator = sheet.iterator();
                    Row row;
                    DataFormatter formatter = new DataFormatter();
                    while (rowIterator.hasNext()) {
                        valor="2";
                        row = rowIterator.next();
                        Iterator<Cell> cellIterator = row.cellIterator();
                        Cell cell;
                        Producto producto = new Producto();
                        if (formatter.formatCellValue(row.getCell(0)).equals("ISBN") || formatter.formatCellValue(row.getCell(0)).equals("isbn")) {
                        } else {
                            if (formatter.formatCellValue(row.getCell(0)).length() == 13) {
                                if (pj.productoTPV(formatter.formatCellValue(row.getCell(0))) == 0) {
                                    agregados++;
                                    producto.setIsbn(formatter.formatCellValue(row.getCell(0)));
                                    producto.setTitulo(formatter.formatCellValue(row.getCell(1)));

                                    if (formatter.formatCellValue(row.getCell(2)).length() > 0) {
                                        String precio = "";
                                        precio = formatter.formatCellValue(row.getCell(2));
                                        precio = precio.replace("$", "");
                                        precio = precio.replace(" ", "");
                                        precio = precio.replace(",", "");
                                        producto.setPreciocompra(precio);
                                        producto.setPrecioventa(precio);
                                    } else {
                                        producto.setPreciocompra("0");
                                        producto.setPrecioventa("0");
                                        valor = "1";
                                        pendientes++;
                                    }

                                    producto.setAutor("Sin autor");
                                    producto.setSerie("Sin serie");
                                    producto.setTextlit(tipo);
                                    producto.setNivelId(nivel);
                                    producto.setEditorialId1(editorial);
                                    producto.setEstado(valor);
                                    ptj.create(producto);
                                } else {
                                    if (actualizar == 1) {
                                        producto = pj.findProducto(pj.productoTPV(formatter.formatCellValue(row.getCell(0))));
                                        if (formatter.formatCellValue(row.getCell(1)).length() > 0) {
                                            producto.setTitulo(formatter.formatCellValue(row.getCell(1)));
                                        } else {
                                            producto.setTitulo("-----");
                                            valor = "1";
                                            pendientes++;
                                        }

                                        String precio = "";
                                        if (formatter.formatCellValue(row.getCell(2)).length() > 0) {
                                            precio = formatter.formatCellValue(row.getCell(2));
                                            precio = precio.replace("$", "");
                                            precio = precio.replace(" ", "");
                                            precio = precio.replace(",", "");
                                            producto.setPreciocompra(precio);
                                            producto.setPrecioventa(precio);
                                        } else {
                                            producto.setPreciocompra("0");
                                            producto.setPrecioventa("0");
                                            valor = "1";
                                            pendientes++;
                                        }
                                        producto.setTextlit(tipo);
                                        producto.setNivelId(nivel);
                                        producto.setEditorialId1(editorial);
                                        producto.setEstado(valor);
                                        pj.edit(producto);
                                        editados++;
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
        if (validar == 100) {
            response.sendRedirect("buscarProducto?noti=23&agregados=" + agregados + "&noAgregados=" + editados);
        } else {
            response.sendRedirect("subirListaProductos.jsp?ma=" + validar);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
