/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Producto;
import Modelo.Sucursal;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.ProductoJpaController;
import ModeloJSP.SucursalJpaController;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "verStock", urlPatterns = {"/verStock"})
public class verStock extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

        System.out.println("Id: ");
        int idProducto = Integer.parseInt(request.getParameter("idProducto"));
        System.out.println("Id: "+idProducto);
        SucursalJpaController s = new SucursalJpaController(emf);
        List<Sucursal> sucursales = s.findSucursalEntities();
        AlmacenJpaController a = new AlmacenJpaController(emf);
        ProductoJpaController p=new ProductoJpaController(emf);
        Producto producto=p.findProducto(idProducto);
        
        String pregunta=producto.getTitulo();
        
        pregunta = pregunta.replace("á", "&aacute");
        pregunta = pregunta.replace("é", "&eacute;");
        pregunta = pregunta.replace("í", "&iacute;");
        pregunta = pregunta.replace("ó", "&oacute;");
        pregunta = pregunta.replace("ú", "&uacute;");
        pregunta = pregunta.replace("ñ", "&ntilde;");
        pregunta = pregunta.replace("¿", "&iquest;");
        
        String valor = "<h4>"+pregunta+"</h4><table class=\"table table-responsive table-striped tabla-producto letraChica\">\n"
                + "    <tr>\n"
                + "        <th>Sucursal</th>\n"
                + "        <th>Stock</th>\n"
                + "    </tr>";

        for (Sucursal sc : sucursales) {
            if (sc.getId() != 1) {
                valor += "<tr><td>" + sc.getNombre() + "</td><td>" + a.productosAlmacen(idProducto, sc.getId()) + "</td></tr>";
            }
        }
        valor += "</table>";

        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType("application/x-json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        outputStream.print(new Gson().toJson(valor));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
