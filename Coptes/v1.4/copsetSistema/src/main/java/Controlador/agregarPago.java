/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Caja;
import Modelo.Pago;
import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import Modelo.Usuario;
import ModeloJSP.CajaJpaController;
import ModeloJSP.PagoJpaController;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoproveedorJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarPago", urlPatterns = {"/agregarPago"})
public class agregarPago extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        int idPedido = Integer.parseInt(request.getParameter("idPedido"));
        int accion=0;
        if(request.getParameter("accion")!=null){
            accion=Integer.parseInt(request.getParameter("accion"));
        }
        PedidoJpaController p = new PedidoJpaController(emf);
        PedidoproveedorJpaController ppv = new PedidoproveedorJpaController(emf);
        Pedido pedido = p.findPedido(idPedido);
        double monto = Double.parseDouble(request.getParameter("monto"));
        Usuario informacion = (Usuario) sesion.getAttribute("informacion");
        Calendar fecha = new GregorianCalendar();
        PagoJpaController pg = new PagoJpaController(emf);
        CajaJpaController cj = new CajaJpaController(emf);

        double faltante = 0;
        for (Pago pa : pedido.getPagoList()) {
            faltante += pa.getMonto();
        }

        double total = pedido.getTotal();
        

        if (monto <= total - faltante && monto <= total) {
            faltante = pedido.getTotal() - faltante;
            if (faltante == monto) {
                
                for (Pedidoproveedor pp : pedido.getPedidoproveedorList()) {
                    pp.setEstado(6);
                    try {
                        ppv.edit(pp);
                    } catch (Exception ex) {
                        Logger.getLogger(agregarPago.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }

            Pago pago = new Pago();
            pago.setFecha(fecha.getTime());
            pago.setMonto(monto);
            pago.setPedidoId(pedido);
            pago.setUsuarioId(informacion);

            pg.create(pago);

            if (accion==1) {
                Caja caja=cj.findCaja(cj.cerrarCaja(informacion.getSucursalId().getId()));
                double cantidad=caja.getInicio()+monto;
                caja.setInicio(cantidad);
                try {
                    cj.edit(caja);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(agregarPago.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(agregarPago.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        response.sendRedirect("pedido.jsp?id=" + pedido.getId() + "&list=pagos&noti=17");
        }else{
        response.sendRedirect("pedido.jsp?id=" + pedido.getId() + "&list=pagos&noti=18");
            
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
