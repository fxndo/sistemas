/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Direccion;
import Modelo.Permisos;
import Modelo.Sucursal;
import Modelo.Usuario;
import ModeloJSP.DireccionJpaController;
import ModeloJSP.PermisosJpaController;
import ModeloJSP.SucursalJpaController;
import ModeloJSP.UsuarioJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarUsuario", urlPatterns = {"/agregarUsuario"})
public class agregarUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

        UsuarioJpaController u = new UsuarioJpaController(emf);
        Usuario usuario = new Usuario();

        DireccionJpaController d = new DireccionJpaController(emf);
        Direccion direccion = new Direccion();
        SucursalJpaController s = new SucursalJpaController(emf);

        String nombre = request.getParameter("nombre");
        String cargo = request.getParameter("cargo");
        String telefono = request.getParameter("telefono");
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");

        if (u.verificarEmail(email) != 1) {

            int idSucursal = Integer.parseInt(request.getParameter("sucursal"));
            Sucursal sucursal = s.findSucursal(idSucursal);

            int tpv = 0;
            int compras = 0;
            int almacen = 0;
            int pagos = 0;
            int remision = 0;
            int admin = 0;
            int pedidos = 0;
            int ventas = 0;
            int superUsuario = 0;
            int proveedores = 0;
            int productos = 0;
            int traslados=0;

                tpv = Integer.parseInt(request.getParameter("tpv"));
                compras =Integer.parseInt(request.getParameter("compras"));
                almacen = Integer.parseInt(request.getParameter("almacen"));
                pagos = Integer.parseInt(request.getParameter("pagos"));
                remision = Integer.parseInt(request.getParameter("remisiones"));
                admin = Integer.parseInt(request.getParameter("administrador"));
                superUsuario = Integer.parseInt(request.getParameter("superUsuario"));
                pedidos = Integer.parseInt(request.getParameter("pedidos"));
                ventas = Integer.parseInt(request.getParameter("ventas"));
                proveedores = Integer.parseInt(request.getParameter("proveedores"));
                traslados = Integer.parseInt(request.getParameter("traslados"));
                productos = 1;

            usuario.setNombre(nombre);
            usuario.setTipo(cargo);
            usuario.setTelefono(telefono);
            usuario.setEmail(email);
            usuario.setPassword(pass);
            usuario.setSucursalId(sucursal);

            u.create(usuario);

            PermisosJpaController pm = new PermisosJpaController(emf);
            Permisos permiso = new Permisos();

            permiso.setAdministracionp(admin);
            permiso.setAlmacenp(almacen);
            permiso.setComprasp(compras);
            permiso.setPagosp(pagos);
            permiso.setPedidosp(pedidos);
            permiso.setProductosp(productos);
            permiso.setProveedoresp(proveedores);
            permiso.setRemisionp(remision);
            permiso.setTpvp(tpv);
            permiso.setUsuarioId(usuario);
            permiso.setVentap(ventas);
            permiso.setSuperusuario(superUsuario);
            permiso.setTraslados(traslados);
            pm.create(permiso);

            response.sendRedirect("usuarios.jsp");
        } else {
            response.sendRedirect("usuarios.jsp?m=564");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
