/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Editorial;
import Modelo.Nivel;
import Modelo.Producto;
import Modelo.Proveedor;
import ModeloJSP.EditorialJpaController;
import ModeloJSP.NivelJpaController;
import ModeloJSP.ProductoJpaController;
import ModeloJSP.ProveedorJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "editarProducto", urlPatterns = {"/editarProducto"})
public class editarProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        
        int idProducto=Integer.parseInt(request.getParameter("idProducto"));
        String isbn =request.getParameter("isbn");
        String textlit= request.getParameter("textlit");
        String titulo= request.getParameter("titulo");
        int idEditorial=Integer.parseInt(request.getParameter("idEditorial"));
        String autor= request.getParameter("autor");
        int idNivel=Integer.parseInt(request.getParameter("idNivel"));
        String pventa= request.getParameter("pventa");
        
        NivelJpaController n=new NivelJpaController(emf);
        Nivel nivel=n.findNivel(idNivel);
        
        EditorialJpaController ed=new EditorialJpaController(emf);
        Editorial editorial=ed.findEditorial(idEditorial);
        
        ProductoJpaController p=new ProductoJpaController(emf);
        Producto producto=p.findProducto(idProducto);
        
        producto.setIsbn(isbn);
        producto.setTextlit(textlit);
        producto.setTitulo(titulo);
        producto.setEditorialId1(editorial);
        producto.setAutor(autor);
        producto.setNivelId(nivel);
        producto.setPrecioventa(pventa);
        producto.setEstado("2");
        
        try {
            p.edit(producto);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(editarProducto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(editarProducto.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(request.getParameter("regresar")!=null){
            request.getRequestDispatcher("pedido.jsp?id="+request.getParameter("regresar")+"&list=proveedores").forward(request, response);
        }else{
            request.getRequestDispatcher("buscarProducto").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
