/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Sucursal;
import Modelo.Usuario;
import ModeloJSP.SucursalJpaController;
import ModeloJSP.UsuarioJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "iniciarSesion", urlPatterns = {"/iniciarSesion"})
public class iniciarSesion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        HttpSession csrf=request.getSession(true);
        
        String token="token";
        String tokenSesion="sesion";
        int validar=0;
        if(request.getParameter("csrfToken")!=null && csrf.getAttribute("token")!=null){
            token=request.getParameter("csrfToken");
            tokenSesion=csrf.getAttribute("token").toString();
        }
        
        if(!token.equals(tokenSesion)){
            validar=1;
        }
        
        String email = request.getParameter("usuario");
        String password = request.getParameter("password");
        
        
        
        SucursalJpaController s=new SucursalJpaController(emf);
        
        UsuarioJpaController u = new UsuarioJpaController(emf);
        int estado = 0;
        estado = u.logear(email, password);
        if (estado > 0 && validar==0) {
            HttpSession sesion = request.getSession(true);
            sesion.setMaxInactiveInterval(3600);
            Usuario usuario = u.findUsuario(estado);
            Sucursal suc=s.findSucursal(usuario.getSucursalId().getId());
            sesion.setAttribute("informacion", usuario);
            sesion.setAttribute("sucursal", suc);
            if(s.findSucursalEntities().size()>1){
            request.getRequestDispatcher("inicio.jsp").forward(request, response);
            }else{
            request.getRequestDispatcher("bienvenido.jsp").forward(request, response);
                
            }
        } else {
            request.getRequestDispatcher("index.jsp?m=497").forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
