/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.SucursalJpaController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Thread.sleep;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;

/**
 *
 * @author fernando
 */
@WebServlet(name = "reporteRemision", urlPatterns = {"/reporteRemision"})
public class reporteRemision extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        SimpleDateFormat formato = new SimpleDateFormat("dd/MMMM/yyyy", new Locale("es"));
        SimpleDateFormat formato2 = new SimpleDateFormat("hh:mm a", new Locale("es"));
        Calendar fecha = new GregorianCalendar();
        SucursalJpaController s=new SucursalJpaController(emf);
        PedidoJpaController p = new PedidoJpaController(emf);
        Pedido pedido = p.findPedido(Integer.parseInt(request.getParameter("id")));
//        PedidoHasProductoJpaController ph=new PedidoHasProductoJpaController(emf);
        
        Locale locale = new Locale("es","MX");
        NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

        
        

        ///////////////////////CAMBIO DE DIRECTORIO//////////////////////////////
        String rutaArchivo = request.getServletContext().getRealPath("");
//        File archivoXLS = new File(rutaArchivo + "\\reportes\\reporteRemision.xls");
        File archivoXLS = new File(rutaArchivo + "/reportes/reporteRemision.xls");

        archivoXLS.createNewFile();

        Workbook libro = new HSSFWorkbook();
        FileOutputStream archivo = new FileOutputStream(archivoXLS);

        Sheet hoja = libro.createSheet("Compra");

        CellStyle my_style = libro.createCellStyle();
        CellStyle my_style2 = libro.createCellStyle();
        CellStyle my_style2i = libro.createCellStyle();
        CellStyle my_style2d = libro.createCellStyle();
        CellStyle my_style2id = libro.createCellStyle();
        CellStyle my_style3 = libro.createCellStyle();
        CellStyle my_style3i = libro.createCellStyle();
        CellStyle my_style3ic = libro.createCellStyle();
        CellStyle my_style3icd = libro.createCellStyle();
        CellStyle my_style3id = libro.createCellStyle();
        CellStyle my_style3idd = libro.createCellStyle();
        CellStyle my_style4 = libro.createCellStyle();
        CellStyle my_style4d = libro.createCellStyle();
        CellStyle my_style5 = libro.createCellStyle();
        CellStyle my_style6 = libro.createCellStyle();

        HSSFPalette palette2 = ((HSSFWorkbook) libro).getCustomPalette();
        palette2.setColorAtIndex((short) 58, (byte) 255, (byte) 73, (byte) 0);

        HSSFPalette palette3 = ((HSSFWorkbook) libro).getCustomPalette();
        palette3.setColorAtIndex((short) 59, (byte) 0, (byte) 0, (byte) 0);

        HSSFPalette palette4 = ((HSSFWorkbook) libro).getCustomPalette();
        palette4.setColorAtIndex((short) 57, (byte) 247, (byte) 247, (byte) 247);

        Font font = libro.createFont();
        font.setColor(HSSFColor.WHITE.index);
        font.setFontHeightInPoints((short) 7);

        Font font2 = libro.createFont();
        font2.setColor(palette3.getColor(59).getIndex());
        font2.setFontHeightInPoints((short) 7);
//        font2.setBoldweight(Short.MAX_VALUE);

        Font font3 = libro.createFont();
        font3.setColor(HSSFColor.BLACK.index);
        font3.setFontHeightInPoints((short) 7);
        font3.setBoldweight(Short.MAX_VALUE);

        Font font4 = libro.createFont();
        font4.setColor(palette3.getColor(59).getIndex());
        font4.setFontHeightInPoints((short) 7);
        font4.setBoldweight(Short.MAX_VALUE);

        Font font5 = libro.createFont();
        font5.setFontHeightInPoints((short) 7);
        font5.setColor(palette3.getColor(59).getIndex());

        my_style.setFont(font);
        my_style.setFillForegroundColor(palette2.getColor(58).getIndex());
        my_style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style.setAlignment(my_style.ALIGN_CENTER);
        my_style.setFont(font);

        my_style2.setFont(font2);
        my_style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style2.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style2.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style2.setBorderBottom(HSSFColor.BLACK.index);
        my_style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);

        my_style2i.setFont(font2);
        my_style2i.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style2i.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style2i.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style2i.setBorderBottom(HSSFColor.BLACK.index);
        my_style2i.setBorderLeft(HSSFColor.BLACK.index);
        my_style2i.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style2i.setBorderLeft(HSSFCellStyle.BORDER_THIN);

        my_style2d.setFont(font2);
        my_style2d.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style2d.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style2d.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style2d.setBorderBottom(HSSFColor.BLACK.index);
        my_style2d.setBorderRight(HSSFColor.BLACK.index);
        my_style2d.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style2d.setBorderRight(HSSFCellStyle.BORDER_THIN);

        my_style2id.setFont(font2);
        my_style2id.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style2id.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style2id.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style2id.setBorderBottom(HSSFColor.BLACK.index);
        my_style2id.setBorderLeft(HSSFColor.BLACK.index);
        my_style2id.setBorderRight(HSSFColor.BLACK.index);
        my_style2id.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style2id.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style2id.setBorderRight(HSSFCellStyle.BORDER_THIN);

        my_style3.setFont(font3);
        my_style3.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3.setFillForegroundColor(HSSFColor.WHITE.index);

        my_style3i.setFont(font3);
        my_style3i.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3i.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3i.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3i.setBorderLeft(HSSFColor.BLACK.index);
        my_style3i.setBorderLeft(HSSFCellStyle.BORDER_THIN);

        my_style3ic.setFont(font3);
        my_style3ic.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3ic.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3ic.setAlignment(my_style.ALIGN_CENTER);
        my_style3ic.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3ic.setBorderLeft(HSSFColor.BLACK.index);
        my_style3ic.setBorderLeft(HSSFCellStyle.BORDER_THIN);

        my_style3icd.setFont(font3);
        my_style3icd.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3icd.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3icd.setAlignment(my_style.ALIGN_RIGHT);
        my_style3icd.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3icd.setBorderLeft(HSSFColor.BLACK.index);
        my_style3icd.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        

        my_style3id.setFont(font3);
        my_style3id.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3id.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3id.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3id.setBorderLeft(HSSFColor.BLACK.index);
        my_style3id.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style3id.setBorderLeft(HSSFColor.BLACK.index);
        my_style3id.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style3id.setBorderRight(HSSFColor.BLACK.index);
        my_style3id.setBorderRight(HSSFCellStyle.BORDER_THIN);

        my_style3idd.setFont(font3);
        my_style3idd.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3idd.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3idd.setAlignment(my_style.ALIGN_RIGHT);
        my_style3idd.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3idd.setBorderLeft(HSSFColor.BLACK.index);
        my_style3idd.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style3idd.setBorderLeft(HSSFColor.BLACK.index);
        my_style3idd.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style3idd.setBorderRight(HSSFColor.BLACK.index);
        my_style3idd.setBorderRight(HSSFCellStyle.BORDER_THIN);
       
        

        my_style4.setFont(font4);
        my_style4.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style4.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style4.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style4.setBorderLeft(HSSFColor.BLACK.index);
        my_style4.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style4.setBorderBottom(HSSFColor.BLACK.index);
        my_style4.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style4.setBorderRight(HSSFColor.BLACK.index);
        my_style4.setBorderRight(HSSFCellStyle.BORDER_THIN);
        my_style4.setBorderTop(HSSFColor.BLACK.index);
        my_style4.setBorderTop(HSSFCellStyle.BORDER_THIN);

        my_style4d.setFont(font4);
        my_style4d.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style4d.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style4d.setAlignment(my_style.ALIGN_RIGHT);
        my_style4d.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style4d.setBorderLeft(HSSFColor.BLACK.index);
        my_style4d.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style4d.setBorderBottom(HSSFColor.BLACK.index);
        my_style4d.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style4d.setBorderRight(HSSFColor.BLACK.index);
        my_style4d.setBorderRight(HSSFCellStyle.BORDER_THIN);
        my_style4d.setBorderTop(HSSFColor.BLACK.index);
        my_style4d.setBorderTop(HSSFCellStyle.BORDER_THIN);
        

        my_style5.setFont(font5);
        my_style5.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style5.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style5.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style5.setAlignment(my_style.ALIGN_LEFT);

        my_style6.setFont(font5);
        my_style6.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style6.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style6.setFillForegroundColor(palette4.getColor(57).getIndex());
        my_style6.setAlignment(my_style.ALIGN_LEFT);

        /////////////////////////////////////////////
//        FileInputStream inputStream = new FileInputStream(request.getServletContext().getRealPath("")+"\\images\\logo.jpg");
        FileInputStream inputStream = new FileInputStream(request.getServletContext().getRealPath("")+"/images/logo.jpg");
        byte[] bytes = IOUtils.toByteArray(inputStream);
        int pictureIdx = libro.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
        inputStream.close();
        CreationHelper helper = libro.getCreationHelper();
        Drawing drawing = hoja.createDrawingPatriarch();
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(0); //Column B
        anchor.setRow1(0); //Row 3
        anchor.setCol2(3); //Column C
        anchor.setRow2(4); //Row 4
        Picture pict = drawing.createPicture(anchor, pictureIdx);
        /////////////////////////////////////////////
        Row cabecera = hoja.createRow(0);
//        cabecera.setHeightInPoints(30);
//        hoja.setColumnWidth(0, 2000);
        hoja.setColumnWidth(0, 3000);
        hoja.setColumnWidth(1, 2500);
        hoja.setColumnWidth(2, 2500);
        hoja.setColumnWidth(3, 2500);
        hoja.setColumnWidth(4, 2500);
        hoja.setColumnWidth(5, 3000);
        hoja.setColumnWidth(6, 3000);
        hoja.setColumnWidth(7, 3000);

//        Cell cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);

        Cell cab = cabecera.createCell(0);
        cab.setCellValue("");
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);

        ////////////////////////////////////////////
        cabecera = hoja.createRow(1);
        
//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);
        
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        if(request.getParameter("tipo")!=null && request.getParameter("tipo").equals("remision")){
            cab.setCellValue("NOTA DE REMISIÓN");
        }else{
            cab.setCellValue("NOTA DE PEDIDO");
        }
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(2);
        
//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);
        
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(3);
        
//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);
        
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(4);
        
//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);
        
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(5);
//        cabecera.setHeightInPoints(20);
        
//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(0);
        cab.setCellValue("COMERCIALIZADORA DE PROYECTOS, TECNOLOGIAS, EDUCATIVOS Y SUMINISTROS.");
        cab.setCellStyle(my_style3);
        
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(6);
        
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(7);
//        cabecera.setHeightInPoints(20);
        
//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(0);
        cab.setCellValue("FECHA");
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(1);
        cab.setCellValue(formato.format(pedido.getFecha()));
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(6);
        cab.setCellValue("FOLIO NO.");
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(7);
        cab.setCellValue("PD-"+pedido.getId());
        cab.setCellStyle(my_style2id);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(8);
//        cabecera.setHeightInPoints(20);
        
//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(0);
        cab.setCellValue("VENDEDOR ");
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(1);
        cab.setCellValue(pedido.getUsuarioId().getNombre());
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellValue("FECHA DE PAGO: ");
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2d);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(9);
//        cabecera.setHeightInPoints(20);
        
//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(0);
        cab.setCellValue("ESCUELA");
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(1);
        cab.setCellValue(pedido.getClienteId().getEscuela());
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellValue("REPRESENTANTE:");
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(6);
        cab.setCellValue(pedido.getClienteId().getNombre());
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2d);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(10);
        
        cab = cabecera.createCell(0);
        cab.setCellValue("DOMICILIO: ");
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(1);
        cab.setCellValue("Calle "+pedido.getClienteId().getDireccionId().getCalle()+" Col."+pedido.getClienteId().getDireccionId().getColonia()+","+pedido.getClienteId().getDireccionId().getDelegacion()+" C.P. "+pedido.getClienteId().getDireccionId().getCp());
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2d);
        //////////////////////////////////////////////
        cabecera = hoja.createRow(11);
        
        cab = cabecera.createCell(0);
        cab.setCellValue("Colonia: ");
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(1);
        cab.setCellValue(pedido.getClienteId().getDireccionId().getColonia()+","+pedido.getClienteId().getDireccionId().getDelegacion()+" C.P. "+pedido.getClienteId().getDireccionId().getCp());
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellValue("Telefono");
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(6);
        cab.setCellValue(pedido.getClienteId().getTelefono());
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2d);
        //////////////////////////////////////////////
        cabecera = hoja.createRow(12);
        
        cab = cabecera.createCell(0);
        cab.setCellValue("Ciudad Estado: ");
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(1);
        cab.setCellValue(pedido.getClienteId().getDireccionId().getCiudad()+","+pedido.getClienteId().getDireccionId().getEstado());
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellValue("RFC:");
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(6);
        cab.setCellValue("");
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2d);
        //////////////////////////////////////////////
        cabecera = hoja.createRow(13);
        
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);
        
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2);
        //////////////////////////////////////////////
        cabecera = hoja.createRow(14);


        cab = cabecera.createCell(0);
        cab.setCellValue(" Titulo,serie,editorial");
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(4);
        cab.setCellValue("Cantidad");
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(5);
        cab.setCellValue("Precio Unitario");
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(6);
        cab.setCellValue("Descuento");
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(7);
        cab.setCellValue("Importe");
        cab.setCellStyle(my_style2id);

        
        int valor = 15;
        int valor2 = 1;
        double total=0;
        for(Pedidoproveedor pv: pedido.getPedidoproveedorList()){
            for (Pedidoparte php : pv.getPedidoparteList()) {
                Row fila = hoja.createRow(valor);

                Cell cab2 = fila.createCell(0);
                cab2.setCellValue(php.getProductoId().getIsbn());
                cab2.setCellStyle(my_style3i);
                

                cab2 = fila.createCell(1);
                if(php.getTipo()==2 && php.getEstado()==2){
                    cab2.setCellValue("("+s.findSucursal(php.getSucursalId().getId()).getNombre()+") "+php.getProductoId().getTitulo()); 
                }else{
                    cab2.setCellValue(php.getProductoId().getTitulo());
                }
                cab2.setCellStyle(my_style3);

                cab2 = fila.createCell(2);
                cab2.setCellStyle(my_style3);
                
                
                cab2 = fila.createCell(3);
                cab2.setCellValue(php.getProductoId().getEditorialId1().getNombre());
                cab2.setCellStyle(my_style3);
                

                cab2 = fila.createCell(4);
                cab2.setCellValue((php.getCantidad()-php.getFaltante()));
                cab2.setCellStyle(my_style3ic);
                
                
                cab2 = fila.createCell(5);
                cab2.setCellValue(nf.format(Double.parseDouble(php.getPrecio())));
                cab2.setCellStyle(my_style3icd);
                
                cab2 = fila.createCell(6);
                cab2.setCellValue(" % "+php.getDescuento());
                cab2.setCellStyle(my_style3ic);
                
                double descuento = Double.parseDouble(php.getPrecio()) * php.getDescuento() / 100;
                double semi = Double.parseDouble(php.getProductoId().getPrecioventa()) - descuento;
                double importe=(php.getCantidad()-php.getFaltante()-php.getDevolucion()) * semi;
                total += importe;
        
                cab2 = fila.createCell(7);
                cab2.setCellValue(nf.format(new BigDecimal(importe).setScale(0, RoundingMode.UP).doubleValue()));
                cab2.setCellStyle(my_style3idd);
                
                valor++;
                valor2++;
            }
        }
        
        
        
        cabecera = hoja.createRow(valor);
        
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2i);
        
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2id);

        valor++;

        cabecera = hoja.createRow(valor);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        valor++;

        cabecera = hoja.createRow(valor);
        cab = cabecera.createCell(0);
        cab.setCellValue("( " + convertirLetra(String.valueOf(new BigDecimal(total).setScale(0, RoundingMode.UP).doubleValue()))+") pesos 00/100 MXN");
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellValue(nf.format(new BigDecimal(total).setScale(0, RoundingMode.UP).doubleValue()));
        cab.setCellStyle(my_style4d);
        
        valor++;
        valor++;
        /////////////////////////////////////////////
//        FileInputStream inputStream2 = new FileInputStream(request.getServletContext().getRealPath("")+"\\images\\contrato.png");
        FileInputStream inputStream2 = new FileInputStream(request.getServletContext().getRealPath("")+"/images/contrato.png");
        byte[] bytes2 = IOUtils.toByteArray(inputStream2);
        int pictureIdx2 = libro.addPicture(bytes2, Workbook.PICTURE_TYPE_PNG);
        inputStream2.close();
        CreationHelper helper2 = libro.getCreationHelper();
        Drawing drawing2 = hoja.createDrawingPatriarch();
        ClientAnchor anchor2 = helper2.createClientAnchor();
        anchor2.setCol1(0); //Column B
        anchor2.setRow1(valor); //Row 3
        anchor2.setCol2(8); //Column C
        anchor2.setRow2(valor+19); //Row 4
        Picture pict2 = drawing2.createPicture(anchor2, pictureIdx2);
        /////////////////////////////////////////////
        
        libro.write(archivo);
        archivo.close();
        try {
            sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(reporteCompra.class.getName()).log(Level.SEVERE, null, ex);
        }
//        Desktop.getDesktop().open(archivoXLS);
        response.sendRedirect("reportes/reporteRemision.xls");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
    private String convertirLetra(String numero){
    String valor="";
    System.out.println("numero: "+numero);
    String[] partes=numero.split("\\.");
    int entero=Integer.parseInt(partes[0]);
    int miles=entero/1000;
    if(miles>=2){
        valor+= convertirLetra(String.valueOf(miles))+" mil ";
        entero-=miles*1000;
    }
    if(miles==1){
        valor+="mil ";
        entero-=miles*1000;
    }
    int centenas = entero / 100;
        int decenas  = (entero % 100) / 10;
        int unidades = (entero % 10);
 
        switch (centenas) {
            case 0: break;
            case 1:
                if (decenas == 0 && unidades == 0) {
                    valor+="Cien ";
                    return valor;
                }
                else valor+="Ciento ";
                break;
            case 2: valor+="Doscientos "; break;
            case 3: valor+="Trescientos "; break;
            case 4: valor+="Cuatrocientos "; break;
            case 5: valor+="Quinientos "; break;
            case 6: valor+="Seiscientos "; break;
            case 7: valor+="Setecientos "; break;
            case 8: valor+="Ochocientos "; break;
            case 9: valor+="Novecientos "; break;
        }
 
        switch (decenas) {
            case 0: break;
            case 1:
                if (unidades == 0) { valor+="Diez "; return valor; }
                else if (unidades == 1) { valor+="Once "; return valor; }
                else if (unidades == 2) { valor+="Doce "; return valor; }
                else if (unidades == 3) { valor+="Trece "; return valor; }
                else if (unidades == 4) { valor+="Catorce "; return valor; }
                else if (unidades == 5) { valor+="Quince "; return valor; }
                else valor+="Dieci";
                break;
            case 2:
                if (unidades == 0) { valor+="Veinte "; return valor; }
                else valor+="Veinti";
                break;
            case 3: valor+="Treinta "; break;
            case 4: valor+="Cuarenta "; break;
            case 5: valor+="Cincuenta "; break;
            case 6: valor+="Sesenta "; break;
            case 7: valor+="Setenta "; break;
            case 8: valor+="Ochenta "; break;
            case 9: valor+="Noventa "; break;
        }
 
        if (decenas > 2 && unidades > 0)
            valor+="y ";
 
        switch (unidades) {
            case 0: break;
            case 1: valor+="Un "; break;
            case 2: valor+="Dos "; break;
            case 3: valor+="Tres "; break;
            case 4: valor+="Cuatro "; break;
            case 5: valor+="Cinco "; break;
            case 6: valor+="Seis "; break;
            case 7: valor+="Siete "; break;
            case 8: valor+="Ocho "; break;
            case 9: valor+="Nueve "; break;
        }
        
        if(partes.length>1){
            if(partes[1].length()==1){
                partes[1]+="0";
            }
            if(!partes[1].equals("00")){
                valor+=" con "+convertirLetra(partes[1])+" centavos.";
            }
        }
 
    return valor;
}
}
