/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Direccion;
import Modelo.Proveedor;
import ModeloJSP.DireccionJpaController;
import ModeloJSP.ProveedorJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarProveedor", urlPatterns = {"/agregarProveedor"})
public class agregarProveedor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

        String clave = request.getParameter("clave");
        String completo = request.getParameter("completo");
//        String rfc=request.getParameter("rfc");
        String porcentaje = "0";
        String email = request.getParameter("email");
        String telefono = request.getParameter("telefono");

        String calle = request.getParameter("calle");
        String colonia = request.getParameter("colonia");
        String delegacion = request.getParameter("delegacion");
        String cp = request.getParameter("cp");
        String ciudad = request.getParameter("ciudad");
        String estado = request.getParameter("estado");

        DireccionJpaController d = new DireccionJpaController(emf);
        Direccion direccion = new Direccion();

        direccion.setCalle(calle);
        direccion.setColonia(colonia);
        direccion.setDelegacion(delegacion);
        direccion.setCp(cp);
        direccion.setCiudad(ciudad);
        direccion.setEstado(estado);

        d.create(direccion);

        ProveedorJpaController p = new ProveedorJpaController(emf);
        Proveedor proveedor = new Proveedor();

        proveedor.setClave(clave);
        proveedor.setNombre(completo);
        proveedor.setPorcentaje("1");
        proveedor.setDireccionId(direccion);
//        proveedor.setRfc(rfc);
        proveedor.setPorcentaje(porcentaje);
        proveedor.setTelefono(telefono);
        proveedor.setEmail(email);
        proveedor.setEstado("1");

        p.create(proveedor);

//        response.sendRedirect("proveedores.jsp?noti=4");
        response.sendRedirect("proveedor.jsp?id="+proveedor.getId()+"&noti=4&list=editoriales");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
