/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Almacen;
import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoparteJpaController;
import ModeloJSP.PedidoproveedorJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "devolucionPedidoProveedor", urlPatterns = {"/devolucionPedidoProveedor"})
public class devolucionPedidoProveedor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        PedidoparteJpaController p = new PedidoparteJpaController(emf);
        PedidoproveedorJpaController pv = new PedidoproveedorJpaController(emf);

        AlmacenJpaController a = new AlmacenJpaController(emf);

        int idPedido = Integer.parseInt(request.getParameter("idPedido"));

        double resta = 0;
        for (int z = 1; z < 300; z++) {
            if (request.getParameter("idPro" + z) != null) {
                int idPro = Integer.parseInt(request.getParameter("idPro" + z));
                Pedidoparte pedido = p.findPedidoparte(idPro);
                int cantidad = 0;
                cantidad = Integer.parseInt(request.getParameter("cantidad" + z));
                pedido.setDevolucion(cantidad);
                try {
                    p.edit(pedido);
                } catch (Exception ex) {
                    Logger.getLogger(entregaPedidoProveedor.class.getName()).log(Level.SEVERE, null, ex);
                }
                resta += (Double.parseDouble(pedido.getPrecio()) - (Double.parseDouble(pedido.getPrecio()) * (pedido.getDescuento() / 100))) * cantidad;

                int idAlmacen = a.obtenerAlmacen(pedido.getProductoId().getId(), pedido.getPedidoProveedorid().getPedidoId().getSucursalId().getId());
                Almacen almacen = new Almacen();
                if (idAlmacen != 0) {
                    almacen = a.findAlmacen(idAlmacen);
                    cantidad += almacen.getCantidad();
                }
                almacen.setCantidad(cantidad);
                if (idAlmacen != 0) {
                    try {
                        a.edit(almacen);
                    } catch (Exception ex) {
                        Logger.getLogger(devolucionPedidoProveedor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    almacen.setProductoId(pedido.getProductoId());
                    almacen.setSucursalId(pedido.getPedidoProveedorid().getPedidoId().getSucursalId());
                    a.create(almacen);
                }

            }
        }

        PedidoJpaController ped = new PedidoJpaController(emf);
        Pedido superPedido = ped.findPedido(idPedido);
        superPedido.setEstado("12");
        
        
        double total = 0;
        for (Pedidoproveedor pprf : superPedido.getPedidoproveedorList()) {
            for (Pedidoparte ppaf : pprf.getPedidoparteList()) {
                double descuento = Double.parseDouble(ppaf.getPrecio()) * ppaf.getDescuento() / 100;
                double semi = Double.parseDouble(ppaf.getProductoId().getPrecioventa()) - descuento;
                total += (ppaf.getCantidad()-ppaf.getFaltante()-ppaf.getDevolucion()) * semi;
            }
        }
        total= new BigDecimal(total).setScale(0, RoundingMode.UP).doubleValue();
        superPedido.setTotal(total);
        
        
        
//        superPedido.setTotal(superPedido.getTotal() - resta);
        try {
            ped.edit(superPedido);
        } catch (Exception ex) {
            Logger.getLogger(entregaPedidoProveedor.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("pedido.jsp?id=" + superPedido.getId() + "&list=productos&noti=16");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
