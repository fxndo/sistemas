/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoparteJpaController;
import ModeloJSP.PedidoproveedorJpaController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;

/**
 *
 * @author fernando
 */
@WebServlet(name = "reporteCompra", urlPatterns = {"/reporteCompra"})
public class reporteCompra extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

        PedidoproveedorJpaController p = new PedidoproveedorJpaController(emf);
        PedidoparteJpaController ppa=new PedidoparteJpaController(emf);
        
        List<Pedidoparte>partes=ppa.listaPedidoParteTipo(Integer.parseInt(request.getParameter("id")));
        Pedidoproveedor pedido = p.findPedidoproveedor(Integer.parseInt(request.getParameter("id")));
        
        String rutaArchivo = request.getServletContext().getRealPath("");
        File archivoXLS = new File(rutaArchivo + "/reportes/reporteCompra.xls");
        archivoXLS.createNewFile();
        Workbook libro = new HSSFWorkbook();
        FileOutputStream archivo = new FileOutputStream(archivoXLS);
        Sheet hoja = libro.createSheet("Compra");
        CellStyle my_style = libro.createCellStyle();
        CellStyle my_style2 = libro.createCellStyle();
        CellStyle my_style3 = libro.createCellStyle();
        CellStyle my_style4 = libro.createCellStyle();
        CellStyle my_style5 = libro.createCellStyle();
        CellStyle my_style6 = libro.createCellStyle();

        HSSFPalette palette2 = ((HSSFWorkbook) libro).getCustomPalette();
        palette2.setColorAtIndex((short) 58, (byte) 255, (byte) 73, (byte) 0);

        HSSFPalette palette3 = ((HSSFWorkbook) libro).getCustomPalette();
        palette3.setColorAtIndex((short) 59, (byte) 63, (byte) 63, (byte) 63);

        HSSFPalette palette4 = ((HSSFWorkbook) libro).getCustomPalette();
        palette4.setColorAtIndex((short) 57, (byte) 247, (byte) 247, (byte) 247);

        Font font = libro.createFont();
        font.setColor(HSSFColor.WHITE.index);

        Font font2 = libro.createFont();
        font2.setColor(palette3.getColor(59).getIndex());
        font2.setFontHeightInPoints((short) 8);

        Font font3 = libro.createFont();
        font3.setColor(palette2.getColor(59).getIndex());
        font3.setFontHeightInPoints((short) 8);
        font3.setBoldweight(Short.MAX_VALUE);

        Font font4 = libro.createFont();
        font4.setColor(palette3.getColor(59).getIndex());
        font4.setBoldweight(Short.MAX_VALUE);

        Font font5 = libro.createFont();
        font5.setColor(palette3.getColor(59).getIndex());

        my_style.setFont(font);
        my_style.setFillForegroundColor(palette2.getColor(58).getIndex());
        my_style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style.setAlignment(my_style.ALIGN_CENTER);
        my_style.setFont(font);

        my_style2.setFont(font2);
        my_style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style2.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style2.setFillForegroundColor(HSSFColor.WHITE.index);

        my_style3.setFont(font3);
        my_style3.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3.setFillForegroundColor(HSSFColor.LIGHT_ORANGE.index);

        my_style4.setFont(font4);
        my_style4.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style4.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style4.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style4.setAlignment(my_style.ALIGN_LEFT);

        my_style5.setFont(font5);
        my_style5.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style5.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style5.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style5.setAlignment(my_style.ALIGN_LEFT);

        my_style6.setFont(font5);
        my_style6.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style6.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style6.setFillForegroundColor(palette4.getColor(57).getIndex());
        my_style6.setAlignment(my_style.ALIGN_LEFT);

        FileInputStream inputStream = new FileInputStream(request.getServletContext().getRealPath("") + "/images/logo.jpg");
        byte[] bytes = IOUtils.toByteArray(inputStream);
        int pictureIdx = libro.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
        inputStream.close();
        CreationHelper helper = libro.getCreationHelper();
        Drawing drawing = hoja.createDrawingPatriarch();
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(0); //Column B
        anchor.setRow1(0); //Row 3
        anchor.setCol2(3); //Column C
        anchor.setRow2(5); //Row 4
        Picture pict = drawing.createPicture(anchor, pictureIdx);
        Row cabecera = hoja.createRow(0);

        hoja.setColumnWidth(0, 3000);
        hoja.setColumnWidth(1, 4000);
        hoja.setColumnWidth(2, 7000);
        hoja.setColumnWidth(3, 6000);
        hoja.setColumnWidth(4, 1500);

        Cell cab = cabecera.createCell(0);
        cab.setCellValue("");
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);

        ////////////////////////////////////////////
        cabecera = hoja.createRow(1);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(2);

        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(3);

        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(4);

        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(5);

        cab = cabecera.createCell(0);
        cab.setCellValue("DIRECCION DE ENVIO: PERICON No. 116 PLAZA ANDROMEDA LOCAL 35 COL. MIRAVAL, CUERNVACA, MORELOS C.P 62270");
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(6);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(7);
        cab = cabecera.createCell(0);
        cab.setCellValue("FAVOR DE ENVIAR CON LOGISTICOS JT");
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(8);
        cab = cabecera.createCell(0);
        cab.setCellValue("VIDAL CHAIRES No. 36 COL. MOCTEZUMA 1a SECCION. C.P. 15500 DEL. VENUSTIANO CARRANZA");
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(9);
        cab = cabecera.createCell(0);
        cab.setCellValue("MEXICO D.F. TEL: 5784-7075");
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(10);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        //////////////////////////////////////////////
        cabecera = hoja.createRow(11);
        cab = cabecera.createCell(0);
        cab.setCellValue("Tipo");
        cab.setCellStyle(my_style);

        cab = cabecera.createCell(1);
        cab.setCellValue("ISBN");
        cab.setCellStyle(my_style);

        cab = cabecera.createCell(2);
        cab.setCellValue("Titulo");
        cab.setCellStyle(my_style);

        cab = cabecera.createCell(3);
        cab.setCellValue("Editorial");
        cab.setCellStyle(my_style);

        cab = cabecera.createCell(4);
        cab.setCellValue("Cant.");
        cab.setCellStyle(my_style);

        int valor = 12;
        for (Pedidoparte php : partes) {
            if (php.getFaltante() > 0) {
                Row fila = hoja.createRow(valor);
                Cell cab2 = fila.createCell(0);
                cab2.setCellValue(php.getProductoId().getTextlit());
                if (valor % 2 == 0) {
                    cab2.setCellStyle(my_style5);
                } else {
                    cab2.setCellStyle(my_style6);
                }

                cab2 = fila.createCell(1);
                cab2.setCellValue(php.getProductoId().getIsbn());
                if (valor % 2 == 0) {
                    cab2.setCellStyle(my_style5);
                } else {
                    cab2.setCellStyle(my_style6);
                }

                cab2 = fila.createCell(2);
                cab2.setCellValue(php.getProductoId().getTitulo());
                if (valor % 2 == 0) {
                    cab2.setCellStyle(my_style5);
                } else {
                    cab2.setCellStyle(my_style6);
                }

                cab2 = fila.createCell(3);
                cab2.setCellValue(php.getProductoId().getEditorialId1().getNombre());

                if (valor % 2 == 0) {
                    cab2.setCellStyle(my_style5);
                } else {
                    cab2.setCellStyle(my_style6);
                }
                cab2 = fila.createCell(4);
                cab2.setCellValue(php.getFaltante());

                if (valor % 2 == 0) {
                    cab2.setCellStyle(my_style5);
                } else {
                    cab2.setCellStyle(my_style6);
                }

                valor++;
            }
        }

        libro.write(archivo);
        archivo.close();
        pedido.setEstado(2);
        try {
            p.edit(pedido);
        } catch (Exception ex) {
            Logger.getLogger(reporteCompra.class.getName()).log(Level.SEVERE, null, ex);
        }

        PedidoJpaController ped = new PedidoJpaController(emf);
        Pedido superPedido = ped.findPedido(pedido.getPedidoId().getId());
        int valorV = 0;
        for (Pedidoproveedor pvv : superPedido.getPedidoproveedorList()) {
            if (pvv.getEstado() == 1||pvv.getEstado()==3) {
                valorV = 1;
            }
        }
        if (valorV == 0) {
            superPedido.setEstado("6");
        } else {
            superPedido.setEstado("5");

        }
        try {
            ped.edit(superPedido);
        } catch (Exception ex) {
            Logger.getLogger(reporteCompra.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(reporteCompra.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("reportes/reporteCompra.xls");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
