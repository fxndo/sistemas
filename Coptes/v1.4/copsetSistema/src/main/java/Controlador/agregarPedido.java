/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Cliente;
import Modelo.Direccion;
import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import Modelo.Producto;
import Modelo.Sucursal;
import Modelo.Usuario;
import ModeloJSP.ClienteJpaController;
import ModeloJSP.DireccionJpaController;
import ModeloJSP.EditorialJpaController;
import ModeloJSP.NivelJpaController;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoparteJpaController;
import ModeloJSP.PedidoproveedorJpaController;
import ModeloJSP.ProductoJpaController;
import ModeloJSP.SucursalJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarPedido", urlPatterns = {"/agregarPedido"})
public class agregarPedido extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        Calendar fecha = new GregorianCalendar();
        SucursalJpaController s = new SucursalJpaController(emf);
        ClienteJpaController c = new ClienteJpaController(emf);
        PedidoJpaController p = new PedidoJpaController(emf);
        Pedido pedido = new Pedido();
        ProductoJpaController pr = new ProductoJpaController(emf);
        PedidoproveedorJpaController ppe = new PedidoproveedorJpaController(emf);
        PedidoparteJpaController ppa = new PedidoparteJpaController(emf);
        NivelJpaController n = new NivelJpaController(emf);
        DireccionJpaController d = new DireccionJpaController(emf);
        EditorialJpaController ed = new EditorialJpaController(emf);
        
        int idSucursal = Integer.parseInt(request.getParameter("sucursal"));
        Sucursal sucursal = s.findSucursal(idSucursal);
        Usuario usuario = (Usuario) sesion.getAttribute("informacion");
        Date entrega = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd", new Locale("es"));
        try {
            entrega = formato.parse(request.getParameter("entrega"));
        } catch (ParseException ex) {
            Logger.getLogger(agregarPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

///////////////////////////////////////////////////////  Agregar Cliente Nuevo   ///////////////////////////////////////////////////////////
        Cliente cliente = new Cliente();
        if (request.getParameter("nombreC") != null) {
            Direccion direccion = new Direccion();
            direccion.setCalle(request.getParameter("calleC"));
            direccion.setCiudad(request.getParameter("ciudadC"));
            direccion.setColonia(request.getParameter("coloniaC"));
            direccion.setCp(request.getParameter("cpC"));
            direccion.setDelegacion(request.getParameter("delegacionC"));
            direccion.setEstado(request.getParameter("estadoC"));
           
            
            d.create(direccion);
            
            cliente.setCelular(request.getParameter("celularC"));
            cliente.setDireccionId(direccion);
            cliente.setEmail(request.getParameter("emailC"));
            cliente.setEscuela(request.getParameter("escuelaC"));
            cliente.setNombre(request.getParameter("nombreC"));
            cliente.setNumerotarjeta("1");
            cliente.setTipo(1);
            cliente.setTelefono(request.getParameter("telefonoC"));
            c.create(cliente);
        } else {
            int idCliente = Integer.parseInt(request.getParameter("cliente"));
            cliente = c.findCliente(idCliente);
        }

////////////////////////////////////////////////////////    Crear Pedido Nuevo    /////////////////////////////////////////////////////////////
        pedido.setClienteId(cliente);
        pedido.setSucursalId(sucursal);
        pedido.setUsuarioId(usuario);
        pedido.setFecha(fecha.getTime());
        pedido.setEstado("1");
        pedido.setEntrega(entrega);
        pedido.setDescuento(0);
        pedido.setTotal(0.0);
        p.create(pedido);

////////////////////////////////////////////////////////    Crear PedidoProveedor NUll    /////////////////////////////////////////////////////////////
        Pedidoproveedor pedidoProveedor = new Pedidoproveedor();
        pedidoProveedor.setEstado(0);
        pedidoProveedor.setPedidoId(pedido);
        ppe.create(pedidoProveedor);

////////////////////////////////////////////////////////    Agregar Productos (150 MAX)   /////////////////////////////////////////////////////////////
        for (int x = 1; x < 151; x++) {
            if (request.getParameter("isbn" + x) != null && request.getParameter("isbn" + x).length() > 0) {
                Producto producto = new Producto();
                if (pr.existeProducto(request.getParameter("isbn" + x))) {
                    producto = pr.findProducto(pr.productoTPV(request.getParameter("isbn" + x)));
                } else {
                    /////////////////////////////////// Crear Producto  /////////////////////////////
                    producto.setIsbn(request.getParameter("isbn" + x));
                    producto.setTitulo(request.getParameter("titulo" + x));
                    producto.setEditorialId1(ed.findEditorial(Integer.parseInt(request.getParameter("editorial" + x))));
                    producto.setNivelId(n.findNivel(1));
                    producto.setAutor("Sin Autor");
                    producto.setTextlit("Sin Tipo");
                    producto.setSerie("Sin información");
                    producto.setPreciocompra("0");
                    producto.setPrecioventa(request.getParameter("precio"+x));
                    producto.setEstado("2");
                    pr.create(producto);
                }

////////////////////////////////////////////////////////    Crear PedidoParte    /////////////////////////////////////////////////////////////
                Pedidoparte pedidoparte = new Pedidoparte();
                pedidoparte.setPedidoProveedorid(pedidoProveedor);
                pedidoparte.setProductoId(producto);
                pedidoparte.setPrecio(producto.getPrecioventa());
                pedidoparte.setCantidad(Integer.parseInt(request.getParameter("cantidad" + x)));
                pedidoparte.setDevolucion(0);
                pedidoparte.setTipo(1);
                pedidoparte.setEstado(1);
                pedidoparte.setDescuento(Integer.parseInt(request.getParameter("descuento"+x)));
                pedidoparte.setFaltante(Integer.parseInt(request.getParameter("cantidad" + x)));
                ppa.create(pedidoparte);
            }
        }
        
        pedido = p.findPedido(pedido.getId());
        double total = 0;
        for (Pedidoproveedor pprf : pedido.getPedidoproveedorList()) {
            for (Pedidoparte ppaf : pprf.getPedidoparteList()) {
                double descuento = Double.parseDouble(ppaf.getPrecio()) * ppaf.getDescuento() / 100;
                double semi = Double.parseDouble(ppaf.getProductoId().getPrecioventa()) - descuento;
                total += ppaf.getCantidad() * semi;
            }
        }
        total= new BigDecimal(total).setScale(0, RoundingMode.HALF_EVEN).doubleValue();
        pedido.setTotal(total);
        
        try {
            p.edit(pedido);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(generarProveedores.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(generarProveedores.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.sendRedirect("pedido.jsp?id="+pedido.getId());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
