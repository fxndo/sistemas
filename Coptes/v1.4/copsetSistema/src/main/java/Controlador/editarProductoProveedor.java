/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Almacen;
import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoparteJpaController;
import ModeloJSP.SucursalJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "editarProductoProveedor", urlPatterns = {"/editarProductoProveedor"})
public class editarProductoProveedor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        int cantidad = Integer.parseInt(request.getParameter("nuevaCantidad"));
        int idPedidoParte = Integer.parseInt(request.getParameter("ppa"));
        int idSucursal = Integer.parseInt(request.getParameter("idSucursal"));

        PedidoJpaController pj = new PedidoJpaController(emf);
        PedidoparteJpaController ppaj = new PedidoparteJpaController(emf);
        AlmacenJpaController alj=new AlmacenJpaController(emf);
        SucursalJpaController scj=new SucursalJpaController(emf);

        Pedidoparte pedidoparte = ppaj.findPedidoparte(idPedidoParte);
        Pedido pedido = pj.findPedido(pedidoparte.getPedidoProveedorid().getPedidoId().getId());
        
        int idAlmacen=alj.obtenerAlmacen(pedidoparte.getProductoId().getId(), idSucursal);
        Almacen almacen=new Almacen();
        
        if(idAlmacen!=0){
            almacen=alj.findAlmacen(idAlmacen);
        }else{
            almacen.setSucursalId(scj.findSucursal(idSucursal));
            almacen.setProductoId(pedidoparte.getProductoId());
            almacen.setCantidad(0);
            alj.create(almacen);
        }
//        ------------------------------------------------------------------------------------------------
        if (pedidoparte.getCantidad() == cantidad) {
            try {
                ppaj.destroy(idPedidoParte);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(editarProductoProveedor.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            pedidoparte.setCantidad(pedidoparte.getCantidad() - cantidad);
            try {
                ppaj.edit(pedidoparte);
            } catch (Exception ex) {
                Logger.getLogger(editarProductoProveedor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        almacen.setCantidad(almacen.getCantidad()+cantidad);
        
        try {
            alj.edit(almacen);
        } catch (Exception ex) {
            Logger.getLogger(editarProductoProveedor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        pedido = pj.findPedido(pedido.getId());
        double total = 0;
        for (Pedidoproveedor pprf : pedido.getPedidoproveedorList()) {
            for (Pedidoparte ppaf : pprf.getPedidoparteList()) {
                double descuento = Double.parseDouble(ppaf.getPrecio()) * ppaf.getDescuento() / 100;
                double semi = Double.parseDouble(ppaf.getProductoId().getPrecioventa()) - descuento;
                total += ppaf.getCantidad() * semi;
            }
        }
        total= new BigDecimal(total).setScale(0, RoundingMode.HALF_EVEN).doubleValue();
        pedido.setTotal(total);
        
        try {
            pj.edit(pedido);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(generarProveedores.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(generarProveedores.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.sendRedirect("pedido.jsp?id="+pedido.getId()+"&list=productos");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
