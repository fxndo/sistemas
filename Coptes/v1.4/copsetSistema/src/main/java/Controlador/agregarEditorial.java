/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Editorial;
import Modelo.Proveedor;
import Modelo.Proveedoreditorial;
import ModeloJSP.EditorialJpaController;
import ModeloJSP.ProveedorJpaController;
import ModeloJSP.ProveedoreditorialJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarEditorial", urlPatterns = {"/agregarEditorial"})
public class agregarEditorial extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        
        String editorial = request.getParameter("editorial");
        int idEditorial=0;
        if(request.getParameter("idEditorial")!=null){
            idEditorial= Integer.parseInt(request.getParameter("idEditorial"));
        }
        int idProveedor = Integer.parseInt(request.getParameter("idProveedor"));
        
        ProveedorJpaController p = new ProveedorJpaController(emf);
        Proveedor prov = p.findProveedor(idProveedor);
        
        EditorialJpaController ed = new EditorialJpaController(emf);
        Editorial edit = new Editorial();
        
        if (idEditorial == 0) {
            edit.setNombre(editorial);
            ed.create(edit);
        }else{
            edit=ed.findEditorial(idEditorial);
        }
        
        ProveedoreditorialJpaController pee=new ProveedoreditorialJpaController(emf);
        Proveedoreditorial proEd=new Proveedoreditorial();
        
        proEd.setEditorialId(edit);
        proEd.setProveedorId(prov);
        pee.create(proEd);

        response.sendRedirect("proveedor.jsp?id="+prov.getId()+"&list=editoriales&noti=5");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
