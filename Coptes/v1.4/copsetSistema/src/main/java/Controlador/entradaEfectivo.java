/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Caja;
import Modelo.Sucursal;
import Modelo.Usuario;
import ModeloJSP.CajaJpaController;
import ModeloJSP.UsuarioJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "entradaEfectivo", urlPatterns = {"/entradaEfectivo"})
public class entradaEfectivo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        Usuario informacion = (Usuario) sesion.getAttribute("informacion");
        Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursalTPV");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        UsuarioJpaController u = new UsuarioJpaController(emf);
        CajaJpaController c = new CajaJpaController(emf);
        Caja caja = c.findCaja(c.cerrarCaja(sucursal.getId()));
        double valor = 0;

        if (u.verificarPass(request.getParameter("pass"),sucursal.getId()) == 1) {
            double monto = Double.parseDouble(request.getParameter("monto"));
            valor = caja.getInicio();
            if (request.getParameter("act").equals("1")) {
                valor += monto;
            } else {
                valor -= monto;
            }
            try {
                caja.setInicio(valor);
                c.edit(caja);
            } catch (Exception ex) {
                Logger.getLogger(entradaEfectivo.class.getName()).log(Level.SEVERE, null, ex);
            }
            response.sendRedirect("tpv.jsp?m=666");
        } else {
            response.sendRedirect("tpv.jsp?m=667");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
