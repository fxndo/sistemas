/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Caja;
//import Modelo.PedidoHasProducto;
import Modelo.Producto;
import Modelo.Sucursal;
import Modelo.Usuario;
import Modelo.Ventas;
import Modelo.VentasHasProducto;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.CajaJpaController;
import ModeloJSP.ProductoJpaController;
import ModeloJSP.SucursalJpaController;
import ModeloJSP.UsuarioJpaController;
import ModeloJSP.VentasHasProductoJpaController;
import ModeloJSP.VentasJpaController;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarTPV", urlPatterns = {"/agregarTPV"})
public class agregarTPV extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        HttpSession venta = request.getSession(true);
        Calendar fecha = new GregorianCalendar();
        AlmacenJpaController a = new AlmacenJpaController(emf);
        String serial = request.getParameter("serial");
        Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursalTPV");
        ProductoJpaController p = new ProductoJpaController(emf);
        VentasJpaController v = new VentasJpaController(emf);
        Usuario informacion = (Usuario) sesion.getAttribute("informacion");
        Locale locale = new Locale("es","MX");
        NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
        
        
        int nuevo=0;
        if(request.getParameter("nuevo")!=null){
            nuevo=1;
        }
        
        double pTotal=0;
        int idProducto = p.productoTPV(serial);
        String cadena = "";
        if (idProducto != 0) {
            Producto producto = p.findProducto(idProducto);
            VentasHasProducto pedido=new VentasHasProducto();
            pedido.setCantidad(1);
            pedido.setProducto(producto);
            pedido.setDescuento(0);
            pedido.setDevolucion(0);
            pTotal=Float.parseFloat(pedido.getProducto().getPrecioventa());
            if (a.productosAlmacen(producto.getId(), sucursal.getId()) > 0) {

                cadena += "<tr class='noEmpty2' id='producto" + pedido.getProducto().getId() + "'>";
            } else {
                cadena += "<tr class='empty' id='producto" + pedido.getProducto().getId() + "'>";
            }
            cadena += "<td>" + pedido.getProducto().getIsbn() + "</td> <td>"
            + pedido.getProducto().getTitulo() + "</td> <td class=\"saldoTotal\"><div>" + pedido.getProducto().getPrecioventa() 
            + "</div></td> <td id='cantidadProducto" + pedido.getProducto().getId() 
            + "'><div class=\"btnCantidad\" onclick=\"agregarCantidad('"+pedido.getProducto().getId()
            +"','"+a.productosAlmacen(producto.getId(), sucursal.getId())+"');\">1</div></td> <td><div class=\"btnDescuento\" onclick=\"agregarDescuento('"
            +pedido.getProducto().getId()+"')\">"+pedido.getDescuento()+"%</div></td> <td class=\"saldoVerde\"><div>$" + new BigDecimal(pTotal).setScale(0, RoundingMode.HALF_EVEN).doubleValue() + "</div></td> <td class='gris'><strong>" 
            + a.productosAlmacen(producto.getId(), sucursal.getId()) + "</strong></td> <td><a href=\"restarTPV?id=" 
            + producto.getId() + "\"><div class=\"btnEliminar\"><img src='images/less.png' class='iconoNormalGrande'></div></a></td></tr>-";

            List<VentasHasProducto> ventah=new ArrayList<VentasHasProducto>();
            if (venta.getAttribute("venta") != null) {
                ventah = (List<VentasHasProducto>) venta.getAttribute("venta");
                ventah.add(pedido);
                venta.setAttribute("venta", ventah);
            } else {
                ventah.add(pedido);
                venta.setAttribute("venta", ventah);
            }

        } else {
            cadena = "error";
        }
        
        double total=0;
        List<VentasHasProducto>ventas=(List<VentasHasProducto>)venta.getAttribute("venta");
        for(VentasHasProducto vh: ventas){
            double precio=Double.parseDouble(vh.getProducto().getPrecioventa());
            precio-=precio*(vh.getDescuento()/100);
            total+=vh.getCantidad()*precio;
            total=new BigDecimal(total).setScale(0, RoundingMode.UP).doubleValue();
        }
        
        if(nuevo==0){
        cadena+=nf.format(total)+"-"+total+"-"+nf.format(total+(total)*0.03);   
        }
        cadena=cadena.replace("ñ", "n");
        cadena=cadena.replace("Ñ", "N");
        
        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType("application/x-json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        outputStream.print(new Gson().toJson(cadena));

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
