/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Sucursal;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.ProductoJpaController;
import ModeloJSP.SucursalJpaController;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "verificarAlmacen", urlPatterns = {"/verificarAlmacen"})
public class verificarAlmacen extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        AlmacenJpaController a=new AlmacenJpaController(emf);
        ProductoJpaController p=new ProductoJpaController(emf);
        SucursalJpaController s=new SucursalJpaController(emf);
        List<Sucursal> sucursales = s.listaSucursales();
        
        int cantidadSolicitada=Integer.parseInt(request.getParameter("cantidad"));
        String isbn=request.getParameter("isbn");
        int idSucursal=Integer.parseInt(request.getParameter("idSucursal"));
        
        int idProducto=p.serialProducto(isbn);
        int cantidadAlmacen=a.productosAlmacen(idProducto, idSucursal);
        String cadena="";
        
        System.out.println("Solicitada: "+cantidadSolicitada);
        System.out.println("Almacen: "+cantidadAlmacen);
        
        cadena+="<option value='0'>Proveedor</option>";
//        if(cantidadSolicitada<=cantidadAlmacen){
//            cadena+="<option value='"+1+"'>Proveedor"
//                    + "</option><option value='"+2+"'>Almacen</option>";
//        }else{
//            cadena+="<option value='"+1+"'>Proveedor</option>";
//            
//        }

        for(Sucursal ss: sucursales){
            if(cantidadSolicitada<=a.productosAlmacen(idProducto, ss.getId())){
                cadena+="<option value='"+ss.getId()+"'>"+ss.getNombre()+"</option>";
            }
        }

        
        ServletOutputStream outputStream = response.getOutputStream();
            response.setContentType("application/x-json;charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            outputStream.print(new Gson().toJson(cadena));
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
