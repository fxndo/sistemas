/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Descuento;
import Modelo.Editorial;
import Modelo.Proveedor;
import Modelo.Proveedoreditorial;
import ModeloJSP.DescuentoJpaController;
import ModeloJSP.EditorialJpaController;
import ModeloJSP.ProveedorJpaController;
import ModeloJSP.ProveedoreditorialJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarDescuento", urlPatterns = {"/agregarDescuento"})
public class agregarDescuento extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd", new Locale("es"));
        int idProEd = Integer.parseInt(request.getParameter("idProEd"));
        int cantidad = Integer.parseInt(request.getParameter("cantidad"));

        Date inicio = new Date();
        Date termino = new Date();

        try {
            inicio = formato.parse(request.getParameter("inicio"));
            termino = formato.parse(request.getParameter("termino"));
        } catch (ParseException ex) {
            Logger.getLogger(agregarDescuento.class.getName()).log(Level.SEVERE, null, ex);
        }

        DescuentoJpaController de = new DescuentoJpaController(emf);
        Descuento descuento = new Descuento();

        ProveedoreditorialJpaController pe = new ProveedoreditorialJpaController(emf);
        Proveedoreditorial proEd = pe.findProveedoreditorial(idProEd);

        if (proEd.getDescuentoId() != null) {
            descuento = de.findDescuento(proEd.getDescuentoId().getId());
            descuento.setCantidad(cantidad);
            descuento.setInicio(inicio);
            descuento.setFinal1(termino);
            try {
                de.edit(descuento);
            } catch (Exception ex) {
                Logger.getLogger(agregarDescuento.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            descuento.setCantidad(cantidad);
            descuento.setInicio(inicio);
            descuento.setFinal1(termino);
            de.create(descuento);
            
            Proveedoreditorial proEd2 = pe.findProveedoreditorial(proEd.getId());
            proEd2.setDescuentoId(descuento);
            try {
                pe.edit(proEd2);
            } catch (Exception ex) {
                Logger.getLogger(agregarDescuento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        response.sendRedirect("editoriales.jsp?noti=22");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
