/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Devolucion;
import Modelo.Usuario;
import Modelo.Ventas;
import Modelo.VentasHasProducto;
import ModeloJSP.DevolucionJpaController;
import ModeloJSP.VentasHasProductoJpaController;
import ModeloJSP.VentasJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "devolucionVenta", urlPatterns = {"/devolucionVenta"})
public class devolucionVenta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        Calendar fecha = new GregorianCalendar();
        Usuario informacion = (Usuario) sesion.getAttribute("informacion");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        int idVenta=Integer.parseInt(request.getParameter("idVenta"));
        
        VentasJpaController vj=new VentasJpaController(emf);
        Ventas venta=vj.findVentas(idVenta);
        VentasHasProductoJpaController vhj=new VentasHasProductoJpaController(emf);
        
        int valor=1;
        Double total=0.0;
        for(VentasHasProducto vh: venta.getVentasHasProductoList()){
            vh.setDevolucion(Integer.parseInt(request.getParameter("cantidad"+valor)));
            total+=Double.parseDouble(vh.getProducto().getPrecioventa())-(Double.parseDouble(vh.getProducto().getPrecioventa())*vh.getDescuento()/100)*vh.getDevolucion();
            try {
                vhj.edit(vh);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(devolucionVenta.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(devolucionVenta.class.getName()).log(Level.SEVERE, null, ex);
            }
            valor++;
        }
        DevolucionJpaController dj=new DevolucionJpaController(emf);
        Devolucion devolucion=new Devolucion();
        devolucion.setUsuarioId(informacion);
        devolucion.setFecha(fecha.getTime());
        dj.create(devolucion);
        
        venta.setDevolucionId(devolucion);
        venta.setTipo(total.toString());
        venta.setTotal(venta.getTotal()-total);
        
        try {
            vj.edit(venta);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(devolucionVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(devolucionVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        
        response.sendRedirect("venta.jsp?id="+venta.getId()+"&noti=27");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
