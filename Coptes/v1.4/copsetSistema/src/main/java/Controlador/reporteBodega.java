/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Almacen;
import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import Modelo.Sucursal;
import Modelo.Usuario;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.SucursalJpaController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.lang.Thread.sleep;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;

/**
 *
 * @author fernando
 */
@WebServlet(name = "reporteBodega", urlPatterns = {"/reporteBodega"})
public class reporteBodega extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        SimpleDateFormat formato = new SimpleDateFormat("dd/MMMM/yyyy", new Locale("es"));
        SimpleDateFormat formato2 = new SimpleDateFormat("hh:mm a", new Locale("es"));
        Calendar fecha = new GregorianCalendar();
        Usuario informacion = (Usuario) sesion.getAttribute("informacion");
        int idSucursal = Integer.parseInt(request.getParameter("idSucursal"));

        AlmacenJpaController al = new AlmacenJpaController(emf);
        PedidoJpaController pd = new PedidoJpaController(emf);
        
        SucursalJpaController sc=new SucursalJpaController(emf);
        Sucursal sucursal=sc.findSucursal(idSucursal);
        
        List<Almacen> almacenes=new ArrayList<Almacen>();
        List<Pedido> pedidos=new ArrayList<Pedido>();
        
        if(idSucursal!=0){
            almacenes=al.listaAlmacen(idSucursal);
            pedidos=pd.pedidoBodega(idSucursal);
        }

        ///////////////////////CAMBIO DE DIRECTORIO//////////////////////////////
        String rutaArchivo = request.getServletContext().getRealPath("");
//        File archivoXLS = new File(rutaArchivo + "\\reportes\\reporteBodega.xls");
        File archivoXLS = new File(rutaArchivo + "/reportes/reporteBodega.xls");

        archivoXLS.createNewFile();

        Workbook libro = new HSSFWorkbook();
        FileOutputStream archivo = new FileOutputStream(archivoXLS);

        Sheet hoja = libro.createSheet("Compra");

        CellStyle my_style = libro.createCellStyle();
        CellStyle my_style2 = libro.createCellStyle();
        CellStyle my_style2i = libro.createCellStyle();
        CellStyle my_style2d = libro.createCellStyle();
        CellStyle my_style2id = libro.createCellStyle();
        CellStyle my_style3 = libro.createCellStyle();
        CellStyle my_style3i = libro.createCellStyle();
        CellStyle my_style3ic = libro.createCellStyle();
        CellStyle my_style3icd = libro.createCellStyle();
        CellStyle my_style3id = libro.createCellStyle();
        CellStyle my_style3idd = libro.createCellStyle();
        CellStyle my_style4 = libro.createCellStyle();
        CellStyle my_style4d = libro.createCellStyle();
        CellStyle my_style5 = libro.createCellStyle();
        CellStyle my_style6 = libro.createCellStyle();

        HSSFPalette palette2 = ((HSSFWorkbook) libro).getCustomPalette();
        palette2.setColorAtIndex((short) 58, (byte) 255, (byte) 73, (byte) 0);

        HSSFPalette palette3 = ((HSSFWorkbook) libro).getCustomPalette();
        palette3.setColorAtIndex((short) 59, (byte) 0, (byte) 0, (byte) 0);

        HSSFPalette palette4 = ((HSSFWorkbook) libro).getCustomPalette();
        palette4.setColorAtIndex((short) 57, (byte) 247, (byte) 247, (byte) 247);

        Font font = libro.createFont();
        font.setColor(HSSFColor.WHITE.index);
        font.setFontHeightInPoints((short) 7);

        Font font2 = libro.createFont();
        font2.setColor(palette3.getColor(59).getIndex());
        font2.setFontHeightInPoints((short) 7);
//        font2.setBoldweight(Short.MAX_VALUE);

        Font font3 = libro.createFont();
        font3.setColor(HSSFColor.BLACK.index);
        font3.setFontHeightInPoints((short) 7);
        font3.setBoldweight(Short.MAX_VALUE);

        Font font4 = libro.createFont();
        font4.setColor(palette3.getColor(59).getIndex());
        font4.setFontHeightInPoints((short) 7);
        font4.setBoldweight(Short.MAX_VALUE);

        Font font5 = libro.createFont();
        font5.setFontHeightInPoints((short) 7);
        font5.setColor(palette3.getColor(59).getIndex());

        my_style.setFont(font);
        my_style.setFillForegroundColor(palette2.getColor(58).getIndex());
        my_style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style.setAlignment(my_style.ALIGN_CENTER);
        my_style.setFont(font);

        my_style2.setFont(font2);
        my_style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style2.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style2.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style2.setBorderBottom(HSSFColor.BLACK.index);
        my_style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);

        my_style2i.setFont(font2);
        my_style2i.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style2i.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style2i.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style2i.setBorderBottom(HSSFColor.BLACK.index);
        my_style2i.setBorderLeft(HSSFColor.BLACK.index);
        my_style2i.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style2i.setBorderLeft(HSSFCellStyle.BORDER_THIN);

        my_style2d.setFont(font2);
        my_style2d.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style2d.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style2d.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style2d.setBorderBottom(HSSFColor.BLACK.index);
        my_style2d.setBorderRight(HSSFColor.BLACK.index);
        my_style2d.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style2d.setBorderRight(HSSFCellStyle.BORDER_THIN);

        my_style2id.setFont(font2);
        my_style2id.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style2id.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style2id.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style2id.setBorderBottom(HSSFColor.BLACK.index);
        my_style2id.setBorderLeft(HSSFColor.BLACK.index);
        my_style2id.setBorderRight(HSSFColor.BLACK.index);
        my_style2id.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style2id.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style2id.setBorderRight(HSSFCellStyle.BORDER_THIN);

        my_style3.setFont(font3);
        my_style3.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3.setFillForegroundColor(HSSFColor.WHITE.index);

        my_style3i.setFont(font3);
        my_style3i.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3i.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3i.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3i.setBorderLeft(HSSFColor.BLACK.index);
        my_style3i.setBorderLeft(HSSFCellStyle.BORDER_THIN);

        my_style3ic.setFont(font3);
        my_style3ic.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3ic.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3ic.setAlignment(my_style.ALIGN_CENTER);
        my_style3ic.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3ic.setBorderLeft(HSSFColor.BLACK.index);
        my_style3ic.setBorderLeft(HSSFCellStyle.BORDER_THIN);

        my_style3icd.setFont(font3);
        my_style3icd.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3icd.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3icd.setAlignment(my_style.ALIGN_RIGHT);
        my_style3icd.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3icd.setBorderLeft(HSSFColor.BLACK.index);
        my_style3icd.setBorderLeft(HSSFCellStyle.BORDER_THIN);

        my_style3id.setFont(font3);
        my_style3id.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3id.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3id.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3id.setBorderLeft(HSSFColor.BLACK.index);
        my_style3id.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style3id.setBorderLeft(HSSFColor.BLACK.index);
        my_style3id.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style3id.setBorderRight(HSSFColor.BLACK.index);
        my_style3id.setBorderRight(HSSFCellStyle.BORDER_THIN);

        my_style3idd.setFont(font3);
        my_style3idd.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style3idd.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style3idd.setAlignment(my_style.ALIGN_RIGHT);
        my_style3idd.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style3idd.setBorderLeft(HSSFColor.BLACK.index);
        my_style3idd.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style3idd.setBorderLeft(HSSFColor.BLACK.index);
        my_style3idd.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style3idd.setBorderRight(HSSFColor.BLACK.index);
        my_style3idd.setBorderRight(HSSFCellStyle.BORDER_THIN);

        my_style4.setFont(font4);
        my_style4.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style4.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style4.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style4.setBorderLeft(HSSFColor.BLACK.index);
        my_style4.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style4.setBorderBottom(HSSFColor.BLACK.index);
        my_style4.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style4.setBorderRight(HSSFColor.BLACK.index);
        my_style4.setBorderRight(HSSFCellStyle.BORDER_THIN);
        my_style4.setBorderTop(HSSFColor.BLACK.index);
        my_style4.setBorderTop(HSSFCellStyle.BORDER_THIN);

        my_style4d.setFont(font4);
        my_style4d.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style4d.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style4d.setAlignment(my_style.ALIGN_RIGHT);
        my_style4d.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style4d.setBorderLeft(HSSFColor.BLACK.index);
        my_style4d.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        my_style4d.setBorderBottom(HSSFColor.BLACK.index);
        my_style4d.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        my_style4d.setBorderRight(HSSFColor.BLACK.index);
        my_style4d.setBorderRight(HSSFCellStyle.BORDER_THIN);
        my_style4d.setBorderTop(HSSFColor.BLACK.index);
        my_style4d.setBorderTop(HSSFCellStyle.BORDER_THIN);

        my_style5.setFont(font5);
        my_style5.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style5.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style5.setFillForegroundColor(HSSFColor.WHITE.index);
        my_style5.setAlignment(my_style.ALIGN_LEFT);

        my_style6.setFont(font5);
        my_style6.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        my_style6.setVerticalAlignment(my_style.VERTICAL_CENTER);
        my_style6.setFillForegroundColor(palette4.getColor(57).getIndex());
        my_style6.setAlignment(my_style.ALIGN_LEFT);

        /////////////////////////////////////////////
        FileInputStream inputStream = new FileInputStream(request.getServletContext().getRealPath("")+"/images/logo.jpg");
//        FileInputStream inputStream = new FileInputStream(request.getServletContext().getRealPath("") + "\\images\\logo.jpg");
        byte[] bytes = IOUtils.toByteArray(inputStream);
        int pictureIdx = libro.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
        inputStream.close();
        CreationHelper helper = libro.getCreationHelper();
        Drawing drawing = hoja.createDrawingPatriarch();
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(0); //Column B
        anchor.setRow1(0); //Row 3
        anchor.setCol2(3); //Column C
        anchor.setRow2(4); //Row 4
        Picture pict = drawing.createPicture(anchor, pictureIdx);
        /////////////////////////////////////////////
        Row cabecera = hoja.createRow(0);
//        cabecera.setHeightInPoints(30);
//        hoja.setColumnWidth(0, 2000);
        hoja.setColumnWidth(0, 3000);
        hoja.setColumnWidth(1, 2500);
        hoja.setColumnWidth(2, 2500);
        hoja.setColumnWidth(3, 2500);
        hoja.setColumnWidth(4, 2500);
        hoja.setColumnWidth(5, 3000);
        hoja.setColumnWidth(6, 3000);
        hoja.setColumnWidth(7, 3000);

//        Cell cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        Cell cab = cabecera.createCell(0);
        cab.setCellValue("");
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);

        ////////////////////////////////////////////
        cabecera = hoja.createRow(1);

//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellValue("Bodega");
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(2);

//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(3);

//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(4);

//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(5);
//        cabecera.setHeightInPoints(20);

//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(0);
        cab.setCellValue("COMERCIALIZADORA DE PROYECTOS, TECNOLOGIAS, EDUCATIVOS Y SUMINISTROS.");
        cab.setCellStyle(my_style3);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(6);

        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(7);
//        cabecera.setHeightInPoints(20);

//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(0);
        cab.setCellValue("FECHA");
        cab.setCellStyle(my_style2i);

        cab = cabecera.createCell(1);
        cab.setCellValue(formato.format(fecha.getTime()));
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(6);
        cab.setCellValue("Sucursal");
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(7);
        cab.setCellValue(sucursal.getNombre());
        cab.setCellStyle(my_style2id);
        ////////////////////////////////////////////
        cabecera = hoja.createRow(8);
//        cabecera.setHeightInPoints(20);

//        cab = cabecera.createCell(0);
//        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(0);
        cab.setCellValue("Usuario ");
        cab.setCellStyle(my_style2i);

        cab = cabecera.createCell(1);
        cab.setCellValue(informacion.getNombre());
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2d);
        ////////////////////////////////////////////
        
        cabecera = hoja.createRow(9);

        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2);
        //////////////////////////////////////////////
        cabecera = hoja.createRow(10);

        cab = cabecera.createCell(0);
        cab.setCellValue(" Titulo,serie");
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);

        cab = cabecera.createCell(5);
        cab.setCellValue("Editorial");
        cab.setCellStyle(my_style2i);

        cab = cabecera.createCell(6);
        cab.setCellValue("Cantidad");
        cab.setCellStyle(my_style2i);

        cab = cabecera.createCell(7);
        cab.setCellValue("Tipo");
        cab.setCellStyle(my_style2id);

        int valor = 11;
        int valor2 = 1;
        double total = 0;
//        for (Pedidoproveedor pv : pedido.getPedidoproveedorList()) {
            for (Almacen aa : almacenes) {
                Row fila = hoja.createRow(valor);

                Cell cab2 = fila.createCell(0);
                cab2.setCellValue(aa.getProductoId().getIsbn());
                cab2.setCellStyle(my_style3i);

                cab2 = fila.createCell(1);
                cab2.setCellValue(aa.getProductoId().getTitulo());
                cab2.setCellStyle(my_style3);

                cab2 = fila.createCell(2);
                cab2.setCellStyle(my_style3);

                cab2 = fila.createCell(3);
                cab2.setCellStyle(my_style3);

                cab2 = fila.createCell(4);
                cab2.setCellStyle(my_style3);

                cab2 = fila.createCell(5);
                cab2.setCellValue(aa.getProductoId().getEditorialId1().getNombre());
                cab2.setCellStyle(my_style3icd);

                cab2 = fila.createCell(6);
                cab2.setCellValue(aa.getCantidad());
                total+=aa.getCantidad();
                cab2.setCellStyle(my_style3ic);

                cab2 = fila.createCell(7);
                cab2.setCellValue("Venta");
                cab2.setCellStyle(my_style3idd);
                valor++;
                valor2++;
            }
//        }


        for (Pedido ppd : pedidos) {
            for (Pedidoproveedor ppv : ppd.getPedidoproveedorList()) {
                for(Pedidoparte ppp: ppv.getPedidoparteList()){
                Row fila = hoja.createRow(valor);

                Cell cab2 = fila.createCell(0);
                cab2.setCellValue(ppp.getProductoId().getIsbn());
                cab2.setCellStyle(my_style3i);

                cab2 = fila.createCell(1);
                cab2.setCellValue(ppp.getProductoId().getTitulo());
                cab2.setCellStyle(my_style3);

                cab2 = fila.createCell(2);
                cab2.setCellStyle(my_style3);

                cab2 = fila.createCell(3);
                cab2.setCellStyle(my_style3);

                cab2 = fila.createCell(4);
                cab2.setCellStyle(my_style3);

                cab2 = fila.createCell(5);
                cab2.setCellValue(ppp.getProductoId().getEditorialId1().getNombre());
                cab2.setCellStyle(my_style3icd);

                cab2 = fila.createCell(6);
                cab2.setCellValue(ppp.getCantidad());
                total+=ppp.getCantidad();
                cab2.setCellStyle(my_style3ic);

                cab2 = fila.createCell(7);
                cab2.setCellValue("Pedido");
                cab2.setCellStyle(my_style3idd);
                valor++;
                valor2++;
            
                }
            }
        }

        cabecera = hoja.createRow(valor);

        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style2i);

        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style2);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style2i);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style2id);

        valor++;

        cabecera = hoja.createRow(valor);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellStyle(my_style3);
        valor++;

        cabecera = hoja.createRow(valor);
        cab = cabecera.createCell(0);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(1);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(2);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(3);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(4);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(5);
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(6);
        cab.setCellValue("Total");
        cab.setCellStyle(my_style3);
        cab = cabecera.createCell(7);
        cab.setCellValue(total);
        cab.setCellStyle(my_style4d);

        valor++;
        valor++;
        /////////////////////////////////////////////

        libro.write(archivo);
        archivo.close();
        try {
            sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(reporteCompra.class.getName()).log(Level.SEVERE, null, ex);
        }
//        Desktop.getDesktop().open(archivoXLS);
        response.sendRedirect("reportes/reporteBodega.xls");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
