/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoparteJpaController;
import ModeloJSP.PedidoproveedorJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "entregaPedidoProveedor", urlPatterns = {"/entregaPedidoProveedor"})
public class entregaPedidoProveedor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        int idPedidoProveedor = Integer.parseInt(request.getParameter("idPedidoProveedor"));
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        PedidoparteJpaController p = new PedidoparteJpaController(emf);
        PedidoproveedorJpaController pv = new PedidoproveedorJpaController(emf);
        int valor = 4;
        for (int z = 1; z < 100; z++) {
            if (request.getParameter("idPro" + z) != null) {
                int idPro = Integer.parseInt(request.getParameter("idPro" + z));
                Pedidoparte pedido = p.findPedidoparte(idPro);
                int cantidad = 0;
                if (request.getParameter("completo" + z).equals("0")) {
                    cantidad = Integer.parseInt(request.getParameter("cantidad" + z));
                    pedido.setFaltante(cantidad);
                    try {
                        p.edit(pedido);
                    } catch (Exception ex) {
                        Logger.getLogger(entregaPedidoProveedor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    valor = 3;
                    System.out.println("Incompleto:" + pedido.getId());
                } else {
                    System.out.println("Completo:" + pedido.getId());
                    pedido.setFaltante(0);
                    try {
                        p.edit(pedido);
                    } catch (Exception ex) {
                        Logger.getLogger(entregaPedidoProveedor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        Pedidoproveedor pedidoP = pv.findPedidoproveedor(idPedidoProveedor);
        pedidoP.setEstado(valor);
        try {
            pv.edit(pedidoP);
        } catch (Exception ex) {
            Logger.getLogger(entregaPedidoProveedor.class.getName()).log(Level.SEVERE, null, ex);
        }

        PedidoJpaController ped = new PedidoJpaController(emf);
        Pedido superPedido = ped.findPedido(pedidoP.getPedidoId().getId());
        int valorV = 0;
        for (Pedidoproveedor pp : superPedido.getPedidoproveedorList()) {
            if (pp.getEstado() == 3) {
                valorV = 1;
            }
        }
        if (valorV == 0) {
            superPedido.setEstado("8");
        } else {
            superPedido.setEstado("7");
        }
        try {
            ped.edit(superPedido);
        } catch (Exception ex) {
            Logger.getLogger(entregaPedidoProveedor.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("almacen.jsp?noti=12");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
