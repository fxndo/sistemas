/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Almacen;
import Modelo.Caja;
import Modelo.Sucursal;
import Modelo.Usuario;
import Modelo.Ventas;
import Modelo.VentasHasProducto;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.CajaJpaController;
import ModeloJSP.VentasHasProductoJpaController;
import ModeloJSP.VentasJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.awt.Desktop;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarVenta", urlPatterns = {"/agregarVenta"})
public class agregarVenta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        HttpSession venta = request.getSession(true);
        HttpSession pasada = request.getSession(true);
        Usuario informacion = (Usuario) sesion.getAttribute("informacion");
        Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursalTPV");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        Calendar fecha = new GregorianCalendar();
        AlmacenJpaController a = new AlmacenJpaController(emf);
        VentasHasProductoJpaController vhp=new VentasHasProductoJpaController(emf);
        
        CajaJpaController c=new CajaJpaController(emf);
        Caja caja=c.findCaja(c.cerrarCaja(sucursal.getId()));
        
        List<VentasHasProducto>ventas=(List<VentasHasProducto>) venta.getAttribute("venta");
        
        double pago=Double.parseDouble(request.getParameter("entradaEfectivo"));
        double efectivoCaja=caja.getInicio();
        
        efectivoCaja+=pago;
        caja.setInicio(efectivoCaja);
        try {
            c.edit(caja);
        } catch (Exception ex) {
            Logger.getLogger(agregarVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String tipo="";
        if(request.getParameter("comprobante").length()>0){
            tipo=request.getParameter("comprobante");
        }else{
            tipo=request.getParameter("entradaEfectivo");
        }
        
        
        VentasJpaController v=new VentasJpaController(emf);
        Ventas ventah=new Ventas();
        ventah.setCajaId(caja);
        ventah.setFecha(fecha.getTime());
        ventah.setTotal(Double.parseDouble(request.getParameter("total")));
        ventah.setTipo(tipo);
        ventah.setUsuarioId(informacion);
        ventah.setSucursalId(sucursal);
        v.create(ventah);
        for(VentasHasProducto vh: ventas){
            vh.setVentas(ventah);
            vh.setPrecio(vh.getProducto().getPrecioventa());
            Almacen almacen=a.findAlmacen(a.obtenerAlmacen(vh.getProducto().getId(), sucursal.getId()));
            int cantidad=almacen.getCantidad();
            cantidad-=vh.getCantidad();
            almacen.setCantidad(cantidad);
            try {
                a.edit(almacen);
                vhp.create(vh);
            } catch (Exception ex) {
                Logger.getLogger(agregarVenta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        pasada.setAttribute("pasada", ventah);
        venta.removeAttribute("venta");
        response.sendRedirect("tpv.jsp?imprimir=1234");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
