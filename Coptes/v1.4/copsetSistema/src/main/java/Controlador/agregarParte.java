/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import Modelo.Producto;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoparteJpaController;
import ModeloJSP.PedidoproveedorJpaController;
import ModeloJSP.ProductoJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarParte", urlPatterns = {"/agregarParte"})
public class agregarParte extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        int idProducto=Integer.parseInt(request.getParameter("idProducto"));
        int idPedido=Integer.parseInt(request.getParameter("idPedido"));
        int cantidad=Integer.parseInt(request.getParameter("cantidad"));
        
        PedidoJpaController pj=new PedidoJpaController(emf);
        PedidoproveedorJpaController pprj=new PedidoproveedorJpaController(emf);
        PedidoparteJpaController ppaj=new PedidoparteJpaController(emf);
        ProductoJpaController prj=new ProductoJpaController(emf);
        
        Pedido pedido=pj.findPedido(idPedido);
        Pedidoproveedor pedPro=pprj.findPedidoproveedor(pedido.getPedidoproveedorList().get(0).getId());
        Producto producto=prj.findProducto(idProducto);
        
        Pedidoparte parte=new Pedidoparte();
        parte.setCantidad(cantidad);
        parte.setDescuento(0);
        parte.setDevolucion(0);
        parte.setFaltante(cantidad);
        parte.setPedidoProveedorid(pedPro);
        parte.setEstado(1);
        parte.setPrecio(producto.getPrecioventa());
        parte.setTipo(1);
        parte.setProductoId(producto);
        
        ppaj.create(parte);
        response.sendRedirect("pedido.jsp?id="+pedido.getId()+"&list=productos&noti=9");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
