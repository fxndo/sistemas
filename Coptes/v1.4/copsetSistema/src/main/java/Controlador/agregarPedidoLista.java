/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Pedidoparte;
import Modelo.Producto;
import ModeloJSP.EditorialJpaController;
import ModeloJSP.NivelJpaController;
import ModeloJSP.ProductoJpaController;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarPedidoLista", urlPatterns = {"/agregarPedidoLista"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, // 10 MB 
        maxFileSize = 1024 * 1024 * 50, // 50 MB
        maxRequestSize = 1024 * 1024 * 100)
public class agregarPedidoLista extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        Part part = request.getPart("archivo");
        ProductoJpaController pj = new ProductoJpaController(emf);
        HttpSession listaPedido = request.getSession(true);
        List<Pedidoparte> partes = new ArrayList<Pedidoparte>();
        EditorialJpaController ed = new EditorialJpaController(emf);
        NivelJpaController n = new NivelJpaController(emf);

        int agregados = 0;
        int noAgregados = 0;
        int validar = 100;
        int campo = 0;
        int valor = 1;
        try {
            String applicationPath = request.getServletContext().getRealPath("") + "/reportes/";
            String uploadFilePath = applicationPath;
            String fileName = null;
            if (!part.getSubmittedFileName().substring(part.getSubmittedFileName().lastIndexOf(".") + 1).equals("xlsx")) {
                validar = 101;
            } else {

                fileName = "listaProductos" + "." + part.getSubmittedFileName().substring(part.getSubmittedFileName().lastIndexOf(".") + 1);
                File imagen = new File(applicationPath + fileName);
                if (imagen.exists()) {
                    imagen.delete();
                }
                part.write(uploadFilePath + fileName);
                try {
                    sleep(3000);
                } catch (Exception ex) {
                }

                FileInputStream file = new FileInputStream(new File(uploadFilePath + fileName));
                XSSFWorkbook worbook = new XSSFWorkbook(file);
                XSSFSheet sheet = worbook.getSheetAt(0);
                Iterator<Row> rowIterator = sheet.iterator();
                Row row;
                DataFormatter formatter = new DataFormatter();

                while (rowIterator.hasNext()) {
                    String estadoProducto = "2";
                    row = rowIterator.next();
                    Iterator<Cell> cellIterator = row.cellIterator();
                    Cell cell;
                    if (formatter.formatCellValue(row.getCell(0)).equals("ISBN") || formatter.formatCellValue(row.getCell(0)).equals("isbn")) {
                    }else{
                        if (formatter.formatCellValue(row.getCell(0)).length() == 13) {
                            Pedidoparte parte = new Pedidoparte();
                            Producto producto = new Producto();
                            if (pj.productoTPV(formatter.formatCellValue(row.getCell(0))) != 0) {
                                agregados++;
                                producto = pj.findProducto(pj.productoTPV(formatter.formatCellValue(row.getCell(0))));

                            } else {

                                producto.setIsbn(formatter.formatCellValue(row.getCell(0)));
                                if (formatter.formatCellValue(row.getCell(1)).length() > 0 && formatter.formatCellValue(row.getCell(1)).length() < 250) {
                                    producto.setTitulo(formatter.formatCellValue(row.getCell(1)));
                                    System.out.println("@@ Titulo:" + formatter.formatCellValue(row.getCell(1)));
                                } else {
                                    validar = 103;
                                    campo = valor;
                                    break;
                                }
                                producto.setSerie(formatter.formatCellValue(row.getCell(2)));
                                if (ed.buscarEditorial(formatter.formatCellValue(row.getCell(3))) > 0) {
                                    producto.setEditorialId1(ed.findEditorial(ed.buscarEditorial(formatter.formatCellValue(row.getCell(3)))));
                                } else {
                                    producto.setEditorialId1(null);
                                    estadoProducto = "1";
                                }
                                producto.setNivelId(n.findNivel(3));
                                producto.setAutor("Sin Autor");
                                producto.setTextlit("Texto");
                                producto.setPreciocompra("0");
                                if(formatter.formatCellValue(row.getCell(4)).length()>0 && formatter.formatCellValue(row.getCell(4)).length()<6){
                                    producto.setPrecioventa(formatter.formatCellValue(row.getCell(4)));
                                }else{
                                    validar=104;
                                    campo=valor;
                                    break;
                                }
                                
                                producto.setEstado(estadoProducto);
//                        pj.create(producto);
                            }

                            parte.setProductoId(producto);
                            parte.setDescuento(Integer.parseInt(formatter.formatCellValue(row.getCell(5))));
                            if (formatter.formatCellValue(row.getCell(6)).length() > 0){
                                parte.setCantidad(Integer.parseInt(formatter.formatCellValue(row.getCell(6))));
                            }else{
                                validar=105;
                                campo=valor;
                                break;
                            }
                            partes.add(parte);
                        } else {
                            validar = 102;
                            campo = valor;
                            break;
                        }
                    }
                            valor++;
                }

                if (validar == 100) {
                    listaPedido.setAttribute("partes", partes);
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
        request.getRequestDispatcher("nuevoPedido.jsp?ma=" + validar + "&campo=" + campo).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
