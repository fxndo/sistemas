/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Almacen;
import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "pasarBodega", urlPatterns = {"/pasarBodega"})
public class pasarBodega extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        int idPedido = Integer.parseInt(request.getParameter("idPedido"));

        PedidoJpaController p = new PedidoJpaController(emf);
        Pedido pedido = p.findPedido(idPedido);

        AlmacenJpaController a = new AlmacenJpaController(emf);

        for (Pedidoproveedor pp : pedido.getPedidoproveedorList()) {
            for (Pedidoparte pa : pp.getPedidoparteList()) {
                int idAlmacen = a.obtenerAlmacen(pa.getProductoId().getId(),pedido.getSucursalId().getId());

                Almacen almacen = new Almacen();
                if (idAlmacen == 0) {
                    almacen.setCantidad(pa.getCantidad());
                    almacen.setProductoId(pa.getProductoId());
                    almacen.setSucursalId(pedido.getSucursalId());
                    a.create(almacen);
                } else {

                    almacen = a.findAlmacen(idAlmacen);
                    int cantidad = 0;
                    cantidad = almacen.getCantidad() + pa.getCantidad();
                    almacen.setCantidad(cantidad);
                    try {
                        a.edit(almacen);
                    } catch (Exception ex) {
                        Logger.getLogger(pasarBodega.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }

        }
        
        pedido.setEstado("11");
        try {
            p.edit(pedido);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(pasarBodega.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(pasarBodega.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.sendRedirect("pedidos.jsp?noti=26");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
