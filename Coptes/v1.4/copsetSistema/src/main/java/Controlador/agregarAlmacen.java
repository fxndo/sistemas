/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Almacen;
import Modelo.Producto;
import Modelo.Sucursal;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.ProductoJpaController;
import ModeloJSP.SucursalJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "agregarAlmacen", urlPatterns = {"/agregarAlmacen"})
public class agregarAlmacen extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        AlmacenJpaController a = new AlmacenJpaController(emf);
        Almacen almacen = new Almacen();
        int unidades = Integer.parseInt(request.getParameter("unidades"));
        int idSucursal = Integer.parseInt(request.getParameter("idSucursal"));
        int idProducto = Integer.parseInt(request.getParameter("idProducto"));

        int idAlmacen = a.obtenerAlmacen(idProducto, idSucursal);
        if (idAlmacen == 0) {
            SucursalJpaController s = new SucursalJpaController(emf);
            Sucursal sucursal = s.findSucursal(idSucursal);
            ProductoJpaController p = new ProductoJpaController(emf);
            Producto producto = p.findProducto(idProducto);
            almacen.setCantidad(unidades);
            almacen.setProductoId(producto);
            almacen.setSucursalId(sucursal);
            a.create(almacen);
        } else {

            almacen = a.findAlmacen(idAlmacen);
            int cantidad = 0;
            if (request.getParameter("idEditar") != null) {
                cantidad = unidades;
            } else {
                cantidad = almacen.getCantidad() + unidades;

            }
            almacen.setCantidad(cantidad);

            try {
                a.edit(almacen);
            } catch (Exception ex) {
                Logger.getLogger(agregarAlmacen.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        request.getRequestDispatcher("producto.jsp?id="+idProducto+"&noti=20").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
