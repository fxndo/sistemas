/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Producto;
import Modelo.Sucursal;
import Modelo.Usuario;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.ProductoJpaController;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "buscarParte", urlPatterns = {"/buscarParte"})
public class buscarParte extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        String objeto = request.getParameter("objeto");
        int idPedido = Integer.parseInt(request.getParameter("id"));
        PedidoJpaController pj = new PedidoJpaController(emf);
        Pedido pedido=new Pedido();
        if(idPedido!=0){
        pedido = pj.findPedido(idPedido);
        }
        ProductoJpaController p = new ProductoJpaController(emf);
        List<Producto> productos = p.buscarProductos(objeto, 0);
        Usuario informacion = (Usuario) sesion.getAttribute("informacion");
        String cadena="vacio";
        if(productos.size()>0){
        cadena = "<tr>\n"
                + "                            <th>Tutulo</th>\n"
                + "                            <th>Serie</th>\n"
                + "                            <th>Editorial</th>\n";
                if(idPedido!=0){
                cadena+="<th style='width: 110px;'>Cantidad</th>\n";
                }
                cadena+="</tr>";
        for (Producto pd : productos) {
            int valor = 0;
            if(idPedido!=0){
            for (Pedidoparte ppa : pedido.getPedidoproveedorList().get(0).getPedidoparteList()) {
                if (ppa.getProductoId().getId()==pd.getId()){
                    valor = 1;
                }
            }
            }
            if (valor != 1) {

                cadena += "<tr><td>" + pd.getTitulo() + "</td>";
                if (pd.getSerie() != null) {
                    cadena += "<td>" + pd.getSerie();
                } else {
                    cadena += "<td><span class='noInformacion'>Sin Informacion</span>";
                }
                cadena += "</td> <td>" + pd.getEditorialId1().getNombre()
                        + "</td><td style='padding:0px;'><form action='agregarParte' method='post'><input type='hidden' name='idPedido' value='" + idPedido + "'><input type='hidden' name='idProducto' value='" + pd.getId() + "'>"
                        + "<input type='text' name='cantidad' required=\"on\" class='entradaTablac'>"
                        + "<div style='padding:6px;display:inline-block;'>";
                cadena += "<button type='submit' class='btnAgregar quitarBoton'><img onclick=\"incluirProducto('" + pd.getIsbn() + "')\" class='iconoNormal' src='images/suma2.png'></button></div></form></td></tr>";
            }
        }
        }

        ServletOutputStream outputStream = response.getOutputStream();
        response.setContentType("application/x-json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        outputStream.print(new Gson().toJson(cadena));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
