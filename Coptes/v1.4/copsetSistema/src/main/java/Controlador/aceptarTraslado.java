/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Almacen;
import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import Modelo.Proveedor;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoparteJpaController;
import ModeloJSP.PedidoproveedorJpaController;
import ModeloJSP.ProveedorJpaController;
import ModeloJSP.ProveedoreditorialJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "aceptarTraslado", urlPatterns = {"/aceptarTraslado"})
public class aceptarTraslado extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        
        AlmacenJpaController a = new AlmacenJpaController(emf);
        PedidoparteJpaController ppaj = new PedidoparteJpaController(emf);
        PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
        ProveedorJpaController pvj = new ProveedorJpaController(emf);
        PedidoJpaController pdj=new PedidoJpaController(emf);
        ProveedoreditorialJpaController pej=new ProveedoreditorialJpaController(emf);
        int idParte = Integer.parseInt(request.getParameter("id"));
        int almacen = Integer.parseInt(request.getParameter("cantidad"));
        Pedidoparte parte = ppaj.findPedidoparte(idParte);
        int cantidad = parte.getCantidad();

        
        if (cantidad > almacen) {
            Pedidoproveedor pedidoPro = new Pedidoproveedor();
            Pedidoparte parte2 = new Pedidoparte();
            if (pprj.pedidoP(parte.getPedidoProveedorid().getPedidoId().getId(), pvj.listaProveedoresEditorial(parte.getProductoId().getEditorialId1().getId()).get(0).getId()) > 0) {
                pedidoPro = pprj.findPedidoproveedor(pprj.pedidoP(parte.getPedidoProveedorid().getPedidoId().getId(), pvj.listaProveedoresEditorial(parte.getProductoId().getEditorialId1().getId()).get(0).getId()));

            } else {
                pedidoPro.setEstado(1);
                pedidoPro.setPedidoId(parte.getPedidoProveedorid().getPedidoId());
                pedidoPro.setProveedorId(pvj.listaProveedoresEditorial(parte.getProductoId().getEditorialId1().getId()).get(0));
                pprj.create(pedidoPro);

                pedidoPro = pprj.findPedidoproveedor(pprj.pedidoP(parte.getPedidoProveedorid().getPedidoId().getId(), pvj.listaProveedoresEditorial(parte.getProductoId().getEditorialId1().getId()).get(0).getId()));
            }
            if (almacen > 0) {

                parte2.setCantidad(cantidad - almacen);
                parte2.setDescuento(parte.getDescuento());
                parte2.setDevolucion(0);
                parte2.setEstado(1);
                parte2.setFaltante(cantidad - almacen);
                parte2.setPedidoProveedorid(pedidoPro);
                parte2.setProductoId(parte.getProductoId());
                parte2.setTipo(1);
                parte2.setPrecio(parte.getProductoId().getPrecioventa());
                parte2.setDescuentoProveedor(pej.proveedorDescuento(parte.getProductoId().getEditorialId1().getId(), pedidoPro.getProveedorId().getId()));
                
                ppaj.create(parte2);

                parte.setCantidad(almacen);
                parte.setFaltante(0);
                parte.setEstado(3);
                parte.setTipo(1);
            } else {
                
                parte.setSucursalId(null);
                parte.setPedidoProveedorid(pedidoPro);
                parte.setEstado(4);
            }
        } else {
            parte.setFaltante(0);
            parte.setEstado(3);
        }

        if (almacen > 0) {
            Almacen almacenPedido = a.findAlmacen(a.obtenerAlmacen(parte.getProductoId().getId(), parte.getSucursalId().getId()));
            almacenPedido.setCantidad(almacenPedido.getCantidad() - almacen);
            try {
                a.edit(almacenPedido);
            } catch (Exception ex) {
                Logger.getLogger(aceptarTraslado.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        Pedidoproveedor pedidoProViejo=pprj.findPedidoproveedor(parte.getPedidoProveedorid().getId());
        if (almacen > 0) {
            pedidoProViejo.setEstado(2);
        }else{
            pedidoProViejo.setEstado(1);
        }
        try {
            pprj.edit(pedidoProViejo);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(aceptarTraslado.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(aceptarTraslado.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            ppaj.edit(parte);
        } catch (Exception ex) {
            Logger.getLogger(aceptarTraslado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        Pedido pedido=pdj.findPedido(parte.getPedidoProveedorid().getPedidoId().getId());
        int traslados=0;
        for(Pedidoproveedor pprf: pedido.getPedidoproveedorList()){
            for(Pedidoparte ppaf: pprf.getPedidoparteList()){
                if(ppaf.getEstado()==2){
                    traslados=1;
                }
            }
        }
        
        if(traslados==0){
            pedido.setEstado("4");
            try {
                pdj.edit(pedido);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(aceptarTraslado.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(aceptarTraslado.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        response.sendRedirect("traslados.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
