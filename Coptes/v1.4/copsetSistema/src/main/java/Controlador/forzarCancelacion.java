/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Almacen;
import Modelo.Pedido;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import Modelo.Sucursal;
import ModeloJSP.AlmacenJpaController;
import ModeloJSP.PedidoJpaController;
import ModeloJSP.PedidoparteJpaController;
import ModeloJSP.PedidoproveedorJpaController;
import ModeloJSP.SucursalJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "forzarCancelacion", urlPatterns = {"/forzarCancelacion"})
public class forzarCancelacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("cerrarSesion").forward(request, response);
        }

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

        int idSucursal = 0;
        if (request.getParameter("idSucursal") != null) {
            idSucursal = Integer.parseInt(request.getParameter("idSucursal"));
        }
        int idPedido = Integer.parseInt(request.getParameter("idPedido"));

        SucursalJpaController scj = new SucursalJpaController(emf);
        PedidoJpaController pj = new PedidoJpaController(emf);
        AlmacenJpaController alj = new AlmacenJpaController(emf);
        PedidoparteJpaController ppaj = new PedidoparteJpaController(emf);
        PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);

        Almacen almacen = new Almacen();
        Sucursal sucursal = new Sucursal();
        sucursal = scj.findSucursal(idSucursal);
        Pedido pedido = pj.findPedido(idPedido);
        int valor = Integer.parseInt(pedido.getEstado());

        if (valor == 8 || valor == 9 || valor == 10) {
            for (Pedidoproveedor pprf : pedido.getPedidoproveedorList()) {
                for (Pedidoparte ppaf : pprf.getPedidoparteList()) {
                    if (idSucursal != 0) {
                        if (alj.obtenerAlmacen(ppaf.getProductoId().getId(), idSucursal) > 0) {
                            almacen = alj.findAlmacen(alj.obtenerAlmacen(ppaf.getProductoId().getId(), idSucursal));
                            almacen.setCantidad(almacen.getCantidad() + ppaf.getCantidad());
                            try {
                                alj.edit(almacen);
                            } catch (Exception ex) {
                                Logger.getLogger(forzarCancelacion.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else {
                            almacen.setCantidad(ppaf.getCantidad());
                            almacen.setProductoId(ppaf.getProductoId());
                            almacen.setSucursalId(sucursal);
                            alj.create(almacen);
                        }

                        ppaf.setEstado(6);
                        try {
                            ppaj.edit(ppaf);
                        } catch (Exception ex) {
                            Logger.getLogger(forzarCancelacion.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                pprf.setEstado(5);
                try {
                    pprj.edit(pprf);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(forzarCancelacion.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(forzarCancelacion.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        pedido.setEstado("15");
        try {
            pj.edit(pedido);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(forzarCancelacion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(forzarCancelacion.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("pedidos.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
