/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Direccion;
import Modelo.Permisos;
import Modelo.Sucursal;
import Modelo.Usuario;
import ModeloJSP.DireccionJpaController;
import ModeloJSP.PermisosJpaController;
import ModeloJSP.SucursalJpaController;
import ModeloJSP.UsuarioJpaController;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author fernando
 */
@WebServlet(name = "editarUsuario", urlPatterns = {"/editarUsuario"})
public class editarUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession(true);
        if (sesion.getAttribute("informacion") != null) {
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

        UsuarioJpaController u = new UsuarioJpaController(emf);
        Usuario usuario = u.findUsuario(Integer.parseInt(request.getParameter("idUsuario")));

        DireccionJpaController d = new DireccionJpaController(emf);
        Direccion direccion = new Direccion();

        SucursalJpaController s = new SucursalJpaController(emf);

        String nombre = request.getParameter("nombre");
        String cargo = request.getParameter("cargo");
        String telefono = request.getParameter("telefono");
        String email = request.getParameter("email");

        int idSucursal = Integer.parseInt(request.getParameter("sucursal"));
        Sucursal sucursal = s.findSucursal(idSucursal);

        int tpv = Integer.parseInt(request.getParameter("tpv"));
        int compras = Integer.parseInt(request.getParameter("compras"));
        int almacen = Integer.parseInt(request.getParameter("almacen"));
        int pagos = Integer.parseInt(request.getParameter("pagos"));
        int remision = Integer.parseInt(request.getParameter("remisiones"));
        int admin = Integer.parseInt(request.getParameter("administrador"));
        int superUsuario = Integer.parseInt(request.getParameter("superUsuario"));
        int pedidos = Integer.parseInt(request.getParameter("pedidos"));
        int ventas = Integer.parseInt(request.getParameter("ventas"));
        int proveedores = Integer.parseInt(request.getParameter("proveedores"));
        int traslados = Integer.parseInt(request.getParameter("traslados"));
        int productos = 1;
        
        System.out.println("Compra: "+compras);

        usuario.setNombre(nombre);
        usuario.setTipo(cargo);
        usuario.setTelefono(telefono);
        usuario.setEmail(email);
        usuario.setSucursalId(sucursal);

        try {
            u.edit(usuario);
            System.out.println("Editado");
        } catch (Exception ex) {
            Logger.getLogger(editarUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        PermisosJpaController pm = new PermisosJpaController(emf);
        Permisos permiso = usuario.getPermisosList().get(0);

        permiso.setAdministracionp(admin);
        permiso.setAlmacenp(almacen);
        permiso.setComprasp(compras);
        permiso.setPagosp(pagos);
        permiso.setPedidosp(pedidos);
        permiso.setProductosp(productos);
        permiso.setProveedoresp(proveedores);
        permiso.setRemisionp(remision);
        permiso.setTpvp(tpv);
        permiso.setUsuarioId(usuario);
        permiso.setVentap(ventas);
        permiso.setSuperusuario(superUsuario);
        permiso.setTraslados(traslados);
//        pm.create(permiso);

        try {
            pm.edit(permiso);
        } catch (Exception ex) {
            Logger.getLogger(editarUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("usuarios.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
