/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Producto;
import Modelo.Sucursal;
import Modelo.Ventas;
import Modelo.VentasHasProducto;
import Modelo.VentasHasProductoPK;
import ModeloJSP.exceptions.NonexistentEntityException;
import ModeloJSP.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class VentasHasProductoJpaController implements Serializable {

    public VentasHasProductoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(VentasHasProducto ventasHasProducto) throws PreexistingEntityException, Exception {
        if (ventasHasProducto.getVentasHasProductoPK() == null) {
            ventasHasProducto.setVentasHasProductoPK(new VentasHasProductoPK());
        }
        ventasHasProducto.getVentasHasProductoPK().setVentasId(ventasHasProducto.getVentas().getId());
        ventasHasProducto.getVentasHasProductoPK().setProductoId(ventasHasProducto.getProducto().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto producto = ventasHasProducto.getProducto();
            if (producto != null) {
                producto = em.getReference(producto.getClass(), producto.getId());
                ventasHasProducto.setProducto(producto);
            }
            Sucursal sucursalId = ventasHasProducto.getSucursalId();
            if (sucursalId != null) {
                sucursalId = em.getReference(sucursalId.getClass(), sucursalId.getId());
                ventasHasProducto.setSucursalId(sucursalId);
            }
            Ventas ventas = ventasHasProducto.getVentas();
            if (ventas != null) {
                ventas = em.getReference(ventas.getClass(), ventas.getId());
                ventasHasProducto.setVentas(ventas);
            }
            em.persist(ventasHasProducto);
            if (producto != null) {
                producto.getVentasHasProductoList().add(ventasHasProducto);
                producto = em.merge(producto);
            }
            if (sucursalId != null) {
                sucursalId.getVentasHasProductoList().add(ventasHasProducto);
                sucursalId = em.merge(sucursalId);
            }
            if (ventas != null) {
                ventas.getVentasHasProductoList().add(ventasHasProducto);
                ventas = em.merge(ventas);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findVentasHasProducto(ventasHasProducto.getVentasHasProductoPK()) != null) {
                throw new PreexistingEntityException("VentasHasProducto " + ventasHasProducto + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(VentasHasProducto ventasHasProducto) throws NonexistentEntityException, Exception {
        ventasHasProducto.getVentasHasProductoPK().setVentasId(ventasHasProducto.getVentas().getId());
        ventasHasProducto.getVentasHasProductoPK().setProductoId(ventasHasProducto.getProducto().getId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VentasHasProducto persistentVentasHasProducto = em.find(VentasHasProducto.class, ventasHasProducto.getVentasHasProductoPK());
            Producto productoOld = persistentVentasHasProducto.getProducto();
            Producto productoNew = ventasHasProducto.getProducto();
            Sucursal sucursalIdOld = persistentVentasHasProducto.getSucursalId();
            Sucursal sucursalIdNew = ventasHasProducto.getSucursalId();
            Ventas ventasOld = persistentVentasHasProducto.getVentas();
            Ventas ventasNew = ventasHasProducto.getVentas();
            if (productoNew != null) {
                productoNew = em.getReference(productoNew.getClass(), productoNew.getId());
                ventasHasProducto.setProducto(productoNew);
            }
            if (sucursalIdNew != null) {
                sucursalIdNew = em.getReference(sucursalIdNew.getClass(), sucursalIdNew.getId());
                ventasHasProducto.setSucursalId(sucursalIdNew);
            }
            if (ventasNew != null) {
                ventasNew = em.getReference(ventasNew.getClass(), ventasNew.getId());
                ventasHasProducto.setVentas(ventasNew);
            }
            ventasHasProducto = em.merge(ventasHasProducto);
            if (productoOld != null && !productoOld.equals(productoNew)) {
                productoOld.getVentasHasProductoList().remove(ventasHasProducto);
                productoOld = em.merge(productoOld);
            }
            if (productoNew != null && !productoNew.equals(productoOld)) {
                productoNew.getVentasHasProductoList().add(ventasHasProducto);
                productoNew = em.merge(productoNew);
            }
            if (sucursalIdOld != null && !sucursalIdOld.equals(sucursalIdNew)) {
                sucursalIdOld.getVentasHasProductoList().remove(ventasHasProducto);
                sucursalIdOld = em.merge(sucursalIdOld);
            }
            if (sucursalIdNew != null && !sucursalIdNew.equals(sucursalIdOld)) {
                sucursalIdNew.getVentasHasProductoList().add(ventasHasProducto);
                sucursalIdNew = em.merge(sucursalIdNew);
            }
            if (ventasOld != null && !ventasOld.equals(ventasNew)) {
                ventasOld.getVentasHasProductoList().remove(ventasHasProducto);
                ventasOld = em.merge(ventasOld);
            }
            if (ventasNew != null && !ventasNew.equals(ventasOld)) {
                ventasNew.getVentasHasProductoList().add(ventasHasProducto);
                ventasNew = em.merge(ventasNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                VentasHasProductoPK id = ventasHasProducto.getVentasHasProductoPK();
                if (findVentasHasProducto(id) == null) {
                    throw new NonexistentEntityException("The ventasHasProducto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(VentasHasProductoPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VentasHasProducto ventasHasProducto;
            try {
                ventasHasProducto = em.getReference(VentasHasProducto.class, id);
                ventasHasProducto.getVentasHasProductoPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ventasHasProducto with id " + id + " no longer exists.", enfe);
            }
            Producto producto = ventasHasProducto.getProducto();
            if (producto != null) {
                producto.getVentasHasProductoList().remove(ventasHasProducto);
                producto = em.merge(producto);
            }
            Sucursal sucursalId = ventasHasProducto.getSucursalId();
            if (sucursalId != null) {
                sucursalId.getVentasHasProductoList().remove(ventasHasProducto);
                sucursalId = em.merge(sucursalId);
            }
            Ventas ventas = ventasHasProducto.getVentas();
            if (ventas != null) {
                ventas.getVentasHasProductoList().remove(ventasHasProducto);
                ventas = em.merge(ventas);
            }
            em.remove(ventasHasProducto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<VentasHasProducto> findVentasHasProductoEntities() {
        return findVentasHasProductoEntities(true, -1, -1);
    }

    public List<VentasHasProducto> findVentasHasProductoEntities(int maxResults, int firstResult) {
        return findVentasHasProductoEntities(false, maxResults, firstResult);
    }

    private List<VentasHasProducto> findVentasHasProductoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(VentasHasProducto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public VentasHasProducto findVentasHasProducto(VentasHasProductoPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(VentasHasProducto.class, id);
        } finally {
            em.close();
        }
    }

    public int getVentasHasProductoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<VentasHasProducto> rt = cq.from(VentasHasProducto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        public List<VentasHasProducto> ventasEditorial(int idEditorial){
        EntityManager em = getEntityManager();
        List<VentasHasProducto>ventas=new ArrayList<VentasHasProducto>();
        try {
                
            TypedQuery<VentasHasProducto> resultList = em.createQuery("SELECT v FROM VentasHasProducto v WHERE v.producto.editorialId1.id=:idEditorial", VentasHasProducto.class);
            resultList.setParameter("idEditorial",idEditorial);
            ventas = resultList.getResultList();
            
        } finally {
            em.close();
        }
            return ventas;
    }
    
}
