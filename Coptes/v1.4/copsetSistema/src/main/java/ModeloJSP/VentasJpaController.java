/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Caja;
import Modelo.Devolucion;
import Modelo.Sucursal;
import Modelo.Usuario;
import Modelo.Ventas;
import Modelo.VentasHasProducto;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class VentasJpaController implements Serializable {

    public VentasJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ventas ventas) {
        if (ventas.getVentasHasProductoList() == null) {
            ventas.setVentasHasProductoList(new ArrayList<VentasHasProducto>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Caja cajaId = ventas.getCajaId();
            if (cajaId != null) {
                cajaId = em.getReference(cajaId.getClass(), cajaId.getId());
                ventas.setCajaId(cajaId);
            }
            Devolucion devolucionId = ventas.getDevolucionId();
            if (devolucionId != null) {
                devolucionId = em.getReference(devolucionId.getClass(), devolucionId.getId());
                ventas.setDevolucionId(devolucionId);
            }
            Sucursal sucursalId = ventas.getSucursalId();
            if (sucursalId != null) {
                sucursalId = em.getReference(sucursalId.getClass(), sucursalId.getId());
                ventas.setSucursalId(sucursalId);
            }
            Usuario usuarioId = ventas.getUsuarioId();
            if (usuarioId != null) {
                usuarioId = em.getReference(usuarioId.getClass(), usuarioId.getId());
                ventas.setUsuarioId(usuarioId);
            }
            List<VentasHasProducto> attachedVentasHasProductoList = new ArrayList<VentasHasProducto>();
            for (VentasHasProducto ventasHasProductoListVentasHasProductoToAttach : ventas.getVentasHasProductoList()) {
                ventasHasProductoListVentasHasProductoToAttach = em.getReference(ventasHasProductoListVentasHasProductoToAttach.getClass(), ventasHasProductoListVentasHasProductoToAttach.getVentasHasProductoPK());
                attachedVentasHasProductoList.add(ventasHasProductoListVentasHasProductoToAttach);
            }
            ventas.setVentasHasProductoList(attachedVentasHasProductoList);
            em.persist(ventas);
            if (cajaId != null) {
                cajaId.getVentasList().add(ventas);
                cajaId = em.merge(cajaId);
            }
            if (devolucionId != null) {
                devolucionId.getVentasList().add(ventas);
                devolucionId = em.merge(devolucionId);
            }
            if (sucursalId != null) {
                sucursalId.getVentasList().add(ventas);
                sucursalId = em.merge(sucursalId);
            }
            if (usuarioId != null) {
                usuarioId.getVentasList().add(ventas);
                usuarioId = em.merge(usuarioId);
            }
            for (VentasHasProducto ventasHasProductoListVentasHasProducto : ventas.getVentasHasProductoList()) {
                Ventas oldVentasOfVentasHasProductoListVentasHasProducto = ventasHasProductoListVentasHasProducto.getVentas();
                ventasHasProductoListVentasHasProducto.setVentas(ventas);
                ventasHasProductoListVentasHasProducto = em.merge(ventasHasProductoListVentasHasProducto);
                if (oldVentasOfVentasHasProductoListVentasHasProducto != null) {
                    oldVentasOfVentasHasProductoListVentasHasProducto.getVentasHasProductoList().remove(ventasHasProductoListVentasHasProducto);
                    oldVentasOfVentasHasProductoListVentasHasProducto = em.merge(oldVentasOfVentasHasProductoListVentasHasProducto);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ventas ventas) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ventas persistentVentas = em.find(Ventas.class, ventas.getId());
            Caja cajaIdOld = persistentVentas.getCajaId();
            Caja cajaIdNew = ventas.getCajaId();
            Devolucion devolucionIdOld = persistentVentas.getDevolucionId();
            Devolucion devolucionIdNew = ventas.getDevolucionId();
            Sucursal sucursalIdOld = persistentVentas.getSucursalId();
            Sucursal sucursalIdNew = ventas.getSucursalId();
            Usuario usuarioIdOld = persistentVentas.getUsuarioId();
            Usuario usuarioIdNew = ventas.getUsuarioId();
            List<VentasHasProducto> ventasHasProductoListOld = persistentVentas.getVentasHasProductoList();
            List<VentasHasProducto> ventasHasProductoListNew = ventas.getVentasHasProductoList();
            List<String> illegalOrphanMessages = null;
            for (VentasHasProducto ventasHasProductoListOldVentasHasProducto : ventasHasProductoListOld) {
                if (!ventasHasProductoListNew.contains(ventasHasProductoListOldVentasHasProducto)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain VentasHasProducto " + ventasHasProductoListOldVentasHasProducto + " since its ventas field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (cajaIdNew != null) {
                cajaIdNew = em.getReference(cajaIdNew.getClass(), cajaIdNew.getId());
                ventas.setCajaId(cajaIdNew);
            }
            if (devolucionIdNew != null) {
                devolucionIdNew = em.getReference(devolucionIdNew.getClass(), devolucionIdNew.getId());
                ventas.setDevolucionId(devolucionIdNew);
            }
            if (sucursalIdNew != null) {
                sucursalIdNew = em.getReference(sucursalIdNew.getClass(), sucursalIdNew.getId());
                ventas.setSucursalId(sucursalIdNew);
            }
            if (usuarioIdNew != null) {
                usuarioIdNew = em.getReference(usuarioIdNew.getClass(), usuarioIdNew.getId());
                ventas.setUsuarioId(usuarioIdNew);
            }
            List<VentasHasProducto> attachedVentasHasProductoListNew = new ArrayList<VentasHasProducto>();
            for (VentasHasProducto ventasHasProductoListNewVentasHasProductoToAttach : ventasHasProductoListNew) {
                ventasHasProductoListNewVentasHasProductoToAttach = em.getReference(ventasHasProductoListNewVentasHasProductoToAttach.getClass(), ventasHasProductoListNewVentasHasProductoToAttach.getVentasHasProductoPK());
                attachedVentasHasProductoListNew.add(ventasHasProductoListNewVentasHasProductoToAttach);
            }
            ventasHasProductoListNew = attachedVentasHasProductoListNew;
            ventas.setVentasHasProductoList(ventasHasProductoListNew);
            ventas = em.merge(ventas);
            if (cajaIdOld != null && !cajaIdOld.equals(cajaIdNew)) {
                cajaIdOld.getVentasList().remove(ventas);
                cajaIdOld = em.merge(cajaIdOld);
            }
            if (cajaIdNew != null && !cajaIdNew.equals(cajaIdOld)) {
                cajaIdNew.getVentasList().add(ventas);
                cajaIdNew = em.merge(cajaIdNew);
            }
            if (devolucionIdOld != null && !devolucionIdOld.equals(devolucionIdNew)) {
                devolucionIdOld.getVentasList().remove(ventas);
                devolucionIdOld = em.merge(devolucionIdOld);
            }
            if (devolucionIdNew != null && !devolucionIdNew.equals(devolucionIdOld)) {
                devolucionIdNew.getVentasList().add(ventas);
                devolucionIdNew = em.merge(devolucionIdNew);
            }
            if (sucursalIdOld != null && !sucursalIdOld.equals(sucursalIdNew)) {
                sucursalIdOld.getVentasList().remove(ventas);
                sucursalIdOld = em.merge(sucursalIdOld);
            }
            if (sucursalIdNew != null && !sucursalIdNew.equals(sucursalIdOld)) {
                sucursalIdNew.getVentasList().add(ventas);
                sucursalIdNew = em.merge(sucursalIdNew);
            }
            if (usuarioIdOld != null && !usuarioIdOld.equals(usuarioIdNew)) {
                usuarioIdOld.getVentasList().remove(ventas);
                usuarioIdOld = em.merge(usuarioIdOld);
            }
            if (usuarioIdNew != null && !usuarioIdNew.equals(usuarioIdOld)) {
                usuarioIdNew.getVentasList().add(ventas);
                usuarioIdNew = em.merge(usuarioIdNew);
            }
            for (VentasHasProducto ventasHasProductoListNewVentasHasProducto : ventasHasProductoListNew) {
                if (!ventasHasProductoListOld.contains(ventasHasProductoListNewVentasHasProducto)) {
                    Ventas oldVentasOfVentasHasProductoListNewVentasHasProducto = ventasHasProductoListNewVentasHasProducto.getVentas();
                    ventasHasProductoListNewVentasHasProducto.setVentas(ventas);
                    ventasHasProductoListNewVentasHasProducto = em.merge(ventasHasProductoListNewVentasHasProducto);
                    if (oldVentasOfVentasHasProductoListNewVentasHasProducto != null && !oldVentasOfVentasHasProductoListNewVentasHasProducto.equals(ventas)) {
                        oldVentasOfVentasHasProductoListNewVentasHasProducto.getVentasHasProductoList().remove(ventasHasProductoListNewVentasHasProducto);
                        oldVentasOfVentasHasProductoListNewVentasHasProducto = em.merge(oldVentasOfVentasHasProductoListNewVentasHasProducto);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ventas.getId();
                if (findVentas(id) == null) {
                    throw new NonexistentEntityException("The ventas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ventas ventas;
            try {
                ventas = em.getReference(Ventas.class, id);
                ventas.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ventas with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<VentasHasProducto> ventasHasProductoListOrphanCheck = ventas.getVentasHasProductoList();
            for (VentasHasProducto ventasHasProductoListOrphanCheckVentasHasProducto : ventasHasProductoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Ventas (" + ventas + ") cannot be destroyed since the VentasHasProducto " + ventasHasProductoListOrphanCheckVentasHasProducto + " in its ventasHasProductoList field has a non-nullable ventas field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Caja cajaId = ventas.getCajaId();
            if (cajaId != null) {
                cajaId.getVentasList().remove(ventas);
                cajaId = em.merge(cajaId);
            }
            Devolucion devolucionId = ventas.getDevolucionId();
            if (devolucionId != null) {
                devolucionId.getVentasList().remove(ventas);
                devolucionId = em.merge(devolucionId);
            }
            Sucursal sucursalId = ventas.getSucursalId();
            if (sucursalId != null) {
                sucursalId.getVentasList().remove(ventas);
                sucursalId = em.merge(sucursalId);
            }
            Usuario usuarioId = ventas.getUsuarioId();
            if (usuarioId != null) {
                usuarioId.getVentasList().remove(ventas);
                usuarioId = em.merge(usuarioId);
            }
            em.remove(ventas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ventas> findVentasEntities() {
        return findVentasEntities(true, -1, -1);
    }

    public List<Ventas> findVentasEntities(int maxResults, int firstResult) {
        return findVentasEntities(false, maxResults, firstResult);
    }

    private List<Ventas> findVentasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ventas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ventas findVentas(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ventas.class, id);
        } finally {
            em.close();
        }
    }

    public int getVentasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ventas> rt = cq.from(Ventas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        public List<Ventas> ventaSucursal(int objeto, int idSucursal, int pagina) {
        EntityManager em = getEntityManager();
        List<Ventas> ventas = new ArrayList<Ventas>();
        String query="";
        try {
            if (idSucursal == 1) {
                if(objeto==0){
                    query="SELECT v FROM Ventas v ORDER BY v.fecha ASC";
                }else{
                    query="SELECT v FROM Ventas v WHERE v.id =" + objeto + " ORDER BY v.fecha ASC";
                }
                TypedQuery<Ventas> resultList = em.createQuery(query, Ventas.class).setFirstResult(pagina * 20).setMaxResults(20);
//                resultList.setParameter("objeto",objeto);
                ventas = resultList.getResultList();

            } else {
                if(objeto==0){
                    query="SELECT v FROM Ventas v WHERE v.sucursalId.id =:idSucursal ORDER BY v.fecha ASC";
                }else{
                    query="SELECT v FROM Ventas v WHERE v.sucursalId.id =:idSucursal AND v.id =:objeto ORDER BY v.fecha ASC";
                }
                TypedQuery<Ventas> resultList = em.createQuery(query, Ventas.class).setFirstResult(20 * pagina).setMaxResults(20);
                resultList.setParameter("idSucursal", idSucursal);
                if(objeto!=0){
                resultList.setParameter("objeto", objeto);
                }
                ventas = resultList.getResultList();
            }
        } finally {
            em.close();
        }
        return ventas;
    }

    public int numeroVentaSucursal(int idSucursal) {
        EntityManager em = getEntityManager();
        int numero = 0;

        try {
            if (idSucursal == 1) {
                TypedQuery<Ventas> resultList = em.createQuery("SELECT v FROM Ventas v ORDER BY v.fecha ASC", Ventas.class);
                numero = resultList.getResultList().size();

            } else {

                TypedQuery<Ventas> resultList = em.createQuery("SELECT v FROM Ventas v WHERE v.sucursalId.id =:idSucursal ORDER BY v.fecha ASC ", Ventas.class);
                resultList.setParameter("idSucursal", idSucursal);
                numero = resultList.getResultList().size();
            }
        } finally {
            em.close();
        }
        return numero;
    }

    public double ventasMes(int idSucursal) {
        EntityManager em = getEntityManager();
        double valor = 0;
        try {
            TypedQuery<Ventas> resultList = em.createQuery("SELECT v FROM Ventas v WHERE v.sucursalId.id =:idSucursal AND v.fecha >= :mesActual", Ventas.class);
            resultList.setParameter("idSucursal", idSucursal);
            Date date = Date.from(Instant.now());
            date.setDate(1);
            resultList.setParameter("mesActual", date, TemporalType.DATE);

            for (Ventas vt : resultList.getResultList()) {
                for (VentasHasProducto vh : vt.getVentasHasProductoList()) {
                    valor += vh.getCantidad();
                }
            }

        } finally {
            em.close();
        }
        return valor;
    }

    public double gananciasVentasMes(int idSucursal) {
        EntityManager em = getEntityManager();
        double valor = 0;
        try {
            TypedQuery<Ventas> resultList = em.createQuery("SELECT v FROM Ventas v WHERE v.sucursalId.id =:idSucursal AND v.fecha >= :mesActual", Ventas.class);
            resultList.setParameter("idSucursal", idSucursal);
            Date date = Date.from(Instant.now());
            date.setDate(1);
            resultList.setParameter("mesActual", date, TemporalType.DATE);

            for (Ventas vt : resultList.getResultList()) {
                for (VentasHasProducto vh : vt.getVentasHasProductoList()) {
                    valor += vh.getCantidad() * (Double.parseDouble(vh.getProducto().getPrecioventa()) - Double.parseDouble(vh.getProducto().getPreciocompra()));
                }
            }

        } finally {
            em.close();
        }
        return valor;
    }

    public int ventasUsuaio(int idUsuario) {
        EntityManager em = getEntityManager();
        int valor = 0;
        try {
            TypedQuery<Ventas> resultList = em.createQuery("SELECT v FROM Ventas v WHERE v.usuarioId.id=:idUsuario", Ventas.class);
            resultList.setParameter("idUsuario", idUsuario);

            List<Ventas> ventas = resultList.getResultList();

            for (Ventas vt : ventas) {
                for (VentasHasProducto vh : vt.getVentasHasProductoList()) {
                    valor += vh.getCantidad();
                }
            }

        } finally {
            em.close();
        }
        return valor;
    }
    
}
