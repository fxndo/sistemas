/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Sucursal;
import Modelo.Cliente;
import java.util.ArrayList;
import java.util.List;
import Modelo.Remisionp;
import Modelo.Permisos;
import Modelo.Pago;
import Modelo.Devolucion;
import Modelo.Pedido;
import Modelo.Ventas;
import Modelo.Caja;
import Modelo.Usuario;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class UsuarioJpaController implements Serializable {

    public UsuarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) {
        if (usuario.getClienteList() == null) {
            usuario.setClienteList(new ArrayList<Cliente>());
        }
        if (usuario.getRemisionpList() == null) {
            usuario.setRemisionpList(new ArrayList<Remisionp>());
        }
        if (usuario.getPermisosList() == null) {
            usuario.setPermisosList(new ArrayList<Permisos>());
        }
        if (usuario.getPagoList() == null) {
            usuario.setPagoList(new ArrayList<Pago>());
        }
        if (usuario.getDevolucionList() == null) {
            usuario.setDevolucionList(new ArrayList<Devolucion>());
        }
        if (usuario.getPedidoList() == null) {
            usuario.setPedidoList(new ArrayList<Pedido>());
        }
        if (usuario.getVentasList() == null) {
            usuario.setVentasList(new ArrayList<Ventas>());
        }
        if (usuario.getCajaList() == null) {
            usuario.setCajaList(new ArrayList<Caja>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sucursal sucursalId = usuario.getSucursalId();
            if (sucursalId != null) {
                sucursalId = em.getReference(sucursalId.getClass(), sucursalId.getId());
                usuario.setSucursalId(sucursalId);
            }
            List<Cliente> attachedClienteList = new ArrayList<Cliente>();
            for (Cliente clienteListClienteToAttach : usuario.getClienteList()) {
                clienteListClienteToAttach = em.getReference(clienteListClienteToAttach.getClass(), clienteListClienteToAttach.getId());
                attachedClienteList.add(clienteListClienteToAttach);
            }
            usuario.setClienteList(attachedClienteList);
            List<Remisionp> attachedRemisionpList = new ArrayList<Remisionp>();
            for (Remisionp remisionpListRemisionpToAttach : usuario.getRemisionpList()) {
                remisionpListRemisionpToAttach = em.getReference(remisionpListRemisionpToAttach.getClass(), remisionpListRemisionpToAttach.getId());
                attachedRemisionpList.add(remisionpListRemisionpToAttach);
            }
            usuario.setRemisionpList(attachedRemisionpList);
            List<Permisos> attachedPermisosList = new ArrayList<Permisos>();
            for (Permisos permisosListPermisosToAttach : usuario.getPermisosList()) {
                permisosListPermisosToAttach = em.getReference(permisosListPermisosToAttach.getClass(), permisosListPermisosToAttach.getId());
                attachedPermisosList.add(permisosListPermisosToAttach);
            }
            usuario.setPermisosList(attachedPermisosList);
            List<Pago> attachedPagoList = new ArrayList<Pago>();
            for (Pago pagoListPagoToAttach : usuario.getPagoList()) {
                pagoListPagoToAttach = em.getReference(pagoListPagoToAttach.getClass(), pagoListPagoToAttach.getId());
                attachedPagoList.add(pagoListPagoToAttach);
            }
            usuario.setPagoList(attachedPagoList);
            List<Devolucion> attachedDevolucionList = new ArrayList<Devolucion>();
            for (Devolucion devolucionListDevolucionToAttach : usuario.getDevolucionList()) {
                devolucionListDevolucionToAttach = em.getReference(devolucionListDevolucionToAttach.getClass(), devolucionListDevolucionToAttach.getId());
                attachedDevolucionList.add(devolucionListDevolucionToAttach);
            }
            usuario.setDevolucionList(attachedDevolucionList);
            List<Pedido> attachedPedidoList = new ArrayList<Pedido>();
            for (Pedido pedidoListPedidoToAttach : usuario.getPedidoList()) {
                pedidoListPedidoToAttach = em.getReference(pedidoListPedidoToAttach.getClass(), pedidoListPedidoToAttach.getId());
                attachedPedidoList.add(pedidoListPedidoToAttach);
            }
            usuario.setPedidoList(attachedPedidoList);
            List<Ventas> attachedVentasList = new ArrayList<Ventas>();
            for (Ventas ventasListVentasToAttach : usuario.getVentasList()) {
                ventasListVentasToAttach = em.getReference(ventasListVentasToAttach.getClass(), ventasListVentasToAttach.getId());
                attachedVentasList.add(ventasListVentasToAttach);
            }
            usuario.setVentasList(attachedVentasList);
            List<Caja> attachedCajaList = new ArrayList<Caja>();
            for (Caja cajaListCajaToAttach : usuario.getCajaList()) {
                cajaListCajaToAttach = em.getReference(cajaListCajaToAttach.getClass(), cajaListCajaToAttach.getId());
                attachedCajaList.add(cajaListCajaToAttach);
            }
            usuario.setCajaList(attachedCajaList);
            em.persist(usuario);
            if (sucursalId != null) {
                sucursalId.getUsuarioList().add(usuario);
                sucursalId = em.merge(sucursalId);
            }
            for (Cliente clienteListCliente : usuario.getClienteList()) {
                clienteListCliente.getUsuarioList().add(usuario);
                clienteListCliente = em.merge(clienteListCliente);
            }
            for (Remisionp remisionpListRemisionp : usuario.getRemisionpList()) {
                Usuario oldUsuarioIdOfRemisionpListRemisionp = remisionpListRemisionp.getUsuarioId();
                remisionpListRemisionp.setUsuarioId(usuario);
                remisionpListRemisionp = em.merge(remisionpListRemisionp);
                if (oldUsuarioIdOfRemisionpListRemisionp != null) {
                    oldUsuarioIdOfRemisionpListRemisionp.getRemisionpList().remove(remisionpListRemisionp);
                    oldUsuarioIdOfRemisionpListRemisionp = em.merge(oldUsuarioIdOfRemisionpListRemisionp);
                }
            }
            for (Permisos permisosListPermisos : usuario.getPermisosList()) {
                Usuario oldUsuarioIdOfPermisosListPermisos = permisosListPermisos.getUsuarioId();
                permisosListPermisos.setUsuarioId(usuario);
                permisosListPermisos = em.merge(permisosListPermisos);
                if (oldUsuarioIdOfPermisosListPermisos != null) {
                    oldUsuarioIdOfPermisosListPermisos.getPermisosList().remove(permisosListPermisos);
                    oldUsuarioIdOfPermisosListPermisos = em.merge(oldUsuarioIdOfPermisosListPermisos);
                }
            }
            for (Pago pagoListPago : usuario.getPagoList()) {
                Usuario oldUsuarioIdOfPagoListPago = pagoListPago.getUsuarioId();
                pagoListPago.setUsuarioId(usuario);
                pagoListPago = em.merge(pagoListPago);
                if (oldUsuarioIdOfPagoListPago != null) {
                    oldUsuarioIdOfPagoListPago.getPagoList().remove(pagoListPago);
                    oldUsuarioIdOfPagoListPago = em.merge(oldUsuarioIdOfPagoListPago);
                }
            }
            for (Devolucion devolucionListDevolucion : usuario.getDevolucionList()) {
                Usuario oldUsuarioIdOfDevolucionListDevolucion = devolucionListDevolucion.getUsuarioId();
                devolucionListDevolucion.setUsuarioId(usuario);
                devolucionListDevolucion = em.merge(devolucionListDevolucion);
                if (oldUsuarioIdOfDevolucionListDevolucion != null) {
                    oldUsuarioIdOfDevolucionListDevolucion.getDevolucionList().remove(devolucionListDevolucion);
                    oldUsuarioIdOfDevolucionListDevolucion = em.merge(oldUsuarioIdOfDevolucionListDevolucion);
                }
            }
            for (Pedido pedidoListPedido : usuario.getPedidoList()) {
                Usuario oldUsuarioIdOfPedidoListPedido = pedidoListPedido.getUsuarioId();
                pedidoListPedido.setUsuarioId(usuario);
                pedidoListPedido = em.merge(pedidoListPedido);
                if (oldUsuarioIdOfPedidoListPedido != null) {
                    oldUsuarioIdOfPedidoListPedido.getPedidoList().remove(pedidoListPedido);
                    oldUsuarioIdOfPedidoListPedido = em.merge(oldUsuarioIdOfPedidoListPedido);
                }
            }
            for (Ventas ventasListVentas : usuario.getVentasList()) {
                Usuario oldUsuarioIdOfVentasListVentas = ventasListVentas.getUsuarioId();
                ventasListVentas.setUsuarioId(usuario);
                ventasListVentas = em.merge(ventasListVentas);
                if (oldUsuarioIdOfVentasListVentas != null) {
                    oldUsuarioIdOfVentasListVentas.getVentasList().remove(ventasListVentas);
                    oldUsuarioIdOfVentasListVentas = em.merge(oldUsuarioIdOfVentasListVentas);
                }
            }
            for (Caja cajaListCaja : usuario.getCajaList()) {
                Usuario oldUsuarioIdOfCajaListCaja = cajaListCaja.getUsuarioId();
                cajaListCaja.setUsuarioId(usuario);
                cajaListCaja = em.merge(cajaListCaja);
                if (oldUsuarioIdOfCajaListCaja != null) {
                    oldUsuarioIdOfCajaListCaja.getCajaList().remove(cajaListCaja);
                    oldUsuarioIdOfCajaListCaja = em.merge(oldUsuarioIdOfCajaListCaja);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuario usuario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario persistentUsuario = em.find(Usuario.class, usuario.getId());
            Sucursal sucursalIdOld = persistentUsuario.getSucursalId();
            Sucursal sucursalIdNew = usuario.getSucursalId();
            List<Cliente> clienteListOld = persistentUsuario.getClienteList();
            List<Cliente> clienteListNew = usuario.getClienteList();
            List<Remisionp> remisionpListOld = persistentUsuario.getRemisionpList();
            List<Remisionp> remisionpListNew = usuario.getRemisionpList();
            List<Permisos> permisosListOld = persistentUsuario.getPermisosList();
            List<Permisos> permisosListNew = usuario.getPermisosList();
            List<Pago> pagoListOld = persistentUsuario.getPagoList();
            List<Pago> pagoListNew = usuario.getPagoList();
            List<Devolucion> devolucionListOld = persistentUsuario.getDevolucionList();
            List<Devolucion> devolucionListNew = usuario.getDevolucionList();
            List<Pedido> pedidoListOld = persistentUsuario.getPedidoList();
            List<Pedido> pedidoListNew = usuario.getPedidoList();
            List<Ventas> ventasListOld = persistentUsuario.getVentasList();
            List<Ventas> ventasListNew = usuario.getVentasList();
            List<Caja> cajaListOld = persistentUsuario.getCajaList();
            List<Caja> cajaListNew = usuario.getCajaList();
            List<String> illegalOrphanMessages = null;
            for (Remisionp remisionpListOldRemisionp : remisionpListOld) {
                if (!remisionpListNew.contains(remisionpListOldRemisionp)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Remisionp " + remisionpListOldRemisionp + " since its usuarioId field is not nullable.");
                }
            }
            for (Permisos permisosListOldPermisos : permisosListOld) {
                if (!permisosListNew.contains(permisosListOldPermisos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Permisos " + permisosListOldPermisos + " since its usuarioId field is not nullable.");
                }
            }
            for (Pago pagoListOldPago : pagoListOld) {
                if (!pagoListNew.contains(pagoListOldPago)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pago " + pagoListOldPago + " since its usuarioId field is not nullable.");
                }
            }
            for (Devolucion devolucionListOldDevolucion : devolucionListOld) {
                if (!devolucionListNew.contains(devolucionListOldDevolucion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Devolucion " + devolucionListOldDevolucion + " since its usuarioId field is not nullable.");
                }
            }
            for (Pedido pedidoListOldPedido : pedidoListOld) {
                if (!pedidoListNew.contains(pedidoListOldPedido)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pedido " + pedidoListOldPedido + " since its usuarioId field is not nullable.");
                }
            }
            for (Ventas ventasListOldVentas : ventasListOld) {
                if (!ventasListNew.contains(ventasListOldVentas)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ventas " + ventasListOldVentas + " since its usuarioId field is not nullable.");
                }
            }
            for (Caja cajaListOldCaja : cajaListOld) {
                if (!cajaListNew.contains(cajaListOldCaja)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Caja " + cajaListOldCaja + " since its usuarioId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (sucursalIdNew != null) {
                sucursalIdNew = em.getReference(sucursalIdNew.getClass(), sucursalIdNew.getId());
                usuario.setSucursalId(sucursalIdNew);
            }
            List<Cliente> attachedClienteListNew = new ArrayList<Cliente>();
            for (Cliente clienteListNewClienteToAttach : clienteListNew) {
                clienteListNewClienteToAttach = em.getReference(clienteListNewClienteToAttach.getClass(), clienteListNewClienteToAttach.getId());
                attachedClienteListNew.add(clienteListNewClienteToAttach);
            }
            clienteListNew = attachedClienteListNew;
            usuario.setClienteList(clienteListNew);
            List<Remisionp> attachedRemisionpListNew = new ArrayList<Remisionp>();
            for (Remisionp remisionpListNewRemisionpToAttach : remisionpListNew) {
                remisionpListNewRemisionpToAttach = em.getReference(remisionpListNewRemisionpToAttach.getClass(), remisionpListNewRemisionpToAttach.getId());
                attachedRemisionpListNew.add(remisionpListNewRemisionpToAttach);
            }
            remisionpListNew = attachedRemisionpListNew;
            usuario.setRemisionpList(remisionpListNew);
            List<Permisos> attachedPermisosListNew = new ArrayList<Permisos>();
            for (Permisos permisosListNewPermisosToAttach : permisosListNew) {
                permisosListNewPermisosToAttach = em.getReference(permisosListNewPermisosToAttach.getClass(), permisosListNewPermisosToAttach.getId());
                attachedPermisosListNew.add(permisosListNewPermisosToAttach);
            }
            permisosListNew = attachedPermisosListNew;
            usuario.setPermisosList(permisosListNew);
            List<Pago> attachedPagoListNew = new ArrayList<Pago>();
            for (Pago pagoListNewPagoToAttach : pagoListNew) {
                pagoListNewPagoToAttach = em.getReference(pagoListNewPagoToAttach.getClass(), pagoListNewPagoToAttach.getId());
                attachedPagoListNew.add(pagoListNewPagoToAttach);
            }
            pagoListNew = attachedPagoListNew;
            usuario.setPagoList(pagoListNew);
            List<Devolucion> attachedDevolucionListNew = new ArrayList<Devolucion>();
            for (Devolucion devolucionListNewDevolucionToAttach : devolucionListNew) {
                devolucionListNewDevolucionToAttach = em.getReference(devolucionListNewDevolucionToAttach.getClass(), devolucionListNewDevolucionToAttach.getId());
                attachedDevolucionListNew.add(devolucionListNewDevolucionToAttach);
            }
            devolucionListNew = attachedDevolucionListNew;
            usuario.setDevolucionList(devolucionListNew);
            List<Pedido> attachedPedidoListNew = new ArrayList<Pedido>();
            for (Pedido pedidoListNewPedidoToAttach : pedidoListNew) {
                pedidoListNewPedidoToAttach = em.getReference(pedidoListNewPedidoToAttach.getClass(), pedidoListNewPedidoToAttach.getId());
                attachedPedidoListNew.add(pedidoListNewPedidoToAttach);
            }
            pedidoListNew = attachedPedidoListNew;
            usuario.setPedidoList(pedidoListNew);
            List<Ventas> attachedVentasListNew = new ArrayList<Ventas>();
            for (Ventas ventasListNewVentasToAttach : ventasListNew) {
                ventasListNewVentasToAttach = em.getReference(ventasListNewVentasToAttach.getClass(), ventasListNewVentasToAttach.getId());
                attachedVentasListNew.add(ventasListNewVentasToAttach);
            }
            ventasListNew = attachedVentasListNew;
            usuario.setVentasList(ventasListNew);
            List<Caja> attachedCajaListNew = new ArrayList<Caja>();
            for (Caja cajaListNewCajaToAttach : cajaListNew) {
                cajaListNewCajaToAttach = em.getReference(cajaListNewCajaToAttach.getClass(), cajaListNewCajaToAttach.getId());
                attachedCajaListNew.add(cajaListNewCajaToAttach);
            }
            cajaListNew = attachedCajaListNew;
            usuario.setCajaList(cajaListNew);
            usuario = em.merge(usuario);
            if (sucursalIdOld != null && !sucursalIdOld.equals(sucursalIdNew)) {
                sucursalIdOld.getUsuarioList().remove(usuario);
                sucursalIdOld = em.merge(sucursalIdOld);
            }
            if (sucursalIdNew != null && !sucursalIdNew.equals(sucursalIdOld)) {
                sucursalIdNew.getUsuarioList().add(usuario);
                sucursalIdNew = em.merge(sucursalIdNew);
            }
            for (Cliente clienteListOldCliente : clienteListOld) {
                if (!clienteListNew.contains(clienteListOldCliente)) {
                    clienteListOldCliente.getUsuarioList().remove(usuario);
                    clienteListOldCliente = em.merge(clienteListOldCliente);
                }
            }
            for (Cliente clienteListNewCliente : clienteListNew) {
                if (!clienteListOld.contains(clienteListNewCliente)) {
                    clienteListNewCliente.getUsuarioList().add(usuario);
                    clienteListNewCliente = em.merge(clienteListNewCliente);
                }
            }
            for (Remisionp remisionpListNewRemisionp : remisionpListNew) {
                if (!remisionpListOld.contains(remisionpListNewRemisionp)) {
                    Usuario oldUsuarioIdOfRemisionpListNewRemisionp = remisionpListNewRemisionp.getUsuarioId();
                    remisionpListNewRemisionp.setUsuarioId(usuario);
                    remisionpListNewRemisionp = em.merge(remisionpListNewRemisionp);
                    if (oldUsuarioIdOfRemisionpListNewRemisionp != null && !oldUsuarioIdOfRemisionpListNewRemisionp.equals(usuario)) {
                        oldUsuarioIdOfRemisionpListNewRemisionp.getRemisionpList().remove(remisionpListNewRemisionp);
                        oldUsuarioIdOfRemisionpListNewRemisionp = em.merge(oldUsuarioIdOfRemisionpListNewRemisionp);
                    }
                }
            }
            for (Permisos permisosListNewPermisos : permisosListNew) {
                if (!permisosListOld.contains(permisosListNewPermisos)) {
                    Usuario oldUsuarioIdOfPermisosListNewPermisos = permisosListNewPermisos.getUsuarioId();
                    permisosListNewPermisos.setUsuarioId(usuario);
                    permisosListNewPermisos = em.merge(permisosListNewPermisos);
                    if (oldUsuarioIdOfPermisosListNewPermisos != null && !oldUsuarioIdOfPermisosListNewPermisos.equals(usuario)) {
                        oldUsuarioIdOfPermisosListNewPermisos.getPermisosList().remove(permisosListNewPermisos);
                        oldUsuarioIdOfPermisosListNewPermisos = em.merge(oldUsuarioIdOfPermisosListNewPermisos);
                    }
                }
            }
            for (Pago pagoListNewPago : pagoListNew) {
                if (!pagoListOld.contains(pagoListNewPago)) {
                    Usuario oldUsuarioIdOfPagoListNewPago = pagoListNewPago.getUsuarioId();
                    pagoListNewPago.setUsuarioId(usuario);
                    pagoListNewPago = em.merge(pagoListNewPago);
                    if (oldUsuarioIdOfPagoListNewPago != null && !oldUsuarioIdOfPagoListNewPago.equals(usuario)) {
                        oldUsuarioIdOfPagoListNewPago.getPagoList().remove(pagoListNewPago);
                        oldUsuarioIdOfPagoListNewPago = em.merge(oldUsuarioIdOfPagoListNewPago);
                    }
                }
            }
            for (Devolucion devolucionListNewDevolucion : devolucionListNew) {
                if (!devolucionListOld.contains(devolucionListNewDevolucion)) {
                    Usuario oldUsuarioIdOfDevolucionListNewDevolucion = devolucionListNewDevolucion.getUsuarioId();
                    devolucionListNewDevolucion.setUsuarioId(usuario);
                    devolucionListNewDevolucion = em.merge(devolucionListNewDevolucion);
                    if (oldUsuarioIdOfDevolucionListNewDevolucion != null && !oldUsuarioIdOfDevolucionListNewDevolucion.equals(usuario)) {
                        oldUsuarioIdOfDevolucionListNewDevolucion.getDevolucionList().remove(devolucionListNewDevolucion);
                        oldUsuarioIdOfDevolucionListNewDevolucion = em.merge(oldUsuarioIdOfDevolucionListNewDevolucion);
                    }
                }
            }
            for (Pedido pedidoListNewPedido : pedidoListNew) {
                if (!pedidoListOld.contains(pedidoListNewPedido)) {
                    Usuario oldUsuarioIdOfPedidoListNewPedido = pedidoListNewPedido.getUsuarioId();
                    pedidoListNewPedido.setUsuarioId(usuario);
                    pedidoListNewPedido = em.merge(pedidoListNewPedido);
                    if (oldUsuarioIdOfPedidoListNewPedido != null && !oldUsuarioIdOfPedidoListNewPedido.equals(usuario)) {
                        oldUsuarioIdOfPedidoListNewPedido.getPedidoList().remove(pedidoListNewPedido);
                        oldUsuarioIdOfPedidoListNewPedido = em.merge(oldUsuarioIdOfPedidoListNewPedido);
                    }
                }
            }
            for (Ventas ventasListNewVentas : ventasListNew) {
                if (!ventasListOld.contains(ventasListNewVentas)) {
                    Usuario oldUsuarioIdOfVentasListNewVentas = ventasListNewVentas.getUsuarioId();
                    ventasListNewVentas.setUsuarioId(usuario);
                    ventasListNewVentas = em.merge(ventasListNewVentas);
                    if (oldUsuarioIdOfVentasListNewVentas != null && !oldUsuarioIdOfVentasListNewVentas.equals(usuario)) {
                        oldUsuarioIdOfVentasListNewVentas.getVentasList().remove(ventasListNewVentas);
                        oldUsuarioIdOfVentasListNewVentas = em.merge(oldUsuarioIdOfVentasListNewVentas);
                    }
                }
            }
            for (Caja cajaListNewCaja : cajaListNew) {
                if (!cajaListOld.contains(cajaListNewCaja)) {
                    Usuario oldUsuarioIdOfCajaListNewCaja = cajaListNewCaja.getUsuarioId();
                    cajaListNewCaja.setUsuarioId(usuario);
                    cajaListNewCaja = em.merge(cajaListNewCaja);
                    if (oldUsuarioIdOfCajaListNewCaja != null && !oldUsuarioIdOfCajaListNewCaja.equals(usuario)) {
                        oldUsuarioIdOfCajaListNewCaja.getCajaList().remove(cajaListNewCaja);
                        oldUsuarioIdOfCajaListNewCaja = em.merge(oldUsuarioIdOfCajaListNewCaja);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = usuario.getId();
                if (findUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario;
            try {
                usuario = em.getReference(Usuario.class, id);
                usuario.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Remisionp> remisionpListOrphanCheck = usuario.getRemisionpList();
            for (Remisionp remisionpListOrphanCheckRemisionp : remisionpListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Remisionp " + remisionpListOrphanCheckRemisionp + " in its remisionpList field has a non-nullable usuarioId field.");
            }
            List<Permisos> permisosListOrphanCheck = usuario.getPermisosList();
            for (Permisos permisosListOrphanCheckPermisos : permisosListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Permisos " + permisosListOrphanCheckPermisos + " in its permisosList field has a non-nullable usuarioId field.");
            }
            List<Pago> pagoListOrphanCheck = usuario.getPagoList();
            for (Pago pagoListOrphanCheckPago : pagoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Pago " + pagoListOrphanCheckPago + " in its pagoList field has a non-nullable usuarioId field.");
            }
            List<Devolucion> devolucionListOrphanCheck = usuario.getDevolucionList();
            for (Devolucion devolucionListOrphanCheckDevolucion : devolucionListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Devolucion " + devolucionListOrphanCheckDevolucion + " in its devolucionList field has a non-nullable usuarioId field.");
            }
            List<Pedido> pedidoListOrphanCheck = usuario.getPedidoList();
            for (Pedido pedidoListOrphanCheckPedido : pedidoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Pedido " + pedidoListOrphanCheckPedido + " in its pedidoList field has a non-nullable usuarioId field.");
            }
            List<Ventas> ventasListOrphanCheck = usuario.getVentasList();
            for (Ventas ventasListOrphanCheckVentas : ventasListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Ventas " + ventasListOrphanCheckVentas + " in its ventasList field has a non-nullable usuarioId field.");
            }
            List<Caja> cajaListOrphanCheck = usuario.getCajaList();
            for (Caja cajaListOrphanCheckCaja : cajaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Caja " + cajaListOrphanCheckCaja + " in its cajaList field has a non-nullable usuarioId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Sucursal sucursalId = usuario.getSucursalId();
            if (sucursalId != null) {
                sucursalId.getUsuarioList().remove(usuario);
                sucursalId = em.merge(sucursalId);
            }
            List<Cliente> clienteList = usuario.getClienteList();
            for (Cliente clienteListCliente : clienteList) {
                clienteListCliente.getUsuarioList().remove(usuario);
                clienteListCliente = em.merge(clienteListCliente);
            }
            em.remove(usuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuario> findUsuarioEntities() {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult) {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findUsuario(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuario> rt = cq.from(Usuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        public int logear(String email,String password){
        System.out.println("Checada inicio de sesion");
        EntityManager em = getEntityManager();
        int id=0;
            
        try {
            TypedQuery<Usuario> resultList = em.createQuery("SELECT c FROM Usuario c WHERE c.email=:email AND c.password=:password", Usuario.class);
            resultList.setParameter("email",email);
            resultList.setParameter("password",password);
           
            Usuario usuario=resultList.getSingleResult();
            if(usuario.getTipo().equals("eliminado")){
                
            }else{
                id=usuario.getId();
            }
            
        } finally {
            em.close();
            return id;
        }
    }
    
    public int verificarPass(String password,int idSucursal){
        EntityManager em = getEntityManager();
        int id=0;
            
        try {
            TypedQuery<Permisos> resultList = em.createQuery("SELECT p FROM Permisos p WHERE p.administracionp=1 AND p.usuarioId.password=:password AND (p.usuarioId.sucursalId.id =:idSucursal OR p.usuarioId.id=1) ", Permisos.class);
            resultList.setParameter("password",password);
            resultList.setParameter("idSucursal",idSucursal);
            if(resultList.getResultList().size()>0){
            id=1;
            }
            System.out.println("@@ Numero"+resultList.getResultList().size());
        } finally {
            em.close();
            return id;
        }
    }
    public int verificarEmail(String email){
        EntityManager em = getEntityManager();
        int id=0;
            
        try {
            TypedQuery<Permisos> resultList = em.createQuery("SELECT u FROM Usuario u WHERE u.email=:email ", Permisos.class);
            resultList.setParameter("email",email);
            if(resultList.getResultList().size()>0){
                id=1;
            }
        } finally {
            em.close();
            return id;
        }
    }
    
    public List<Usuario> usuarioSucursal(int idSucursal,int pagina){
        EntityManager em = getEntityManager();
        List<Usuario>usuarios=new ArrayList<Usuario>();
        
        try {
            if(idSucursal==1){
            TypedQuery<Usuario> resultList = em.createQuery("SELECT u FROM Usuario u WHERE u.tipo NOT LIKE 'eliminado' ORDER BY u.nombre ASC", Usuario.class).setFirstResult(pagina*20).setMaxResults(20);
            usuarios = resultList.getResultList();
                
            }else{
                
            TypedQuery<Usuario> resultList = em.createQuery("SELECT u FROM Usuario u WHERE u.sucursalId.id =:idSucursal AND u.tipo NOT LIKE 'eliminado' ORDER BY u.nombre ASC", Usuario.class).setFirstResult(pagina*20).setMaxResults(20);
            resultList.setParameter("idSucursal", idSucursal);
            usuarios = resultList.getResultList();
            }
        } finally {
            em.close();
        }
            return usuarios;
    }
    
    public int usuarioSucursalCuenta(int idSucursal){
        EntityManager em = getEntityManager();
        int numero=0;
        
        try {
            if(idSucursal==1){
            TypedQuery<Usuario> resultList = em.createQuery("SELECT u FROM Usuario u WHERE u.tipo NOT LIKE 'eliminado' ORDER BY u.nombre ASC", Usuario.class);
            numero = resultList.getResultList().size();
            }else{
                
            TypedQuery<Usuario> resultList = em.createQuery("SELECT u FROM Usuario u WHERE u.sucursalId.id =:idSucursal AND u.tipo NOT LIKE 'eliminado' ORDER BY u.nombre ASC", Usuario.class);
            resultList.setParameter("idSucursal", idSucursal);
            numero = resultList.getResultList().size();
            }
        } finally {
            em.close();
        }
            return numero;
    }
    
}
