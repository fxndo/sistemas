/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import Modelo.Pedidoparte;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Pedidoproveedor;
import Modelo.Producto;
import Modelo.Sucursal;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class PedidoparteJpaController implements Serializable {

    public PedidoparteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pedidoparte pedidoparte) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedidoproveedor pedidoProveedorid = pedidoparte.getPedidoProveedorid();
            if (pedidoProveedorid != null) {
                pedidoProveedorid = em.getReference(pedidoProveedorid.getClass(), pedidoProveedorid.getId());
                pedidoparte.setPedidoProveedorid(pedidoProveedorid);
            }
            Producto productoId = pedidoparte.getProductoId();
            if (productoId != null) {
                productoId = em.getReference(productoId.getClass(), productoId.getId());
                pedidoparte.setProductoId(productoId);
            }
            Sucursal sucursalId = pedidoparte.getSucursalId();
            if (sucursalId != null) {
                sucursalId = em.getReference(sucursalId.getClass(), sucursalId.getId());
                pedidoparte.setSucursalId(sucursalId);
            }
            em.persist(pedidoparte);
            if (pedidoProveedorid != null) {
                pedidoProveedorid.getPedidoparteList().add(pedidoparte);
                pedidoProveedorid = em.merge(pedidoProveedorid);
            }
            if (productoId != null) {
                productoId.getPedidoparteList().add(pedidoparte);
                productoId = em.merge(productoId);
            }
            if (sucursalId != null) {
                sucursalId.getPedidoparteList().add(pedidoparte);
                sucursalId = em.merge(sucursalId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pedidoparte pedidoparte) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedidoparte persistentPedidoparte = em.find(Pedidoparte.class, pedidoparte.getId());
            Pedidoproveedor pedidoProveedoridOld = persistentPedidoparte.getPedidoProveedorid();
            Pedidoproveedor pedidoProveedoridNew = pedidoparte.getPedidoProveedorid();
            Producto productoIdOld = persistentPedidoparte.getProductoId();
            Producto productoIdNew = pedidoparte.getProductoId();
            Sucursal sucursalIdOld = persistentPedidoparte.getSucursalId();
            Sucursal sucursalIdNew = pedidoparte.getSucursalId();
            if (pedidoProveedoridNew != null) {
                pedidoProveedoridNew = em.getReference(pedidoProveedoridNew.getClass(), pedidoProveedoridNew.getId());
                pedidoparte.setPedidoProveedorid(pedidoProveedoridNew);
            }
            if (productoIdNew != null) {
                productoIdNew = em.getReference(productoIdNew.getClass(), productoIdNew.getId());
                pedidoparte.setProductoId(productoIdNew);
            }
            if (sucursalIdNew != null) {
                sucursalIdNew = em.getReference(sucursalIdNew.getClass(), sucursalIdNew.getId());
                pedidoparte.setSucursalId(sucursalIdNew);
            }
            pedidoparte = em.merge(pedidoparte);
            if (pedidoProveedoridOld != null && !pedidoProveedoridOld.equals(pedidoProveedoridNew)) {
                pedidoProveedoridOld.getPedidoparteList().remove(pedidoparte);
                pedidoProveedoridOld = em.merge(pedidoProveedoridOld);
            }
            if (pedidoProveedoridNew != null && !pedidoProveedoridNew.equals(pedidoProveedoridOld)) {
                pedidoProveedoridNew.getPedidoparteList().add(pedidoparte);
                pedidoProveedoridNew = em.merge(pedidoProveedoridNew);
            }
            if (productoIdOld != null && !productoIdOld.equals(productoIdNew)) {
                productoIdOld.getPedidoparteList().remove(pedidoparte);
                productoIdOld = em.merge(productoIdOld);
            }
            if (productoIdNew != null && !productoIdNew.equals(productoIdOld)) {
                productoIdNew.getPedidoparteList().add(pedidoparte);
                productoIdNew = em.merge(productoIdNew);
            }
            if (sucursalIdOld != null && !sucursalIdOld.equals(sucursalIdNew)) {
                sucursalIdOld.getPedidoparteList().remove(pedidoparte);
                sucursalIdOld = em.merge(sucursalIdOld);
            }
            if (sucursalIdNew != null && !sucursalIdNew.equals(sucursalIdOld)) {
                sucursalIdNew.getPedidoparteList().add(pedidoparte);
                sucursalIdNew = em.merge(sucursalIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pedidoparte.getId();
                if (findPedidoparte(id) == null) {
                    throw new NonexistentEntityException("The pedidoparte with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedidoparte pedidoparte;
            try {
                pedidoparte = em.getReference(Pedidoparte.class, id);
                pedidoparte.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pedidoparte with id " + id + " no longer exists.", enfe);
            }
            Pedidoproveedor pedidoProveedorid = pedidoparte.getPedidoProveedorid();
            if (pedidoProveedorid != null) {
                pedidoProveedorid.getPedidoparteList().remove(pedidoparte);
                pedidoProveedorid = em.merge(pedidoProveedorid);
            }
            Producto productoId = pedidoparte.getProductoId();
            if (productoId != null) {
                productoId.getPedidoparteList().remove(pedidoparte);
                productoId = em.merge(productoId);
            }
            Sucursal sucursalId = pedidoparte.getSucursalId();
            if (sucursalId != null) {
                sucursalId.getPedidoparteList().remove(pedidoparte);
                sucursalId = em.merge(sucursalId);
            }
            em.remove(pedidoparte);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pedidoparte> findPedidoparteEntities() {
        return findPedidoparteEntities(true, -1, -1);
    }

    public List<Pedidoparte> findPedidoparteEntities(int maxResults, int firstResult) {
        return findPedidoparteEntities(false, maxResults, firstResult);
    }

    private List<Pedidoparte> findPedidoparteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pedidoparte.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pedidoparte findPedidoparte(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pedidoparte.class, id);
        } finally {
            em.close();
        }
    }

    public int getPedidoparteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pedidoparte> rt = cq.from(Pedidoparte.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        public List<Pedidoparte> listaPedidoParte(int idPedidoProveedor) {
        EntityManager em = getEntityManager();
        List<Pedidoparte>pedidoparte=new ArrayList<Pedidoparte>();
        try {

            TypedQuery<Pedidoparte> resultList = em.createQuery("SELECT p FROM Pedidoparte p WHERE p.pedidoProveedorid.id=:idPedidoProveedor", Pedidoparte.class);
            resultList.setParameter("idPedidoProveedor", idPedidoProveedor);
            if (resultList.getResultList().size() > 0) {
                pedidoparte = resultList.getResultList();
            }
        } finally {
            em.close();
        }
        return pedidoparte;
    }
    
    public List<Pedidoparte> listaPedidoParteTipo(int idPedidoProveedor) {
        EntityManager em = getEntityManager();
        List<Pedidoparte>pedidoparte=new ArrayList<Pedidoparte>();
        try {

            TypedQuery<Pedidoparte> resultList = em.createQuery("SELECT p FROM Pedidoparte p WHERE p.pedidoProveedorid.id=:idPedidoProveedor ORDER BY p.tipo,p.productoId.titulo ASC", Pedidoparte.class);
            resultList.setParameter("idPedidoProveedor", idPedidoProveedor);
            if (resultList.getResultList().size() > 0) {
                pedidoparte = resultList.getResultList();
            }
        } finally {
            em.close();
        }
        return pedidoparte;
    }
    
    public List<Pedidoparte> traslados(int idSucursal) {
        EntityManager em = getEntityManager();
        List<Pedidoparte>pedidoparte=new ArrayList<Pedidoparte>();
        try {
            String query="SELECT p FROM Pedidoparte p WHERE p.sucursalId.id=:idSucursal AND p.estado=2 AND p.pedidoProveedorid.pedidoId.estado=3";
            if(idSucursal==1){
                query="SELECT p FROM Pedidoparte p WHERE p.sucursalId.id IS NOT NULL AND p.estado=2 AND p.pedidoProveedorid.pedidoId.estado=3";
            }
            TypedQuery<Pedidoparte> resultList = em.createQuery(query, Pedidoparte.class);
            if(idSucursal!=1){
                resultList.setParameter("idSucursal", idSucursal);
            }
            if (resultList.getResultList().size() > 0) {
                pedidoparte = resultList.getResultList();
            }
        } finally {
            em.close();
        }
        return pedidoparte;
    }
    
}
