/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import Modelo.Devolucion;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Usuario;
import Modelo.Pedido;
import java.util.ArrayList;
import java.util.List;
import Modelo.Ventas;
import ModeloJSP.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author fernando
 */
public class DevolucionJpaController implements Serializable {

    public DevolucionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Devolucion devolucion) {
        if (devolucion.getPedidoList() == null) {
            devolucion.setPedidoList(new ArrayList<Pedido>());
        }
        if (devolucion.getVentasList() == null) {
            devolucion.setVentasList(new ArrayList<Ventas>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuarioId = devolucion.getUsuarioId();
            if (usuarioId != null) {
                usuarioId = em.getReference(usuarioId.getClass(), usuarioId.getId());
                devolucion.setUsuarioId(usuarioId);
            }
            List<Pedido> attachedPedidoList = new ArrayList<Pedido>();
            for (Pedido pedidoListPedidoToAttach : devolucion.getPedidoList()) {
                pedidoListPedidoToAttach = em.getReference(pedidoListPedidoToAttach.getClass(), pedidoListPedidoToAttach.getId());
                attachedPedidoList.add(pedidoListPedidoToAttach);
            }
            devolucion.setPedidoList(attachedPedidoList);
            List<Ventas> attachedVentasList = new ArrayList<Ventas>();
            for (Ventas ventasListVentasToAttach : devolucion.getVentasList()) {
                ventasListVentasToAttach = em.getReference(ventasListVentasToAttach.getClass(), ventasListVentasToAttach.getId());
                attachedVentasList.add(ventasListVentasToAttach);
            }
            devolucion.setVentasList(attachedVentasList);
            em.persist(devolucion);
            if (usuarioId != null) {
                usuarioId.getDevolucionList().add(devolucion);
                usuarioId = em.merge(usuarioId);
            }
            for (Pedido pedidoListPedido : devolucion.getPedidoList()) {
                Devolucion oldDevolucionIdOfPedidoListPedido = pedidoListPedido.getDevolucionId();
                pedidoListPedido.setDevolucionId(devolucion);
                pedidoListPedido = em.merge(pedidoListPedido);
                if (oldDevolucionIdOfPedidoListPedido != null) {
                    oldDevolucionIdOfPedidoListPedido.getPedidoList().remove(pedidoListPedido);
                    oldDevolucionIdOfPedidoListPedido = em.merge(oldDevolucionIdOfPedidoListPedido);
                }
            }
            for (Ventas ventasListVentas : devolucion.getVentasList()) {
                Devolucion oldDevolucionIdOfVentasListVentas = ventasListVentas.getDevolucionId();
                ventasListVentas.setDevolucionId(devolucion);
                ventasListVentas = em.merge(ventasListVentas);
                if (oldDevolucionIdOfVentasListVentas != null) {
                    oldDevolucionIdOfVentasListVentas.getVentasList().remove(ventasListVentas);
                    oldDevolucionIdOfVentasListVentas = em.merge(oldDevolucionIdOfVentasListVentas);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Devolucion devolucion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Devolucion persistentDevolucion = em.find(Devolucion.class, devolucion.getId());
            Usuario usuarioIdOld = persistentDevolucion.getUsuarioId();
            Usuario usuarioIdNew = devolucion.getUsuarioId();
            List<Pedido> pedidoListOld = persistentDevolucion.getPedidoList();
            List<Pedido> pedidoListNew = devolucion.getPedidoList();
            List<Ventas> ventasListOld = persistentDevolucion.getVentasList();
            List<Ventas> ventasListNew = devolucion.getVentasList();
            if (usuarioIdNew != null) {
                usuarioIdNew = em.getReference(usuarioIdNew.getClass(), usuarioIdNew.getId());
                devolucion.setUsuarioId(usuarioIdNew);
            }
            List<Pedido> attachedPedidoListNew = new ArrayList<Pedido>();
            for (Pedido pedidoListNewPedidoToAttach : pedidoListNew) {
                pedidoListNewPedidoToAttach = em.getReference(pedidoListNewPedidoToAttach.getClass(), pedidoListNewPedidoToAttach.getId());
                attachedPedidoListNew.add(pedidoListNewPedidoToAttach);
            }
            pedidoListNew = attachedPedidoListNew;
            devolucion.setPedidoList(pedidoListNew);
            List<Ventas> attachedVentasListNew = new ArrayList<Ventas>();
            for (Ventas ventasListNewVentasToAttach : ventasListNew) {
                ventasListNewVentasToAttach = em.getReference(ventasListNewVentasToAttach.getClass(), ventasListNewVentasToAttach.getId());
                attachedVentasListNew.add(ventasListNewVentasToAttach);
            }
            ventasListNew = attachedVentasListNew;
            devolucion.setVentasList(ventasListNew);
            devolucion = em.merge(devolucion);
            if (usuarioIdOld != null && !usuarioIdOld.equals(usuarioIdNew)) {
                usuarioIdOld.getDevolucionList().remove(devolucion);
                usuarioIdOld = em.merge(usuarioIdOld);
            }
            if (usuarioIdNew != null && !usuarioIdNew.equals(usuarioIdOld)) {
                usuarioIdNew.getDevolucionList().add(devolucion);
                usuarioIdNew = em.merge(usuarioIdNew);
            }
            for (Pedido pedidoListOldPedido : pedidoListOld) {
                if (!pedidoListNew.contains(pedidoListOldPedido)) {
                    pedidoListOldPedido.setDevolucionId(null);
                    pedidoListOldPedido = em.merge(pedidoListOldPedido);
                }
            }
            for (Pedido pedidoListNewPedido : pedidoListNew) {
                if (!pedidoListOld.contains(pedidoListNewPedido)) {
                    Devolucion oldDevolucionIdOfPedidoListNewPedido = pedidoListNewPedido.getDevolucionId();
                    pedidoListNewPedido.setDevolucionId(devolucion);
                    pedidoListNewPedido = em.merge(pedidoListNewPedido);
                    if (oldDevolucionIdOfPedidoListNewPedido != null && !oldDevolucionIdOfPedidoListNewPedido.equals(devolucion)) {
                        oldDevolucionIdOfPedidoListNewPedido.getPedidoList().remove(pedidoListNewPedido);
                        oldDevolucionIdOfPedidoListNewPedido = em.merge(oldDevolucionIdOfPedidoListNewPedido);
                    }
                }
            }
            for (Ventas ventasListOldVentas : ventasListOld) {
                if (!ventasListNew.contains(ventasListOldVentas)) {
                    ventasListOldVentas.setDevolucionId(null);
                    ventasListOldVentas = em.merge(ventasListOldVentas);
                }
            }
            for (Ventas ventasListNewVentas : ventasListNew) {
                if (!ventasListOld.contains(ventasListNewVentas)) {
                    Devolucion oldDevolucionIdOfVentasListNewVentas = ventasListNewVentas.getDevolucionId();
                    ventasListNewVentas.setDevolucionId(devolucion);
                    ventasListNewVentas = em.merge(ventasListNewVentas);
                    if (oldDevolucionIdOfVentasListNewVentas != null && !oldDevolucionIdOfVentasListNewVentas.equals(devolucion)) {
                        oldDevolucionIdOfVentasListNewVentas.getVentasList().remove(ventasListNewVentas);
                        oldDevolucionIdOfVentasListNewVentas = em.merge(oldDevolucionIdOfVentasListNewVentas);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = devolucion.getId();
                if (findDevolucion(id) == null) {
                    throw new NonexistentEntityException("The devolucion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Devolucion devolucion;
            try {
                devolucion = em.getReference(Devolucion.class, id);
                devolucion.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The devolucion with id " + id + " no longer exists.", enfe);
            }
            Usuario usuarioId = devolucion.getUsuarioId();
            if (usuarioId != null) {
                usuarioId.getDevolucionList().remove(devolucion);
                usuarioId = em.merge(usuarioId);
            }
            List<Pedido> pedidoList = devolucion.getPedidoList();
            for (Pedido pedidoListPedido : pedidoList) {
                pedidoListPedido.setDevolucionId(null);
                pedidoListPedido = em.merge(pedidoListPedido);
            }
            List<Ventas> ventasList = devolucion.getVentasList();
            for (Ventas ventasListVentas : ventasList) {
                ventasListVentas.setDevolucionId(null);
                ventasListVentas = em.merge(ventasListVentas);
            }
            em.remove(devolucion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Devolucion> findDevolucionEntities() {
        return findDevolucionEntities(true, -1, -1);
    }

    public List<Devolucion> findDevolucionEntities(int maxResults, int firstResult) {
        return findDevolucionEntities(false, maxResults, firstResult);
    }

    private List<Devolucion> findDevolucionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Devolucion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Devolucion findDevolucion(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Devolucion.class, id);
        } finally {
            em.close();
        }
    }

    public int getDevolucionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Devolucion> rt = cq.from(Devolucion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
