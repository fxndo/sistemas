/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Direccion;
import Modelo.Pedidoparte;
import java.util.ArrayList;
import java.util.List;
import Modelo.VentasHasProducto;
import Modelo.Almacen;
import Modelo.Pedido;
import Modelo.Ventas;
import Modelo.Usuario;
import Modelo.Caja;
import Modelo.Sucursal;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class SucursalJpaController implements Serializable {

    public SucursalJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sucursal sucursal) {
        if (sucursal.getPedidoparteList() == null) {
            sucursal.setPedidoparteList(new ArrayList<Pedidoparte>());
        }
        if (sucursal.getVentasHasProductoList() == null) {
            sucursal.setVentasHasProductoList(new ArrayList<VentasHasProducto>());
        }
        if (sucursal.getAlmacenList() == null) {
            sucursal.setAlmacenList(new ArrayList<Almacen>());
        }
        if (sucursal.getPedidoList() == null) {
            sucursal.setPedidoList(new ArrayList<Pedido>());
        }
        if (sucursal.getVentasList() == null) {
            sucursal.setVentasList(new ArrayList<Ventas>());
        }
        if (sucursal.getUsuarioList() == null) {
            sucursal.setUsuarioList(new ArrayList<Usuario>());
        }
        if (sucursal.getCajaList() == null) {
            sucursal.setCajaList(new ArrayList<Caja>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Direccion direccionId = sucursal.getDireccionId();
            if (direccionId != null) {
                direccionId = em.getReference(direccionId.getClass(), direccionId.getId());
                sucursal.setDireccionId(direccionId);
            }
            List<Pedidoparte> attachedPedidoparteList = new ArrayList<Pedidoparte>();
            for (Pedidoparte pedidoparteListPedidoparteToAttach : sucursal.getPedidoparteList()) {
                pedidoparteListPedidoparteToAttach = em.getReference(pedidoparteListPedidoparteToAttach.getClass(), pedidoparteListPedidoparteToAttach.getId());
                attachedPedidoparteList.add(pedidoparteListPedidoparteToAttach);
            }
            sucursal.setPedidoparteList(attachedPedidoparteList);
            List<VentasHasProducto> attachedVentasHasProductoList = new ArrayList<VentasHasProducto>();
            for (VentasHasProducto ventasHasProductoListVentasHasProductoToAttach : sucursal.getVentasHasProductoList()) {
                ventasHasProductoListVentasHasProductoToAttach = em.getReference(ventasHasProductoListVentasHasProductoToAttach.getClass(), ventasHasProductoListVentasHasProductoToAttach.getVentasHasProductoPK());
                attachedVentasHasProductoList.add(ventasHasProductoListVentasHasProductoToAttach);
            }
            sucursal.setVentasHasProductoList(attachedVentasHasProductoList);
            List<Almacen> attachedAlmacenList = new ArrayList<Almacen>();
            for (Almacen almacenListAlmacenToAttach : sucursal.getAlmacenList()) {
                almacenListAlmacenToAttach = em.getReference(almacenListAlmacenToAttach.getClass(), almacenListAlmacenToAttach.getId());
                attachedAlmacenList.add(almacenListAlmacenToAttach);
            }
            sucursal.setAlmacenList(attachedAlmacenList);
            List<Pedido> attachedPedidoList = new ArrayList<Pedido>();
            for (Pedido pedidoListPedidoToAttach : sucursal.getPedidoList()) {
                pedidoListPedidoToAttach = em.getReference(pedidoListPedidoToAttach.getClass(), pedidoListPedidoToAttach.getId());
                attachedPedidoList.add(pedidoListPedidoToAttach);
            }
            sucursal.setPedidoList(attachedPedidoList);
            List<Ventas> attachedVentasList = new ArrayList<Ventas>();
            for (Ventas ventasListVentasToAttach : sucursal.getVentasList()) {
                ventasListVentasToAttach = em.getReference(ventasListVentasToAttach.getClass(), ventasListVentasToAttach.getId());
                attachedVentasList.add(ventasListVentasToAttach);
            }
            sucursal.setVentasList(attachedVentasList);
            List<Usuario> attachedUsuarioList = new ArrayList<Usuario>();
            for (Usuario usuarioListUsuarioToAttach : sucursal.getUsuarioList()) {
                usuarioListUsuarioToAttach = em.getReference(usuarioListUsuarioToAttach.getClass(), usuarioListUsuarioToAttach.getId());
                attachedUsuarioList.add(usuarioListUsuarioToAttach);
            }
            sucursal.setUsuarioList(attachedUsuarioList);
            List<Caja> attachedCajaList = new ArrayList<Caja>();
            for (Caja cajaListCajaToAttach : sucursal.getCajaList()) {
                cajaListCajaToAttach = em.getReference(cajaListCajaToAttach.getClass(), cajaListCajaToAttach.getId());
                attachedCajaList.add(cajaListCajaToAttach);
            }
            sucursal.setCajaList(attachedCajaList);
            em.persist(sucursal);
            if (direccionId != null) {
                direccionId.getSucursalList().add(sucursal);
                direccionId = em.merge(direccionId);
            }
            for (Pedidoparte pedidoparteListPedidoparte : sucursal.getPedidoparteList()) {
                Sucursal oldSucursalIdOfPedidoparteListPedidoparte = pedidoparteListPedidoparte.getSucursalId();
                pedidoparteListPedidoparte.setSucursalId(sucursal);
                pedidoparteListPedidoparte = em.merge(pedidoparteListPedidoparte);
                if (oldSucursalIdOfPedidoparteListPedidoparte != null) {
                    oldSucursalIdOfPedidoparteListPedidoparte.getPedidoparteList().remove(pedidoparteListPedidoparte);
                    oldSucursalIdOfPedidoparteListPedidoparte = em.merge(oldSucursalIdOfPedidoparteListPedidoparte);
                }
            }
            for (VentasHasProducto ventasHasProductoListVentasHasProducto : sucursal.getVentasHasProductoList()) {
                Sucursal oldSucursalIdOfVentasHasProductoListVentasHasProducto = ventasHasProductoListVentasHasProducto.getSucursalId();
                ventasHasProductoListVentasHasProducto.setSucursalId(sucursal);
                ventasHasProductoListVentasHasProducto = em.merge(ventasHasProductoListVentasHasProducto);
                if (oldSucursalIdOfVentasHasProductoListVentasHasProducto != null) {
                    oldSucursalIdOfVentasHasProductoListVentasHasProducto.getVentasHasProductoList().remove(ventasHasProductoListVentasHasProducto);
                    oldSucursalIdOfVentasHasProductoListVentasHasProducto = em.merge(oldSucursalIdOfVentasHasProductoListVentasHasProducto);
                }
            }
            for (Almacen almacenListAlmacen : sucursal.getAlmacenList()) {
                Sucursal oldSucursalIdOfAlmacenListAlmacen = almacenListAlmacen.getSucursalId();
                almacenListAlmacen.setSucursalId(sucursal);
                almacenListAlmacen = em.merge(almacenListAlmacen);
                if (oldSucursalIdOfAlmacenListAlmacen != null) {
                    oldSucursalIdOfAlmacenListAlmacen.getAlmacenList().remove(almacenListAlmacen);
                    oldSucursalIdOfAlmacenListAlmacen = em.merge(oldSucursalIdOfAlmacenListAlmacen);
                }
            }
            for (Pedido pedidoListPedido : sucursal.getPedidoList()) {
                Sucursal oldSucursalIdOfPedidoListPedido = pedidoListPedido.getSucursalId();
                pedidoListPedido.setSucursalId(sucursal);
                pedidoListPedido = em.merge(pedidoListPedido);
                if (oldSucursalIdOfPedidoListPedido != null) {
                    oldSucursalIdOfPedidoListPedido.getPedidoList().remove(pedidoListPedido);
                    oldSucursalIdOfPedidoListPedido = em.merge(oldSucursalIdOfPedidoListPedido);
                }
            }
            for (Ventas ventasListVentas : sucursal.getVentasList()) {
                Sucursal oldSucursalIdOfVentasListVentas = ventasListVentas.getSucursalId();
                ventasListVentas.setSucursalId(sucursal);
                ventasListVentas = em.merge(ventasListVentas);
                if (oldSucursalIdOfVentasListVentas != null) {
                    oldSucursalIdOfVentasListVentas.getVentasList().remove(ventasListVentas);
                    oldSucursalIdOfVentasListVentas = em.merge(oldSucursalIdOfVentasListVentas);
                }
            }
            for (Usuario usuarioListUsuario : sucursal.getUsuarioList()) {
                Sucursal oldSucursalIdOfUsuarioListUsuario = usuarioListUsuario.getSucursalId();
                usuarioListUsuario.setSucursalId(sucursal);
                usuarioListUsuario = em.merge(usuarioListUsuario);
                if (oldSucursalIdOfUsuarioListUsuario != null) {
                    oldSucursalIdOfUsuarioListUsuario.getUsuarioList().remove(usuarioListUsuario);
                    oldSucursalIdOfUsuarioListUsuario = em.merge(oldSucursalIdOfUsuarioListUsuario);
                }
            }
            for (Caja cajaListCaja : sucursal.getCajaList()) {
                Sucursal oldSucursalIdOfCajaListCaja = cajaListCaja.getSucursalId();
                cajaListCaja.setSucursalId(sucursal);
                cajaListCaja = em.merge(cajaListCaja);
                if (oldSucursalIdOfCajaListCaja != null) {
                    oldSucursalIdOfCajaListCaja.getCajaList().remove(cajaListCaja);
                    oldSucursalIdOfCajaListCaja = em.merge(oldSucursalIdOfCajaListCaja);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sucursal sucursal) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sucursal persistentSucursal = em.find(Sucursal.class, sucursal.getId());
            Direccion direccionIdOld = persistentSucursal.getDireccionId();
            Direccion direccionIdNew = sucursal.getDireccionId();
            List<Pedidoparte> pedidoparteListOld = persistentSucursal.getPedidoparteList();
            List<Pedidoparte> pedidoparteListNew = sucursal.getPedidoparteList();
            List<VentasHasProducto> ventasHasProductoListOld = persistentSucursal.getVentasHasProductoList();
            List<VentasHasProducto> ventasHasProductoListNew = sucursal.getVentasHasProductoList();
            List<Almacen> almacenListOld = persistentSucursal.getAlmacenList();
            List<Almacen> almacenListNew = sucursal.getAlmacenList();
            List<Pedido> pedidoListOld = persistentSucursal.getPedidoList();
            List<Pedido> pedidoListNew = sucursal.getPedidoList();
            List<Ventas> ventasListOld = persistentSucursal.getVentasList();
            List<Ventas> ventasListNew = sucursal.getVentasList();
            List<Usuario> usuarioListOld = persistentSucursal.getUsuarioList();
            List<Usuario> usuarioListNew = sucursal.getUsuarioList();
            List<Caja> cajaListOld = persistentSucursal.getCajaList();
            List<Caja> cajaListNew = sucursal.getCajaList();
            List<String> illegalOrphanMessages = null;
            for (Almacen almacenListOldAlmacen : almacenListOld) {
                if (!almacenListNew.contains(almacenListOldAlmacen)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Almacen " + almacenListOldAlmacen + " since its sucursalId field is not nullable.");
                }
            }
            for (Pedido pedidoListOldPedido : pedidoListOld) {
                if (!pedidoListNew.contains(pedidoListOldPedido)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pedido " + pedidoListOldPedido + " since its sucursalId field is not nullable.");
                }
            }
            for (Ventas ventasListOldVentas : ventasListOld) {
                if (!ventasListNew.contains(ventasListOldVentas)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ventas " + ventasListOldVentas + " since its sucursalId field is not nullable.");
                }
            }
            for (Caja cajaListOldCaja : cajaListOld) {
                if (!cajaListNew.contains(cajaListOldCaja)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Caja " + cajaListOldCaja + " since its sucursalId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (direccionIdNew != null) {
                direccionIdNew = em.getReference(direccionIdNew.getClass(), direccionIdNew.getId());
                sucursal.setDireccionId(direccionIdNew);
            }
            List<Pedidoparte> attachedPedidoparteListNew = new ArrayList<Pedidoparte>();
            for (Pedidoparte pedidoparteListNewPedidoparteToAttach : pedidoparteListNew) {
                pedidoparteListNewPedidoparteToAttach = em.getReference(pedidoparteListNewPedidoparteToAttach.getClass(), pedidoparteListNewPedidoparteToAttach.getId());
                attachedPedidoparteListNew.add(pedidoparteListNewPedidoparteToAttach);
            }
            pedidoparteListNew = attachedPedidoparteListNew;
            sucursal.setPedidoparteList(pedidoparteListNew);
            List<VentasHasProducto> attachedVentasHasProductoListNew = new ArrayList<VentasHasProducto>();
            for (VentasHasProducto ventasHasProductoListNewVentasHasProductoToAttach : ventasHasProductoListNew) {
                ventasHasProductoListNewVentasHasProductoToAttach = em.getReference(ventasHasProductoListNewVentasHasProductoToAttach.getClass(), ventasHasProductoListNewVentasHasProductoToAttach.getVentasHasProductoPK());
                attachedVentasHasProductoListNew.add(ventasHasProductoListNewVentasHasProductoToAttach);
            }
            ventasHasProductoListNew = attachedVentasHasProductoListNew;
            sucursal.setVentasHasProductoList(ventasHasProductoListNew);
            List<Almacen> attachedAlmacenListNew = new ArrayList<Almacen>();
            for (Almacen almacenListNewAlmacenToAttach : almacenListNew) {
                almacenListNewAlmacenToAttach = em.getReference(almacenListNewAlmacenToAttach.getClass(), almacenListNewAlmacenToAttach.getId());
                attachedAlmacenListNew.add(almacenListNewAlmacenToAttach);
            }
            almacenListNew = attachedAlmacenListNew;
            sucursal.setAlmacenList(almacenListNew);
            List<Pedido> attachedPedidoListNew = new ArrayList<Pedido>();
            for (Pedido pedidoListNewPedidoToAttach : pedidoListNew) {
                pedidoListNewPedidoToAttach = em.getReference(pedidoListNewPedidoToAttach.getClass(), pedidoListNewPedidoToAttach.getId());
                attachedPedidoListNew.add(pedidoListNewPedidoToAttach);
            }
            pedidoListNew = attachedPedidoListNew;
            sucursal.setPedidoList(pedidoListNew);
            List<Ventas> attachedVentasListNew = new ArrayList<Ventas>();
            for (Ventas ventasListNewVentasToAttach : ventasListNew) {
                ventasListNewVentasToAttach = em.getReference(ventasListNewVentasToAttach.getClass(), ventasListNewVentasToAttach.getId());
                attachedVentasListNew.add(ventasListNewVentasToAttach);
            }
            ventasListNew = attachedVentasListNew;
            sucursal.setVentasList(ventasListNew);
            List<Usuario> attachedUsuarioListNew = new ArrayList<Usuario>();
            for (Usuario usuarioListNewUsuarioToAttach : usuarioListNew) {
                usuarioListNewUsuarioToAttach = em.getReference(usuarioListNewUsuarioToAttach.getClass(), usuarioListNewUsuarioToAttach.getId());
                attachedUsuarioListNew.add(usuarioListNewUsuarioToAttach);
            }
            usuarioListNew = attachedUsuarioListNew;
            sucursal.setUsuarioList(usuarioListNew);
            List<Caja> attachedCajaListNew = new ArrayList<Caja>();
            for (Caja cajaListNewCajaToAttach : cajaListNew) {
                cajaListNewCajaToAttach = em.getReference(cajaListNewCajaToAttach.getClass(), cajaListNewCajaToAttach.getId());
                attachedCajaListNew.add(cajaListNewCajaToAttach);
            }
            cajaListNew = attachedCajaListNew;
            sucursal.setCajaList(cajaListNew);
            sucursal = em.merge(sucursal);
            if (direccionIdOld != null && !direccionIdOld.equals(direccionIdNew)) {
                direccionIdOld.getSucursalList().remove(sucursal);
                direccionIdOld = em.merge(direccionIdOld);
            }
            if (direccionIdNew != null && !direccionIdNew.equals(direccionIdOld)) {
                direccionIdNew.getSucursalList().add(sucursal);
                direccionIdNew = em.merge(direccionIdNew);
            }
            for (Pedidoparte pedidoparteListOldPedidoparte : pedidoparteListOld) {
                if (!pedidoparteListNew.contains(pedidoparteListOldPedidoparte)) {
                    pedidoparteListOldPedidoparte.setSucursalId(null);
                    pedidoparteListOldPedidoparte = em.merge(pedidoparteListOldPedidoparte);
                }
            }
            for (Pedidoparte pedidoparteListNewPedidoparte : pedidoparteListNew) {
                if (!pedidoparteListOld.contains(pedidoparteListNewPedidoparte)) {
                    Sucursal oldSucursalIdOfPedidoparteListNewPedidoparte = pedidoparteListNewPedidoparte.getSucursalId();
                    pedidoparteListNewPedidoparte.setSucursalId(sucursal);
                    pedidoparteListNewPedidoparte = em.merge(pedidoparteListNewPedidoparte);
                    if (oldSucursalIdOfPedidoparteListNewPedidoparte != null && !oldSucursalIdOfPedidoparteListNewPedidoparte.equals(sucursal)) {
                        oldSucursalIdOfPedidoparteListNewPedidoparte.getPedidoparteList().remove(pedidoparteListNewPedidoparte);
                        oldSucursalIdOfPedidoparteListNewPedidoparte = em.merge(oldSucursalIdOfPedidoparteListNewPedidoparte);
                    }
                }
            }
            for (VentasHasProducto ventasHasProductoListOldVentasHasProducto : ventasHasProductoListOld) {
                if (!ventasHasProductoListNew.contains(ventasHasProductoListOldVentasHasProducto)) {
                    ventasHasProductoListOldVentasHasProducto.setSucursalId(null);
                    ventasHasProductoListOldVentasHasProducto = em.merge(ventasHasProductoListOldVentasHasProducto);
                }
            }
            for (VentasHasProducto ventasHasProductoListNewVentasHasProducto : ventasHasProductoListNew) {
                if (!ventasHasProductoListOld.contains(ventasHasProductoListNewVentasHasProducto)) {
                    Sucursal oldSucursalIdOfVentasHasProductoListNewVentasHasProducto = ventasHasProductoListNewVentasHasProducto.getSucursalId();
                    ventasHasProductoListNewVentasHasProducto.setSucursalId(sucursal);
                    ventasHasProductoListNewVentasHasProducto = em.merge(ventasHasProductoListNewVentasHasProducto);
                    if (oldSucursalIdOfVentasHasProductoListNewVentasHasProducto != null && !oldSucursalIdOfVentasHasProductoListNewVentasHasProducto.equals(sucursal)) {
                        oldSucursalIdOfVentasHasProductoListNewVentasHasProducto.getVentasHasProductoList().remove(ventasHasProductoListNewVentasHasProducto);
                        oldSucursalIdOfVentasHasProductoListNewVentasHasProducto = em.merge(oldSucursalIdOfVentasHasProductoListNewVentasHasProducto);
                    }
                }
            }
            for (Almacen almacenListNewAlmacen : almacenListNew) {
                if (!almacenListOld.contains(almacenListNewAlmacen)) {
                    Sucursal oldSucursalIdOfAlmacenListNewAlmacen = almacenListNewAlmacen.getSucursalId();
                    almacenListNewAlmacen.setSucursalId(sucursal);
                    almacenListNewAlmacen = em.merge(almacenListNewAlmacen);
                    if (oldSucursalIdOfAlmacenListNewAlmacen != null && !oldSucursalIdOfAlmacenListNewAlmacen.equals(sucursal)) {
                        oldSucursalIdOfAlmacenListNewAlmacen.getAlmacenList().remove(almacenListNewAlmacen);
                        oldSucursalIdOfAlmacenListNewAlmacen = em.merge(oldSucursalIdOfAlmacenListNewAlmacen);
                    }
                }
            }
            for (Pedido pedidoListNewPedido : pedidoListNew) {
                if (!pedidoListOld.contains(pedidoListNewPedido)) {
                    Sucursal oldSucursalIdOfPedidoListNewPedido = pedidoListNewPedido.getSucursalId();
                    pedidoListNewPedido.setSucursalId(sucursal);
                    pedidoListNewPedido = em.merge(pedidoListNewPedido);
                    if (oldSucursalIdOfPedidoListNewPedido != null && !oldSucursalIdOfPedidoListNewPedido.equals(sucursal)) {
                        oldSucursalIdOfPedidoListNewPedido.getPedidoList().remove(pedidoListNewPedido);
                        oldSucursalIdOfPedidoListNewPedido = em.merge(oldSucursalIdOfPedidoListNewPedido);
                    }
                }
            }
            for (Ventas ventasListNewVentas : ventasListNew) {
                if (!ventasListOld.contains(ventasListNewVentas)) {
                    Sucursal oldSucursalIdOfVentasListNewVentas = ventasListNewVentas.getSucursalId();
                    ventasListNewVentas.setSucursalId(sucursal);
                    ventasListNewVentas = em.merge(ventasListNewVentas);
                    if (oldSucursalIdOfVentasListNewVentas != null && !oldSucursalIdOfVentasListNewVentas.equals(sucursal)) {
                        oldSucursalIdOfVentasListNewVentas.getVentasList().remove(ventasListNewVentas);
                        oldSucursalIdOfVentasListNewVentas = em.merge(oldSucursalIdOfVentasListNewVentas);
                    }
                }
            }
            for (Usuario usuarioListOldUsuario : usuarioListOld) {
                if (!usuarioListNew.contains(usuarioListOldUsuario)) {
                    usuarioListOldUsuario.setSucursalId(null);
                    usuarioListOldUsuario = em.merge(usuarioListOldUsuario);
                }
            }
            for (Usuario usuarioListNewUsuario : usuarioListNew) {
                if (!usuarioListOld.contains(usuarioListNewUsuario)) {
                    Sucursal oldSucursalIdOfUsuarioListNewUsuario = usuarioListNewUsuario.getSucursalId();
                    usuarioListNewUsuario.setSucursalId(sucursal);
                    usuarioListNewUsuario = em.merge(usuarioListNewUsuario);
                    if (oldSucursalIdOfUsuarioListNewUsuario != null && !oldSucursalIdOfUsuarioListNewUsuario.equals(sucursal)) {
                        oldSucursalIdOfUsuarioListNewUsuario.getUsuarioList().remove(usuarioListNewUsuario);
                        oldSucursalIdOfUsuarioListNewUsuario = em.merge(oldSucursalIdOfUsuarioListNewUsuario);
                    }
                }
            }
            for (Caja cajaListNewCaja : cajaListNew) {
                if (!cajaListOld.contains(cajaListNewCaja)) {
                    Sucursal oldSucursalIdOfCajaListNewCaja = cajaListNewCaja.getSucursalId();
                    cajaListNewCaja.setSucursalId(sucursal);
                    cajaListNewCaja = em.merge(cajaListNewCaja);
                    if (oldSucursalIdOfCajaListNewCaja != null && !oldSucursalIdOfCajaListNewCaja.equals(sucursal)) {
                        oldSucursalIdOfCajaListNewCaja.getCajaList().remove(cajaListNewCaja);
                        oldSucursalIdOfCajaListNewCaja = em.merge(oldSucursalIdOfCajaListNewCaja);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sucursal.getId();
                if (findSucursal(id) == null) {
                    throw new NonexistentEntityException("The sucursal with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sucursal sucursal;
            try {
                sucursal = em.getReference(Sucursal.class, id);
                sucursal.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sucursal with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Almacen> almacenListOrphanCheck = sucursal.getAlmacenList();
            for (Almacen almacenListOrphanCheckAlmacen : almacenListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sucursal (" + sucursal + ") cannot be destroyed since the Almacen " + almacenListOrphanCheckAlmacen + " in its almacenList field has a non-nullable sucursalId field.");
            }
            List<Pedido> pedidoListOrphanCheck = sucursal.getPedidoList();
            for (Pedido pedidoListOrphanCheckPedido : pedidoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sucursal (" + sucursal + ") cannot be destroyed since the Pedido " + pedidoListOrphanCheckPedido + " in its pedidoList field has a non-nullable sucursalId field.");
            }
            List<Ventas> ventasListOrphanCheck = sucursal.getVentasList();
            for (Ventas ventasListOrphanCheckVentas : ventasListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sucursal (" + sucursal + ") cannot be destroyed since the Ventas " + ventasListOrphanCheckVentas + " in its ventasList field has a non-nullable sucursalId field.");
            }
            List<Caja> cajaListOrphanCheck = sucursal.getCajaList();
            for (Caja cajaListOrphanCheckCaja : cajaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sucursal (" + sucursal + ") cannot be destroyed since the Caja " + cajaListOrphanCheckCaja + " in its cajaList field has a non-nullable sucursalId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Direccion direccionId = sucursal.getDireccionId();
            if (direccionId != null) {
                direccionId.getSucursalList().remove(sucursal);
                direccionId = em.merge(direccionId);
            }
            List<Pedidoparte> pedidoparteList = sucursal.getPedidoparteList();
            for (Pedidoparte pedidoparteListPedidoparte : pedidoparteList) {
                pedidoparteListPedidoparte.setSucursalId(null);
                pedidoparteListPedidoparte = em.merge(pedidoparteListPedidoparte);
            }
            List<VentasHasProducto> ventasHasProductoList = sucursal.getVentasHasProductoList();
            for (VentasHasProducto ventasHasProductoListVentasHasProducto : ventasHasProductoList) {
                ventasHasProductoListVentasHasProducto.setSucursalId(null);
                ventasHasProductoListVentasHasProducto = em.merge(ventasHasProductoListVentasHasProducto);
            }
            List<Usuario> usuarioList = sucursal.getUsuarioList();
            for (Usuario usuarioListUsuario : usuarioList) {
                usuarioListUsuario.setSucursalId(null);
                usuarioListUsuario = em.merge(usuarioListUsuario);
            }
            em.remove(sucursal);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sucursal> findSucursalEntities() {
        return findSucursalEntities(true, -1, -1);
    }

    public List<Sucursal> findSucursalEntities(int maxResults, int firstResult) {
        return findSucursalEntities(false, maxResults, firstResult);
    }

    private List<Sucursal> findSucursalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sucursal.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sucursal findSucursal(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sucursal.class, id);
        } finally {
            em.close();
        }
    }

    public int getSucursalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sucursal> rt = cq.from(Sucursal.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
         public List<Sucursal> listaSucursales(){
        EntityManager em = getEntityManager();
        List<Sucursal>proveedores=new ArrayList<Sucursal>();
        try {
            
            TypedQuery<Sucursal> resultList = em.createQuery("SELECT s FROM Sucursal s WHERE s.nombre NOT LIKE 'Eliminado' ORDER BY s.nombre ASC ", Sucursal.class);
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
}
