/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import Modelo.Cliente;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Direccion;
import Modelo.Usuario;
import java.util.ArrayList;
import java.util.List;
import Modelo.Pedido;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class ClienteJpaController implements Serializable {

    public ClienteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cliente cliente) {
        if (cliente.getUsuarioList() == null) {
            cliente.setUsuarioList(new ArrayList<Usuario>());
        }
        if (cliente.getPedidoList() == null) {
            cliente.setPedidoList(new ArrayList<Pedido>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Direccion direccionId = cliente.getDireccionId();
            if (direccionId != null) {
                direccionId = em.getReference(direccionId.getClass(), direccionId.getId());
                cliente.setDireccionId(direccionId);
            }
            List<Usuario> attachedUsuarioList = new ArrayList<Usuario>();
            for (Usuario usuarioListUsuarioToAttach : cliente.getUsuarioList()) {
                usuarioListUsuarioToAttach = em.getReference(usuarioListUsuarioToAttach.getClass(), usuarioListUsuarioToAttach.getId());
                attachedUsuarioList.add(usuarioListUsuarioToAttach);
            }
            cliente.setUsuarioList(attachedUsuarioList);
            List<Pedido> attachedPedidoList = new ArrayList<Pedido>();
            for (Pedido pedidoListPedidoToAttach : cliente.getPedidoList()) {
                pedidoListPedidoToAttach = em.getReference(pedidoListPedidoToAttach.getClass(), pedidoListPedidoToAttach.getId());
                attachedPedidoList.add(pedidoListPedidoToAttach);
            }
            cliente.setPedidoList(attachedPedidoList);
            em.persist(cliente);
            if (direccionId != null) {
                direccionId.getClienteList().add(cliente);
                direccionId = em.merge(direccionId);
            }
            for (Usuario usuarioListUsuario : cliente.getUsuarioList()) {
                usuarioListUsuario.getClienteList().add(cliente);
                usuarioListUsuario = em.merge(usuarioListUsuario);
            }
            for (Pedido pedidoListPedido : cliente.getPedidoList()) {
                Cliente oldClienteIdOfPedidoListPedido = pedidoListPedido.getClienteId();
                pedidoListPedido.setClienteId(cliente);
                pedidoListPedido = em.merge(pedidoListPedido);
                if (oldClienteIdOfPedidoListPedido != null) {
                    oldClienteIdOfPedidoListPedido.getPedidoList().remove(pedidoListPedido);
                    oldClienteIdOfPedidoListPedido = em.merge(oldClienteIdOfPedidoListPedido);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cliente cliente) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente persistentCliente = em.find(Cliente.class, cliente.getId());
            Direccion direccionIdOld = persistentCliente.getDireccionId();
            Direccion direccionIdNew = cliente.getDireccionId();
            List<Usuario> usuarioListOld = persistentCliente.getUsuarioList();
            List<Usuario> usuarioListNew = cliente.getUsuarioList();
            List<Pedido> pedidoListOld = persistentCliente.getPedidoList();
            List<Pedido> pedidoListNew = cliente.getPedidoList();
            List<String> illegalOrphanMessages = null;
            for (Pedido pedidoListOldPedido : pedidoListOld) {
                if (!pedidoListNew.contains(pedidoListOldPedido)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pedido " + pedidoListOldPedido + " since its clienteId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (direccionIdNew != null) {
                direccionIdNew = em.getReference(direccionIdNew.getClass(), direccionIdNew.getId());
                cliente.setDireccionId(direccionIdNew);
            }
            List<Usuario> attachedUsuarioListNew = new ArrayList<Usuario>();
            for (Usuario usuarioListNewUsuarioToAttach : usuarioListNew) {
                usuarioListNewUsuarioToAttach = em.getReference(usuarioListNewUsuarioToAttach.getClass(), usuarioListNewUsuarioToAttach.getId());
                attachedUsuarioListNew.add(usuarioListNewUsuarioToAttach);
            }
            usuarioListNew = attachedUsuarioListNew;
            cliente.setUsuarioList(usuarioListNew);
            List<Pedido> attachedPedidoListNew = new ArrayList<Pedido>();
            for (Pedido pedidoListNewPedidoToAttach : pedidoListNew) {
                pedidoListNewPedidoToAttach = em.getReference(pedidoListNewPedidoToAttach.getClass(), pedidoListNewPedidoToAttach.getId());
                attachedPedidoListNew.add(pedidoListNewPedidoToAttach);
            }
            pedidoListNew = attachedPedidoListNew;
            cliente.setPedidoList(pedidoListNew);
            cliente = em.merge(cliente);
            if (direccionIdOld != null && !direccionIdOld.equals(direccionIdNew)) {
                direccionIdOld.getClienteList().remove(cliente);
                direccionIdOld = em.merge(direccionIdOld);
            }
            if (direccionIdNew != null && !direccionIdNew.equals(direccionIdOld)) {
                direccionIdNew.getClienteList().add(cliente);
                direccionIdNew = em.merge(direccionIdNew);
            }
            for (Usuario usuarioListOldUsuario : usuarioListOld) {
                if (!usuarioListNew.contains(usuarioListOldUsuario)) {
                    usuarioListOldUsuario.getClienteList().remove(cliente);
                    usuarioListOldUsuario = em.merge(usuarioListOldUsuario);
                }
            }
            for (Usuario usuarioListNewUsuario : usuarioListNew) {
                if (!usuarioListOld.contains(usuarioListNewUsuario)) {
                    usuarioListNewUsuario.getClienteList().add(cliente);
                    usuarioListNewUsuario = em.merge(usuarioListNewUsuario);
                }
            }
            for (Pedido pedidoListNewPedido : pedidoListNew) {
                if (!pedidoListOld.contains(pedidoListNewPedido)) {
                    Cliente oldClienteIdOfPedidoListNewPedido = pedidoListNewPedido.getClienteId();
                    pedidoListNewPedido.setClienteId(cliente);
                    pedidoListNewPedido = em.merge(pedidoListNewPedido);
                    if (oldClienteIdOfPedidoListNewPedido != null && !oldClienteIdOfPedidoListNewPedido.equals(cliente)) {
                        oldClienteIdOfPedidoListNewPedido.getPedidoList().remove(pedidoListNewPedido);
                        oldClienteIdOfPedidoListNewPedido = em.merge(oldClienteIdOfPedidoListNewPedido);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cliente.getId();
                if (findCliente(id) == null) {
                    throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente cliente;
            try {
                cliente = em.getReference(Cliente.class, id);
                cliente.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cliente with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Pedido> pedidoListOrphanCheck = cliente.getPedidoList();
            for (Pedido pedidoListOrphanCheckPedido : pedidoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cliente (" + cliente + ") cannot be destroyed since the Pedido " + pedidoListOrphanCheckPedido + " in its pedidoList field has a non-nullable clienteId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Direccion direccionId = cliente.getDireccionId();
            if (direccionId != null) {
                direccionId.getClienteList().remove(cliente);
                direccionId = em.merge(direccionId);
            }
            List<Usuario> usuarioList = cliente.getUsuarioList();
            for (Usuario usuarioListUsuario : usuarioList) {
                usuarioListUsuario.getClienteList().remove(cliente);
                usuarioListUsuario = em.merge(usuarioListUsuario);
            }
            em.remove(cliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cliente> findClienteEntities() {
        return findClienteEntities(true, -1, -1);
    }

    public List<Cliente> findClienteEntities(int maxResults, int firstResult) {
        return findClienteEntities(false, maxResults, firstResult);
    }

    private List<Cliente> findClienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cliente findCliente(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getClienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cliente> rt = cq.from(Cliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        public List<Cliente> clienteListaCompleta(){
        EntityManager em = getEntityManager();
        List<Cliente>proveedores=new ArrayList<Cliente>();
        try {
            
            TypedQuery<Cliente> resultList = em.createQuery("SELECT p FROM Cliente p WHERE p.numerotarjeta=1 OR p.numerotarjeta=2 ORDER BY p.escuela ASC ", Cliente.class);
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
     public List<Cliente> clienteLista(int pagina){
        EntityManager em = getEntityManager();
        List<Cliente>proveedores=new ArrayList<Cliente>();
        try {
            
            TypedQuery<Cliente> resultList = em.createQuery("SELECT p FROM Cliente p WHERE p.numerotarjeta=1 OR p.tipo<3 ORDER BY p.escuela ASC ", Cliente.class).setFirstResult(pagina*20).setMaxResults(20);
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
    public List<Cliente> clienteBuscar(int pagina,String objeto){
        EntityManager em = getEntityManager();
        List<Cliente>proveedores=new ArrayList<Cliente>();
        try {
            
            TypedQuery<Cliente> resultList = em.createQuery("SELECT p FROM Cliente p WHERE (p.numerotarjeta=1 OR p.tipo<3) AND (p.nombre LIKE :objeto OR p.escuela LIKE :objeto) ORDER BY p.escuela ASC ", Cliente.class).setFirstResult(pagina*20).setMaxResults(20);
            resultList.setParameter("objeto", "%"+objeto+"%");
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
     public int clienteListaCuenta(String objeto){
        EntityManager em = getEntityManager();
        int numero=0;
        try {
            
            TypedQuery<Cliente> resultList = em.createQuery("SELECT p FROM Cliente p WHERE (p.numerotarjeta=1 OR p.tipo<3) AND (p.nombre LIKE :objeto OR p.escuela LIKE :objeto) ORDER BY p.escuela ASC  ", Cliente.class);
            resultList.setParameter("objeto", "%"+objeto+"%");
            numero = resultList.getResultList().size();
        } finally {
            em.close();
        }
            return numero;
    }
     
     public List<Cliente> deudoresLista(){
        EntityManager em = getEntityManager();
        List<Cliente>proveedores=new ArrayList<Cliente>();
        try {
            
            TypedQuery<Cliente> resultList = em.createQuery("SELECT p FROM Cliente p WHERE p.numerotarjeta=1 ORDER BY p.escuela ASC ", Cliente.class);
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
}
