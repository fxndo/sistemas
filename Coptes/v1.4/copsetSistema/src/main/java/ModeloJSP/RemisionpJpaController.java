/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Pedido;
import Modelo.Remisionp;
import Modelo.Usuario;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author fernando
 */
public class RemisionpJpaController implements Serializable {

    public RemisionpJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Remisionp remisionp) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedido pedidoId = remisionp.getPedidoId();
            if (pedidoId != null) {
                pedidoId = em.getReference(pedidoId.getClass(), pedidoId.getId());
                remisionp.setPedidoId(pedidoId);
            }
            Usuario usuarioId = remisionp.getUsuarioId();
            if (usuarioId != null) {
                usuarioId = em.getReference(usuarioId.getClass(), usuarioId.getId());
                remisionp.setUsuarioId(usuarioId);
            }
            em.persist(remisionp);
            if (pedidoId != null) {
                pedidoId.getRemisionpList().add(remisionp);
                pedidoId = em.merge(pedidoId);
            }
            if (usuarioId != null) {
                usuarioId.getRemisionpList().add(remisionp);
                usuarioId = em.merge(usuarioId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Remisionp remisionp) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Remisionp persistentRemisionp = em.find(Remisionp.class, remisionp.getId());
            Pedido pedidoIdOld = persistentRemisionp.getPedidoId();
            Pedido pedidoIdNew = remisionp.getPedidoId();
            Usuario usuarioIdOld = persistentRemisionp.getUsuarioId();
            Usuario usuarioIdNew = remisionp.getUsuarioId();
            if (pedidoIdNew != null) {
                pedidoIdNew = em.getReference(pedidoIdNew.getClass(), pedidoIdNew.getId());
                remisionp.setPedidoId(pedidoIdNew);
            }
            if (usuarioIdNew != null) {
                usuarioIdNew = em.getReference(usuarioIdNew.getClass(), usuarioIdNew.getId());
                remisionp.setUsuarioId(usuarioIdNew);
            }
            remisionp = em.merge(remisionp);
            if (pedidoIdOld != null && !pedidoIdOld.equals(pedidoIdNew)) {
                pedidoIdOld.getRemisionpList().remove(remisionp);
                pedidoIdOld = em.merge(pedidoIdOld);
            }
            if (pedidoIdNew != null && !pedidoIdNew.equals(pedidoIdOld)) {
                pedidoIdNew.getRemisionpList().add(remisionp);
                pedidoIdNew = em.merge(pedidoIdNew);
            }
            if (usuarioIdOld != null && !usuarioIdOld.equals(usuarioIdNew)) {
                usuarioIdOld.getRemisionpList().remove(remisionp);
                usuarioIdOld = em.merge(usuarioIdOld);
            }
            if (usuarioIdNew != null && !usuarioIdNew.equals(usuarioIdOld)) {
                usuarioIdNew.getRemisionpList().add(remisionp);
                usuarioIdNew = em.merge(usuarioIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = remisionp.getId();
                if (findRemisionp(id) == null) {
                    throw new NonexistentEntityException("The remisionp with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Remisionp remisionp;
            try {
                remisionp = em.getReference(Remisionp.class, id);
                remisionp.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The remisionp with id " + id + " no longer exists.", enfe);
            }
            Pedido pedidoId = remisionp.getPedidoId();
            if (pedidoId != null) {
                pedidoId.getRemisionpList().remove(remisionp);
                pedidoId = em.merge(pedidoId);
            }
            Usuario usuarioId = remisionp.getUsuarioId();
            if (usuarioId != null) {
                usuarioId.getRemisionpList().remove(remisionp);
                usuarioId = em.merge(usuarioId);
            }
            em.remove(remisionp);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Remisionp> findRemisionpEntities() {
        return findRemisionpEntities(true, -1, -1);
    }

    public List<Remisionp> findRemisionpEntities(int maxResults, int firstResult) {
        return findRemisionpEntities(false, maxResults, firstResult);
    }

    private List<Remisionp> findRemisionpEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Remisionp.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Remisionp findRemisionp(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Remisionp.class, id);
        } finally {
            em.close();
        }
    }

    public int getRemisionpCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Remisionp> rt = cq.from(Remisionp.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
