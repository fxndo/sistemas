/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Caja;
import Modelo.Pago;
import Modelo.Pedido;
import Modelo.Usuario;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author fernando
 */
public class PagoJpaController implements Serializable {

    public PagoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pago pago) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Caja cajaId = pago.getCajaId();
            if (cajaId != null) {
                cajaId = em.getReference(cajaId.getClass(), cajaId.getId());
                pago.setCajaId(cajaId);
            }
            Pedido pedidoId = pago.getPedidoId();
            if (pedidoId != null) {
                pedidoId = em.getReference(pedidoId.getClass(), pedidoId.getId());
                pago.setPedidoId(pedidoId);
            }
            Usuario usuarioId = pago.getUsuarioId();
            if (usuarioId != null) {
                usuarioId = em.getReference(usuarioId.getClass(), usuarioId.getId());
                pago.setUsuarioId(usuarioId);
            }
            em.persist(pago);
            if (cajaId != null) {
                cajaId.getPagoList().add(pago);
                cajaId = em.merge(cajaId);
            }
            if (pedidoId != null) {
                pedidoId.getPagoList().add(pago);
                pedidoId = em.merge(pedidoId);
            }
            if (usuarioId != null) {
                usuarioId.getPagoList().add(pago);
                usuarioId = em.merge(usuarioId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pago pago) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pago persistentPago = em.find(Pago.class, pago.getId());
            Caja cajaIdOld = persistentPago.getCajaId();
            Caja cajaIdNew = pago.getCajaId();
            Pedido pedidoIdOld = persistentPago.getPedidoId();
            Pedido pedidoIdNew = pago.getPedidoId();
            Usuario usuarioIdOld = persistentPago.getUsuarioId();
            Usuario usuarioIdNew = pago.getUsuarioId();
            if (cajaIdNew != null) {
                cajaIdNew = em.getReference(cajaIdNew.getClass(), cajaIdNew.getId());
                pago.setCajaId(cajaIdNew);
            }
            if (pedidoIdNew != null) {
                pedidoIdNew = em.getReference(pedidoIdNew.getClass(), pedidoIdNew.getId());
                pago.setPedidoId(pedidoIdNew);
            }
            if (usuarioIdNew != null) {
                usuarioIdNew = em.getReference(usuarioIdNew.getClass(), usuarioIdNew.getId());
                pago.setUsuarioId(usuarioIdNew);
            }
            pago = em.merge(pago);
            if (cajaIdOld != null && !cajaIdOld.equals(cajaIdNew)) {
                cajaIdOld.getPagoList().remove(pago);
                cajaIdOld = em.merge(cajaIdOld);
            }
            if (cajaIdNew != null && !cajaIdNew.equals(cajaIdOld)) {
                cajaIdNew.getPagoList().add(pago);
                cajaIdNew = em.merge(cajaIdNew);
            }
            if (pedidoIdOld != null && !pedidoIdOld.equals(pedidoIdNew)) {
                pedidoIdOld.getPagoList().remove(pago);
                pedidoIdOld = em.merge(pedidoIdOld);
            }
            if (pedidoIdNew != null && !pedidoIdNew.equals(pedidoIdOld)) {
                pedidoIdNew.getPagoList().add(pago);
                pedidoIdNew = em.merge(pedidoIdNew);
            }
            if (usuarioIdOld != null && !usuarioIdOld.equals(usuarioIdNew)) {
                usuarioIdOld.getPagoList().remove(pago);
                usuarioIdOld = em.merge(usuarioIdOld);
            }
            if (usuarioIdNew != null && !usuarioIdNew.equals(usuarioIdOld)) {
                usuarioIdNew.getPagoList().add(pago);
                usuarioIdNew = em.merge(usuarioIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pago.getId();
                if (findPago(id) == null) {
                    throw new NonexistentEntityException("The pago with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pago pago;
            try {
                pago = em.getReference(Pago.class, id);
                pago.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pago with id " + id + " no longer exists.", enfe);
            }
            Caja cajaId = pago.getCajaId();
            if (cajaId != null) {
                cajaId.getPagoList().remove(pago);
                cajaId = em.merge(cajaId);
            }
            Pedido pedidoId = pago.getPedidoId();
            if (pedidoId != null) {
                pedidoId.getPagoList().remove(pago);
                pedidoId = em.merge(pedidoId);
            }
            Usuario usuarioId = pago.getUsuarioId();
            if (usuarioId != null) {
                usuarioId.getPagoList().remove(pago);
                usuarioId = em.merge(usuarioId);
            }
            em.remove(pago);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pago> findPagoEntities() {
        return findPagoEntities(true, -1, -1);
    }

    public List<Pago> findPagoEntities(int maxResults, int firstResult) {
        return findPagoEntities(false, maxResults, firstResult);
    }

    private List<Pago> findPagoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pago.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pago findPago(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pago.class, id);
        } finally {
            em.close();
        }
    }

    public int getPagoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pago> rt = cq.from(Pago.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
