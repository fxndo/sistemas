/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import Modelo.Caja;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Sucursal;
import Modelo.Usuario;
import Modelo.Pago;
import java.util.ArrayList;
import java.util.List;
import Modelo.Ventas;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class CajaJpaController implements Serializable {

    public CajaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Caja caja) {
        if (caja.getPagoList() == null) {
            caja.setPagoList(new ArrayList<Pago>());
        }
        if (caja.getVentasList() == null) {
            caja.setVentasList(new ArrayList<Ventas>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sucursal sucursalId = caja.getSucursalId();
            if (sucursalId != null) {
                sucursalId = em.getReference(sucursalId.getClass(), sucursalId.getId());
                caja.setSucursalId(sucursalId);
            }
            Usuario usuarioId = caja.getUsuarioId();
            if (usuarioId != null) {
                usuarioId = em.getReference(usuarioId.getClass(), usuarioId.getId());
                caja.setUsuarioId(usuarioId);
            }
            List<Pago> attachedPagoList = new ArrayList<Pago>();
            for (Pago pagoListPagoToAttach : caja.getPagoList()) {
                pagoListPagoToAttach = em.getReference(pagoListPagoToAttach.getClass(), pagoListPagoToAttach.getId());
                attachedPagoList.add(pagoListPagoToAttach);
            }
            caja.setPagoList(attachedPagoList);
            List<Ventas> attachedVentasList = new ArrayList<Ventas>();
            for (Ventas ventasListVentasToAttach : caja.getVentasList()) {
                ventasListVentasToAttach = em.getReference(ventasListVentasToAttach.getClass(), ventasListVentasToAttach.getId());
                attachedVentasList.add(ventasListVentasToAttach);
            }
            caja.setVentasList(attachedVentasList);
            em.persist(caja);
            if (sucursalId != null) {
                sucursalId.getCajaList().add(caja);
                sucursalId = em.merge(sucursalId);
            }
            if (usuarioId != null) {
                usuarioId.getCajaList().add(caja);
                usuarioId = em.merge(usuarioId);
            }
            for (Pago pagoListPago : caja.getPagoList()) {
                Caja oldCajaIdOfPagoListPago = pagoListPago.getCajaId();
                pagoListPago.setCajaId(caja);
                pagoListPago = em.merge(pagoListPago);
                if (oldCajaIdOfPagoListPago != null) {
                    oldCajaIdOfPagoListPago.getPagoList().remove(pagoListPago);
                    oldCajaIdOfPagoListPago = em.merge(oldCajaIdOfPagoListPago);
                }
            }
            for (Ventas ventasListVentas : caja.getVentasList()) {
                Caja oldCajaIdOfVentasListVentas = ventasListVentas.getCajaId();
                ventasListVentas.setCajaId(caja);
                ventasListVentas = em.merge(ventasListVentas);
                if (oldCajaIdOfVentasListVentas != null) {
                    oldCajaIdOfVentasListVentas.getVentasList().remove(ventasListVentas);
                    oldCajaIdOfVentasListVentas = em.merge(oldCajaIdOfVentasListVentas);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Caja caja) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Caja persistentCaja = em.find(Caja.class, caja.getId());
            Sucursal sucursalIdOld = persistentCaja.getSucursalId();
            Sucursal sucursalIdNew = caja.getSucursalId();
            Usuario usuarioIdOld = persistentCaja.getUsuarioId();
            Usuario usuarioIdNew = caja.getUsuarioId();
            List<Pago> pagoListOld = persistentCaja.getPagoList();
            List<Pago> pagoListNew = caja.getPagoList();
            List<Ventas> ventasListOld = persistentCaja.getVentasList();
            List<Ventas> ventasListNew = caja.getVentasList();
            List<String> illegalOrphanMessages = null;
            for (Ventas ventasListOldVentas : ventasListOld) {
                if (!ventasListNew.contains(ventasListOldVentas)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Ventas " + ventasListOldVentas + " since its cajaId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (sucursalIdNew != null) {
                sucursalIdNew = em.getReference(sucursalIdNew.getClass(), sucursalIdNew.getId());
                caja.setSucursalId(sucursalIdNew);
            }
            if (usuarioIdNew != null) {
                usuarioIdNew = em.getReference(usuarioIdNew.getClass(), usuarioIdNew.getId());
                caja.setUsuarioId(usuarioIdNew);
            }
            List<Pago> attachedPagoListNew = new ArrayList<Pago>();
            for (Pago pagoListNewPagoToAttach : pagoListNew) {
                pagoListNewPagoToAttach = em.getReference(pagoListNewPagoToAttach.getClass(), pagoListNewPagoToAttach.getId());
                attachedPagoListNew.add(pagoListNewPagoToAttach);
            }
            pagoListNew = attachedPagoListNew;
            caja.setPagoList(pagoListNew);
            List<Ventas> attachedVentasListNew = new ArrayList<Ventas>();
            for (Ventas ventasListNewVentasToAttach : ventasListNew) {
                ventasListNewVentasToAttach = em.getReference(ventasListNewVentasToAttach.getClass(), ventasListNewVentasToAttach.getId());
                attachedVentasListNew.add(ventasListNewVentasToAttach);
            }
            ventasListNew = attachedVentasListNew;
            caja.setVentasList(ventasListNew);
            caja = em.merge(caja);
            if (sucursalIdOld != null && !sucursalIdOld.equals(sucursalIdNew)) {
                sucursalIdOld.getCajaList().remove(caja);
                sucursalIdOld = em.merge(sucursalIdOld);
            }
            if (sucursalIdNew != null && !sucursalIdNew.equals(sucursalIdOld)) {
                sucursalIdNew.getCajaList().add(caja);
                sucursalIdNew = em.merge(sucursalIdNew);
            }
            if (usuarioIdOld != null && !usuarioIdOld.equals(usuarioIdNew)) {
                usuarioIdOld.getCajaList().remove(caja);
                usuarioIdOld = em.merge(usuarioIdOld);
            }
            if (usuarioIdNew != null && !usuarioIdNew.equals(usuarioIdOld)) {
                usuarioIdNew.getCajaList().add(caja);
                usuarioIdNew = em.merge(usuarioIdNew);
            }
            for (Pago pagoListOldPago : pagoListOld) {
                if (!pagoListNew.contains(pagoListOldPago)) {
                    pagoListOldPago.setCajaId(null);
                    pagoListOldPago = em.merge(pagoListOldPago);
                }
            }
            for (Pago pagoListNewPago : pagoListNew) {
                if (!pagoListOld.contains(pagoListNewPago)) {
                    Caja oldCajaIdOfPagoListNewPago = pagoListNewPago.getCajaId();
                    pagoListNewPago.setCajaId(caja);
                    pagoListNewPago = em.merge(pagoListNewPago);
                    if (oldCajaIdOfPagoListNewPago != null && !oldCajaIdOfPagoListNewPago.equals(caja)) {
                        oldCajaIdOfPagoListNewPago.getPagoList().remove(pagoListNewPago);
                        oldCajaIdOfPagoListNewPago = em.merge(oldCajaIdOfPagoListNewPago);
                    }
                }
            }
            for (Ventas ventasListNewVentas : ventasListNew) {
                if (!ventasListOld.contains(ventasListNewVentas)) {
                    Caja oldCajaIdOfVentasListNewVentas = ventasListNewVentas.getCajaId();
                    ventasListNewVentas.setCajaId(caja);
                    ventasListNewVentas = em.merge(ventasListNewVentas);
                    if (oldCajaIdOfVentasListNewVentas != null && !oldCajaIdOfVentasListNewVentas.equals(caja)) {
                        oldCajaIdOfVentasListNewVentas.getVentasList().remove(ventasListNewVentas);
                        oldCajaIdOfVentasListNewVentas = em.merge(oldCajaIdOfVentasListNewVentas);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = caja.getId();
                if (findCaja(id) == null) {
                    throw new NonexistentEntityException("The caja with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Caja caja;
            try {
                caja = em.getReference(Caja.class, id);
                caja.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The caja with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Ventas> ventasListOrphanCheck = caja.getVentasList();
            for (Ventas ventasListOrphanCheckVentas : ventasListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Caja (" + caja + ") cannot be destroyed since the Ventas " + ventasListOrphanCheckVentas + " in its ventasList field has a non-nullable cajaId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Sucursal sucursalId = caja.getSucursalId();
            if (sucursalId != null) {
                sucursalId.getCajaList().remove(caja);
                sucursalId = em.merge(sucursalId);
            }
            Usuario usuarioId = caja.getUsuarioId();
            if (usuarioId != null) {
                usuarioId.getCajaList().remove(caja);
                usuarioId = em.merge(usuarioId);
            }
            List<Pago> pagoList = caja.getPagoList();
            for (Pago pagoListPago : pagoList) {
                pagoListPago.setCajaId(null);
                pagoListPago = em.merge(pagoListPago);
            }
            em.remove(caja);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Caja> findCajaEntities() {
        return findCajaEntities(true, -1, -1);
    }

    public List<Caja> findCajaEntities(int maxResults, int firstResult) {
        return findCajaEntities(false, maxResults, firstResult);
    }

    private List<Caja> findCajaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Caja.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Caja findCaja(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Caja.class, id);
        } finally {
            em.close();
        }
    }

    public int getCajaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Caja> rt = cq.from(Caja.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        public int verificarCaja(int idSucursal){
        EntityManager em = getEntityManager();
        int verificar=0;
        try {
            TypedQuery<Caja> resultList = em.createQuery("SELECT c FROM Caja c WHERE c.estado=1 AND c.sucursalId.id=:idSucursal", Caja.class);
            resultList.setParameter("idSucursal", idSucursal);
            List<Caja> caja = resultList.getResultList();
            if(caja.size()>0){
                verificar=1;
            }
        } finally {
            em.close();
        }
            return verificar;
    }
    
     public int cerrarCaja(int idSucursal){
        EntityManager em = getEntityManager();
        int verificar=0;
        try {
            TypedQuery<Caja> resultList = em.createQuery("SELECT c FROM Caja c WHERE c.estado=1 AND c.sucursalId.id=:idSucursal", Caja.class);
            resultList.setParameter("idSucursal", idSucursal);
            if(resultList.getResultList().size()>0){
                verificar=resultList.getSingleResult().getId();
            }
        } finally {
            em.close();
        }
            return verificar;
    }
     
    public List<Caja> busquedaCaja(Date inicio,Date termino,int pagina) throws ParseException{
        EntityManager em = getEntityManager();
        SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");
        Date fecha=formato2.parse("1988-07-11 00:00:00");
        Date termino2=termino;
        int in=0;
        int te=0;
        if(inicio.equals(fecha)){
            in=1;
        }
        if(termino.equals(fecha)){
             te=1;
         }
            try {
                String query="SELECT c FROM Caja c";
                if(in==0 || te==0){
                    query+=" WHERE";
                }
                if(in==0){
                    query+=" c.fecha >= :inicio";
                }
                if(te==0){
                    if(in==0){
                    query+=" AND";
                        
                    }
                    query+=" c.fecha <:termino";
                }
                TypedQuery<Caja> resultList = em.createQuery(query, Caja.class).setFirstResult(pagina*20).setMaxResults(20);                
                
                if(te==0){
                    termino2.setHours(23);
                    termino2.setMinutes(59);
                    resultList.setParameter("termino",termino2);
                    System.out.println("Hora Termino2: "+termino2);
                }
                if(in==0){
                    resultList.setParameter("inicio",inicio,TemporalType.DATE);
                    System.out.println("Hora Inicio2: "+inicio);
                }
                System.out.println(query);
                
                List<Caja> cajas=resultList.getResultList();
            return cajas;
            
        } finally {
            em.close();
        }
    }
    
    public int busquedaCajaCuenta(Date inicio,Date termino) throws ParseException{
        EntityManager em = getEntityManager();
        SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");
        Date fecha=formato2.parse("1988-07-11 00:00:00");
        Date termino2=termino;
        int in=0;
        int te=0;
        if(inicio.equals(fecha)){
            in=1;
        }
        if(termino.equals(fecha)){
             te=1;
         }
            try {
                String query="SELECT c FROM Caja c";
                if(in==0 || te==0){
                    query+=" WHERE";
                }
                if(in==0){
                    query+=" c.fecha >= :inicio";
                }
                if(te==0){
                    if(in==0){
                    query+=" AND";
                        
                    }
                    query+=" c.fecha <:termino";
                }
                TypedQuery<Caja> resultList = em.createQuery(query, Caja.class);                
                
                if(te==0){
                    termino2.setHours(23);
                    termino2.setMinutes(59);
                    resultList.setParameter("termino",termino2);
                    System.out.println("Hora Termino2: "+termino2);
                }
                if(in==0){
                    resultList.setParameter("inicio",inicio,TemporalType.DATE);
                    System.out.println("Hora Inicio2: "+inicio);
                }
                System.out.println(query);
                
                int numero=resultList.getResultList().size();
            return numero;
            
        } finally {
            em.close();
        }
    }
    
}
