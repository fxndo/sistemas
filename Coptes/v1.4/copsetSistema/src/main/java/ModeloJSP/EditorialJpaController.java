/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import Modelo.Editorial;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Producto;
import java.util.ArrayList;
import java.util.List;
import Modelo.Proveedoreditorial;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class EditorialJpaController implements Serializable {

    public EditorialJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Editorial editorial) {
        if (editorial.getProductoList() == null) {
            editorial.setProductoList(new ArrayList<Producto>());
        }
        if (editorial.getProveedoreditorialList() == null) {
            editorial.setProveedoreditorialList(new ArrayList<Proveedoreditorial>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Producto> attachedProductoList = new ArrayList<Producto>();
            for (Producto productoListProductoToAttach : editorial.getProductoList()) {
                productoListProductoToAttach = em.getReference(productoListProductoToAttach.getClass(), productoListProductoToAttach.getId());
                attachedProductoList.add(productoListProductoToAttach);
            }
            editorial.setProductoList(attachedProductoList);
            List<Proveedoreditorial> attachedProveedoreditorialList = new ArrayList<Proveedoreditorial>();
            for (Proveedoreditorial proveedoreditorialListProveedoreditorialToAttach : editorial.getProveedoreditorialList()) {
                proveedoreditorialListProveedoreditorialToAttach = em.getReference(proveedoreditorialListProveedoreditorialToAttach.getClass(), proveedoreditorialListProveedoreditorialToAttach.getId());
                attachedProveedoreditorialList.add(proveedoreditorialListProveedoreditorialToAttach);
            }
            editorial.setProveedoreditorialList(attachedProveedoreditorialList);
            em.persist(editorial);
            for (Producto productoListProducto : editorial.getProductoList()) {
                Editorial oldEditorialId1OfProductoListProducto = productoListProducto.getEditorialId1();
                productoListProducto.setEditorialId1(editorial);
                productoListProducto = em.merge(productoListProducto);
                if (oldEditorialId1OfProductoListProducto != null) {
                    oldEditorialId1OfProductoListProducto.getProductoList().remove(productoListProducto);
                    oldEditorialId1OfProductoListProducto = em.merge(oldEditorialId1OfProductoListProducto);
                }
            }
            for (Proveedoreditorial proveedoreditorialListProveedoreditorial : editorial.getProveedoreditorialList()) {
                Editorial oldEditorialIdOfProveedoreditorialListProveedoreditorial = proveedoreditorialListProveedoreditorial.getEditorialId();
                proveedoreditorialListProveedoreditorial.setEditorialId(editorial);
                proveedoreditorialListProveedoreditorial = em.merge(proveedoreditorialListProveedoreditorial);
                if (oldEditorialIdOfProveedoreditorialListProveedoreditorial != null) {
                    oldEditorialIdOfProveedoreditorialListProveedoreditorial.getProveedoreditorialList().remove(proveedoreditorialListProveedoreditorial);
                    oldEditorialIdOfProveedoreditorialListProveedoreditorial = em.merge(oldEditorialIdOfProveedoreditorialListProveedoreditorial);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Editorial editorial) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Editorial persistentEditorial = em.find(Editorial.class, editorial.getId());
            List<Producto> productoListOld = persistentEditorial.getProductoList();
            List<Producto> productoListNew = editorial.getProductoList();
            List<Proveedoreditorial> proveedoreditorialListOld = persistentEditorial.getProveedoreditorialList();
            List<Proveedoreditorial> proveedoreditorialListNew = editorial.getProveedoreditorialList();
            List<String> illegalOrphanMessages = null;
            for (Producto productoListOldProducto : productoListOld) {
                if (!productoListNew.contains(productoListOldProducto)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Producto " + productoListOldProducto + " since its editorialId1 field is not nullable.");
                }
            }
            for (Proveedoreditorial proveedoreditorialListOldProveedoreditorial : proveedoreditorialListOld) {
                if (!proveedoreditorialListNew.contains(proveedoreditorialListOldProveedoreditorial)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Proveedoreditorial " + proveedoreditorialListOldProveedoreditorial + " since its editorialId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Producto> attachedProductoListNew = new ArrayList<Producto>();
            for (Producto productoListNewProductoToAttach : productoListNew) {
                productoListNewProductoToAttach = em.getReference(productoListNewProductoToAttach.getClass(), productoListNewProductoToAttach.getId());
                attachedProductoListNew.add(productoListNewProductoToAttach);
            }
            productoListNew = attachedProductoListNew;
            editorial.setProductoList(productoListNew);
            List<Proveedoreditorial> attachedProveedoreditorialListNew = new ArrayList<Proveedoreditorial>();
            for (Proveedoreditorial proveedoreditorialListNewProveedoreditorialToAttach : proveedoreditorialListNew) {
                proveedoreditorialListNewProveedoreditorialToAttach = em.getReference(proveedoreditorialListNewProveedoreditorialToAttach.getClass(), proveedoreditorialListNewProveedoreditorialToAttach.getId());
                attachedProveedoreditorialListNew.add(proveedoreditorialListNewProveedoreditorialToAttach);
            }
            proveedoreditorialListNew = attachedProveedoreditorialListNew;
            editorial.setProveedoreditorialList(proveedoreditorialListNew);
            editorial = em.merge(editorial);
            for (Producto productoListNewProducto : productoListNew) {
                if (!productoListOld.contains(productoListNewProducto)) {
                    Editorial oldEditorialId1OfProductoListNewProducto = productoListNewProducto.getEditorialId1();
                    productoListNewProducto.setEditorialId1(editorial);
                    productoListNewProducto = em.merge(productoListNewProducto);
                    if (oldEditorialId1OfProductoListNewProducto != null && !oldEditorialId1OfProductoListNewProducto.equals(editorial)) {
                        oldEditorialId1OfProductoListNewProducto.getProductoList().remove(productoListNewProducto);
                        oldEditorialId1OfProductoListNewProducto = em.merge(oldEditorialId1OfProductoListNewProducto);
                    }
                }
            }
            for (Proveedoreditorial proveedoreditorialListNewProveedoreditorial : proveedoreditorialListNew) {
                if (!proveedoreditorialListOld.contains(proveedoreditorialListNewProveedoreditorial)) {
                    Editorial oldEditorialIdOfProveedoreditorialListNewProveedoreditorial = proveedoreditorialListNewProveedoreditorial.getEditorialId();
                    proveedoreditorialListNewProveedoreditorial.setEditorialId(editorial);
                    proveedoreditorialListNewProveedoreditorial = em.merge(proveedoreditorialListNewProveedoreditorial);
                    if (oldEditorialIdOfProveedoreditorialListNewProveedoreditorial != null && !oldEditorialIdOfProveedoreditorialListNewProveedoreditorial.equals(editorial)) {
                        oldEditorialIdOfProveedoreditorialListNewProveedoreditorial.getProveedoreditorialList().remove(proveedoreditorialListNewProveedoreditorial);
                        oldEditorialIdOfProveedoreditorialListNewProveedoreditorial = em.merge(oldEditorialIdOfProveedoreditorialListNewProveedoreditorial);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = editorial.getId();
                if (findEditorial(id) == null) {
                    throw new NonexistentEntityException("The editorial with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Editorial editorial;
            try {
                editorial = em.getReference(Editorial.class, id);
                editorial.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The editorial with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Producto> productoListOrphanCheck = editorial.getProductoList();
            for (Producto productoListOrphanCheckProducto : productoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Editorial (" + editorial + ") cannot be destroyed since the Producto " + productoListOrphanCheckProducto + " in its productoList field has a non-nullable editorialId1 field.");
            }
            List<Proveedoreditorial> proveedoreditorialListOrphanCheck = editorial.getProveedoreditorialList();
            for (Proveedoreditorial proveedoreditorialListOrphanCheckProveedoreditorial : proveedoreditorialListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Editorial (" + editorial + ") cannot be destroyed since the Proveedoreditorial " + proveedoreditorialListOrphanCheckProveedoreditorial + " in its proveedoreditorialList field has a non-nullable editorialId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(editorial);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Editorial> findEditorialEntities() {
        return findEditorialEntities(true, -1, -1);
    }

    public List<Editorial> findEditorialEntities(int maxResults, int firstResult) {
        return findEditorialEntities(false, maxResults, firstResult);
    }

    private List<Editorial> findEditorialEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Editorial.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Editorial findEditorial(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Editorial.class, id);
        } finally {
            em.close();
        }
    }

    public int getEditorialCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Editorial> rt = cq.from(Editorial.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
     public List<Editorial> listaEditorialesProveedor(int idProveedor) {
        EntityManager em = getEntityManager();
        List<Editorial> pedidos = new ArrayList<Editorial>();

        try {
                TypedQuery<Editorial> resultList = em.createQuery("SELECT e FROM Editorial e, Proveedoreditorial p WHERE p.editorialId.id=e.id AND p.proveedorId.id=:idProveedor ORDER BY e.nombre ASC", Editorial.class);
                resultList.setParameter("idProveedor", idProveedor);
                pedidos = resultList.getResultList();
        } finally {
            em.close();
        }
        return pedidos;
    }
    
    public List<Editorial> listaEditoriales() {
        EntityManager em = getEntityManager();
        List<Editorial> pedidos = new ArrayList<Editorial>();

        try {
                TypedQuery<Editorial> resultList = em.createQuery("SELECT e FROM Editorial e ORDER BY e.nombre ASC", Editorial.class);
                pedidos = resultList.getResultList();
        } finally {
            em.close();
        }
        return pedidos;
    }
    
    public int buscarEditorial(String editorial) {
        EntityManager em = getEntityManager();
        int valor=0;
        try {   System.out.println("@@ editorial: "+editorial);
                TypedQuery<Editorial> resultList = em.createQuery("SELECT e FROM Editorial e WHERE e.nombre LIKE :editorial ", Editorial.class);
                resultList.setParameter("editorial", "%"+editorial+"%");
                if(resultList.getResultList().size()>0){
                    valor = resultList.getSingleResult().getId();
                }
        } finally {
            em.close();
        }
        return valor;
    }
    
}
