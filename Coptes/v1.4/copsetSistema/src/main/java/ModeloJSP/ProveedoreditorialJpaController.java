/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Descuento;
import Modelo.Editorial;
import Modelo.Pedidoproveedor;
import Modelo.Proveedor;
import Modelo.Proveedoreditorial;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class ProveedoreditorialJpaController implements Serializable {

    public ProveedoreditorialJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Proveedoreditorial proveedoreditorial) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Descuento descuentoId = proveedoreditorial.getDescuentoId();
            if (descuentoId != null) {
                descuentoId = em.getReference(descuentoId.getClass(), descuentoId.getId());
                proveedoreditorial.setDescuentoId(descuentoId);
            }
            Editorial editorialId = proveedoreditorial.getEditorialId();
            if (editorialId != null) {
                editorialId = em.getReference(editorialId.getClass(), editorialId.getId());
                proveedoreditorial.setEditorialId(editorialId);
            }
            Proveedor proveedorId = proveedoreditorial.getProveedorId();
            if (proveedorId != null) {
                proveedorId = em.getReference(proveedorId.getClass(), proveedorId.getId());
                proveedoreditorial.setProveedorId(proveedorId);
            }
            em.persist(proveedoreditorial);
            if (descuentoId != null) {
                descuentoId.getProveedoreditorialList().add(proveedoreditorial);
                descuentoId = em.merge(descuentoId);
            }
            if (editorialId != null) {
                editorialId.getProveedoreditorialList().add(proveedoreditorial);
                editorialId = em.merge(editorialId);
            }
            if (proveedorId != null) {
                proveedorId.getProveedoreditorialList().add(proveedoreditorial);
                proveedorId = em.merge(proveedorId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Proveedoreditorial proveedoreditorial) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Proveedoreditorial persistentProveedoreditorial = em.find(Proveedoreditorial.class, proveedoreditorial.getId());
            Descuento descuentoIdOld = persistentProveedoreditorial.getDescuentoId();
            Descuento descuentoIdNew = proveedoreditorial.getDescuentoId();
            Editorial editorialIdOld = persistentProveedoreditorial.getEditorialId();
            Editorial editorialIdNew = proveedoreditorial.getEditorialId();
            Proveedor proveedorIdOld = persistentProveedoreditorial.getProveedorId();
            Proveedor proveedorIdNew = proveedoreditorial.getProveedorId();
            if (descuentoIdNew != null) {
                descuentoIdNew = em.getReference(descuentoIdNew.getClass(), descuentoIdNew.getId());
                proveedoreditorial.setDescuentoId(descuentoIdNew);
            }
            if (editorialIdNew != null) {
                editorialIdNew = em.getReference(editorialIdNew.getClass(), editorialIdNew.getId());
                proveedoreditorial.setEditorialId(editorialIdNew);
            }
            if (proveedorIdNew != null) {
                proveedorIdNew = em.getReference(proveedorIdNew.getClass(), proveedorIdNew.getId());
                proveedoreditorial.setProveedorId(proveedorIdNew);
            }
            proveedoreditorial = em.merge(proveedoreditorial);
            if (descuentoIdOld != null && !descuentoIdOld.equals(descuentoIdNew)) {
                descuentoIdOld.getProveedoreditorialList().remove(proveedoreditorial);
                descuentoIdOld = em.merge(descuentoIdOld);
            }
            if (descuentoIdNew != null && !descuentoIdNew.equals(descuentoIdOld)) {
                descuentoIdNew.getProveedoreditorialList().add(proveedoreditorial);
                descuentoIdNew = em.merge(descuentoIdNew);
            }
            if (editorialIdOld != null && !editorialIdOld.equals(editorialIdNew)) {
                editorialIdOld.getProveedoreditorialList().remove(proveedoreditorial);
                editorialIdOld = em.merge(editorialIdOld);
            }
            if (editorialIdNew != null && !editorialIdNew.equals(editorialIdOld)) {
                editorialIdNew.getProveedoreditorialList().add(proveedoreditorial);
                editorialIdNew = em.merge(editorialIdNew);
            }
            if (proveedorIdOld != null && !proveedorIdOld.equals(proveedorIdNew)) {
                proveedorIdOld.getProveedoreditorialList().remove(proveedoreditorial);
                proveedorIdOld = em.merge(proveedorIdOld);
            }
            if (proveedorIdNew != null && !proveedorIdNew.equals(proveedorIdOld)) {
                proveedorIdNew.getProveedoreditorialList().add(proveedoreditorial);
                proveedorIdNew = em.merge(proveedorIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = proveedoreditorial.getId();
                if (findProveedoreditorial(id) == null) {
                    throw new NonexistentEntityException("The proveedoreditorial with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Proveedoreditorial proveedoreditorial;
            try {
                proveedoreditorial = em.getReference(Proveedoreditorial.class, id);
                proveedoreditorial.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The proveedoreditorial with id " + id + " no longer exists.", enfe);
            }
            Descuento descuentoId = proveedoreditorial.getDescuentoId();
            if (descuentoId != null) {
                descuentoId.getProveedoreditorialList().remove(proveedoreditorial);
                descuentoId = em.merge(descuentoId);
            }
            Editorial editorialId = proveedoreditorial.getEditorialId();
            if (editorialId != null) {
                editorialId.getProveedoreditorialList().remove(proveedoreditorial);
                editorialId = em.merge(editorialId);
            }
            Proveedor proveedorId = proveedoreditorial.getProveedorId();
            if (proveedorId != null) {
                proveedorId.getProveedoreditorialList().remove(proveedoreditorial);
                proveedorId = em.merge(proveedorId);
            }
            em.remove(proveedoreditorial);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Proveedoreditorial> findProveedoreditorialEntities() {
        return findProveedoreditorialEntities(true, -1, -1);
    }

    public List<Proveedoreditorial> findProveedoreditorialEntities(int maxResults, int firstResult) {
        return findProveedoreditorialEntities(false, maxResults, firstResult);
    }

    private List<Proveedoreditorial> findProveedoreditorialEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Proveedoreditorial.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Proveedoreditorial findProveedoreditorial(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Proveedoreditorial.class, id);
        } finally {
            em.close();
        }
    }

    public int getProveedoreditorialCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Proveedoreditorial> rt = cq.from(Proveedoreditorial.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        public int buscarProveedorEditorial (int idEditorial,int idProveedor){
        EntityManager em = getEntityManager();
        int valor=0;
        try {
            TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Proveedoreditorial p WHERE p.editorialId.id=:idEditorial AND p.proveedorId.id=:idProveedor", Pedidoproveedor.class);
            resultList.setParameter("idEditorial", idEditorial);
            resultList.setParameter("idProveedor", idProveedor);
            if(resultList.getResultList().size()>0){
            valor = resultList.getSingleResult().getId();
            }
        } finally {
            em.close();
        }
            return valor;
    }
    
    public List<Proveedoreditorial> proveedorEditorialLista (int pagina){
        EntityManager em = getEntityManager();
        List<Proveedoreditorial>proveedores=new ArrayList<Proveedoreditorial>();
        try {
            TypedQuery<Proveedoreditorial> resultList = em.createQuery("SELECT p FROM Proveedoreditorial p ORDER BY p.proveedorId.nombre ASC", Proveedoreditorial.class).setFirstResult(pagina*20).setMaxResults(20);
            
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
    public int proveedorEditorialListaCuenta (){
        EntityManager em = getEntityManager();
        int numero=0;
        try {
            TypedQuery<Proveedoreditorial> resultList = em.createQuery("SELECT p FROM Proveedoreditorial p ORDER BY p.proveedorId.nombre ASC", Proveedoreditorial.class);
            
            numero = resultList.getResultList().size();
        } finally {
            em.close();
        }
            return numero;
    }
    
    public int proveedorDescuento (int idEditorial,int idProveedor){
        EntityManager em = getEntityManager();
        int valor=0;
        try {
            TypedQuery<Proveedoreditorial> resultList = em.createQuery("SELECT p FROM Proveedoreditorial p WHERE p.editorialId.id=:idEditorial AND p.proveedorId.id=:idProveedor", Proveedoreditorial.class);
            resultList.setParameter("idEditorial", idEditorial);
            resultList.setParameter("idProveedor", idProveedor);
            if(resultList.getResultList().size()>0){
                if(resultList.getSingleResult().getDescuentoId()!=null){
                valor = resultList.getSingleResult().getDescuentoId().getCantidad();
                }
            }
        } finally {
            em.close();
        }
            return valor;
    }
    
}
