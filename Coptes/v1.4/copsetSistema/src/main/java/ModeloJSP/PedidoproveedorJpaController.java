/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Pedido;
import Modelo.Proveedor;
import Modelo.Pedidoparte;
import Modelo.Pedidoproveedor;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class PedidoproveedorJpaController implements Serializable {

    public PedidoproveedorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pedidoproveedor pedidoproveedor) {
        if (pedidoproveedor.getPedidoparteList() == null) {
            pedidoproveedor.setPedidoparteList(new ArrayList<Pedidoparte>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedido pedidoId = pedidoproveedor.getPedidoId();
            if (pedidoId != null) {
                pedidoId = em.getReference(pedidoId.getClass(), pedidoId.getId());
                pedidoproveedor.setPedidoId(pedidoId);
            }
            Proveedor proveedorId = pedidoproveedor.getProveedorId();
            if (proveedorId != null) {
                proveedorId = em.getReference(proveedorId.getClass(), proveedorId.getId());
                pedidoproveedor.setProveedorId(proveedorId);
            }
            List<Pedidoparte> attachedPedidoparteList = new ArrayList<Pedidoparte>();
            for (Pedidoparte pedidoparteListPedidoparteToAttach : pedidoproveedor.getPedidoparteList()) {
                pedidoparteListPedidoparteToAttach = em.getReference(pedidoparteListPedidoparteToAttach.getClass(), pedidoparteListPedidoparteToAttach.getId());
                attachedPedidoparteList.add(pedidoparteListPedidoparteToAttach);
            }
            pedidoproveedor.setPedidoparteList(attachedPedidoparteList);
            em.persist(pedidoproveedor);
            if (pedidoId != null) {
                pedidoId.getPedidoproveedorList().add(pedidoproveedor);
                pedidoId = em.merge(pedidoId);
            }
            if (proveedorId != null) {
                proveedorId.getPedidoproveedorList().add(pedidoproveedor);
                proveedorId = em.merge(proveedorId);
            }
            for (Pedidoparte pedidoparteListPedidoparte : pedidoproveedor.getPedidoparteList()) {
                Pedidoproveedor oldPedidoProveedoridOfPedidoparteListPedidoparte = pedidoparteListPedidoparte.getPedidoProveedorid();
                pedidoparteListPedidoparte.setPedidoProveedorid(pedidoproveedor);
                pedidoparteListPedidoparte = em.merge(pedidoparteListPedidoparte);
                if (oldPedidoProveedoridOfPedidoparteListPedidoparte != null) {
                    oldPedidoProveedoridOfPedidoparteListPedidoparte.getPedidoparteList().remove(pedidoparteListPedidoparte);
                    oldPedidoProveedoridOfPedidoparteListPedidoparte = em.merge(oldPedidoProveedoridOfPedidoparteListPedidoparte);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pedidoproveedor pedidoproveedor) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedidoproveedor persistentPedidoproveedor = em.find(Pedidoproveedor.class, pedidoproveedor.getId());
            Pedido pedidoIdOld = persistentPedidoproveedor.getPedidoId();
            Pedido pedidoIdNew = pedidoproveedor.getPedidoId();
            Proveedor proveedorIdOld = persistentPedidoproveedor.getProveedorId();
            Proveedor proveedorIdNew = pedidoproveedor.getProveedorId();
            List<Pedidoparte> pedidoparteListOld = persistentPedidoproveedor.getPedidoparteList();
            List<Pedidoparte> pedidoparteListNew = pedidoproveedor.getPedidoparteList();
            List<String> illegalOrphanMessages = null;
            for (Pedidoparte pedidoparteListOldPedidoparte : pedidoparteListOld) {
                if (!pedidoparteListNew.contains(pedidoparteListOldPedidoparte)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pedidoparte " + pedidoparteListOldPedidoparte + " since its pedidoProveedorid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (pedidoIdNew != null) {
                pedidoIdNew = em.getReference(pedidoIdNew.getClass(), pedidoIdNew.getId());
                pedidoproveedor.setPedidoId(pedidoIdNew);
            }
            if (proveedorIdNew != null) {
                proveedorIdNew = em.getReference(proveedorIdNew.getClass(), proveedorIdNew.getId());
                pedidoproveedor.setProveedorId(proveedorIdNew);
            }
            List<Pedidoparte> attachedPedidoparteListNew = new ArrayList<Pedidoparte>();
            for (Pedidoparte pedidoparteListNewPedidoparteToAttach : pedidoparteListNew) {
                pedidoparteListNewPedidoparteToAttach = em.getReference(pedidoparteListNewPedidoparteToAttach.getClass(), pedidoparteListNewPedidoparteToAttach.getId());
                attachedPedidoparteListNew.add(pedidoparteListNewPedidoparteToAttach);
            }
            pedidoparteListNew = attachedPedidoparteListNew;
            pedidoproveedor.setPedidoparteList(pedidoparteListNew);
            pedidoproveedor = em.merge(pedidoproveedor);
            if (pedidoIdOld != null && !pedidoIdOld.equals(pedidoIdNew)) {
                pedidoIdOld.getPedidoproveedorList().remove(pedidoproveedor);
                pedidoIdOld = em.merge(pedidoIdOld);
            }
            if (pedidoIdNew != null && !pedidoIdNew.equals(pedidoIdOld)) {
                pedidoIdNew.getPedidoproveedorList().add(pedidoproveedor);
                pedidoIdNew = em.merge(pedidoIdNew);
            }
            if (proveedorIdOld != null && !proveedorIdOld.equals(proveedorIdNew)) {
                proveedorIdOld.getPedidoproveedorList().remove(pedidoproveedor);
                proveedorIdOld = em.merge(proveedorIdOld);
            }
            if (proveedorIdNew != null && !proveedorIdNew.equals(proveedorIdOld)) {
                proveedorIdNew.getPedidoproveedorList().add(pedidoproveedor);
                proveedorIdNew = em.merge(proveedorIdNew);
            }
            for (Pedidoparte pedidoparteListNewPedidoparte : pedidoparteListNew) {
                if (!pedidoparteListOld.contains(pedidoparteListNewPedidoparte)) {
                    Pedidoproveedor oldPedidoProveedoridOfPedidoparteListNewPedidoparte = pedidoparteListNewPedidoparte.getPedidoProveedorid();
                    pedidoparteListNewPedidoparte.setPedidoProveedorid(pedidoproveedor);
                    pedidoparteListNewPedidoparte = em.merge(pedidoparteListNewPedidoparte);
                    if (oldPedidoProveedoridOfPedidoparteListNewPedidoparte != null && !oldPedidoProveedoridOfPedidoparteListNewPedidoparte.equals(pedidoproveedor)) {
                        oldPedidoProveedoridOfPedidoparteListNewPedidoparte.getPedidoparteList().remove(pedidoparteListNewPedidoparte);
                        oldPedidoProveedoridOfPedidoparteListNewPedidoparte = em.merge(oldPedidoProveedoridOfPedidoparteListNewPedidoparte);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pedidoproveedor.getId();
                if (findPedidoproveedor(id) == null) {
                    throw new NonexistentEntityException("The pedidoproveedor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedidoproveedor pedidoproveedor;
            try {
                pedidoproveedor = em.getReference(Pedidoproveedor.class, id);
                pedidoproveedor.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pedidoproveedor with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Pedidoparte> pedidoparteListOrphanCheck = pedidoproveedor.getPedidoparteList();
            for (Pedidoparte pedidoparteListOrphanCheckPedidoparte : pedidoparteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pedidoproveedor (" + pedidoproveedor + ") cannot be destroyed since the Pedidoparte " + pedidoparteListOrphanCheckPedidoparte + " in its pedidoparteList field has a non-nullable pedidoProveedorid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Pedido pedidoId = pedidoproveedor.getPedidoId();
            if (pedidoId != null) {
                pedidoId.getPedidoproveedorList().remove(pedidoproveedor);
                pedidoId = em.merge(pedidoId);
            }
            Proveedor proveedorId = pedidoproveedor.getProveedorId();
            if (proveedorId != null) {
                proveedorId.getPedidoproveedorList().remove(pedidoproveedor);
                proveedorId = em.merge(proveedorId);
            }
            em.remove(pedidoproveedor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pedidoproveedor> findPedidoproveedorEntities() {
        return findPedidoproveedorEntities(true, -1, -1);
    }

    public List<Pedidoproveedor> findPedidoproveedorEntities(int maxResults, int firstResult) {
        return findPedidoproveedorEntities(false, maxResults, firstResult);
    }

    private List<Pedidoproveedor> findPedidoproveedorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pedidoproveedor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pedidoproveedor findPedidoproveedor(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pedidoproveedor.class, id);
        } finally {
            em.close();
        }
    }

    public int getPedidoproveedorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pedidoproveedor> rt = cq.from(Pedidoproveedor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        public List<Proveedor> editorialesPedido(int idPedido) {
        EntityManager em = getEntityManager();
        List<Proveedor> proveedores = new ArrayList<Proveedor>();
        try {

            TypedQuery<Proveedor> resultList = em.createQuery("SELECT DISTINCT p FROM Proveedor p, PedidoHasProducto h WHERE h.pedido.id=:idPedido AND h.producto.proveedorId.id=p.id ", Proveedor.class);
            resultList.setParameter("idPedido", idPedido);
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
        return proveedores;
    }

    public int pedidoP(int idPedido, int idProveedor) {
        EntityManager em = getEntityManager();
        int valor = 0;
        try {

            TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Pedidoproveedor p WHERE p.pedidoId.id=:idPedido AND p.proveedorId.id=:idProveedor", Pedidoproveedor.class);
            resultList.setParameter("idPedido", idPedido);
            resultList.setParameter("idProveedor", idProveedor);
            if (resultList.getResultList().size() > 0) {
                valor = resultList.getSingleResult().getId();
            }
        } finally {
            em.close();
        }
        return valor;
    }

    public List<Pedidoproveedor> compraPendiente(int idSucursal) {
        EntityManager em = getEntityManager();
        List<Pedidoproveedor> ppv = new ArrayList<Pedidoproveedor>();
        try {
            if (idSucursal == 1) {
                TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Pedidoproveedor p WHERE p.estado=4 OR p.estado=5", Pedidoproveedor.class);
                ppv = resultList.getResultList();
            }else{
                TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Pedidoproveedor p WHERE (p.estado=4 OR p.estado=5) AND p.pedidoId.sucursalId.id=:idSucursal", Pedidoproveedor.class);
                resultList.setParameter("idSucursal", idSucursal);
                ppv = resultList.getResultList();
            }
        } finally {
            em.close();
        }
        return ppv;
    }

    public List<Pedidoproveedor> pedidoDevolucion(int idSucursal) {
        EntityManager em = getEntityManager();
        List<Pedidoproveedor> ppv = new ArrayList<Pedidoproveedor>();
        try {
            if (idSucursal == 1) {
                TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Pedidoproveedor p,Pedidoparte pa WHERE pa.devolucion>0 AND pa.pedidoProveedorid.id=p.id AND p.estado=5", Pedidoproveedor.class);
                ppv = resultList.getResultList();
            }else{
                TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Pedidoproveedor p,Pedidoparte pa WHERE pa.devolucion>0 AND pa.pedidoProveedorid.id=p.id AND p.pedidoId.sucursalId.id=:idSucursal AND p.estado=5", Pedidoproveedor.class);
                resultList.setParameter("idSucursal", idSucursal);
                ppv = resultList.getResultList();
            }
        } finally {
            em.close();
        }
        return ppv;
    }
    
    public List<Pedidoproveedor> listaPedidoProveedor(int idPedido) {
        EntityManager em = getEntityManager();
        List<Pedidoproveedor>pedidos=new ArrayList<Pedidoproveedor>();
        try {

            TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Pedidoproveedor p WHERE p.pedidoId.id=:idPedido", Pedidoproveedor.class);
            resultList.setParameter("idPedido", idPedido);
                pedidos = resultList.getResultList();
        } finally {
            em.close();
        }
        return pedidos;
    }
    public List<Pedidoproveedor> pedidoProveedores(int pagina) {
        EntityManager em = getEntityManager();
        List<Pedidoproveedor>pedidos=new ArrayList<Pedidoproveedor>();
        try {

            TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Pedidoproveedor p WHERE p.estado=1 OR p.estado=3", Pedidoproveedor.class).setFirstResult(pagina*20).setMaxResults(20);
                pedidos = resultList.getResultList();
        } finally {
            em.close();
        }
        return pedidos;
    }
    public int pedidoProveedoresCuenta() {
        EntityManager em = getEntityManager();
        int numero=0;
        try {
            TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Pedidoproveedor p WHERE (p.estado=1 OR p.estado=3) AND (p.pedidoId.estado=4 OR p.pedidoId.estado=5 OR p.pedidoId.estado=6 OR p.pedidoId.estado=7) ", Pedidoproveedor.class);
                numero = resultList.getResultList().size();
        } finally {
            em.close();
        }
        return numero;
    }
    public List<Pedidoproveedor> listaCompras() {
        EntityManager em = getEntityManager();
        List<Pedidoproveedor>pedidos=new ArrayList<Pedidoproveedor>();
        try {

            TypedQuery<Pedidoproveedor> resultList = em.createQuery("SELECT p FROM Pedidoproveedor p WHERE p.estado=2 AND p.proveedorId IS NOT NULL", Pedidoproveedor.class);
                pedidos = resultList.getResultList();
        } finally {
            em.close();
        }
        return pedidos;
    }
    
}
