/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Cliente;
import Modelo.Devolucion;
import Modelo.Sucursal;
import Modelo.Usuario;
import Modelo.Pedidoproveedor;
import java.util.ArrayList;
import java.util.List;
import Modelo.Remisionp;
import Modelo.Pago;
import Modelo.Pedido;
import Modelo.Pedidoparte;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.time.Instant;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class PedidoJpaController implements Serializable {

    public PedidoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pedido pedido) {
        if (pedido.getPedidoproveedorList() == null) {
            pedido.setPedidoproveedorList(new ArrayList<Pedidoproveedor>());
        }
        if (pedido.getRemisionpList() == null) {
            pedido.setRemisionpList(new ArrayList<Remisionp>());
        }
        if (pedido.getPagoList() == null) {
            pedido.setPagoList(new ArrayList<Pago>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cliente clienteId = pedido.getClienteId();
            if (clienteId != null) {
                clienteId = em.getReference(clienteId.getClass(), clienteId.getId());
                pedido.setClienteId(clienteId);
            }
            Devolucion devolucionId = pedido.getDevolucionId();
            if (devolucionId != null) {
                devolucionId = em.getReference(devolucionId.getClass(), devolucionId.getId());
                pedido.setDevolucionId(devolucionId);
            }
            Sucursal sucursalId = pedido.getSucursalId();
            if (sucursalId != null) {
                sucursalId = em.getReference(sucursalId.getClass(), sucursalId.getId());
                pedido.setSucursalId(sucursalId);
            }
            Usuario usuarioId = pedido.getUsuarioId();
            if (usuarioId != null) {
                usuarioId = em.getReference(usuarioId.getClass(), usuarioId.getId());
                pedido.setUsuarioId(usuarioId);
            }
            List<Pedidoproveedor> attachedPedidoproveedorList = new ArrayList<Pedidoproveedor>();
            for (Pedidoproveedor pedidoproveedorListPedidoproveedorToAttach : pedido.getPedidoproveedorList()) {
                pedidoproveedorListPedidoproveedorToAttach = em.getReference(pedidoproveedorListPedidoproveedorToAttach.getClass(), pedidoproveedorListPedidoproveedorToAttach.getId());
                attachedPedidoproveedorList.add(pedidoproveedorListPedidoproveedorToAttach);
            }
            pedido.setPedidoproveedorList(attachedPedidoproveedorList);
            List<Remisionp> attachedRemisionpList = new ArrayList<Remisionp>();
            for (Remisionp remisionpListRemisionpToAttach : pedido.getRemisionpList()) {
                remisionpListRemisionpToAttach = em.getReference(remisionpListRemisionpToAttach.getClass(), remisionpListRemisionpToAttach.getId());
                attachedRemisionpList.add(remisionpListRemisionpToAttach);
            }
            pedido.setRemisionpList(attachedRemisionpList);
            List<Pago> attachedPagoList = new ArrayList<Pago>();
            for (Pago pagoListPagoToAttach : pedido.getPagoList()) {
                pagoListPagoToAttach = em.getReference(pagoListPagoToAttach.getClass(), pagoListPagoToAttach.getId());
                attachedPagoList.add(pagoListPagoToAttach);
            }
            pedido.setPagoList(attachedPagoList);
            em.persist(pedido);
            if (clienteId != null) {
                clienteId.getPedidoList().add(pedido);
                clienteId = em.merge(clienteId);
            }
            if (devolucionId != null) {
                devolucionId.getPedidoList().add(pedido);
                devolucionId = em.merge(devolucionId);
            }
            if (sucursalId != null) {
                sucursalId.getPedidoList().add(pedido);
                sucursalId = em.merge(sucursalId);
            }
            if (usuarioId != null) {
                usuarioId.getPedidoList().add(pedido);
                usuarioId = em.merge(usuarioId);
            }
            for (Pedidoproveedor pedidoproveedorListPedidoproveedor : pedido.getPedidoproveedorList()) {
                Pedido oldPedidoIdOfPedidoproveedorListPedidoproveedor = pedidoproveedorListPedidoproveedor.getPedidoId();
                pedidoproveedorListPedidoproveedor.setPedidoId(pedido);
                pedidoproveedorListPedidoproveedor = em.merge(pedidoproveedorListPedidoproveedor);
                if (oldPedidoIdOfPedidoproveedorListPedidoproveedor != null) {
                    oldPedidoIdOfPedidoproveedorListPedidoproveedor.getPedidoproveedorList().remove(pedidoproveedorListPedidoproveedor);
                    oldPedidoIdOfPedidoproveedorListPedidoproveedor = em.merge(oldPedidoIdOfPedidoproveedorListPedidoproveedor);
                }
            }
            for (Remisionp remisionpListRemisionp : pedido.getRemisionpList()) {
                Pedido oldPedidoIdOfRemisionpListRemisionp = remisionpListRemisionp.getPedidoId();
                remisionpListRemisionp.setPedidoId(pedido);
                remisionpListRemisionp = em.merge(remisionpListRemisionp);
                if (oldPedidoIdOfRemisionpListRemisionp != null) {
                    oldPedidoIdOfRemisionpListRemisionp.getRemisionpList().remove(remisionpListRemisionp);
                    oldPedidoIdOfRemisionpListRemisionp = em.merge(oldPedidoIdOfRemisionpListRemisionp);
                }
            }
            for (Pago pagoListPago : pedido.getPagoList()) {
                Pedido oldPedidoIdOfPagoListPago = pagoListPago.getPedidoId();
                pagoListPago.setPedidoId(pedido);
                pagoListPago = em.merge(pagoListPago);
                if (oldPedidoIdOfPagoListPago != null) {
                    oldPedidoIdOfPagoListPago.getPagoList().remove(pagoListPago);
                    oldPedidoIdOfPagoListPago = em.merge(oldPedidoIdOfPagoListPago);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pedido pedido) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedido persistentPedido = em.find(Pedido.class, pedido.getId());
            Cliente clienteIdOld = persistentPedido.getClienteId();
            Cliente clienteIdNew = pedido.getClienteId();
            Devolucion devolucionIdOld = persistentPedido.getDevolucionId();
            Devolucion devolucionIdNew = pedido.getDevolucionId();
            Sucursal sucursalIdOld = persistentPedido.getSucursalId();
            Sucursal sucursalIdNew = pedido.getSucursalId();
            Usuario usuarioIdOld = persistentPedido.getUsuarioId();
            Usuario usuarioIdNew = pedido.getUsuarioId();
            List<Pedidoproveedor> pedidoproveedorListOld = persistentPedido.getPedidoproveedorList();
            List<Pedidoproveedor> pedidoproveedorListNew = pedido.getPedidoproveedorList();
            List<Remisionp> remisionpListOld = persistentPedido.getRemisionpList();
            List<Remisionp> remisionpListNew = pedido.getRemisionpList();
            List<Pago> pagoListOld = persistentPedido.getPagoList();
            List<Pago> pagoListNew = pedido.getPagoList();
            List<String> illegalOrphanMessages = null;
            for (Pedidoproveedor pedidoproveedorListOldPedidoproveedor : pedidoproveedorListOld) {
                if (!pedidoproveedorListNew.contains(pedidoproveedorListOldPedidoproveedor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pedidoproveedor " + pedidoproveedorListOldPedidoproveedor + " since its pedidoId field is not nullable.");
                }
            }
            for (Remisionp remisionpListOldRemisionp : remisionpListOld) {
                if (!remisionpListNew.contains(remisionpListOldRemisionp)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Remisionp " + remisionpListOldRemisionp + " since its pedidoId field is not nullable.");
                }
            }
            for (Pago pagoListOldPago : pagoListOld) {
                if (!pagoListNew.contains(pagoListOldPago)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pago " + pagoListOldPago + " since its pedidoId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (clienteIdNew != null) {
                clienteIdNew = em.getReference(clienteIdNew.getClass(), clienteIdNew.getId());
                pedido.setClienteId(clienteIdNew);
            }
            if (devolucionIdNew != null) {
                devolucionIdNew = em.getReference(devolucionIdNew.getClass(), devolucionIdNew.getId());
                pedido.setDevolucionId(devolucionIdNew);
            }
            if (sucursalIdNew != null) {
                sucursalIdNew = em.getReference(sucursalIdNew.getClass(), sucursalIdNew.getId());
                pedido.setSucursalId(sucursalIdNew);
            }
            if (usuarioIdNew != null) {
                usuarioIdNew = em.getReference(usuarioIdNew.getClass(), usuarioIdNew.getId());
                pedido.setUsuarioId(usuarioIdNew);
            }
            List<Pedidoproveedor> attachedPedidoproveedorListNew = new ArrayList<Pedidoproveedor>();
            for (Pedidoproveedor pedidoproveedorListNewPedidoproveedorToAttach : pedidoproveedorListNew) {
                pedidoproveedorListNewPedidoproveedorToAttach = em.getReference(pedidoproveedorListNewPedidoproveedorToAttach.getClass(), pedidoproveedorListNewPedidoproveedorToAttach.getId());
                attachedPedidoproveedorListNew.add(pedidoproveedorListNewPedidoproveedorToAttach);
            }
            pedidoproveedorListNew = attachedPedidoproveedorListNew;
            pedido.setPedidoproveedorList(pedidoproveedorListNew);
            List<Remisionp> attachedRemisionpListNew = new ArrayList<Remisionp>();
            for (Remisionp remisionpListNewRemisionpToAttach : remisionpListNew) {
                remisionpListNewRemisionpToAttach = em.getReference(remisionpListNewRemisionpToAttach.getClass(), remisionpListNewRemisionpToAttach.getId());
                attachedRemisionpListNew.add(remisionpListNewRemisionpToAttach);
            }
            remisionpListNew = attachedRemisionpListNew;
            pedido.setRemisionpList(remisionpListNew);
            List<Pago> attachedPagoListNew = new ArrayList<Pago>();
            for (Pago pagoListNewPagoToAttach : pagoListNew) {
                pagoListNewPagoToAttach = em.getReference(pagoListNewPagoToAttach.getClass(), pagoListNewPagoToAttach.getId());
                attachedPagoListNew.add(pagoListNewPagoToAttach);
            }
            pagoListNew = attachedPagoListNew;
            pedido.setPagoList(pagoListNew);
            pedido = em.merge(pedido);
            if (clienteIdOld != null && !clienteIdOld.equals(clienteIdNew)) {
                clienteIdOld.getPedidoList().remove(pedido);
                clienteIdOld = em.merge(clienteIdOld);
            }
            if (clienteIdNew != null && !clienteIdNew.equals(clienteIdOld)) {
                clienteIdNew.getPedidoList().add(pedido);
                clienteIdNew = em.merge(clienteIdNew);
            }
            if (devolucionIdOld != null && !devolucionIdOld.equals(devolucionIdNew)) {
                devolucionIdOld.getPedidoList().remove(pedido);
                devolucionIdOld = em.merge(devolucionIdOld);
            }
            if (devolucionIdNew != null && !devolucionIdNew.equals(devolucionIdOld)) {
                devolucionIdNew.getPedidoList().add(pedido);
                devolucionIdNew = em.merge(devolucionIdNew);
            }
            if (sucursalIdOld != null && !sucursalIdOld.equals(sucursalIdNew)) {
                sucursalIdOld.getPedidoList().remove(pedido);
                sucursalIdOld = em.merge(sucursalIdOld);
            }
            if (sucursalIdNew != null && !sucursalIdNew.equals(sucursalIdOld)) {
                sucursalIdNew.getPedidoList().add(pedido);
                sucursalIdNew = em.merge(sucursalIdNew);
            }
            if (usuarioIdOld != null && !usuarioIdOld.equals(usuarioIdNew)) {
                usuarioIdOld.getPedidoList().remove(pedido);
                usuarioIdOld = em.merge(usuarioIdOld);
            }
            if (usuarioIdNew != null && !usuarioIdNew.equals(usuarioIdOld)) {
                usuarioIdNew.getPedidoList().add(pedido);
                usuarioIdNew = em.merge(usuarioIdNew);
            }
            for (Pedidoproveedor pedidoproveedorListNewPedidoproveedor : pedidoproveedorListNew) {
                if (!pedidoproveedorListOld.contains(pedidoproveedorListNewPedidoproveedor)) {
                    Pedido oldPedidoIdOfPedidoproveedorListNewPedidoproveedor = pedidoproveedorListNewPedidoproveedor.getPedidoId();
                    pedidoproveedorListNewPedidoproveedor.setPedidoId(pedido);
                    pedidoproveedorListNewPedidoproveedor = em.merge(pedidoproveedorListNewPedidoproveedor);
                    if (oldPedidoIdOfPedidoproveedorListNewPedidoproveedor != null && !oldPedidoIdOfPedidoproveedorListNewPedidoproveedor.equals(pedido)) {
                        oldPedidoIdOfPedidoproveedorListNewPedidoproveedor.getPedidoproveedorList().remove(pedidoproveedorListNewPedidoproveedor);
                        oldPedidoIdOfPedidoproveedorListNewPedidoproveedor = em.merge(oldPedidoIdOfPedidoproveedorListNewPedidoproveedor);
                    }
                }
            }
            for (Remisionp remisionpListNewRemisionp : remisionpListNew) {
                if (!remisionpListOld.contains(remisionpListNewRemisionp)) {
                    Pedido oldPedidoIdOfRemisionpListNewRemisionp = remisionpListNewRemisionp.getPedidoId();
                    remisionpListNewRemisionp.setPedidoId(pedido);
                    remisionpListNewRemisionp = em.merge(remisionpListNewRemisionp);
                    if (oldPedidoIdOfRemisionpListNewRemisionp != null && !oldPedidoIdOfRemisionpListNewRemisionp.equals(pedido)) {
                        oldPedidoIdOfRemisionpListNewRemisionp.getRemisionpList().remove(remisionpListNewRemisionp);
                        oldPedidoIdOfRemisionpListNewRemisionp = em.merge(oldPedidoIdOfRemisionpListNewRemisionp);
                    }
                }
            }
            for (Pago pagoListNewPago : pagoListNew) {
                if (!pagoListOld.contains(pagoListNewPago)) {
                    Pedido oldPedidoIdOfPagoListNewPago = pagoListNewPago.getPedidoId();
                    pagoListNewPago.setPedidoId(pedido);
                    pagoListNewPago = em.merge(pagoListNewPago);
                    if (oldPedidoIdOfPagoListNewPago != null && !oldPedidoIdOfPagoListNewPago.equals(pedido)) {
                        oldPedidoIdOfPagoListNewPago.getPagoList().remove(pagoListNewPago);
                        oldPedidoIdOfPagoListNewPago = em.merge(oldPedidoIdOfPagoListNewPago);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pedido.getId();
                if (findPedido(id) == null) {
                    throw new NonexistentEntityException("The pedido with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pedido pedido;
            try {
                pedido = em.getReference(Pedido.class, id);
                pedido.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pedido with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Pedidoproveedor> pedidoproveedorListOrphanCheck = pedido.getPedidoproveedorList();
            for (Pedidoproveedor pedidoproveedorListOrphanCheckPedidoproveedor : pedidoproveedorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pedido (" + pedido + ") cannot be destroyed since the Pedidoproveedor " + pedidoproveedorListOrphanCheckPedidoproveedor + " in its pedidoproveedorList field has a non-nullable pedidoId field.");
            }
            List<Remisionp> remisionpListOrphanCheck = pedido.getRemisionpList();
            for (Remisionp remisionpListOrphanCheckRemisionp : remisionpListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pedido (" + pedido + ") cannot be destroyed since the Remisionp " + remisionpListOrphanCheckRemisionp + " in its remisionpList field has a non-nullable pedidoId field.");
            }
            List<Pago> pagoListOrphanCheck = pedido.getPagoList();
            for (Pago pagoListOrphanCheckPago : pagoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pedido (" + pedido + ") cannot be destroyed since the Pago " + pagoListOrphanCheckPago + " in its pagoList field has a non-nullable pedidoId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cliente clienteId = pedido.getClienteId();
            if (clienteId != null) {
                clienteId.getPedidoList().remove(pedido);
                clienteId = em.merge(clienteId);
            }
            Devolucion devolucionId = pedido.getDevolucionId();
            if (devolucionId != null) {
                devolucionId.getPedidoList().remove(pedido);
                devolucionId = em.merge(devolucionId);
            }
            Sucursal sucursalId = pedido.getSucursalId();
            if (sucursalId != null) {
                sucursalId.getPedidoList().remove(pedido);
                sucursalId = em.merge(sucursalId);
            }
            Usuario usuarioId = pedido.getUsuarioId();
            if (usuarioId != null) {
                usuarioId.getPedidoList().remove(pedido);
                usuarioId = em.merge(usuarioId);
            }
            em.remove(pedido);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pedido> findPedidoEntities() {
        return findPedidoEntities(true, -1, -1);
    }

    public List<Pedido> findPedidoEntities(int maxResults, int firstResult) {
        return findPedidoEntities(false, maxResults, firstResult);
    }

    private List<Pedido> findPedidoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pedido.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pedido findPedido(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pedido.class, id);
        } finally {
            em.close();
        }
    }

    public int getPedidoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pedido> rt = cq.from(Pedido.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
         public List<Pedido> pedidoSucursal(int idSucursal, String objeto, int finalizado) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();

        try {
            if (idSucursal == 1) {
                TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto OR p.id LIKE :objeto) AND ORDER BY p.estado ASC", Pedido.class);
                resultList.setParameter("objeto", "%" + objeto + "%");
                pedidos = resultList.getResultList();

            } else {

                TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto OR p.id LIKE :objeto) AND p.sucursalId.id=:idSucursal AND (p.estado=1 OR p.estado=2 OR p.estado=3 OR p.estado=4 OR p.estado=5 OR p.estado=6 OR p.estado=7 OR p.estado=8) ORDER BY p.estado ASC", Pedido.class);
                resultList.setParameter("idSucursal", idSucursal);
                resultList.setParameter("objeto", "%" + objeto + "%");
                pedidos = resultList.getResultList();
            }
        } finally {
            em.close();
        }
        return pedidos;
    }

    public double pedidosMes(int idSucursal) {
        EntityManager em = getEntityManager();
        double valor = 0;
        try {
            TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.sucursalId.id =:idSucursal AND p.estado NOT LIKE '13' AND p.fecha >= :mesActual", Pedido.class);
            resultList.setParameter("idSucursal", idSucursal);
            Date date = Date.from(Instant.now());
            date.setDate(1);
            resultList.setParameter("mesActual", date, TemporalType.DATE);

            for (Pedido vt : resultList.getResultList()) {
                for (Pedidoproveedor ppv : vt.getPedidoproveedorList()) {
                    for (Pedidoparte pd : ppv.getPedidoparteList()) {
                        valor += pd.getCantidad();
                    }
                }
            }

        } finally {
            em.close();
        }
        return valor;
    }

    public double gananciasPedidosMes(int idSucursal) {
        EntityManager em = getEntityManager();
        double valor = 0;
        try {
            TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.sucursalId.id =:idSucursal AND p.estado NOT LIKE '13' AND p.fecha >= :mesActual", Pedido.class);
            resultList.setParameter("idSucursal", idSucursal);
            Date date = Date.from(Instant.now());
            date.setDate(1);
            resultList.setParameter("mesActual", date, TemporalType.DATE);

            for (Pedido vt : resultList.getResultList()) {
                for (Pedidoproveedor ppv : vt.getPedidoproveedorList()) {
                    for (Pedidoparte pd : ppv.getPedidoparteList()) {
                        valor += pd.getCantidad() * (Double.parseDouble(pd.getPrecio()) - Double.parseDouble(pd.getPrecio()));
                    }
                }
            }

        } finally {
            em.close();
        }
        return valor;
    }

    public List<Pedido> devolucionesList(int idSucursal) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        try {
            if (idSucursal == 1) {
                TypedQuery<Pedido> resultList = em.createQuery("SELECT DISTINCT p FROM Pedido p, PedidoHasProducto ph WHERE ph.devolucion>0  AND p.id=ph.pedido.id", Pedido.class);
                pedidos = resultList.getResultList();
            } else {

                TypedQuery<Pedido> resultList = em.createQuery("SELECT DISTINCT p FROM Pedido p, PedidoHasProducto ph WHERE ph.pedido.sucursalId.id =:idSucursal AND ph.devolucion>0  AND p.id=ph.pedido.id", Pedido.class);
                resultList.setParameter("idSucursal", idSucursal);
                pedidos = resultList.getResultList();
            }

        } finally {
            em.close();
        }
        return pedidos;
    }

    public List<Pedido> pendientes(int idSucursal) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        try {
            if (idSucursal == 1) {
                TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.estado NOT LIKE 14 AND p.estado NOT LIKE 15 ORDER BY p.entrega ASC ", Pedido.class);
                pedidos = resultList.getResultList();
            } else {
                TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.sucursalId.id =:idSucursal AND p.estado NOT LIKE 14 AND p.estado NOT LIKE 15 ORDER BY p.entrega ASC ", Pedido.class);
                resultList.setParameter("idSucursal", idSucursal);
                pedidos = resultList.getResultList();
            }
        } finally {
            em.close();
        }
        return pedidos;
    }

    public List<Pedido> pedidosCliente(int idCliente) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        try {

            TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.clienteId.id =:idCliente", Pedido.class);
            resultList.setParameter("idCliente", idCliente);
            pedidos = resultList.getResultList();

        } finally {
            em.close();
        }
        return pedidos;
    }

    public List<Pedido> pedidosClienteMuestra(int idCliente) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        try {

            TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.clienteId.id =:idCliente AND (p.estado='11' OR p.estado='12' OR p.estado='13' OR p.estado='14')", Pedido.class).setFirstResult(0).setMaxResults(30);
            resultList.setParameter("idCliente", idCliente);
            pedidos = resultList.getResultList();

        } finally {
            em.close();
        }
        return pedidos;
    }

    public List<Pedido> busquedaPedido(String objeto, int finalizados, int idSucursal, int addm, int idUsuario, int pagina) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        try {
            if (finalizados == 1) {
                if (idSucursal == 1) {
                    if (addm == 1) {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id", Pedido.class).setFirstResult(pagina * 20).setMaxResults(20);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        pedidos = resultList.getResultList();
                    } else {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.usuarioId.id=:idUsuario", Pedido.class).setFirstResult(pagina * 20).setMaxResults(20);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idUsuario", idUsuario);
                        pedidos = resultList.getResultList();
                    }
                } else {
                    if (addm == 1) {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.sucursalId.id=:idSucursal", Pedido.class).setFirstResult(pagina * 20).setMaxResults(20);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idSucursal", idSucursal);
                        pedidos = resultList.getResultList();
                    } else {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.sucursalId.id=:idSucursal AND p.usuarioId.id=:idUsuario", Pedido.class).setFirstResult(pagina * 20).setMaxResults(20);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idSucursal", idSucursal);
                        resultList.setParameter("idUsuario", idUsuario);
                        pedidos = resultList.getResultList();
                    }
                }
            } else {
                if (idSucursal == 1) {
                    if (addm == 1) {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.estado NOT LIKE '14' AND p.estado NOT LIKE '15'", Pedido.class).setFirstResult(pagina * 20).setMaxResults(20);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        pedidos = resultList.getResultList();
                    } else {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.estado NOT LIKE '14' AND p.estado NOT LIKE '15' AND p.usuarioId.id=:idUsuario", Pedido.class).setFirstResult(pagina * 20).setMaxResults(20);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idUsuario", idUsuario);
                        pedidos = resultList.getResultList();
                    }
                } else {
                    if (addm == 1) {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT DISTINCT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND p.estado NOT LIKE '14' AND p.estado NOT LIKE '15' AND p.sucursalId.id=:idSucursal", Pedido.class).setFirstResult(pagina * 20).setMaxResults(20);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idSucursal", idSucursal);
                        pedidos = resultList.getResultList();
                    } else {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.estado NOT LIKE '14' AND p.estado NOT LIKE '15' AND p.sucursalId.id=:idSucursal AND p.usuarioId.id=:idUsuario", Pedido.class).setFirstResult(pagina * 20).setMaxResults(20);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idSucursal", idSucursal);
                        resultList.setParameter("idUsuario", idUsuario);
                        pedidos = resultList.getResultList();
                    }
                }

            }

        } finally {
            em.close();
        }
        return pedidos;
    }

    public int numeroBusquedaPedido(String objeto, int finalizados, int idSucursal, int addm, int idUsuario) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        try {
            if (finalizados == 1) {
                if (idSucursal == 1) {
                    if (addm == 1) {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id", Pedido.class);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        pedidos = resultList.getResultList();
                    } else {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.usuarioId.id=:idUsuario", Pedido.class);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idUsuario", idUsuario);
                        pedidos = resultList.getResultList();
                    }
                } else {
                    if (addm == 1) {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.sucursalId.id=:idSucursal", Pedido.class);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idSucursal", idSucursal);
                        pedidos = resultList.getResultList();
                    } else {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.sucursalId.id=:idSucursal AND p.usuarioId.id=:idUsuario", Pedido.class);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idSucursal", idSucursal);
                        resultList.setParameter("idUsuario", idUsuario);
                        pedidos = resultList.getResultList();
                    }
                }
            } else {
                if (idSucursal == 1) {
                    if (addm == 1) {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.estado NOT LIKE '14' AND p.estado NOT LIKE '15'", Pedido.class);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        pedidos = resultList.getResultList();
                    } else {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND p.estado NOT LIKE '14' AND p.estado NOT LIKE '15' AND p.usuarioId.id=:idUsuario", Pedido.class);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idUsuario", idUsuario);
                        pedidos = resultList.getResultList();
                    }
                } else {
                    if (addm == 1) {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND (p.estado NOT LIKE '14' OR p.estado NOT LIKE '15') AND p.sucursalId.id=:idSucursal", Pedido.class);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idSucursal", idSucursal);
                        pedidos = resultList.getResultList();
                    } else {
                        TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p,Cliente c WHERE (c.nombre LIKE :objeto OR c.escuela LIKE :objeto) AND c.id=p.clienteId.id AND (p.estado NOT LIKE '14' OR p.estado NOT LIKE '15') AND p.sucursalId.id=:idSucursal AND p.usuarioId.id=:idUsuario", Pedido.class);
                        resultList.setParameter("objeto", "%" + objeto + "%");
                        resultList.setParameter("idSucursal", idSucursal);
                        resultList.setParameter("idUsuario", idUsuario);
                        pedidos = resultList.getResultList();
                    }
                }

            }

        } finally {
            em.close();
        }
        return pedidos.size();
    }

    public List<Pedido> pedidoCompras(int idSucursal) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();

        try {
            if (idSucursal == 1) {
                TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.estado=1 ORDER BY p.fecha ASC", Pedido.class);
                pedidos = resultList.getResultList();

            } else {

                TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.sucursalId.id =:idSucursal AND p.estado=1 ORDER BY c.fecha ASC ", Pedido.class);
                resultList.setParameter("idSucursal", idSucursal);
                pedidos = resultList.getResultList();
            }
        } finally {
            em.close();
        }
        return pedidos;
    }

    public List<Pedido> pedidoBodega(int idSucursal) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();

        try {
            TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.sucursalId.id =:idSucursal AND p.estado=10 ", Pedido.class);
            resultList.setParameter("idSucursal", idSucursal);
            pedidos = resultList.getResultList();
        } finally {
            em.close();
        }
        return pedidos;
    }

    public int pedidosUsuario(int idUsuario) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();

        try {
            TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.usuarioId.id=:idUsuario AND p.estado=11 ", Pedido.class);
            resultList.setParameter("idUsuario", idUsuario);
            pedidos = resultList.getResultList();
        } finally {
            em.close();
        }
        return pedidos.size();
    }

    public int pedidosNuevosCuenta(int idSucursal) {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();

        try {
            String query = "";
            if (idSucursal == 1) {
                query = "SELECT p FROM Pedido p WHERE p.estado=1 ";
            } else {
                query = "SELECT p FROM Pedido p WHERE p.sucursalId.id=:idSucursal AND p.estado=1 ";
            }
            TypedQuery<Pedido> resultList = em.createQuery(query, Pedido.class);
            if (idSucursal != 1) {
                resultList.setParameter("idSucursal", idSucursal);
            }
            pedidos = resultList.getResultList();
        } finally {
            em.close();
        }
        return pedidos.size();
    }
    
    public List<Pedido> pedidosAlmacen() {
        EntityManager em = getEntityManager();
        List<Pedido> pedidos = new ArrayList<Pedido>();

        try {
            TypedQuery<Pedido> resultList = em.createQuery("SELECT p FROM Pedido p WHERE p.estado=7 OR p.estado=8 ", Pedido.class);
            pedidos = resultList.getResultList();
        } finally {
            em.close();
        }
        return pedidos;
    }
    
}
