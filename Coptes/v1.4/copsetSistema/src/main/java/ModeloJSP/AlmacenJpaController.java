/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import Modelo.Almacen;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Producto;
import Modelo.Sucursal;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class AlmacenJpaController implements Serializable {

    public AlmacenJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Almacen almacen) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto productoId = almacen.getProductoId();
            if (productoId != null) {
                productoId = em.getReference(productoId.getClass(), productoId.getId());
                almacen.setProductoId(productoId);
            }
            Sucursal sucursalId = almacen.getSucursalId();
            if (sucursalId != null) {
                sucursalId = em.getReference(sucursalId.getClass(), sucursalId.getId());
                almacen.setSucursalId(sucursalId);
            }
            em.persist(almacen);
            if (productoId != null) {
                productoId.getAlmacenList().add(almacen);
                productoId = em.merge(productoId);
            }
            if (sucursalId != null) {
                sucursalId.getAlmacenList().add(almacen);
                sucursalId = em.merge(sucursalId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Almacen almacen) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Almacen persistentAlmacen = em.find(Almacen.class, almacen.getId());
            Producto productoIdOld = persistentAlmacen.getProductoId();
            Producto productoIdNew = almacen.getProductoId();
            Sucursal sucursalIdOld = persistentAlmacen.getSucursalId();
            Sucursal sucursalIdNew = almacen.getSucursalId();
            if (productoIdNew != null) {
                productoIdNew = em.getReference(productoIdNew.getClass(), productoIdNew.getId());
                almacen.setProductoId(productoIdNew);
            }
            if (sucursalIdNew != null) {
                sucursalIdNew = em.getReference(sucursalIdNew.getClass(), sucursalIdNew.getId());
                almacen.setSucursalId(sucursalIdNew);
            }
            almacen = em.merge(almacen);
            if (productoIdOld != null && !productoIdOld.equals(productoIdNew)) {
                productoIdOld.getAlmacenList().remove(almacen);
                productoIdOld = em.merge(productoIdOld);
            }
            if (productoIdNew != null && !productoIdNew.equals(productoIdOld)) {
                productoIdNew.getAlmacenList().add(almacen);
                productoIdNew = em.merge(productoIdNew);
            }
            if (sucursalIdOld != null && !sucursalIdOld.equals(sucursalIdNew)) {
                sucursalIdOld.getAlmacenList().remove(almacen);
                sucursalIdOld = em.merge(sucursalIdOld);
            }
            if (sucursalIdNew != null && !sucursalIdNew.equals(sucursalIdOld)) {
                sucursalIdNew.getAlmacenList().add(almacen);
                sucursalIdNew = em.merge(sucursalIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = almacen.getId();
                if (findAlmacen(id) == null) {
                    throw new NonexistentEntityException("The almacen with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Almacen almacen;
            try {
                almacen = em.getReference(Almacen.class, id);
                almacen.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The almacen with id " + id + " no longer exists.", enfe);
            }
            Producto productoId = almacen.getProductoId();
            if (productoId != null) {
                productoId.getAlmacenList().remove(almacen);
                productoId = em.merge(productoId);
            }
            Sucursal sucursalId = almacen.getSucursalId();
            if (sucursalId != null) {
                sucursalId.getAlmacenList().remove(almacen);
                sucursalId = em.merge(sucursalId);
            }
            em.remove(almacen);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Almacen> findAlmacenEntities() {
        return findAlmacenEntities(true, -1, -1);
    }

    public List<Almacen> findAlmacenEntities(int maxResults, int firstResult) {
        return findAlmacenEntities(false, maxResults, firstResult);
    }

    private List<Almacen> findAlmacenEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Almacen.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Almacen findAlmacen(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Almacen.class, id);
        } finally {
            em.close();
        }
    }

    public int getAlmacenCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Almacen> rt = cq.from(Almacen.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public int productosAlmacen(int idProducto, int idSucursal) {
        int cantidad = 0;
        EntityManager em = getEntityManager();
        try {

            TypedQuery<Almacen> resultList = em.createQuery("SELECT a FROM Almacen a WHERE a.productoId.id=:idProducto AND a.sucursalId.id=:idSucursal", Almacen.class);
            resultList.setParameter("idProducto", idProducto);
            resultList.setParameter("idSucursal", idSucursal);
            if (resultList.getResultList().size() == 1) {

                Almacen almacen = resultList.getSingleResult();
                cantidad = almacen.getCantidad();
            }

        } finally {
            em.close();
        }
        return cantidad;
    }

    public int obtenerAlmacen(int idProducto, int idSucursal) {
        int idAlmacen = 0;
        EntityManager em = getEntityManager();
        try {

            TypedQuery<Almacen> resultList = em.createQuery("SELECT a FROM Almacen a WHERE a.productoId.id=:idProducto AND a.sucursalId.id=:idSucursal", Almacen.class);
            resultList.setParameter("idProducto", idProducto);
            resultList.setParameter("idSucursal", idSucursal);
            if (resultList.getResultList().size() == 1) {

                Almacen almacen = resultList.getSingleResult();
                idAlmacen = almacen.getId();
            }
        } finally {
            em.close();
        }
        return idAlmacen;
    }

    public List<Almacen> listaAlmacen(int idSucursal) {
        EntityManager em = getEntityManager();
        List<Almacen>almacenes=new ArrayList<Almacen>();
        try {

            TypedQuery<Almacen> resultList = em.createQuery("SELECT a FROM Almacen a WHERE a.sucursalId.id=:idSucursal", Almacen.class);
            resultList.setParameter("idSucursal", idSucursal);
                almacenes = resultList.getResultList();
        } finally {
            em.close();
        }
        return almacenes;
    }
    
}
