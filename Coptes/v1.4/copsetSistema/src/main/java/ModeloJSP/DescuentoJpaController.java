/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import Modelo.Descuento;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Proveedoreditorial;
import ModeloJSP.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author fernando
 */
public class DescuentoJpaController implements Serializable {

    public DescuentoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Descuento descuento) {
        if (descuento.getProveedoreditorialList() == null) {
            descuento.setProveedoreditorialList(new ArrayList<Proveedoreditorial>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Proveedoreditorial> attachedProveedoreditorialList = new ArrayList<Proveedoreditorial>();
            for (Proveedoreditorial proveedoreditorialListProveedoreditorialToAttach : descuento.getProveedoreditorialList()) {
                proveedoreditorialListProveedoreditorialToAttach = em.getReference(proveedoreditorialListProveedoreditorialToAttach.getClass(), proveedoreditorialListProveedoreditorialToAttach.getId());
                attachedProveedoreditorialList.add(proveedoreditorialListProveedoreditorialToAttach);
            }
            descuento.setProveedoreditorialList(attachedProveedoreditorialList);
            em.persist(descuento);
            for (Proveedoreditorial proveedoreditorialListProveedoreditorial : descuento.getProveedoreditorialList()) {
                Descuento oldDescuentoIdOfProveedoreditorialListProveedoreditorial = proveedoreditorialListProveedoreditorial.getDescuentoId();
                proveedoreditorialListProveedoreditorial.setDescuentoId(descuento);
                proveedoreditorialListProveedoreditorial = em.merge(proveedoreditorialListProveedoreditorial);
                if (oldDescuentoIdOfProveedoreditorialListProveedoreditorial != null) {
                    oldDescuentoIdOfProveedoreditorialListProveedoreditorial.getProveedoreditorialList().remove(proveedoreditorialListProveedoreditorial);
                    oldDescuentoIdOfProveedoreditorialListProveedoreditorial = em.merge(oldDescuentoIdOfProveedoreditorialListProveedoreditorial);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Descuento descuento) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Descuento persistentDescuento = em.find(Descuento.class, descuento.getId());
            List<Proveedoreditorial> proveedoreditorialListOld = persistentDescuento.getProveedoreditorialList();
            List<Proveedoreditorial> proveedoreditorialListNew = descuento.getProveedoreditorialList();
            List<Proveedoreditorial> attachedProveedoreditorialListNew = new ArrayList<Proveedoreditorial>();
            for (Proveedoreditorial proveedoreditorialListNewProveedoreditorialToAttach : proveedoreditorialListNew) {
                proveedoreditorialListNewProveedoreditorialToAttach = em.getReference(proveedoreditorialListNewProveedoreditorialToAttach.getClass(), proveedoreditorialListNewProveedoreditorialToAttach.getId());
                attachedProveedoreditorialListNew.add(proveedoreditorialListNewProveedoreditorialToAttach);
            }
            proveedoreditorialListNew = attachedProveedoreditorialListNew;
            descuento.setProveedoreditorialList(proveedoreditorialListNew);
            descuento = em.merge(descuento);
            for (Proveedoreditorial proveedoreditorialListOldProveedoreditorial : proveedoreditorialListOld) {
                if (!proveedoreditorialListNew.contains(proveedoreditorialListOldProveedoreditorial)) {
                    proveedoreditorialListOldProveedoreditorial.setDescuentoId(null);
                    proveedoreditorialListOldProveedoreditorial = em.merge(proveedoreditorialListOldProveedoreditorial);
                }
            }
            for (Proveedoreditorial proveedoreditorialListNewProveedoreditorial : proveedoreditorialListNew) {
                if (!proveedoreditorialListOld.contains(proveedoreditorialListNewProveedoreditorial)) {
                    Descuento oldDescuentoIdOfProveedoreditorialListNewProveedoreditorial = proveedoreditorialListNewProveedoreditorial.getDescuentoId();
                    proveedoreditorialListNewProveedoreditorial.setDescuentoId(descuento);
                    proveedoreditorialListNewProveedoreditorial = em.merge(proveedoreditorialListNewProveedoreditorial);
                    if (oldDescuentoIdOfProveedoreditorialListNewProveedoreditorial != null && !oldDescuentoIdOfProveedoreditorialListNewProveedoreditorial.equals(descuento)) {
                        oldDescuentoIdOfProveedoreditorialListNewProveedoreditorial.getProveedoreditorialList().remove(proveedoreditorialListNewProveedoreditorial);
                        oldDescuentoIdOfProveedoreditorialListNewProveedoreditorial = em.merge(oldDescuentoIdOfProveedoreditorialListNewProveedoreditorial);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = descuento.getId();
                if (findDescuento(id) == null) {
                    throw new NonexistentEntityException("The descuento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Descuento descuento;
            try {
                descuento = em.getReference(Descuento.class, id);
                descuento.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The descuento with id " + id + " no longer exists.", enfe);
            }
            List<Proveedoreditorial> proveedoreditorialList = descuento.getProveedoreditorialList();
            for (Proveedoreditorial proveedoreditorialListProveedoreditorial : proveedoreditorialList) {
                proveedoreditorialListProveedoreditorial.setDescuentoId(null);
                proveedoreditorialListProveedoreditorial = em.merge(proveedoreditorialListProveedoreditorial);
            }
            em.remove(descuento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Descuento> findDescuentoEntities() {
        return findDescuentoEntities(true, -1, -1);
    }

    public List<Descuento> findDescuentoEntities(int maxResults, int firstResult) {
        return findDescuentoEntities(false, maxResults, firstResult);
    }

    private List<Descuento> findDescuentoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Descuento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Descuento findDescuento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Descuento.class, id);
        } finally {
            em.close();
        }
    }

    public int getDescuentoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Descuento> rt = cq.from(Descuento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
