/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Editorial;
import Modelo.Nivel;
import Modelo.Pedidoparte;
import java.util.ArrayList;
import java.util.List;
import Modelo.VentasHasProducto;
import Modelo.Almacen;
import Modelo.Producto;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class ProductoJpaController implements Serializable {

    public ProductoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Producto producto) {
        if (producto.getPedidoparteList() == null) {
            producto.setPedidoparteList(new ArrayList<Pedidoparte>());
        }
        if (producto.getVentasHasProductoList() == null) {
            producto.setVentasHasProductoList(new ArrayList<VentasHasProducto>());
        }
        if (producto.getAlmacenList() == null) {
            producto.setAlmacenList(new ArrayList<Almacen>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Editorial editorialId1 = producto.getEditorialId1();
            if (editorialId1 != null) {
                editorialId1 = em.getReference(editorialId1.getClass(), editorialId1.getId());
                producto.setEditorialId1(editorialId1);
            }
            Nivel nivelId = producto.getNivelId();
            if (nivelId != null) {
                nivelId = em.getReference(nivelId.getClass(), nivelId.getId());
                producto.setNivelId(nivelId);
            }
            List<Pedidoparte> attachedPedidoparteList = new ArrayList<Pedidoparte>();
            for (Pedidoparte pedidoparteListPedidoparteToAttach : producto.getPedidoparteList()) {
                pedidoparteListPedidoparteToAttach = em.getReference(pedidoparteListPedidoparteToAttach.getClass(), pedidoparteListPedidoparteToAttach.getId());
                attachedPedidoparteList.add(pedidoparteListPedidoparteToAttach);
            }
            producto.setPedidoparteList(attachedPedidoparteList);
            List<VentasHasProducto> attachedVentasHasProductoList = new ArrayList<VentasHasProducto>();
            for (VentasHasProducto ventasHasProductoListVentasHasProductoToAttach : producto.getVentasHasProductoList()) {
                ventasHasProductoListVentasHasProductoToAttach = em.getReference(ventasHasProductoListVentasHasProductoToAttach.getClass(), ventasHasProductoListVentasHasProductoToAttach.getVentasHasProductoPK());
                attachedVentasHasProductoList.add(ventasHasProductoListVentasHasProductoToAttach);
            }
            producto.setVentasHasProductoList(attachedVentasHasProductoList);
            List<Almacen> attachedAlmacenList = new ArrayList<Almacen>();
            for (Almacen almacenListAlmacenToAttach : producto.getAlmacenList()) {
                almacenListAlmacenToAttach = em.getReference(almacenListAlmacenToAttach.getClass(), almacenListAlmacenToAttach.getId());
                attachedAlmacenList.add(almacenListAlmacenToAttach);
            }
            producto.setAlmacenList(attachedAlmacenList);
            em.persist(producto);
            if (editorialId1 != null) {
                editorialId1.getProductoList().add(producto);
                editorialId1 = em.merge(editorialId1);
            }
            if (nivelId != null) {
                nivelId.getProductoList().add(producto);
                nivelId = em.merge(nivelId);
            }
            for (Pedidoparte pedidoparteListPedidoparte : producto.getPedidoparteList()) {
                Producto oldProductoIdOfPedidoparteListPedidoparte = pedidoparteListPedidoparte.getProductoId();
                pedidoparteListPedidoparte.setProductoId(producto);
                pedidoparteListPedidoparte = em.merge(pedidoparteListPedidoparte);
                if (oldProductoIdOfPedidoparteListPedidoparte != null) {
                    oldProductoIdOfPedidoparteListPedidoparte.getPedidoparteList().remove(pedidoparteListPedidoparte);
                    oldProductoIdOfPedidoparteListPedidoparte = em.merge(oldProductoIdOfPedidoparteListPedidoparte);
                }
            }
            for (VentasHasProducto ventasHasProductoListVentasHasProducto : producto.getVentasHasProductoList()) {
                Producto oldProductoOfVentasHasProductoListVentasHasProducto = ventasHasProductoListVentasHasProducto.getProducto();
                ventasHasProductoListVentasHasProducto.setProducto(producto);
                ventasHasProductoListVentasHasProducto = em.merge(ventasHasProductoListVentasHasProducto);
                if (oldProductoOfVentasHasProductoListVentasHasProducto != null) {
                    oldProductoOfVentasHasProductoListVentasHasProducto.getVentasHasProductoList().remove(ventasHasProductoListVentasHasProducto);
                    oldProductoOfVentasHasProductoListVentasHasProducto = em.merge(oldProductoOfVentasHasProductoListVentasHasProducto);
                }
            }
            for (Almacen almacenListAlmacen : producto.getAlmacenList()) {
                Producto oldProductoIdOfAlmacenListAlmacen = almacenListAlmacen.getProductoId();
                almacenListAlmacen.setProductoId(producto);
                almacenListAlmacen = em.merge(almacenListAlmacen);
                if (oldProductoIdOfAlmacenListAlmacen != null) {
                    oldProductoIdOfAlmacenListAlmacen.getAlmacenList().remove(almacenListAlmacen);
                    oldProductoIdOfAlmacenListAlmacen = em.merge(oldProductoIdOfAlmacenListAlmacen);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Producto producto) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto persistentProducto = em.find(Producto.class, producto.getId());
            Editorial editorialId1Old = persistentProducto.getEditorialId1();
            Editorial editorialId1New = producto.getEditorialId1();
            Nivel nivelIdOld = persistentProducto.getNivelId();
            Nivel nivelIdNew = producto.getNivelId();
            List<Pedidoparte> pedidoparteListOld = persistentProducto.getPedidoparteList();
            List<Pedidoparte> pedidoparteListNew = producto.getPedidoparteList();
            List<VentasHasProducto> ventasHasProductoListOld = persistentProducto.getVentasHasProductoList();
            List<VentasHasProducto> ventasHasProductoListNew = producto.getVentasHasProductoList();
            List<Almacen> almacenListOld = persistentProducto.getAlmacenList();
            List<Almacen> almacenListNew = producto.getAlmacenList();
            List<String> illegalOrphanMessages = null;
            for (Pedidoparte pedidoparteListOldPedidoparte : pedidoparteListOld) {
                if (!pedidoparteListNew.contains(pedidoparteListOldPedidoparte)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pedidoparte " + pedidoparteListOldPedidoparte + " since its productoId field is not nullable.");
                }
            }
            for (VentasHasProducto ventasHasProductoListOldVentasHasProducto : ventasHasProductoListOld) {
                if (!ventasHasProductoListNew.contains(ventasHasProductoListOldVentasHasProducto)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain VentasHasProducto " + ventasHasProductoListOldVentasHasProducto + " since its producto field is not nullable.");
                }
            }
            for (Almacen almacenListOldAlmacen : almacenListOld) {
                if (!almacenListNew.contains(almacenListOldAlmacen)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Almacen " + almacenListOldAlmacen + " since its productoId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (editorialId1New != null) {
                editorialId1New = em.getReference(editorialId1New.getClass(), editorialId1New.getId());
                producto.setEditorialId1(editorialId1New);
            }
            if (nivelIdNew != null) {
                nivelIdNew = em.getReference(nivelIdNew.getClass(), nivelIdNew.getId());
                producto.setNivelId(nivelIdNew);
            }
            List<Pedidoparte> attachedPedidoparteListNew = new ArrayList<Pedidoparte>();
            for (Pedidoparte pedidoparteListNewPedidoparteToAttach : pedidoparteListNew) {
                pedidoparteListNewPedidoparteToAttach = em.getReference(pedidoparteListNewPedidoparteToAttach.getClass(), pedidoparteListNewPedidoparteToAttach.getId());
                attachedPedidoparteListNew.add(pedidoparteListNewPedidoparteToAttach);
            }
            pedidoparteListNew = attachedPedidoparteListNew;
            producto.setPedidoparteList(pedidoparteListNew);
            List<VentasHasProducto> attachedVentasHasProductoListNew = new ArrayList<VentasHasProducto>();
            for (VentasHasProducto ventasHasProductoListNewVentasHasProductoToAttach : ventasHasProductoListNew) {
                ventasHasProductoListNewVentasHasProductoToAttach = em.getReference(ventasHasProductoListNewVentasHasProductoToAttach.getClass(), ventasHasProductoListNewVentasHasProductoToAttach.getVentasHasProductoPK());
                attachedVentasHasProductoListNew.add(ventasHasProductoListNewVentasHasProductoToAttach);
            }
            ventasHasProductoListNew = attachedVentasHasProductoListNew;
            producto.setVentasHasProductoList(ventasHasProductoListNew);
            List<Almacen> attachedAlmacenListNew = new ArrayList<Almacen>();
            for (Almacen almacenListNewAlmacenToAttach : almacenListNew) {
                almacenListNewAlmacenToAttach = em.getReference(almacenListNewAlmacenToAttach.getClass(), almacenListNewAlmacenToAttach.getId());
                attachedAlmacenListNew.add(almacenListNewAlmacenToAttach);
            }
            almacenListNew = attachedAlmacenListNew;
            producto.setAlmacenList(almacenListNew);
            producto = em.merge(producto);
            if (editorialId1Old != null && !editorialId1Old.equals(editorialId1New)) {
                editorialId1Old.getProductoList().remove(producto);
                editorialId1Old = em.merge(editorialId1Old);
            }
            if (editorialId1New != null && !editorialId1New.equals(editorialId1Old)) {
                editorialId1New.getProductoList().add(producto);
                editorialId1New = em.merge(editorialId1New);
            }
            if (nivelIdOld != null && !nivelIdOld.equals(nivelIdNew)) {
                nivelIdOld.getProductoList().remove(producto);
                nivelIdOld = em.merge(nivelIdOld);
            }
            if (nivelIdNew != null && !nivelIdNew.equals(nivelIdOld)) {
                nivelIdNew.getProductoList().add(producto);
                nivelIdNew = em.merge(nivelIdNew);
            }
            for (Pedidoparte pedidoparteListNewPedidoparte : pedidoparteListNew) {
                if (!pedidoparteListOld.contains(pedidoparteListNewPedidoparte)) {
                    Producto oldProductoIdOfPedidoparteListNewPedidoparte = pedidoparteListNewPedidoparte.getProductoId();
                    pedidoparteListNewPedidoparte.setProductoId(producto);
                    pedidoparteListNewPedidoparte = em.merge(pedidoparteListNewPedidoparte);
                    if (oldProductoIdOfPedidoparteListNewPedidoparte != null && !oldProductoIdOfPedidoparteListNewPedidoparte.equals(producto)) {
                        oldProductoIdOfPedidoparteListNewPedidoparte.getPedidoparteList().remove(pedidoparteListNewPedidoparte);
                        oldProductoIdOfPedidoparteListNewPedidoparte = em.merge(oldProductoIdOfPedidoparteListNewPedidoparte);
                    }
                }
            }
            for (VentasHasProducto ventasHasProductoListNewVentasHasProducto : ventasHasProductoListNew) {
                if (!ventasHasProductoListOld.contains(ventasHasProductoListNewVentasHasProducto)) {
                    Producto oldProductoOfVentasHasProductoListNewVentasHasProducto = ventasHasProductoListNewVentasHasProducto.getProducto();
                    ventasHasProductoListNewVentasHasProducto.setProducto(producto);
                    ventasHasProductoListNewVentasHasProducto = em.merge(ventasHasProductoListNewVentasHasProducto);
                    if (oldProductoOfVentasHasProductoListNewVentasHasProducto != null && !oldProductoOfVentasHasProductoListNewVentasHasProducto.equals(producto)) {
                        oldProductoOfVentasHasProductoListNewVentasHasProducto.getVentasHasProductoList().remove(ventasHasProductoListNewVentasHasProducto);
                        oldProductoOfVentasHasProductoListNewVentasHasProducto = em.merge(oldProductoOfVentasHasProductoListNewVentasHasProducto);
                    }
                }
            }
            for (Almacen almacenListNewAlmacen : almacenListNew) {
                if (!almacenListOld.contains(almacenListNewAlmacen)) {
                    Producto oldProductoIdOfAlmacenListNewAlmacen = almacenListNewAlmacen.getProductoId();
                    almacenListNewAlmacen.setProductoId(producto);
                    almacenListNewAlmacen = em.merge(almacenListNewAlmacen);
                    if (oldProductoIdOfAlmacenListNewAlmacen != null && !oldProductoIdOfAlmacenListNewAlmacen.equals(producto)) {
                        oldProductoIdOfAlmacenListNewAlmacen.getAlmacenList().remove(almacenListNewAlmacen);
                        oldProductoIdOfAlmacenListNewAlmacen = em.merge(oldProductoIdOfAlmacenListNewAlmacen);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = producto.getId();
                if (findProducto(id) == null) {
                    throw new NonexistentEntityException("The producto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto producto;
            try {
                producto = em.getReference(Producto.class, id);
                producto.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The producto with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Pedidoparte> pedidoparteListOrphanCheck = producto.getPedidoparteList();
            for (Pedidoparte pedidoparteListOrphanCheckPedidoparte : pedidoparteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the Pedidoparte " + pedidoparteListOrphanCheckPedidoparte + " in its pedidoparteList field has a non-nullable productoId field.");
            }
            List<VentasHasProducto> ventasHasProductoListOrphanCheck = producto.getVentasHasProductoList();
            for (VentasHasProducto ventasHasProductoListOrphanCheckVentasHasProducto : ventasHasProductoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the VentasHasProducto " + ventasHasProductoListOrphanCheckVentasHasProducto + " in its ventasHasProductoList field has a non-nullable producto field.");
            }
            List<Almacen> almacenListOrphanCheck = producto.getAlmacenList();
            for (Almacen almacenListOrphanCheckAlmacen : almacenListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Producto (" + producto + ") cannot be destroyed since the Almacen " + almacenListOrphanCheckAlmacen + " in its almacenList field has a non-nullable productoId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Editorial editorialId1 = producto.getEditorialId1();
            if (editorialId1 != null) {
                editorialId1.getProductoList().remove(producto);
                editorialId1 = em.merge(editorialId1);
            }
            Nivel nivelId = producto.getNivelId();
            if (nivelId != null) {
                nivelId.getProductoList().remove(producto);
                nivelId = em.merge(nivelId);
            }
            em.remove(producto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Producto> findProductoEntities() {
        return findProductoEntities(true, -1, -1);
    }

    public List<Producto> findProductoEntities(int maxResults, int firstResult) {
        return findProductoEntities(false, maxResults, firstResult);
    }

    private List<Producto> findProductoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Producto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Producto findProducto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Producto.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Producto> rt = cq.from(Producto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
        public List<Producto> buscarProductos(String objeto,int pagina){
        EntityManager em = getEntityManager();
        List<Producto>productos=new ArrayList<Producto>();
        try {
            
            TypedQuery<Producto> resultList = em.createQuery("SELECT p FROM Producto p WHERE (p.isbn LIKE :objeto OR p.autor LIKE :objeto OR p.titulo LIKE :objeto) AND p.estado=2 OR p.estado=1", Producto.class).setFirstResult(pagina*50).setMaxResults(50);
            resultList.setParameter("objeto", "%"+objeto+"%");
            productos = resultList.getResultList();
        } finally {
            em.close();
        }
            return productos;
    }
    
     public int numeroProductos(String objeto){
        EntityManager em = getEntityManager();
        int numero=0;
        try {
            TypedQuery<Producto> resultList = em.createQuery("SELECT p FROM Producto p WHERE (p.isbn LIKE :objeto OR p.autor LIKE :objeto OR p.titulo LIKE :objeto) AND p.estado=2 OR p.estado=1", Producto.class);
            resultList.setParameter("objeto", "%"+objeto+"%");
            numero=resultList.getResultList().size();
        } finally {
            em.close();
        }
            return numero;
    }
    public List<Producto> productosPendientes(int pagina){
        EntityManager em = getEntityManager();
        List<Producto>productos=new ArrayList<Producto>();
        try {
            
            TypedQuery<Producto> resultList = em.createQuery("SELECT p FROM Producto p WHERE p.estado=1", Producto.class).setFirstResult(pagina*30).setMaxResults(30);
            productos = resultList.getResultList();
        } finally {
            em.close();
        }
            return productos;
    }
     
    
    public int productoTPV(String serial){
        EntityManager em = getEntityManager();
        int idProducto=0;
        try {
            
            TypedQuery<Producto> resultList = em.createQuery("SELECT p FROM Producto p WHERE p.isbn = :serial", Producto.class);
            resultList.setParameter("serial", serial);
            if(resultList.getResultList().size()>0){
            Producto producto = resultList.getSingleResult();
            idProducto=producto.getId();
            }
        } finally {
            em.close();
        }
            return idProducto;
    }
    
    public boolean existeProducto(String serial){
        EntityManager em = getEntityManager();
        boolean existe=false;
        try {
            
            TypedQuery<Producto> resultList = em.createQuery("SELECT p FROM Producto p WHERE p.isbn = :serial", Producto.class);
            resultList.setParameter("serial", serial);
            if(resultList.getResultList().size()>0){
            existe=true;
            }
        } finally {
            em.close();
        }
            return existe;
    }
    
    public int serialProducto(String serial){
        EntityManager em = getEntityManager();
        int idProducto=0;
        try {
            
            TypedQuery<Producto> resultList = em.createQuery("SELECT p FROM Producto p WHERE p.isbn = :serial", Producto.class);
            resultList.setParameter("serial", serial);
            if(resultList.getResultList().size()>0){
            idProducto=resultList.getSingleResult().getId();
            }
        } finally {
            em.close();
        }
            return idProducto;
    }
    
    public List<Producto> listaProductos(int pagina){
        EntityManager em = getEntityManager();
        List<Producto> productos=new ArrayList<Producto>();
        try {
            TypedQuery<Producto> resultList = em.createQuery("SELECT p FROM Producto p WHERE p.estado=1", Producto.class).setFirstResult(5*pagina).setMaxResults(5);
            productos=resultList.getResultList();
        } finally {
            em.close();
        }
            return productos;
    }
    
    public int productosPendientesCuenta(){
        EntityManager em = getEntityManager();
        int numero=0;
        try {
            TypedQuery<Producto> resultList = em.createQuery("SELECT p FROM Producto p WHERE p.estado=1", Producto.class);
            numero=resultList.getResultList().size();
        } finally {
            em.close();
        }
            return numero;
    }
    
}
