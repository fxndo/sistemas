/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Direccion;
import Modelo.Pedidoproveedor;
import Modelo.Proveedor;
import java.util.ArrayList;
import java.util.List;
import Modelo.Proveedoreditorial;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author fernando
 */
public class ProveedorJpaController implements Serializable {

    public ProveedorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Proveedor proveedor) {
        if (proveedor.getPedidoproveedorList() == null) {
            proveedor.setPedidoproveedorList(new ArrayList<Pedidoproveedor>());
        }
        if (proveedor.getProveedoreditorialList() == null) {
            proveedor.setProveedoreditorialList(new ArrayList<Proveedoreditorial>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Direccion direccionId = proveedor.getDireccionId();
            if (direccionId != null) {
                direccionId = em.getReference(direccionId.getClass(), direccionId.getId());
                proveedor.setDireccionId(direccionId);
            }
            List<Pedidoproveedor> attachedPedidoproveedorList = new ArrayList<Pedidoproveedor>();
            for (Pedidoproveedor pedidoproveedorListPedidoproveedorToAttach : proveedor.getPedidoproveedorList()) {
                pedidoproveedorListPedidoproveedorToAttach = em.getReference(pedidoproveedorListPedidoproveedorToAttach.getClass(), pedidoproveedorListPedidoproveedorToAttach.getId());
                attachedPedidoproveedorList.add(pedidoproveedorListPedidoproveedorToAttach);
            }
            proveedor.setPedidoproveedorList(attachedPedidoproveedorList);
            List<Proveedoreditorial> attachedProveedoreditorialList = new ArrayList<Proveedoreditorial>();
            for (Proveedoreditorial proveedoreditorialListProveedoreditorialToAttach : proveedor.getProveedoreditorialList()) {
                proveedoreditorialListProveedoreditorialToAttach = em.getReference(proveedoreditorialListProveedoreditorialToAttach.getClass(), proveedoreditorialListProveedoreditorialToAttach.getId());
                attachedProveedoreditorialList.add(proveedoreditorialListProveedoreditorialToAttach);
            }
            proveedor.setProveedoreditorialList(attachedProveedoreditorialList);
            em.persist(proveedor);
            if (direccionId != null) {
                direccionId.getProveedorList().add(proveedor);
                direccionId = em.merge(direccionId);
            }
            for (Pedidoproveedor pedidoproveedorListPedidoproveedor : proveedor.getPedidoproveedorList()) {
                Proveedor oldProveedorIdOfPedidoproveedorListPedidoproveedor = pedidoproveedorListPedidoproveedor.getProveedorId();
                pedidoproveedorListPedidoproveedor.setProveedorId(proveedor);
                pedidoproveedorListPedidoproveedor = em.merge(pedidoproveedorListPedidoproveedor);
                if (oldProveedorIdOfPedidoproveedorListPedidoproveedor != null) {
                    oldProveedorIdOfPedidoproveedorListPedidoproveedor.getPedidoproveedorList().remove(pedidoproveedorListPedidoproveedor);
                    oldProveedorIdOfPedidoproveedorListPedidoproveedor = em.merge(oldProveedorIdOfPedidoproveedorListPedidoproveedor);
                }
            }
            for (Proveedoreditorial proveedoreditorialListProveedoreditorial : proveedor.getProveedoreditorialList()) {
                Proveedor oldProveedorIdOfProveedoreditorialListProveedoreditorial = proveedoreditorialListProveedoreditorial.getProveedorId();
                proveedoreditorialListProveedoreditorial.setProveedorId(proveedor);
                proveedoreditorialListProveedoreditorial = em.merge(proveedoreditorialListProveedoreditorial);
                if (oldProveedorIdOfProveedoreditorialListProveedoreditorial != null) {
                    oldProveedorIdOfProveedoreditorialListProveedoreditorial.getProveedoreditorialList().remove(proveedoreditorialListProveedoreditorial);
                    oldProveedorIdOfProveedoreditorialListProveedoreditorial = em.merge(oldProveedorIdOfProveedoreditorialListProveedoreditorial);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Proveedor proveedor) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Proveedor persistentProveedor = em.find(Proveedor.class, proveedor.getId());
            Direccion direccionIdOld = persistentProveedor.getDireccionId();
            Direccion direccionIdNew = proveedor.getDireccionId();
            List<Pedidoproveedor> pedidoproveedorListOld = persistentProveedor.getPedidoproveedorList();
            List<Pedidoproveedor> pedidoproveedorListNew = proveedor.getPedidoproveedorList();
            List<Proveedoreditorial> proveedoreditorialListOld = persistentProveedor.getProveedoreditorialList();
            List<Proveedoreditorial> proveedoreditorialListNew = proveedor.getProveedoreditorialList();
            List<String> illegalOrphanMessages = null;
            for (Proveedoreditorial proveedoreditorialListOldProveedoreditorial : proveedoreditorialListOld) {
                if (!proveedoreditorialListNew.contains(proveedoreditorialListOldProveedoreditorial)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Proveedoreditorial " + proveedoreditorialListOldProveedoreditorial + " since its proveedorId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (direccionIdNew != null) {
                direccionIdNew = em.getReference(direccionIdNew.getClass(), direccionIdNew.getId());
                proveedor.setDireccionId(direccionIdNew);
            }
            List<Pedidoproveedor> attachedPedidoproveedorListNew = new ArrayList<Pedidoproveedor>();
            for (Pedidoproveedor pedidoproveedorListNewPedidoproveedorToAttach : pedidoproveedorListNew) {
                pedidoproveedorListNewPedidoproveedorToAttach = em.getReference(pedidoproveedorListNewPedidoproveedorToAttach.getClass(), pedidoproveedorListNewPedidoproveedorToAttach.getId());
                attachedPedidoproveedorListNew.add(pedidoproveedorListNewPedidoproveedorToAttach);
            }
            pedidoproveedorListNew = attachedPedidoproveedorListNew;
            proveedor.setPedidoproveedorList(pedidoproveedorListNew);
            List<Proveedoreditorial> attachedProveedoreditorialListNew = new ArrayList<Proveedoreditorial>();
            for (Proveedoreditorial proveedoreditorialListNewProveedoreditorialToAttach : proveedoreditorialListNew) {
                proveedoreditorialListNewProveedoreditorialToAttach = em.getReference(proveedoreditorialListNewProveedoreditorialToAttach.getClass(), proveedoreditorialListNewProveedoreditorialToAttach.getId());
                attachedProveedoreditorialListNew.add(proveedoreditorialListNewProveedoreditorialToAttach);
            }
            proveedoreditorialListNew = attachedProveedoreditorialListNew;
            proveedor.setProveedoreditorialList(proveedoreditorialListNew);
            proveedor = em.merge(proveedor);
            if (direccionIdOld != null && !direccionIdOld.equals(direccionIdNew)) {
                direccionIdOld.getProveedorList().remove(proveedor);
                direccionIdOld = em.merge(direccionIdOld);
            }
            if (direccionIdNew != null && !direccionIdNew.equals(direccionIdOld)) {
                direccionIdNew.getProveedorList().add(proveedor);
                direccionIdNew = em.merge(direccionIdNew);
            }
            for (Pedidoproveedor pedidoproveedorListOldPedidoproveedor : pedidoproveedorListOld) {
                if (!pedidoproveedorListNew.contains(pedidoproveedorListOldPedidoproveedor)) {
                    pedidoproveedorListOldPedidoproveedor.setProveedorId(null);
                    pedidoproveedorListOldPedidoproveedor = em.merge(pedidoproveedorListOldPedidoproveedor);
                }
            }
            for (Pedidoproveedor pedidoproveedorListNewPedidoproveedor : pedidoproveedorListNew) {
                if (!pedidoproveedorListOld.contains(pedidoproveedorListNewPedidoproveedor)) {
                    Proveedor oldProveedorIdOfPedidoproveedorListNewPedidoproveedor = pedidoproveedorListNewPedidoproveedor.getProveedorId();
                    pedidoproveedorListNewPedidoproveedor.setProveedorId(proveedor);
                    pedidoproveedorListNewPedidoproveedor = em.merge(pedidoproveedorListNewPedidoproveedor);
                    if (oldProveedorIdOfPedidoproveedorListNewPedidoproveedor != null && !oldProveedorIdOfPedidoproveedorListNewPedidoproveedor.equals(proveedor)) {
                        oldProveedorIdOfPedidoproveedorListNewPedidoproveedor.getPedidoproveedorList().remove(pedidoproveedorListNewPedidoproveedor);
                        oldProveedorIdOfPedidoproveedorListNewPedidoproveedor = em.merge(oldProveedorIdOfPedidoproveedorListNewPedidoproveedor);
                    }
                }
            }
            for (Proveedoreditorial proveedoreditorialListNewProveedoreditorial : proveedoreditorialListNew) {
                if (!proveedoreditorialListOld.contains(proveedoreditorialListNewProveedoreditorial)) {
                    Proveedor oldProveedorIdOfProveedoreditorialListNewProveedoreditorial = proveedoreditorialListNewProveedoreditorial.getProveedorId();
                    proveedoreditorialListNewProveedoreditorial.setProveedorId(proveedor);
                    proveedoreditorialListNewProveedoreditorial = em.merge(proveedoreditorialListNewProveedoreditorial);
                    if (oldProveedorIdOfProveedoreditorialListNewProveedoreditorial != null && !oldProveedorIdOfProveedoreditorialListNewProveedoreditorial.equals(proveedor)) {
                        oldProveedorIdOfProveedoreditorialListNewProveedoreditorial.getProveedoreditorialList().remove(proveedoreditorialListNewProveedoreditorial);
                        oldProveedorIdOfProveedoreditorialListNewProveedoreditorial = em.merge(oldProveedorIdOfProveedoreditorialListNewProveedoreditorial);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = proveedor.getId();
                if (findProveedor(id) == null) {
                    throw new NonexistentEntityException("The proveedor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Proveedor proveedor;
            try {
                proveedor = em.getReference(Proveedor.class, id);
                proveedor.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The proveedor with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Proveedoreditorial> proveedoreditorialListOrphanCheck = proveedor.getProveedoreditorialList();
            for (Proveedoreditorial proveedoreditorialListOrphanCheckProveedoreditorial : proveedoreditorialListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Proveedor (" + proveedor + ") cannot be destroyed since the Proveedoreditorial " + proveedoreditorialListOrphanCheckProveedoreditorial + " in its proveedoreditorialList field has a non-nullable proveedorId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Direccion direccionId = proveedor.getDireccionId();
            if (direccionId != null) {
                direccionId.getProveedorList().remove(proveedor);
                direccionId = em.merge(direccionId);
            }
            List<Pedidoproveedor> pedidoproveedorList = proveedor.getPedidoproveedorList();
            for (Pedidoproveedor pedidoproveedorListPedidoproveedor : pedidoproveedorList) {
                pedidoproveedorListPedidoproveedor.setProveedorId(null);
                pedidoproveedorListPedidoproveedor = em.merge(pedidoproveedorListPedidoproveedor);
            }
            em.remove(proveedor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Proveedor> findProveedorEntities() {
        return findProveedorEntities(true, -1, -1);
    }

    public List<Proveedor> findProveedorEntities(int maxResults, int firstResult) {
        return findProveedorEntities(false, maxResults, firstResult);
    }

    private List<Proveedor> findProveedorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Proveedor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Proveedor findProveedor(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Proveedor.class, id);
        } finally {
            em.close();
        }
    }

    public int getProveedorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Proveedor> rt = cq.from(Proveedor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
         public List<Proveedor> editorialesPedido(int idPedido){
        EntityManager em = getEntityManager();
        List<Proveedor>proveedores=new ArrayList<Proveedor>();
        try {
            
            TypedQuery<Proveedor> resultList = em.createQuery("SELECT DISTINCT p FROM Proveedor p, PedidoHasProducto h WHERE h.pedido.id=:idPedido AND h.producto.proveedorId.id=p.id ", Proveedor.class);
            resultList.setParameter("idPedido", idPedido);
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
    public List<Proveedor> proveedoresListaCompleta(){
        EntityManager em = getEntityManager();
        List<Proveedor>proveedores=new ArrayList<Proveedor>();
        try {
            
            TypedQuery<Proveedor> resultList = em.createQuery("SELECT p FROM Proveedor p WHERE p.estado=1 ORDER BY p.nombre ASC ", Proveedor.class);
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
    public List<Proveedor> listaProveedoresEditorial(int idEditorial){
        EntityManager em = getEntityManager();
        List<Proveedor>proveedores=new ArrayList<Proveedor>();
        try {
            
            TypedQuery<Proveedor> resultList = em.createQuery("SELECT p FROM Proveedor p, Proveedoreditorial e WHERE e.editorialId.id=:idEditorial AND e.proveedorId.id=p.id ORDER BY p.porcentaje ASC ", Proveedor.class);
            resultList.setParameter("idEditorial", idEditorial);
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
    public List<Proveedor> proveedoresLista(int pagina){
        EntityManager em = getEntityManager();
        List<Proveedor>proveedores=new ArrayList<Proveedor>();
        try {
            
            TypedQuery<Proveedor> resultList = em.createQuery("SELECT p FROM Proveedor p WHERE p.estado=1 ORDER BY p.nombre ASC ", Proveedor.class).setFirstResult(pagina*20).setMaxResults(20);
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    public List<Proveedor> buscarProveedores(String objeto,int pagina){
        EntityManager em = getEntityManager();
        List<Proveedor>proveedores=new ArrayList<Proveedor>();
        try {
            
            TypedQuery<Proveedor> resultList = em.createQuery("SELECT p FROM Proveedor p WHERE p.estado=1 AND (p.nombre LIKE :objeto OR p.clave LIKE :objeto) ORDER BY p.nombre ASC ", Proveedor.class).setFirstResult(pagina*20).setMaxResults(20);
            resultList.setParameter("objeto", "%"+objeto+"%");
            proveedores = resultList.getResultList();
        } finally {
            em.close();
        }
            return proveedores;
    }
    
    public int proveedoresListaCuenta(String objeto){
        EntityManager em = getEntityManager();
        int numero=0;
        try {
            
            TypedQuery<Proveedor> resultList = em.createQuery("SELECT p FROM Proveedor p WHERE p.estado=1 AND (p.nombre LIKE :objeto OR p.clave LIKE :objeto) ORDER BY p.nombre ASC ", Proveedor.class);
            resultList.setParameter("objeto", "%"+objeto+"%");
            numero = resultList.getResultList().size();
        } finally {
            em.close();
        }
            return numero;
    }
    
}
