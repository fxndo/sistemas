/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModeloJSP;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelo.Cliente;
import Modelo.Direccion;
import java.util.ArrayList;
import java.util.List;
import Modelo.Sucursal;
import Modelo.Proveedor;
import ModeloJSP.exceptions.IllegalOrphanException;
import ModeloJSP.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author fernando
 */
public class DireccionJpaController implements Serializable {

    public DireccionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Direccion direccion) {
        if (direccion.getClienteList() == null) {
            direccion.setClienteList(new ArrayList<Cliente>());
        }
        if (direccion.getSucursalList() == null) {
            direccion.setSucursalList(new ArrayList<Sucursal>());
        }
        if (direccion.getProveedorList() == null) {
            direccion.setProveedorList(new ArrayList<Proveedor>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Cliente> attachedClienteList = new ArrayList<Cliente>();
            for (Cliente clienteListClienteToAttach : direccion.getClienteList()) {
                clienteListClienteToAttach = em.getReference(clienteListClienteToAttach.getClass(), clienteListClienteToAttach.getId());
                attachedClienteList.add(clienteListClienteToAttach);
            }
            direccion.setClienteList(attachedClienteList);
            List<Sucursal> attachedSucursalList = new ArrayList<Sucursal>();
            for (Sucursal sucursalListSucursalToAttach : direccion.getSucursalList()) {
                sucursalListSucursalToAttach = em.getReference(sucursalListSucursalToAttach.getClass(), sucursalListSucursalToAttach.getId());
                attachedSucursalList.add(sucursalListSucursalToAttach);
            }
            direccion.setSucursalList(attachedSucursalList);
            List<Proveedor> attachedProveedorList = new ArrayList<Proveedor>();
            for (Proveedor proveedorListProveedorToAttach : direccion.getProveedorList()) {
                proveedorListProveedorToAttach = em.getReference(proveedorListProveedorToAttach.getClass(), proveedorListProveedorToAttach.getId());
                attachedProveedorList.add(proveedorListProveedorToAttach);
            }
            direccion.setProveedorList(attachedProveedorList);
            em.persist(direccion);
            for (Cliente clienteListCliente : direccion.getClienteList()) {
                Direccion oldDireccionIdOfClienteListCliente = clienteListCliente.getDireccionId();
                clienteListCliente.setDireccionId(direccion);
                clienteListCliente = em.merge(clienteListCliente);
                if (oldDireccionIdOfClienteListCliente != null) {
                    oldDireccionIdOfClienteListCliente.getClienteList().remove(clienteListCliente);
                    oldDireccionIdOfClienteListCliente = em.merge(oldDireccionIdOfClienteListCliente);
                }
            }
            for (Sucursal sucursalListSucursal : direccion.getSucursalList()) {
                Direccion oldDireccionIdOfSucursalListSucursal = sucursalListSucursal.getDireccionId();
                sucursalListSucursal.setDireccionId(direccion);
                sucursalListSucursal = em.merge(sucursalListSucursal);
                if (oldDireccionIdOfSucursalListSucursal != null) {
                    oldDireccionIdOfSucursalListSucursal.getSucursalList().remove(sucursalListSucursal);
                    oldDireccionIdOfSucursalListSucursal = em.merge(oldDireccionIdOfSucursalListSucursal);
                }
            }
            for (Proveedor proveedorListProveedor : direccion.getProveedorList()) {
                Direccion oldDireccionIdOfProveedorListProveedor = proveedorListProveedor.getDireccionId();
                proveedorListProveedor.setDireccionId(direccion);
                proveedorListProveedor = em.merge(proveedorListProveedor);
                if (oldDireccionIdOfProveedorListProveedor != null) {
                    oldDireccionIdOfProveedorListProveedor.getProveedorList().remove(proveedorListProveedor);
                    oldDireccionIdOfProveedorListProveedor = em.merge(oldDireccionIdOfProveedorListProveedor);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Direccion direccion) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Direccion persistentDireccion = em.find(Direccion.class, direccion.getId());
            List<Cliente> clienteListOld = persistentDireccion.getClienteList();
            List<Cliente> clienteListNew = direccion.getClienteList();
            List<Sucursal> sucursalListOld = persistentDireccion.getSucursalList();
            List<Sucursal> sucursalListNew = direccion.getSucursalList();
            List<Proveedor> proveedorListOld = persistentDireccion.getProveedorList();
            List<Proveedor> proveedorListNew = direccion.getProveedorList();
            List<String> illegalOrphanMessages = null;
            for (Cliente clienteListOldCliente : clienteListOld) {
                if (!clienteListNew.contains(clienteListOldCliente)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cliente " + clienteListOldCliente + " since its direccionId field is not nullable.");
                }
            }
            for (Proveedor proveedorListOldProveedor : proveedorListOld) {
                if (!proveedorListNew.contains(proveedorListOldProveedor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Proveedor " + proveedorListOldProveedor + " since its direccionId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Cliente> attachedClienteListNew = new ArrayList<Cliente>();
            for (Cliente clienteListNewClienteToAttach : clienteListNew) {
                clienteListNewClienteToAttach = em.getReference(clienteListNewClienteToAttach.getClass(), clienteListNewClienteToAttach.getId());
                attachedClienteListNew.add(clienteListNewClienteToAttach);
            }
            clienteListNew = attachedClienteListNew;
            direccion.setClienteList(clienteListNew);
            List<Sucursal> attachedSucursalListNew = new ArrayList<Sucursal>();
            for (Sucursal sucursalListNewSucursalToAttach : sucursalListNew) {
                sucursalListNewSucursalToAttach = em.getReference(sucursalListNewSucursalToAttach.getClass(), sucursalListNewSucursalToAttach.getId());
                attachedSucursalListNew.add(sucursalListNewSucursalToAttach);
            }
            sucursalListNew = attachedSucursalListNew;
            direccion.setSucursalList(sucursalListNew);
            List<Proveedor> attachedProveedorListNew = new ArrayList<Proveedor>();
            for (Proveedor proveedorListNewProveedorToAttach : proveedorListNew) {
                proveedorListNewProveedorToAttach = em.getReference(proveedorListNewProveedorToAttach.getClass(), proveedorListNewProveedorToAttach.getId());
                attachedProveedorListNew.add(proveedorListNewProveedorToAttach);
            }
            proveedorListNew = attachedProveedorListNew;
            direccion.setProveedorList(proveedorListNew);
            direccion = em.merge(direccion);
            for (Cliente clienteListNewCliente : clienteListNew) {
                if (!clienteListOld.contains(clienteListNewCliente)) {
                    Direccion oldDireccionIdOfClienteListNewCliente = clienteListNewCliente.getDireccionId();
                    clienteListNewCliente.setDireccionId(direccion);
                    clienteListNewCliente = em.merge(clienteListNewCliente);
                    if (oldDireccionIdOfClienteListNewCliente != null && !oldDireccionIdOfClienteListNewCliente.equals(direccion)) {
                        oldDireccionIdOfClienteListNewCliente.getClienteList().remove(clienteListNewCliente);
                        oldDireccionIdOfClienteListNewCliente = em.merge(oldDireccionIdOfClienteListNewCliente);
                    }
                }
            }
            for (Sucursal sucursalListOldSucursal : sucursalListOld) {
                if (!sucursalListNew.contains(sucursalListOldSucursal)) {
                    sucursalListOldSucursal.setDireccionId(null);
                    sucursalListOldSucursal = em.merge(sucursalListOldSucursal);
                }
            }
            for (Sucursal sucursalListNewSucursal : sucursalListNew) {
                if (!sucursalListOld.contains(sucursalListNewSucursal)) {
                    Direccion oldDireccionIdOfSucursalListNewSucursal = sucursalListNewSucursal.getDireccionId();
                    sucursalListNewSucursal.setDireccionId(direccion);
                    sucursalListNewSucursal = em.merge(sucursalListNewSucursal);
                    if (oldDireccionIdOfSucursalListNewSucursal != null && !oldDireccionIdOfSucursalListNewSucursal.equals(direccion)) {
                        oldDireccionIdOfSucursalListNewSucursal.getSucursalList().remove(sucursalListNewSucursal);
                        oldDireccionIdOfSucursalListNewSucursal = em.merge(oldDireccionIdOfSucursalListNewSucursal);
                    }
                }
            }
            for (Proveedor proveedorListNewProveedor : proveedorListNew) {
                if (!proveedorListOld.contains(proveedorListNewProveedor)) {
                    Direccion oldDireccionIdOfProveedorListNewProveedor = proveedorListNewProveedor.getDireccionId();
                    proveedorListNewProveedor.setDireccionId(direccion);
                    proveedorListNewProveedor = em.merge(proveedorListNewProveedor);
                    if (oldDireccionIdOfProveedorListNewProveedor != null && !oldDireccionIdOfProveedorListNewProveedor.equals(direccion)) {
                        oldDireccionIdOfProveedorListNewProveedor.getProveedorList().remove(proveedorListNewProveedor);
                        oldDireccionIdOfProveedorListNewProveedor = em.merge(oldDireccionIdOfProveedorListNewProveedor);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = direccion.getId();
                if (findDireccion(id) == null) {
                    throw new NonexistentEntityException("The direccion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Direccion direccion;
            try {
                direccion = em.getReference(Direccion.class, id);
                direccion.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The direccion with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Cliente> clienteListOrphanCheck = direccion.getClienteList();
            for (Cliente clienteListOrphanCheckCliente : clienteListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Direccion (" + direccion + ") cannot be destroyed since the Cliente " + clienteListOrphanCheckCliente + " in its clienteList field has a non-nullable direccionId field.");
            }
            List<Proveedor> proveedorListOrphanCheck = direccion.getProveedorList();
            for (Proveedor proveedorListOrphanCheckProveedor : proveedorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Direccion (" + direccion + ") cannot be destroyed since the Proveedor " + proveedorListOrphanCheckProveedor + " in its proveedorList field has a non-nullable direccionId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Sucursal> sucursalList = direccion.getSucursalList();
            for (Sucursal sucursalListSucursal : sucursalList) {
                sucursalListSucursal.setDireccionId(null);
                sucursalListSucursal = em.merge(sucursalListSucursal);
            }
            em.remove(direccion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Direccion> findDireccionEntities() {
        return findDireccionEntities(true, -1, -1);
    }

    public List<Direccion> findDireccionEntities(int maxResults, int firstResult) {
        return findDireccionEntities(false, maxResults, firstResult);
    }

    private List<Direccion> findDireccionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Direccion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Direccion findDireccion(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Direccion.class, id);
        } finally {
            em.close();
        }
    }

    public int getDireccionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Direccion> rt = cq.from(Direccion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
