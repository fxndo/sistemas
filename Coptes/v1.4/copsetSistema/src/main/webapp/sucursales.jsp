<%-- 
    Document   : sucursales
    Created on : 19/05/2018, 02:22:49 AM
    Author     : fernando
--%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="Modelo.Almacen"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);

    List<Sucursal> sucursales = scj.listaSucursales();
    
    int notificacion=0;
    if(request.getParameter("noti")!=null){
        notificacion=Integer.parseInt(request.getParameter("noti"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Sucursales | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Sucursales<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Sucursales</div></div>
            </header>
            <div class="codigoBarra row">
                <div class="botonBarra col-md-2"><div onclick="flotante('nuevaSucursal');" class="btn btn-sm btn-default btn-group-justified"><img src="images/suma2.png" class="btnIcono">Agregar Sucursal</div></div>
            </div>
            <div style="padding: 0px 250px;">
                <%if (sucursales.size() > 1) {%>
                <table class="table table-responsive tabla-producto letraChica">
                    <tr class="cabeceraTabla">
                        <th style="width: 50px;text-align: center;">#</th>
                        <th>Nombre</th>
                        <th style="text-align: center;">No. Usuarios</th>
                        <th style="text-align: center;">Productos</th>
                        <th style="width: 100px;">Bodega</th>
                    </tr>

                    <%int valor = 1;%>
                    <%for (Sucursal sc : sucursales) {%>
                    <%if (sc.getId() != 1) {%>
                    <tr class="renglon<%=valor % 2%>">
                        <td style="text-align: center;"><%=valor%></td>
                        <td><a class="link" href="sucursal.jsp?id=<%=sc.getId()%>"><div class="btnSelector"><%=sc.getNombre()%></div></a></td>
                        <td style="text-align: center;"><strong><%=sc.getUsuarioList().size()%></strong></td>
                                <%int cantidad = 0;%>
                                <%for (Almacen al : sc.getAlmacenList()) {%>
                                <%cantidad += al.getCantidad();%>
                                <%}%>
                        <td style="text-align: center;"><strong><%=cantidad%></strong></td>
                        <td style="text-align: center;" class="devolucionTotal"><a href="bodega.jsp?id=<%=sc.getId()%>"><div><img src="images/almacenBlanco.png" class="iconoNormalGrande"></div></a></td>
                    </tr>
                    <%valor++;%>
                    <%}%>
                    <%}%>

                </table>
                <%} else {%>
                <div class="vacioGrande">
                    No hay Sucursales<br>
                    <img src="images/vacioGrande.png" class="imgVacio">
                </div>
                <%}%>
            </div>
        </main>

        <div id="fondoNegro" class="fondoNegro" style="display: none;"></div>
        <div id="nuevaSucursal" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Nueva Sucursal</div>
            <form action="agregarSucursal" method="post">
                <div class="principalFlotante">
                    <label class="entradaLabel">Nombre Sucursal</label><input class="entrada" type="text" required="" maxlength="45" onkeyup="this.value=this.value.toUpperCase()" name="sucursal"></label><br>
                    <hr>
                    <h5 style="margin-left: 210px;">Dirección</h5>
                    <label class="entradaLabel">Calle</label><input class="entrada" type="text" name="calle" required="on" maxlength="45"></label><br>
                    <label class="entradaLabel">Colonia</label><input class="entrada" type="text" name="colonia" required="on" maxlength="45"></label><br>
                    <label class="entradaLabel">Delegación</label><input class="entrada" type="text" name="delegacion" required="on" maxlength="45"></label><br>
                    <label class="entradaLabel">Codigo Postal</label><input class="entrada" type="text" name="cp" required="on" pattern="[0-9]+" maxlength="45"></label><br>
                    <label class="entradaLabel">Ciudad</label><input class="entrada" type="text" name="ciudad" required="on" maxlength="45"></label><br>
                    <label class="entradaLabel">Estado</label><input class="entrada" type="text" name="estado" required="on" maxlength="45"></label><br>
                </div>
                <div class="botonesFlotante"><button type="submit" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Agregar Sucursal</button><div onclick="flotante('nuevaSucursal')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <!--/////////////////////////////////////////////// Notificaciones  ///////////////////////////////////////-->
       
        <div class="notificacionBloque" style="<%if(notificacion==1){%>display:block;<%}else{%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Sucursal Agregada</div>
                La sucursal se agrego satisfactoriamente.
            </div>
        </div>
        <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
        <%@ include file="menu.jsp" %>
        
    </body>
</html>
