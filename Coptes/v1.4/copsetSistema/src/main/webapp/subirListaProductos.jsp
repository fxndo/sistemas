<%-- 
    Document   : subirListaProductos
    Created on : 14/03/2019, 11:24:09 AM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Nivel"%>
<%@page import="ModeloJSP.NivelJpaController"%>
<%@page import="Modelo.Editorial"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.EditorialJpaController"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%
    HttpSession sesion = request.getSession(true);
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    if (sesion.getAttribute("informacion") != null && informacion.getPermisosList().get(0).getAdministracionp() == 1) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");

    EditorialJpaController edj = new EditorialJpaController(emf);
    NivelJpaController nj = new NivelJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);

    List<Nivel> niveles = nj.nivelLista();
    List<Editorial> editoriales = edj.listaEditoriales();

    int notificacion = 0;
    if (request.getParameter("ma") != null) {
        notificacion = Integer.parseInt(request.getParameter("ma"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Productos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/funciones.js"></script>
        <script src="js/jquery.min.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Productos<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="buscarProducto"><div class="navBloque">Productos</div></a><div class="navegacionSelect">Subir Lista de Productos</div></div>
            </header>

            <div style="padding: 100px 15px;">
                <div class="row">
                    <div class="col-md-6">
                        <form action="agregarListaProductos" method="post" name="listaProductos" enctype="multipart/form-data" onsubmit="envMostrar();">
                            <label class="entradaLabel">Editorial</label><select id="editorial" class="entradaSelect" name="editorial">
                                <option value="0">Seleccione una opcion</option>
                                <%for (Editorial ed : editoriales) {%>
                                <option value="<%=ed.getId()%>"><%=ed.getNombre()%></option>
                                <%}%>
                            </select><br>
                            <label class="entradaLabel">Nivel</label><select id="nivel" name="nivel" class="entradaSelect">
                                <option value="0">Seleccione una opcion</option>
                                <%for (Nivel nv : niveles) {%>
                                <option value="<%=nv.getId()%>"><%=nv.getNombre()%></option>
                                <%}%>
                            </select><br>

                            <label class="entradaLabel">Tipo</label><select id="tipo" name="tipo" class="entradaSelect">
                                <option value="0">---</option>
                                <option value="Texto">Texto</option>
                                <option value="Literatura">Literatura</option>
                            </select><br>
                            <label class="entradaLabel">Hoja</label><select id="hoja" name="hoja" class="entradaCantidad">
                                <option value="0">-</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                            </select><br><br><br>
                            <label class="entradaLabel">Archivo Excel</label><div style="display: inline-block;"><input required="on" onchange="getFilePath();" id="fileUpload" type="file" name="archivo"></div><br><br>
                            <div style="margin-left: 210px;" class="labelSwitch">Actualizar productos repetidos</div>
                            <div id="switchBloque1" onclick="switchf(1)" class="switchBloque switchBlanco">
                                <div id="switch1" class="switch switchIzquierda"></div>
                                <input type="hidden" name="actualizar" id="input1" value="0">
                            </div><br><br>
                            <label class="entradaLabel"> </label><div onclick="enviarFormulariol();" class="btn btn-sm btn-default"><img src="images/suma2.png" class="btnIcono">Subir Archivo</div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h4 class="tituloListado">Instrucciones</h4>
                        <p class="parrafoFlotante">
                            Esta son las instrucciones para subir el archivo de la lista de productos.<br><br>
                            1.- El archivo debe tener una extencion xlsx.<br>
                            2.- El archivo debe contar con tres columnas (ISBN, Titulo, Precio).<br>
                            3.- La lista de productos debe inicar en la columna A y en la fila 2.<br>
                            4.- No debe tener espacios en blanco entre productos.<br>
                            <a href="images/productosPrueba.png" target="_blank">Ejemplo</a><br><br>

                            Selecciona en los campos los datos correspondientes y da clic en <strong>Subir Archivo</strong>
                        </p>
                    </div>
                </div>

            </div>

        </main>
        <div id="esperaCirculo" class="espera" style="display: none;"><img class="imgLoading" src="images/loading2.gif"></div>
        <!--///////////////////////////////////////////    NOTIFICACIONES    /////////////////////////////////////////////-->
        <div class="notificacionBloque" style="<%if (notificacion == 101) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/cancel.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Archivo erroneo</div>
                El <strong>Archivo</strong> no tiene la extención necesaria.
            </div>
        </div>

        <div class="notificacionBloque" style="<%if (notificacion == 102) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/cancel.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">ISBN erroneo</div>
                El <strong>ISBN</strong> (<%=request.getParameter("campo")%>) no es correcto.
            </div>
        </div>

        <div class="notificacionBloque" style="<%if (notificacion == 103) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/cancel.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Titulo erroneo</div>
                El <strong>Titulo</strong> (<%=request.getParameter("campo")%>) no es correcto en tamaño.
            </div>
        </div>

        <div class="notificacionBloque" style="<%if (notificacion == 104) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/cancel.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Precio erroneo</div>
                El <strong>Precio</strong> (<%=request.getParameter("campo")%>) no es correcto.
            </div>
        </div>

        <div id="espera" class="espera" style="display: none;"><img class="imgLoading" src="images/loading2.gif"></div>
            <%@ include file="menu.jsp" %>
        <script type="text/javascript">
            function getFilePath() {
                document.getElementById('elbueno').value = $('#fileUpload').val();
            }

            function envMostrar() {
                document.getElementById('espera').style.display = "block";
                document.getElementById('body').className = "desenfoque";
            }


            function enviarFormulariol() {

                var check = 0;
                if (document.getElementById('editorial').value !== "0") {
                    if (document.getElementById('nivel').value !== "0") {
                        if (document.getElementById('tipo').value !== "0") {
                            if (document.getElementById('hoja').value !== "0") {
                                if (document.getElementById('fileUpload').value != "") {

                                } else {
                                    check = 1;
                                    window.alert("LLena el campó Archivo");
                                }
                            } else {
                                check = 1;
                                window.alert("LLena el campó HOJA");
                            }
                        } else {
                            check = 1;
                            window.alert("LLena el campo TIPO");

                        }
                    } else {
                        check = 1;
                        window.alert("Llena el campo NIVEL");

                    }
                } else {
                    check = 1;
                    window.alert("LLena el campo Editorial");

                }

                if (check == 0) {
//                    window.alert("Bien "+document.getElementById('fileUpload').value);
                    document.getElementById('esperaCirculo').style.display = "block";
                    document.getElementById('body').className = "desenfoque";
                    document.listaProductos.submit();
                }
            }

            function switchf(id) {
                if (document.getElementById('switchBloque' + id).className === "switchBloque switchVerde") {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchBlanco";
                    document.getElementById('switch' + id).className = "switch switchIzquierda";
                    document.getElementById('input' + id).value = "0";
                } else {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchVerde";
                    document.getElementById('switch' + id).className = "switch switchDerecha";
                    document.getElementById('input' + id).value = "1";
                }
            }

        </script>

    </body>
</html>
