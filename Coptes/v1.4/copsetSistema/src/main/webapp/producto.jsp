<%-- 
    Document   : producto
    Created on : 19/04/2018, 05:04:44 PM
    Author     : fernando
--%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Editorial"%>
<%@page import="ModeloJSP.EditorialJpaController"%>
<%@page import="Modelo.Proveedor"%>
<%@page import="ModeloJSP.ProveedorJpaController"%>
<%@page import="ModeloJSP.AlmacenJpaController"%>
<%@page import="Modelo.Nivel"%>
<%@page import="ModeloJSP.NivelJpaController"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Producto"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");

    ProductoJpaController pj = new ProductoJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);
    EditorialJpaController ed = new EditorialJpaController(emf);
    AlmacenJpaController a = new AlmacenJpaController(emf);
    NivelJpaController n = new NivelJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);

    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
    List<Sucursal> sucursales = scj.findSucursalEntities();
    List<Editorial> editoriales = ed.listaEditoriales();
    List<Nivel> niveles = n.findNivelEntities();
    Producto producto = pj.findProducto(Integer.parseInt(request.getParameter("id")));

    int notificacion = 0;
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Producto</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Producto<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion">
                    <a href="inicio.jsp">
                        <%if(request.getParameter("pedido")!=null){%>
                        <a href="pedido.jsp?id=<%=request.getParameter("pedido")%>&list=proveedores"><div class="navBloque"><img src="images/back.png" class="btnIcono">Atras</div></a>
                        <%}%>
                        <div class="navBloque">Inicio</div></a>
                        <a href="buscarProducto"><div class="navBloque">Productos</div></a>
                        <div class="navegacionSelect">Producto</div></div>

            </header>
            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <%if (informacion.getPermisosList().get(0).getAlmacenp() == 1) {%>
                            <div id="stock1" onclick="listado('stock');" class="listadoOpcion"><img src="images/colorStock.png" class="btnIcono2">Stock</div>
                            <%}else{%>
                            <div id="stock1" class="listadoOpcionDes"><img src="images/colorStockGris.png" class="btnIcono2">Stock</div>
                            <%}%>
                            
                            <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1) {%>
                            <div id="opciones1" onclick="listado('opciones');" class="listadoOpcion"><img src="images/colorOpciones.png" class="btnIcono2">Opciones</div>
                            <%}else{%>
                            <div id="opciones1" class="listadoOpcionDes"><img src="images/colorOpcionesGris.png" class="btnIcono2">Opciones</div>
                            <%}%>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">

                    <!--/////////////////////////////////////////////////////Informacion///////////////////////////////////////////////////-->
                    <div class="listPedido" id="informacion">
                        <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                        <p class="parrafoListado">
                            Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                        </p>
                        <div class="separadorListado"></div>
                        <br><br>
                        <div style="margin: 0px 200px;background: white;padding: 10px;">
                            <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                <tr>
                                    <td class="ladoIzquierdo">ISBN</td>
                                    <td class="ladoDerecho"><span class="numPedido"><%=producto.getIsbn()%></span></td>
                                </tr>
                                <tr>
                                    <td class="ladoIzquierdo">Tipo</td>
                                    <td class="ladoDerecho"><%=producto.getTextlit() %></td>
                                </tr>
                                <tr>
                                    <td class="ladoIzquierdo">Titulo</td>
                                    <td class="ladoDerecho"><%=producto.getTitulo() %></td>
                                </tr>
                                <tr>
                                    <td class="ladoIzquierdo">Serie</td>
                                    <td class="ladoDerecho"><%=producto.getSerie() %></td>
                                </tr>
                                <tr>
                                    <td class="ladoIzquierdo">Editorial</td>
                                    <td class="ladoDerecho"><%=producto.getEditorialId1().getNombre()  %></td>
                                </tr>
                                <tr>
                                    <td class="ladoIzquierdo">Autor</td>
                                    <td class="ladoDerecho"><%=producto.getAutor() %></td>
                                </tr>
                                <tr>
                                    <td class="ladoIzquierdo">Nivel</td>
                                    <td class="ladoDerecho"><%=producto.getNivelId().getNombre() %></td>
                                </tr>
                                <tr>
                                    <td class="ladoIzquierdo">Precio</td>
                                    <td class="ladoDerecho"><strong class=""><%=nf.format(Double.parseDouble(producto.getPrecioventa()))%> MXN</strong></td>
                                </tr>
                                <tr>
                                    <td class="ladoIzquierdo">Stock</td>
                                    <%if(sucursal.getId()==1){%>
                                    <td class="ladoDerecho"><strong class="">---</strong></td>
                                    <%}else{%>
                                    <td class="ladoDerecho"><strong class=""><%=a.productosAlmacen(producto.getId(),sucursal.getId()) %></strong></td>
                                    <%}%>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                    <!--<br><br>-->
                    <!--<div class="separadorListado"></div>-->

                    <!--/////////////////////////////////////////////////////Stock///////////////////////////////////////////////////-->


                    <div  class="listPedido" id="stock" style="display: none;">
                    <%if (informacion.getPermisosList().get(0).getAlmacenp() == 1) {%>
                        <!--<div style="padding: 10px 100px;">-->
                        <!--<br>-->
                        <h4 class="tituloListado"><img src="images/colorStock.png" class="iconoTitulo">Stock</h4>
                        <p class="parrafoListado">
                            Agrega producto al stock de la sucursal o edita la cantidad existente.
                        </p>
                        <!--<br>-->
                        <div class="separadorListado"></div>
                        <!--</div>-->
                        <%if(sucursal.getId()!=1){%>
                        <label class="entradaLabel">Cantidad Actual:</label><strong><%=a.productosAlmacen(producto.getId(), sucursal.getId())%></strong> Unidades
                        <%}%>
                        <br><br>
                        <h4 style="margin-left: 210px;" class="tituloListado2">Agregar Stock</h4>
                        <form action="agregarAlmacen" method="post">
                            <label class="entradaLabel">Cantidad a Agregar:</label><input autocomplete="off" class="entradaCantidad" type="text" title="Ingresa la cantidad que se sumara al Stock" onkeypress="return validarEntradaNumero(event);" name="unidades" required="" maxlength="5" pattern="[0-9]+"><br>
                            <%if (sucursal.getId() == 1) {%>
                            <label class="entradaLabel">Sucursal</label><select name="idSucursal" class="entradaSelect">
                                <%for (Sucursal sc : sucursales) {%>
                                <%if (sc.getId() != 1) {%>
                                <option value="<%=sc.getId()%>"><%=sc.getNombre()%></option>
                                <%}%>
                                <%}%>
                            </select><br>
                            <%} else {%>
                            <input type="hidden" name="idSucursal" value="<%=sucursal.getId()%>">
                            <%}%>
                            <input type="hidden" name="idProducto" value="<%=producto.getId()%>">
                            <br>
                            <label class="entradaLabel"> </label><button type="submit" value="Agregar Stock" class="btn btn-default btn-sm"><img src="images/suma2.png" class="btnIcono">Agregar Stock</button>
                        </form>
                        <br>
                        <div class="separadorListado"></div>
                        <!--<br>-->
                        <!------------------------------------------------------------------------------->
                        <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1) {%>
                        <form action="agregarAlmacen" method="post">
                            <h4 style="margin-left: 210px;" class="tituloListado2">Editar Stock</h4>
                            <label class="entradaLabel">Cantidad Nueva</label><input class="entradaCantidad" type="text" name="unidades" title="ingresa la nueva cantidad que remplaza el Stock" required="" maxlength="5" pattern="[0-9]+"><br>
                            <%if (sucursal.getId() == 1) {%>
                            <label class="entradaLabel">Sucursal</label><select name="idSucursal" class="entradaSelect">
                                <%for (Sucursal sc : sucursales) {%>
                                <%if (sc.getId() != 1) {%>
                                <option value="<%=sc.getId()%>"><%=sc.getNombre()%></option>
                                <%}%>
                                <%}%>
                            </select><br>
                            <%} else {%>
                            <input type="hidden" name="idSucursal" value="<%=sucursal.getId()%>">
                            <%}%>
                            <input type="hidden" name="idProducto" value="<%=producto.getId()%>">
                            <input type="hidden" name="idEditar" value="8807">
                            <br>
                            <label class="entradaLabel"> </label><button type="submit" value="Editar Stock" class="btn btn-default btn-sm"><img src="images/editarColor.png" class="btnIcono">Editar Stock</button>
                        </form>
                        <%}%>
                    <%}%>
                    </div>
                    <!--<br><br>-->
                    <!------------------------------------------------------Opciones----------------------------------------->
                    <div  class="listPedido" id="opciones" style="display: none;">
                    <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1) {%>
                        <!--<br>-->
                        <h4 class="tituloListado"><img src="images/colorOpciones.png" class="iconoTitulo">Opciones</h4>
                        <p class="parrafoListado">
                            Listado de Opciones para este <strong>producto</strong>
                        </p>
                        <!--<br>-->
                        <div class="separadorListado"></div>
                        <p class="parrafoListado">
                            Elimina el producto para que no se visualice en la lista de productos y al hacer un nuevo pedido.
                        </p>
                        <label class="apaB"> </label><div onclick="flotante('eliminarProducto');" class="btn btn-default bbtn btn-sm"><img src="images/less.png" class="btnIcono">Eliminar Producto</div>
                        <br><br>
                        <div class="separadorListado2"></div>
                        <p class="parrafoListado">
                            Edita los datos del producto.
                        </p>
                        <label class="apaB"> </label><div onclick="flotante('editarProducto')" class="btn bbtn btn-default btn-sm"><img src="images/editarColor.png" class="btnIcono">Editar Producto</div>
                        <!--</div>-->
                    <%}%>
                    </div>
                    <br><br><br>
                    <!------------------------------------------------------------------------------------------------------------------>
                </div>
            </div>
        </main>

        <div id="eliminarProducto" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Eliminar Producto</div>
            <div class="principalFlotante">
                <br>
                <h4 class="text-center">¿Seguro que quieres eliminar el producto?</h4>
                <br><br>
            </div>
            <div class="botonesFlotante"><a href="eliminarProducto?id=<%=producto.getId()%>" class="btn btn-danger btn-sm"><img src="images/aceptar.png" class="btnIcono">Eliminar Producto</a><div onclick="flotante('eliminarProducto');" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
        </div>

        <div id="editarProducto" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Editar Producto</div>
            <form action="editarProducto" method="post">
                <div class="principalFlotante">
                    <%if(request.getParameter("pedido")!=null){%>
                    <input type="hidden" name="regresar" value="<%=request.getParameter("pedido")%>">
                    <%}%>
                    <label class="entradaLabel">ISBN/Serial</label><input type="text" class="entrada" name="isbn" pattern="[0-9]+" value="<%=producto.getIsbn()%>"><br>
                    <label class="entradaLabel">Texto/Literario</label><select name="textlit" class="entradaSelect">
                        <%if (producto.getTextlit().equals("Texto")) {%>
                        <option selected>Texto</option>
                        <option>Literario</option>
                        <%} else {%>
                        <option>Texto</option>
                        <option selected>Literario</option>
                        <%}%>
                    </select><br>
                    <label class="entradaLabel">Titulo</label><input type="text" class="entrada" name="titulo" maxlength="250" value="<%=producto.getTitulo()%>"><br>
                    <label class="entradaLabel">Serie</label><input type="text" class="entrada" name="serie" maxlength="45" value="<%=producto.getSerie()%>"><br>
                    <label class="entradaLabel">Editorial</label><select name="idEditorial" class="entradaSelect">
                        <%for (Editorial eed : editoriales) {%>
                        <%if (eed.getId() == producto.getEditorialId1().getId()) {%>
                        <option value="<%=eed.getId()%>" selected><%=eed.getNombre()%></option>
                        <%} else {%>
                        <option value="<%=eed.getId()%>"><%=eed.getNombre()%></option>
                        <%}%>
                        <%}%>
                    </select><br>
                    <label class="entradaLabel">Autor</label><input type="text" class="entrada" name="autor" maxlength="99" value="<%=producto.getAutor()%>"><br>
                    <label class="entradaLabel">Nivel</label><select class="entradaSelect" name="idNivel">
                        <%for (Nivel nv : niveles) {%>
                        <%if (nv.getId() == producto.getNivelId().getId()) {%>
                        <option value="<%=nv.getId()%>" selected><%=nv.getNombre()%></option>
                        <%} else {%>
                        <option value="<%=nv.getId()%>"><%=nv.getNombre()%></option>
                        <%}%>
                        <%}%>
                    </select><br>
                    <label class="entradaLabel">Precio $</label><input type="text" onkeypress="return check(event);" class="entrada" name="pventa" placeholder="0.00" pattern="[0-9]+(\.[0-9][0-9]?)?" maxlength="8" value="<%=producto.getPrecioventa()%>"><br>

                    <input type="hidden" name="idProducto" value="<%=producto.getId()%>">
                    <br>
                </div>
                    <div class="botonesFlotante"><button type="submit" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Editar Producto</button><div onclick="flotante('editarProducto');" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <!--/////////////////////////////////////   Notificacion    ///////////////////////////////////////////////-->
        <div class="notificacionBloque" style="<%if (notificacion == 20) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Agregar Stock</div>
                El <strong>producto</strong> fue agregado satisfactoriamente.
            </div>
        </div>
        <div class="notificacionBloque" style="<%if (notificacion == 21) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Editar Stock</div>
                El <strong>stock</strong> fue editado satisfactoriamente.
            </div>
        </div>
        <%@ include file="menu.jsp" %>
        <script>
            function listado(id) {

                document.getElementById('informacion').style.display = "none";
                document.getElementById('informacion1').className = "listadoOpcion";
                document.getElementById('stock').style.display = "none";
                document.getElementById('stock1').className = "listadoOpcion";
                document.getElementById('opciones').style.display = "none";
                document.getElementById('opciones1').className = "listadoOpcion";
                document.getElementById(id).style.display = "block";
                document.getElementById(id + "1").className = "selectL";

            }
        </script>
    </body>
</html>
