<%-- 
    Document   : finanzas
    Created on : 7/08/2018, 12:44:03 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="ModeloJSP.VentasJpaController"%>
<%@page import="ModeloJSP.UsuarioJpaController"%>
<%@page import="Modelo.Editorial"%>
<%@page import="ModeloJSP.EditorialJpaController"%>
<%@page import="ModeloJSP.VentasHasProductoJpaController"%>
<%@page import="Modelo.VentasHasProducto"%>
<%@page import="Modelo.Ventas"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="Modelo.Pago"%>
<%@page import="Modelo.Pedido"%>
<%@page import="Modelo.Cliente"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.ClienteJpaController"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");

    ProductoJpaController pj = new ProductoJpaController(emf);
    ClienteJpaController c = new ClienteJpaController(emf);
    VentasHasProductoJpaController vvh = new VentasHasProductoJpaController(emf);
    EditorialJpaController ed = new EditorialJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

    List<Cliente> deudores = c.deudoresLista();
    List<Editorial> editoriales = ed.listaEditoriales();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Finanzas</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Finanzas<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Finanzas</div></div>

            </header>
            <table class="table tabla-producto">
                <tr class="cabeceraTabla">
                    <th style="width: 60px; text-align: center;"># Pedido</th>
                    <th>Escuela</th>
                    <th>Productos</th>
                    <th>Devoluciones</th>
                    <th>Total</th>
                </tr>
                <%int valor = 1;%>
                <%for (Pedido pdf : p.findPedidoEntities()) {%>
                <tr class="renglon<%=valor % 2%>">
                    <td onclick="mostrarPedido('<%=pdf.getId()%>')" style="text-align: center;"><%=pdf.getId()%></td>
                    <td><%=pdf.getClienteId().getEscuela()%></td>
                    <%int productos = 0;%>
                    <%int devolucion = 0;%>
                    <%for (Pedidoproveedor pprf : pdf.getPedidoproveedorList()) {%>
                    <%for (Pedidoparte ppaf : pprf.getPedidoparteList()) {%>
                    <%productos += ppaf.getCantidad();
                        devolucion += ppaf.getDevolucion();
                    %>
                    <%}%>
                    <%}%>
                    <td><%=productos%></td>
                    <td><%=devolucion%></td>
                    <td><%=pdf.getTotal()%></td>
                </tr>
                <tr><td colspan="5" id="ped<%=pdf.getId()%>"></td></tr>
                    <%valor++;%>
                    <%}%>

            </table>
        </div>
    </main>
    <%@ include file="menu.jsp" %>
    <!-----------------------------------------------Scripts--------------------------------------->
    <script>
        function listado(id) {

        document.getElementById('deudores').style.display = "none";
        document.getElementById('deudores1').className = "listadoOpcion";
        document.getElementById('tpv').style.display = "none";
        document.getElementById('tpv1').className = "listadoOpcion";
        document.getElementById('pedidos').style.display = "none";
        document.getElementById('pedidos1').className = "listadoOpcion";
        document.getElementById(id).style.display = "block";
        document.getElementById(id + "1").className = "selectL";
        }

        function mostrarPedido(id){
//            window.alert("hola");
        document.getElementById('ped' + id).innerHTML = "<table class=\"table\">\n\
    <tr class='cabeceraTabla'><th style=\"width: 100px; text-align: center;\" >Isbn</th>\n\
    <td style=\"width: 200px; text-align: center;\">Titulo</td>\n\
    <td style=\"width: 100px; text-align: center;\">Eitorial</td>\n\
    <td style=\"width: 50px; text-align: center;\">Devoluciones</td>\n\
    <td style=\"width: 50px; text-align: center;\">Productos</td>\n\
    <td>Precio Unitario</td>\n\
    <td>Descuento Escuela</td>\n\
    <td>Descuento a Coptes</td>\n\
    <td>Descuento para Coptes</td>\n\
    <td>$ unit. Coptes</td>\n\
    <td>$ unit. Escuela</td>\n\
    <td>Valor de pedido</td>\n\
    <td>Valor Publico</td>\n\
    <td>Valor coptes</td>\n\
    <td>Valor escuela</td>\n\
    <td>Devolucion Coptes</td>\n\
    <td>Devolucion Escuela</td>\n\
    <td>Utilidad Coptes</td>\n\
    <td>Utilidad Escuela</td>\n\
    <td>Venta Real</td></tr>\n\
</table>";
        }
    </script>
</body>
</html>
