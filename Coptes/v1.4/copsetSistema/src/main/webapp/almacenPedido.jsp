<%-- 
    Document   : almacenPedido
    Created on : 3/07/2018, 01:59:35 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    Pedidoproveedor pedido = pprj.findPedidoproveedor(Integer.parseInt(request.getParameter("id")));
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Almacen | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody"  class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Almacen<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="almacen.jsp"><div class="navBloque">Almacen</div></a><div class="navegacionSelect">Lista</div></div>
            </header>
            <div style="padding: 30px 100px;">
                <p class="parrafoListado">
                    <strong>Nota: </strong>Indica si el pedido del proveedor llego completo marcando la casilla <strong>completo</strong> o poniendo las unidades faltantes en el campo <strong>Faltante</strong>
                </p><br><br>
                <form action="entregaPedidoProveedor" method="post" name="almacenFormulario">
                    <input type="hidden" name="idPedidoProveedor" value="<%=pedido.getId()%>">
                    <table class="table table-responsive tabla-producto letraChica">
                        <tr>
                            <th style="text-align: center;">#</th>
                            <th>Isbn</th>
                            <th>Titulo</th>
                            <th>Autor</th>
                            <th>Serie</th>
                            <th style="width: 80px;text-align: center;">Cantidad</th>
                            <th style="width: 80px;">Completo</th>
                            <th style="width: 50px;">Faltante</th>
                        </tr>
                        <%int valor = 0;%>
                        <%for (Pedidoparte pp : pedido.getPedidoparteList()) {%>
                        <%if (pp.getFaltante() > 0) {%>
                        <%valor++;%>
                        <tr class="renglon<%=valor % 2%>">
                            <td class="gris"><strong><%=valor%></strong></td>
                            <td><%=pp.getProductoId().getIsbn()%></td>
                            <td><%=pp.getProductoId().getTitulo()%></td>
                            <td><%=pp.getProductoId().getAutor()%></td>
                            <%if (pp.getProductoId().getSerie() != null) {%>
                            <td><%=pp.getProductoId().getSerie()%></td>
                            <%} else {%>
                            <td><span class="noInformacion">Sin información</span></td>
                            <%}%>
                            <td style="text-align:center;background: #e0e0e0;border-bottom: 1px solid #d1d1d1;"><strong id="faltante<%=valor%>"><%=pp.getFaltante()%></strong></td>
                            <!--<td style="text-align: center;"><input id="completo<%//= //valor%>" name="completo<%//= //valor%>" type="checkbox"></td>-->
                            <td style="text-align: center;padding: 0px;">
                                <div style="position: relative;top: 3px;" id="switchBloque<%=valor%>" onclick="switchf('<%=valor%>')" class="switchBloque switchBlanco">
                                    <div id="switch<%=valor%>" class="switch switchIzquierda"></div>
                                </div>
                            </td>
                            <td style="padding: 0px;"><input id="cantidad<%=valor%>" onfocus="limpiarCantidad('<%=valor%>');" onkeypress="return validarEntradaNumero(event);" maxlength="3" max="<%=pp.getFaltante()%>" value="0"  name="cantidad<%=valor%>" class="entradaTablac" autocomplete="off" type="number"></td>
                        <input type="hidden" name="idPro<%=valor%>" value="<%=pp.getId()%>">
                        </tr>
                        <input id="completo<%=valor%>" name="completo<%=valor%>" type="hidden" value="0">
                        <%}%>
                        <%}%>

                    </table>
                    <div onclick="checarFormulario()" class="btn btn-default btn-sm"><img src="images/sipermiso.png" class="btnIcono">Aceptar Entrega</div>
                </form>
            </div>
        </main>
        <%@ include file="menu.jsp" %>
        <script>
            function checarFormulario() {
                var check = 0;
                var cuenta=0;
                for (var z = 1; z <=<%=valor%>; z++) {
                    var str = "completo" + z;
                    if (document.getElementById(str).value == 0) {
                        if (document.getElementById('cantidad' + z).value.length > 0 && parseInt(document.getElementById('cantidad' + z).value)>0) {
                            cuenta+=parseInt(document.getElementById('cantidad' + z).value);
                            if (document.getElementById('cantidad' + z).value > parseInt(document.getElementById('faltante' + z).innerHTML)) {
                                check = 1;
                                window.alert("La cantidad es mayor en el campo : " + z);
                                break;
                            }
                        } else {
                            window.alert("Llene el campo: " + z);
                            check = 1;
                            break;
                        }

                    }
                }
               
                if (check === 0) {
                    document.almacenFormulario.submit();
//                    window.alert("Bien"+cuenta);
                }
            }

            function switchf(id) {
                if (document.getElementById('switchBloque' + id).className === "switchBloque switchVerde") {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchBlanco";
                    document.getElementById('switch' + id).className = "switch switchIzquierda";
                    document.getElementById('cantidad' + id).disabled = false;
                    document.getElementById('completo' + id).value = "0";
                } else {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchVerde";
                    document.getElementById('switch' + id).className = "switch switchDerecha";
                    document.getElementById('cantidad' + id).value = "";
                    document.getElementById('cantidad' + id).disabled = true;
                    document.getElementById('completo' + id).value = "1";
                }
            }

            function limpiarCantidad(id) {
                document.getElementById("cantidad" + id).value = "";
            }
        </script>

    </body>
</html>
