<%-- 
    Document   : usuario
    Created on : 19/05/2018, 03:22:02 AM
    Author     : fernando
--%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.UsuarioJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    UsuarioJpaController u = new UsuarioJpaController(emf);
    Usuario usuario = u.findUsuario(Integer.parseInt(request.getParameter("id")));
    SucursalJpaController scj = new SucursalJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);

    List<Sucursal> sucursales = scj.findSucursalEntities();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Usuario | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Usuario<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="usuarios.jsp"><div class="navBloque">Usuarios</div></a><div class="navegacionSelect">Usuario</div></div>
            </header>
            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <div id="permisos1" onclick="listado('permisos');" class="listadoOpcion"><img src="images/colorPermiso.png" class="btnIcono2">Permisos</div>
                            <div id="opciones1" onclick="listado('opciones');" class="listadoOpcion"><img src="images/colorOpciones.png" class="btnIcono2">Opciones</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <!--//////////////////////////////////////////////////  Informacion //////////////////////////////////////////////-->
                    <form action="editarUsuario" method="post">
                        <input type="hidden" name="idUsuario" value="<%=usuario.getId()%>">

                        <div class="listPedido" id="informacion">
                            <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                            <p class="parrafoListado">
                                Para editar el producto cambia los campos a editar y presiona el boton <strong>Siguiente</strong> para editar los permisos.
                            </p>
                            <div class="separadorListado"></div>
                            <br><br>
                            <label class="entradaLabel">Sucursal</label><select class="entradaSelect" name="sucursal">
                                
                                <%if (informacion.getSucursalId().getId() == 1) {%>
                                <option selected="on" value="1">General</option>
                                <%}%>
                                
                                <%for (Sucursal sc : sucursales) {%>
                                <%if (sc.getId() != 1) {%>

                                <%if (sc.getId() == usuario.getSucursalId().getId()) {%>
                                <option selected="on" value="<%=sc.getId()%>"><%=sc.getNombre()%></option>
                                <%} else {%>
                                <option value="<%=sc.getId()%>"><%=sc.getNombre()%></option>
                                <%}%>
                                <%}%>

                                <%}%>
                                
                            </select><br><br>

                            <label class="entradaLabel">Nombre</label><input type="text" name="nombre" value="<%=usuario.getNombre()%>" class="entrada" maxlength="299" required="on"><br>
                            <label class="entradaLabel">Cargo</label><input type="text" name="cargo" value="<%=usuario.getTipo()%>" class="entrada" maxlength="99" required="on"><br>
                            <label class="entradaLabel">Telefono</label><input type="text" name="telefono" value="<%=usuario.getTelefono()%>" class="entrada" maxlength="10" required="on"><br><br>
                            <label class="entradaLabel">Email</label><input type="text" name="email" value="<%=usuario.getEmail()%>" class="entrada" required="on" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"><br>
                            <label class="entradaLabel">Contraseña</label><input type="password" name="password" value="<%=usuario.getPassword()%>" class="entrada" maxlength="44" required="on"><br>
                            <br><br>
                            <label class="entradaLabel"> </label><div onclick="listado('permisos');" class="btn btn-sm btn-default"><img src="images/next.png" class="btnIcono">Siguiente</div>
                        </div>
                        <!--/////////////////////////////////////////////   Permisos    /////////////////////////////////////7-->
                        <div class="listPedido" id="permisos" style="display: none;">
                            <h4 class="tituloListado"><img src="images/colorPermiso.png" class="iconoTitulo">Permisos</h4>
                            <p class="parrafoListado">
                                Otorga permisos al usuario con el switch verde y da clic en <strong>Editar Usuario</strong>.
                            </p>
                            <div class="separadorListado"></div>
                            <!------->
                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Administración</label>
                                <%if (usuario.getPermisosList().get(0).getAdministracionp() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque6" onclick="switchf(6)" class="switchBloque switchVerde">
                                    <div id="switch6" class="switch switchDerecha"></div>
                                </div>
                                <input id="input6" class="form-control" type="hidden" name="administrador" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque6" onclick="switchf(6)" class="switchBloque switchBlanco">
                                    <div id="switch6" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input6" class="form-control" type="hidden" name="administrador" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Iniciar caja TPV</span><br>
                                    <span class="labelSwitch2">Realizar remisiones de sucursal a sucursal</span><br>
                                    <span class="labelSwitch2">Entrada de efectivo caja TPV</span><br>
                                    <span class="labelSwitch2">Salida de efectivo caja TPV</span><br>
                                    <span class="labelSwitch2">Cerrar caja TPV</span><br>
                                    <span class="labelSwitch2">Editar stock</span><br>
                                    <span class="labelSwitch2">Visualizar, editar y crear Descuentos</span><br>
                                    <span class="labelSwitch2">Visualizar, editar y crear Clientes</span><br>
                                    <span class="labelSwitch2">Visualizar, editar y crear Proveedores</span><br>
                                    <span class="labelSwitch2">Visualizar, editar y crear Productos</span><br>
                                    <span class="labelSwitch2">Visualizar historial ventas</span><br>
                                    <span class="labelSwitch2">Visualizar historial de remisiones</span><br>
                                    <span class="labelSwitch2">Visualizar Pedidos ocultos y cancelados</span><br>
                                </div>
                            </div>
                            <!------------>
                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Almacen</label>
                                <%if (usuario.getPermisosList().get(0).getAlmacenp() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque3" onclick="switchf(3)" class="switchBloque switchVerde">
                                    <div id="switch3" class="switch switchDerecha"></div>
                                </div>
                                <input id="input3" class="form-control" type="hidden" name="almacen" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque3" onclick="switchf(3)" class="switchBloque switchBlanco">
                                    <div id="switch3" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input3" class="form-control" type="hidden" name="almacen" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Agregar producto al stock</span><br>
                                    <span class="labelSwitch2">Ingresar pedidos a bodega</span><br>
                                    <span class="labelSwitch2">Administrar Traslados</span><br>
                                    <span class="labelSwitch2">Visualizar Productos</span><br>
                                    <span class="labelSwitch2">Visualizar Pedidos</span><br>
                                </div>
                            </div>
                            <!----------->
                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Compras</label>
                                <%if (usuario.getPermisosList().get(0).getComprasp() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque2" onclick="switchf(2)" class="switchBloque switchVerde">
                                    <div id="switch2" class="switch switchDerecha"></div>
                                </div>
                                <input id="input2" class="form-control" type="hidden" name="compras" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque2" onclick="switchf(2)" class="switchBloque switchBlanco">
                                    <div id="switch2" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input2" class="form-control" type="hidden" name="compras" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Realizar compras de pedidos</span><br>
                                    <span class="labelSwitch2">Visualizar historial de compras</span><br>
                                </div>
                            </div>
                            <!------------>
                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Pagos</label>
                                <%if (usuario.getPermisosList().get(0).getPagosp() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque4" onclick="switchf(4)" class="switchBloque switchVerde">
                                    <div id="switch4" class="switch switchDerecha"></div>
                                </div>
                                <input id="input4" class="form-control" type="hidden" name="pagos" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque4" onclick="switchf(4)" class="switchBloque switchBlanco">
                                    <div id="switch4" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input4" class="form-control" type="hidden" name="pagos" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Realizar pagos de pedidos</span><br>
                                    <span class="labelSwitch2">Visualizar historial pagos</span><br>
                                </div>
                            </div>
                            <!----------->
                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Pedidos</label>
                                <%if (usuario.getPermisosList().get(0).getPedidosp() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque10" onclick="switchf(10)" class="switchBloque switchVerde">
                                    <div id="switch10" class="switch switchDerecha"></div>
                                </div>
                                <input id="input10" class="form-control" type="hidden" name="pedidos" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque10" onclick="switchf(10)" class="switchBloque switchBlanco">
                                    <div id="switch10" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input10" class="form-control" type="hidden" name="pedidos" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Realizar pedidos</span><br>
                                    <span class="labelSwitch2">Visualizar historial de Pedidos</span><br>
                                </div>
                            </div>
                            <!------------------------------------------->
                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Proveedores</label>
                                <%if (usuario.getPermisosList().get(0).getProveedoresp() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque8" onclick="switchf(8)" class="switchBloque switchVerde">
                                    <div id="switch8" class="switch switchDerecha"></div>
                                </div>
                                <input id="input8" class="form-control" type="hidden" name="proveedores" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque8" onclick="switchf(8)" class="switchBloque switchBlanco">
                                    <div id="switch8" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input8" class="form-control" type="hidden" name="proveedores" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Configurar proveedores de pedidos</span><br>
                                    <span class="labelSwitch2">Configurar descuentos de pedidos</span><br>
                                    <span class="labelSwitch2">Crear traslados de pedidos</span><br>
                                </div>
                            </div>
                            <!------------------------------->
                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Remisión</label>
                                <%if (usuario.getPermisosList().get(0).getRemisionp() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque5" onclick="switchf(5)" class="switchBloque switchVerde">
                                    <div id="switch5" class="switch switchDerecha"></div>
                                </div>
                                <input id="input5" class="form-control" type="hidden" name="remisiones" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque5" onclick="switchf(5)" class="switchBloque switchBlanco">
                                    <div id="switch5" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input5" class="form-control" type="hidden" name="remisiones" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Realizar remisiones de pedidos</span><br>
                                    <span class="labelSwitch2">Realizar remisiones de sucursal a sucursal</span><br>
                                    <span class="labelSwitch2">Visualizar lista de Productos</span><br>
                                    <span class="labelSwitch2">Visualizar lista de Clientes</span><br>
                                    <span class="labelSwitch2">Visualizar lista de Pedidos</span><br>
                                    <span class="labelSwitch2">Visualizar historial de remisiones</span><br>
                                </div>
                            </div>
                            <!------------>
                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Super Usuario</label>
                                <%if (usuario.getPermisosList().get(0).getSuperusuario() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque7" onclick="switchf(7)" class="switchBloque switchVerde">
                                    <div id="switch7" class="switch switchDerecha"></div>
                                </div>
                                <input id="input7" class="form-control" type="hidden" name="superUsuario" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque7" onclick="switchf(7)" class="switchBloque switchBlanco">
                                    <div id="switch7" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input7" class="form-control" type="hidden" name="superUsuario" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Visualizar, editar y crear Sucursales</span><br>
                                    <span class="labelSwitch2">Visualizar, editar y crear Usuarios</span><br>
                                    <span class="labelSwitch2">Visualizar, Finanzas</span><br>
                                </div>
                            </div>
                            <!---------------->
                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Terminal Punto de Venta</label>
                                <%if (usuario.getPermisosList().get(0).getTpvp() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque1" onclick="switchf(1)" class="switchBloque switchVerde">
                                    <div id="switch1" class="switch switchDerecha"></div>
                                </div>
                                <input id="input1" class="form-control" type="hidden" name="tpv" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque1" onclick="switchf(1)" class="switchBloque switchBlanco">
                                    <div id="switch1" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input1" class="form-control" type="hidden" name="tpv" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Manejar la Terminal Punto de Venta</span><br>
                                    <span class="labelSwitch2">Visualizar historial de ventas</span><br>
                                </div>
                            </div>
                            <!-------->

                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Traslados</label>
                                <%if (usuario.getPermisosList().get(0).getTraslados() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque9" onclick="switchf(9)" class="switchBloque switchVerde">
                                    <div id="switch9" class="switch switchDerecha"></div>
                                </div>
                                <input id="input9" class="form-control" type="hidden" name="traslados" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque9" onclick="switchf(9)" class="switchBloque switchBlanco">
                                    <div id="switch9" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input9" class="form-control" type="hidden" name="traslados" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Aceptar o denegar los traslados de otras sucursales</span><br>
                                </div>
                            </div>
                            <!----------------------------------------->

                            <div class="bloqueNormal">
                                <label class="tituloSwitch">Ventas</label>
                                <%if (usuario.getPermisosList().get(0).getVentap() == 1) {%>
                                <div style="margin-left: 100px;" id="switchBloque11" onclick="switchf(11)" class="switchBloque switchVerde">
                                    <div id="switch11" class="switch switchDerecha"></div>
                                </div>
                                <input id="input11" class="form-control" type="hidden" name="ventas" value="1">
                                <%} else {%>
                                <div style="margin-left: 100px;" id="switchBloque11" onclick="switchf(11)" class="switchBloque switchBlanco">
                                    <div id="switch11" class="switch switchIzquierda"></div>
                                </div>
                                <input id="input11" class="form-control" type="hidden" name="ventas" value="0">
                                <%}%>
                                <div class="descripcionSwitch">
                                    <span class="labelSwitch2">Visualizar historial de ventas</span><br>
                                </div>
                            </div>

                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-sm btn-default" value="Editar Usuario"><img src="images/editarColor.png" class="btnIcono">Editar Usuario</button>
                            </div>
                        </div>
                        <!--</div>-->
                    </form>
                    <!--/////////////////////////////////////////////////////////////////////   Opciones    ////////////////////////////////////////////////-->
                    <div class="listPedido" id="opciones" style="display: none;">
                        <h4 class="tituloListado"><img src="images/colorOpciones.png" class="iconoTitulo">Opciones</h4>
                        <p class="parrafoListado">
                            Las opciones para el <strong>Usuario</strong> se muestran a continuación.
                        </p>
                        <div class="separadorListado"></div>
                        <br>
                        <%if (usuario.getId() != 1) {%>
                        <div>
                            <p class="parrafoListado">
                                Elimina al usuario para que ya no este en la lista de usuarios y no pueda <strong>Iniciar Sesión</strong>
                            </p>
                            <label class="apaB"> </label><div onclick="flotante('eliminarUsuario')" class="btn bbtn btn-sm btn-default"><img src="images/less.png" class="btnIcono">Eliminar Usuario</div>
                        </div>
                        <%}%>
                        <br><br><br><br>
                        <br><br><br><br>
                        <span class="parrafoListado"><strong>Nota:</strong> No se puede eliminar un usuario si pertenece a la sucursal <strong>General</strong></span>
                    </div>
                    <br><br>
                </div>
            </div>
        </main>
        <div id="eliminarUsuario" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">EliminarUsuario</div>
            <form action="eliminarUsuario" method="post">
                <input type="hidden" name="idUsuario" value="<%=usuario.getId()%>">
                <div class="principalFlotante">
                    
                    <h3 class="tituloListado">¿Estas seguro de eliminar al Usuario?</h3>
                    <br>
                </div>
                <div class="botonesFlotante"><button type="submit" class="btn btn-danger btn-sm"><img src="images/borrarBlanco.png" class="btnIcono">Eliminar Usuario</button><div onclick="flotante('eliminarUsuario')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <%@ include file="menu.jsp" %>
        <script>
            function listado(id) {
                document.getElementById('informacion').style.display = "none";
                document.getElementById('informacion1').className = "listadoOpcion";
                document.getElementById('permisos').style.display = "none";
                document.getElementById('permisos1').className = "listadoOpcion";
                document.getElementById('opciones').style.display = "none";
                document.getElementById('opciones1').className = "listadoOpcion";
                document.getElementById(id).style.display = "block";
                document.getElementById(id + "1").className = "selectL";
            }

            function switchf(id) {
                if (document.getElementById('switchBloque' + id).className === "switchBloque switchVerde") {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchBlanco";
                    document.getElementById('switch' + id).className = "switch switchIzquierda";
                    document.getElementById('input' + id).value = "0";

                } else {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchVerde";
                    document.getElementById('switch' + id).className = "switch switchDerecha";
                    document.getElementById('input' + id).value = "1";
                }
            }
        </script>
    </body>
</html>
