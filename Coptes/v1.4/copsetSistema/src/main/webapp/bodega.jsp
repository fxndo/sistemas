<%-- 
    Document   : bodega
    Created on : 17/08/2018, 11:34:45 AM
    Author     : fernando
--%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="Modelo.Pedido"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ModeloJSP.AlmacenJpaController"%>
<%@page import="Modelo.Almacen"%>
<%@page import="Modelo.Producto"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    AlmacenJpaController al = new AlmacenJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);

    int idSucursal = Integer.parseInt(request.getParameter("id"));

    List<Almacen> almacenes = new ArrayList<Almacen>();
    List<Pedido> pedidos = new ArrayList<Pedido>();

    Sucursal suc = scj.findSucursal(idSucursal);

    if (idSucursal != 0) {
        almacenes = al.listaAlmacen(idSucursal);
        pedidos = p.pedidoBodega(idSucursal);
    } else {

    }


%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Bodega | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody"  class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Bodega<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="sucursales.jsp"><div class="navBloque">Sucursales</div></a><div class="navegacionSelect">Bodega</div></div>
            </header>
            <div class="codigoBarra">
                <a href="reporteBodega?idSucursal=<%=suc.getId()%>"><div class="btn btn-default btn-sm"><img src="images/reporte.png" class="btnIcono">Reporte Bodega</div></a>
                        <%if (idSucursal != 0) {%>
                        <%=suc.getNombre()%>
                        <%} else {%>
                Central
                <%}%>
            </div>
            <%if (almacenes.size() > 0) {%>
            <table class="table table-responsive tabla-producto letraChica">
                <tr class="cabeceraTabla">
                    <th style="width: 50px;text-align: center;">#</th>
                    <th style="width: 100px;">ISBN</th>
                    <th style="padding-left: 15px;">Titulo</th>
                    <th>Serie</th>
                    <th style="width: 80px;">Cantidad</th>
                    <th style="width: 100px;">Tipo</th>
                </tr>
                <%int numero = 1;%>
                <%for (Almacen a : almacenes) {%>
                <tr class="renglon<%=numero % 2%>">
                    <td style="text-align: center;"><strong><%=numero%></strong></td>
                    <td><a class="link2" target="_blank" href="producto.jsp?id=<%=a.getProductoId().getId()%>"><div><%=a.getProductoId().getIsbn()%></div></a></td>
                    <td style="padding-left: 15px;"><%=a.getProductoId().getTitulo()%></td>
                    <%if (a.getProductoId().getSerie() != null) {%>
                    <td><%=a.getProductoId().getSerie()%></td>
                    <%} else {%>
                    <td><span class="noInformacion">Sin información</span></td>
                    <%}%>
                    <td class="gris"><strong><%=a.getCantidad()%></strong></td>
                    <td style="background: #1cd877;border-bottom: 1px solid #19c66d;color:white;text-align: center;"><strong>Venta</strong></td>
                </tr>
                <%numero++;%>
                <%}%>

                <%for (Pedido pp : pedidos) {%>
                <%for (Pedidoproveedor pep : pp.getPedidoproveedorList()) {%>
                <%for (Pedidoparte ppa : pep.getPedidoparteList()) {%>
                <tr class="renglon<%=numero%2%>">
                    <td style="text-align: center;"><strong><%=numero%></strong></td>
                    <td><a class="link2" target="_blank" href="producto.jsp?id=<%=ppa.getProductoId().getId()%>"><%=ppa.getProductoId().getIsbn()%></a></td>
                    <td><%=ppa.getProductoId().getTitulo()%></td>
                    <%if (ppa.getProductoId().getSerie() != null) {%>
                    <td><%=ppa.getProductoId().getSerie()%></td>
                    <%} else {%>
                    <td><span class="noInformacion">Sin información</span></td>
                    <%}%>
                    <td class="gris"><strong><%=ppa.getCantidad()%></strong></td>
                    <td style="background: #10bbef;color:white;border-bottom: 1px solid #0ea6d1;text-align: center;"><strong>Pedido</strong></td>
                </tr>  
                <%numero++;%>
                <%}%>
                <%}%>
                <%}%>

            </table>
            <%} else {%>
            <div class="vacioGrande">
                No hay Productos<br>
                <img src="images/vacioGrande.png" class="imgVacio">
            </div>
            <%}%>
        </main>
        <%@ include file="menu.jsp" %>
    </body>
</html>
