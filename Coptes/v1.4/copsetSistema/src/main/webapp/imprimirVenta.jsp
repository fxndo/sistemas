<%-- 
    Document   : imprimirVenta
    Created on : 17/05/2018, 03:32:33 PM
    Author     : fernando
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="Modelo.VentasHasProducto"%>
<%@page import="Modelo.Ventas"%>
<%@page import="ModeloJSP.VentasJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Calendar fecha = new GregorianCalendar();
    Locale locale = new Locale("es","MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy hh:mm a", new Locale("es"));
    VentasJpaController v=new VentasJpaController(emf);
    Ventas venta=v.findVentas(Integer.parseInt(request.getParameter("venta")));
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/estilo-ticket.css"/>
        <!--<link rel="stylesheet" href="css/bootstrap.min.css"/>-->
    </head>
    <body>
        <div class="bloqueGeneral">
            <div class="imagen">
                <img src="images/logoNegro.jpg">
            </div><br>
            <div class="sucursal">
                Sucursal:<%=sucursal.getNombre()%><br>
                Direccion:
            </div>
            <div class="informacion">
                <h3>Ticket de Compra</h3>
                <span class="venta">No. Venta: JV-100<%=venta.getId()%></span><br>
                Fecha: <%=formato.format(fecha.getTime())%>
            </div>
            <br><br>
            <div class="productos">
                <table class="table table-responsive table-bordered table-condensed">
                    <tr>
                        <th>Concepto</th>
                        <th>Cant.</th>
                        <th>Desc.</th>
                        <th>Neto</th>
                    </tr>
                    <%double total=0;%>
                    <%for(VentasHasProducto vh: venta.getVentasHasProductoList()){%>
                    <tr>
                        <td><%=vh.getProducto().getTitulo()%></td>
                        <td><%=vh.getCantidad()-vh.getDevolucion()%></td>
                        <td><%=vh.getDescuento()%> %</td>
                        <%double subTotal=0;%>
                        <%subTotal=Double.parseDouble(vh.getPrecio());%>
                        <%subTotal-=subTotal*(vh.getDescuento()/100);%>
                        <%subTotal*=(vh.getCantidad()-vh.getDevolucion());%>
                        <td><%=subTotal%></td>
                    </tr>
                    <%}%>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Total:</td>
                        <td><%=nf.format(venta.getTotal())%></td>
                    </tr>
                </table>
            </div>
                    <br><br>
                    <div class="footer">
                        Le ha atendido <%=informacion.getNombre()%>
                        <hr>
                        GRACIAS POR SU COMPRA
                        <hr>
                    </div>
            
        </div>
    </body>
</html>
