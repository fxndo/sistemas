<%@page import="java.util.Random"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
        request.getRequestDispatcher("inicio.jsp").forward(request, response);
    } else {
    }
    
    Random rnd=new Random();
    String token= "";
    String characters= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for ( int i = 0; i < 33; i++ ) {
      token += characters.charAt(rnd.nextInt(characters.length()));
    }
    HttpSession csrf=request.getSession(true);
    csrf.setAttribute("token", token);
    

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Iniciar Sesión</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estilo.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
    </head>
    <body class="margenBody">
        <main id="body" class="">
            <div class="row header">
                <br><br>
                <div class="col-md-4"></div>
                <div class="col-md-4"><img class="imgLogo" src="images/logo2.png"></div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="form-container iniciaSesion">
                        <br><br><br>
                        <form name="formInicio" action="iniciarSesion" method="post" onsubmit="envMostrar();">
                            <input class="entradaInicio" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" minlength="5" maxlength="40" placeholder="E-mail" name="usuario" required="true"><br>
                            <input class="entradaInicio" maxlength="40" type="password" minlength="5" name="password" required="true" placeholder="Contraseña"><br>
                            <input type="hidden" name="csrfToken" value="<%=token%>">
                            <button class="botonInicio" required="true" type="submit">Aceptar</button><br>
                            <%if (request.getParameter("m") != null) {%>
                            <div class="errorPass">Password o Usuario incorrecto</div>
                            <%}%>
                        </form>
                        <div class="version">
                        <span>Sistema Coptes S.A. de C.V.<br>Version 1.4</span>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <div id="espera" class="espera" style="display: none;"><img class="imgLoading" src="images/loading2.gif"></div>
        
        <script>
            function envMostrar() {
                document.getElementById('espera').style.display = "block";
                document.getElementById('body').className="desenfoque";
            }
        </script>
    </body>
</html>
