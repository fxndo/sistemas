<%-- 
    Document   : bienvenido
    Created on : 19/05/2018, 02:11:43 AM
    Author     : fernando
--%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
HttpSession sesion = request.getSession(true);
Usuario informacion=(Usuario)sesion.getAttribute("informacion");
Sucursal sucursal=(Sucursal)sesion.getAttribute("sucursal");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Bienvenido</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/funciones.js"></script>
    </head>
    <body  class="margenBody">
        <header>
            <div class="row">
                <div class="col-md-4"><img class="imgLogo" src="images/logo.png"></div>
                <div class="col-md-4"><div class="letrero">Bienvenido<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
            </div>
        </header>
            <main>
<!--                <div class="apartadoBloque ">
                <div class="apartadoTitulo">Para empezar</div>
                <div class="apartadoPrincipal text-left">-->
<div style="padding: 10px 100px;">
    <h4 class="tituloListado">Para empezar...</h4>
                    <img src="images/usuariog.png" class="imgCredencial">
                    <h4 class="tituloListado2">Cambia tus credenciales</h4><br><br>
                    <form action="cambiarCredenciales" method="post" name="credencial">
                        <div class="row bordeAbajo"><div class="col-md-2"><label>Nombre</label></div><div class="col-md-4"><input type="text" name="nombre" class="nuevaEntrada" required=""></div></div>
                        <div class="row bordeAbajo"><div class="col-md-2"><label>Telefono</label></div><div class="col-md-4"><input type="text" name="telefono" class="nuevaEntrada" required=""></div></div>
                        <div class="row bordeAbajo"><div class="col-md-2"><label>Email</label></div><div class="col-md-4"><input id="email" type="text" name="email" class="nuevaEntrada" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required=""></div></div>
                        <div class="row bordeAbajo"><div class="col-md-2"><label>Contraseña</label></div><div class="col-md-4"><input id="pass" type="password" name="pass" class="nuevaEntrada" required=""></div></div>
                        <div class="row bordeAbajo"><div class="col-md-2"><label>Confirmar contraseña</label></div><div class="col-md-4"><input id="pass2" type="password" name="pass" class="nuevaEntrada" required=""></div></div>
                    <br><br>
                        <p class="parrafoListado">
                            <strong>Nota: </strong>Esta informacion sirve para que inicies sesión posteriormente. 
                        </p>
                    <div class="text-center">
                        <div onclick="verificarFormulario();" class="btn bbtn btn-sm btn-success"><img src="images/aceptar.png" class="btnIcono">Aceptar</div>
                        <a href="sucursales.jsp"><div class="btn bbtn btn-sm btn-primary">Omitir</div></a>
                    </div>
                    </form>   
                </div>
                <!--</div>-->
            </main>
                <script>
                    function verificarFormulario(){
                        if(document.getElementById('email').value.length>0 && document.getElementById('pass').value.length>0){
                            
                        if(document.getElementById('pass').value===document.getElementById('pass2').value){
                                document.credencial.submit();
//                                   window.alert("bien");
                        }else{
                            window.alert("Pasword no coincide");
                        }
                        }else{
                            window.alert("LLena todos los campos");
                            
                        }
                    }
                </script>
    </body>
</html>
