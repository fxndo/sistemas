<%-- 
    Document   : traslados
    Created on : 17/10/2018, 11:16:03 AM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.AlmacenJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    
    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    AlmacenJpaController al = new AlmacenJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);
    
    List<Pedidoparte> partes = ppan.traslados(sucursal.getId());
%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Traslados | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Traslados<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Traslados</div></div>
            </header>
            <%if (partes.size() > 0) {%>
            <table class="tabla-producto table table-responsive letraChica">
                <tr class="cabeceraTabla">
                    <th style="text-align: center;">#</th>
                    <th>Sucursal</th>
                    <th>Titulo</th>
                    <th>ISBN</th>
                    <th style="width: 100px;">Solicitado</th>
                    <th style="width: 100px;">Stock</th>
                    <th style="width: 160px;">Cant</th>
                </tr>
                <%int valor = 1;%>
                <%for (Pedidoparte pe : partes) {%>
                <tr class="renglon<%=valor % 2%>">

                    <td class="gris"><strong><%=valor%></strong></td>
                    <td><%=pe.getPedidoProveedorid().getPedidoId().getSucursalId().getNombre()%></td>
                    <td><%=pe.getProductoId().getTitulo()%></td>
                    <td><%=pe.getProductoId().getIsbn()%></td>
                    <td class="azul"><strong><%=pe.getCantidad()%></strong></td>
                    <td class="verde"><strong><%=al.productosAlmacen(pe.getProductoId().getId(), pe.getSucursalId().getId())%></strong></td>


                    <%int maximo = 0;%>
                    <%if (al.productosAlmacen(pe.getProductoId().getId(), pe.getSucursalId().getId()) > pe.getCantidad()) {%>
                    <%maximo = pe.getCantidad();%>
                    <%} else {%>
                    <%maximo = al.productosAlmacen(pe.getProductoId().getId(), pe.getSucursalId().getId());%>
                    <%}%>



                    <%if (pe.getEstado() == 2) {%>
                    <%if (al.productosAlmacen(pe.getProductoId().getId(), pe.getSucursalId().getId()) > 0) {%>
                    <td style="padding: 0px;">
                        <form action="aceptarTraslado" method="post">
                            <input type="hidden" name="id" value="<%=pe.getId()%>">
                            <input id="cantidad<%=valor%>" onfocus="limpiar('<%=valor%>')" onkeypress="return validarEntradaNumero(event);" type="number" required="" max="<%=maximo%>" name="cantidad" class="entradaTablac" value="<%=maximo%>">
                            <button class="btnAceptar" type="submit">Aceptar</button>
                        </form>
                    </td>
                    <%} else {%>
                    <td style="padding: 0px;">
                        <form action="aceptarTraslado" method="post">
                            <input type="hidden" name="id" value="<%=pe.getId()%>">
                            <input type="number" max="<%=maximo%>" class="entradaTablac" onkeypress="return validarEntradaNumero(event);" name="cantidad"  disabled="on" value="0">
                            <button class="btnAceptar" type="submit">Aceptar</button>
                        </form>
                    </td>
                    <%}%>
                    <%}%>
                    <%if (pe.getEstado() == 3) {%>
                    <td><div class="aceptado">Aceptado</div></td>
                    <%}%>
                    <%if (pe.getEstado() == 4) {%>
                    <td><div class="denegado">Denegado</div></td>
                    <%}%>

                </tr>
                <%valor++;%>
                <%}%>
            </table>
            <%} else {%>
            <div class="vacioGrande">
                No hay Traslados<br>
                <img src="images/vacioGrande.png" class="imgVacio">
            </div>
            <%}%>
        </main>
        <%@ include file="menu.jsp" %>
        <script>
            function limpiar(id) {
                document.getElementById("cantidad" + id).value = "";
            }
        </script>
    </body>
</html>
