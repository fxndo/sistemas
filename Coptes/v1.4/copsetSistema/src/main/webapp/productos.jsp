<%-- 
    Document   : productos
    Created on : 18/04/2018, 06:47:39 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Editorial"%>
<%@page import="ModeloJSP.EditorialJpaController"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="Modelo.Proveedor"%>
<%@page import="ModeloJSP.ProveedorJpaController"%>
<%@page import="ModeloJSP.DescuentoJpaController"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.Nivel"%>
<%@page import="ModeloJSP.NivelJpaController"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.AlmacenJpaController"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Producto"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");

    int noProductos = 0;
    int pagina = 0;
    int elementos = 50;

    if (request.getParameter("pagina") != null) {
        pagina = Integer.parseInt(request.getAttribute("pagina").toString());
    }

    if (request.getAttribute("noProductos") != null) {
        noProductos = Integer.parseInt(request.getAttribute("noProductos").toString());
    }

    String objeto = "";
    if (request.getParameter("objeto") != null) {
        objeto = request.getParameter("objeto");
    }

    AlmacenJpaController a = new AlmacenJpaController(emf);
    NivelJpaController n = new NivelJpaController(emf);
    EditorialJpaController ed = new EditorialJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    ProductoJpaController pj = new ProductoJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

    List<Producto> productos = new ArrayList<Producto>();
    productos = (List<Producto>) request.getAttribute("productos");
    List<Nivel> niveles = n.nivelLista();
    List<Editorial> editoriales = ed.listaEditoriales();

    int notificacion = 0;
    int agregados = 0;
    int noAgregados = 0;
    int finalizados=0;
    
    if(request.getParameter("finalizados")!=null){
        finalizados=Integer.parseInt(request.getParameter("finalizados"));
    }
    
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
        if (request.getParameter("noti").equals("23")) {
            agregados = Integer.parseInt(request.getParameter("agregados"));
            noAgregados = Integer.parseInt(request.getParameter("noAgregados"));
        }
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Productos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/funciones.js"></script>
        <script src="js/jquery.min.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Productos<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Productos</div></div>

            </header>
            <div class="codigoBarra row">
                <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1) {%>
                <div class="nombreBarra col-md-2"><div onclick="flotante('nuevoProducto');" class="btn btn-default btn-sm"><img src="images/suma2.png" class="btnIcono">Agregar Producto</div></div>
                        <%}%>
                <form action="buscarProducto" method="post">
                    <div class="barra col-md-4"><div class="apartado"><input id="numeroSerial" value="<%=objeto%>" maxlength="30" placeholder="Buscar Producto..." autofocus="true" type="text" name="objeto"></div></div>
                    <div class="botonBarra col-md-1"><button type="submit" class="btnBuscar"><img src="images/buscar.png" class="iconoNormal"></button></div>
                    
                <div class="icono col-md-2">
                        <label style="position: relative;bottom: 2px;" class="labelSwitch">Pendientes</label>
                        <div style="position: relative;top:5px;" id="switchBloque1" onclick="switchf(1)" class="switchBloque <%if(finalizados==0){%> switchBlanco <%}else{%> switchVerde <%}%>">
                            <div id="switch1" class="switch <%if(finalizados==0){%> switchIzquierda <%}else{%> switchDerecha<%}%>"></div>
                        </div>
                        <input id="finalizadosIn" type="hidden" value="<%if(finalizados==0){%>0<%}else{%>1<%}%>" name="finalizados">
                    </div>
                </form>
                
                <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1) {%>
                <div class="botonBarra col-md-2"><a href="subirListaProductos.jsp"><div class="btn btn-default btn-sm"><img src="images/subir.png" class="btnIcono">Subir Lista de Productos</div></a></div>
                            <%}%>
                <div class="col-md-1"><div style="position: relative; top: 8px;" onclick="flotante('ayuda1');"><img src="images/ayuda.png" class="btnAyuda"><strong>Ayuda</strong></div></div>
            </div>
            <div>

                <%if (productos.size() > 0) {%>
                <table class="tabla-producto table table-responsive letraChica">

                    <tr id="cabeceraTabla" class="cabeceraTabla">
                        <th style="width: 70px;">ISBN</th>
                        <th style="width: 35%;padding-left: 10px;">Titulo</th>
                        <th>Autor</th>
                        <th>Serie</th>
                        <th style="width: 80px;">Editorial</th>
                        <th style="width: 100px;">Nivel</th>
                        <th style="width: 80px;">Text/Lit</th>
                        <th style="width: 100px;">$ Precio</th>
                        <th style="width: 45px;">Stock</th>
                    </tr>

                    <%int renglon = 1;%>
                    <%for (Producto pd : productos) {%>
                    <tr class="renglon<%=renglon % 2%>">
                        <td style="background: #eaeaea;"><a class="link" href="producto.jsp?id=<%=pd.getId()%>" target=""><div class="btnSelector"><%=pd.getIsbn()%></div></a></td>

                        <td style="padding-left: 10px;">
                            <%if (!pd.getEstado().equals("2")) {%>

                            <span class="pendienteRojo"><%=pd.getTitulo()%></span>
                            <%} else {%>
                            <%=pd.getTitulo()%>
                            <%}%>
                        </td>
                        <td><%=pd.getAutor()%></td>
                        <%if (pd.getSerie() != null) {%>
                        <td><%=pd.getSerie()%></td>
                        <%} else {%>
                        <td><span class="noInformacion">Sin información</span></td>
                        <%}%>
                        <td><%=pd.getEditorialId1().getNombre()%></td>
                        <td><%=pd.getNivelId().getNombre()%></td>
                        <td><%=pd.getTextlit()%></td>
                        <td style="background: #32dc84;" class="saldoVerde"><div><%=nf.format(Double.parseDouble(pd.getPrecioventa()))%></div></td>
                                <%if (sucursal.getId() == 1) {%>
                        <td style="background: #ff784e;" class="saldoGris cursor"><div onclick="stock('<%=pd.getId()%>');"><img src="images/stock.png" class="iconoNormalGrande"></div></td>
                                <%} else {%>
                                <%if (a.productosAlmacen(pd.getId(), sucursal.getId()) > 0) {%>
                        <td class="saldoVerde cursor"><div onclick="stock('<%=pd.getId()%>');"><strong><%=a.productosAlmacen(pd.getId(), sucursal.getId())%></strong></div></td>
                                    <%} else {%>
                        <td class="saldoRojo cursor"><div onclick="stock('<%=pd.getId()%>');"><strong><%=a.productosAlmacen(pd.getId(), sucursal.getId())%></strong></div></td>
                                    <%}%>
                                    <%}%>
                    </tr>
                    <%renglon++;%>
                    <%}%>
                </table>
                <br>
                <!--------------------------------------------- Paginacion  -------------------------------------------------->
                <%if (noProductos > elementos) {%>
                <div class="text-center">
                    <div class="pagina2">Paginas</div>
                    <%int paginas = noProductos / elementos;%>
                    <%if (noProductos % elementos > 0) {%>
                    <%paginas++;%>
                    <%}%>
                    <%if (pagina != 0) {%>
                    <a href="buscarProducto?objeto=<%=objeto%>&pagina=<%=pagina - 1%>"><div class="paginaActiva">Anterior</div></a>
                    <%}%>
                    <%for (int c = 1; c <= paginas; c++) {%>
                    <%if (pagina == c - 1) {%>
                    <div class="paginaActiva"><%=c%></div>
                    <%} else {%>
                    <%if (c < 4 || c > paginas - 3 || c == pagina + 2 || c == pagina) {%>
                    <a href="buscarProducto?objeto=<%=objeto%>&pagina=<%=c - 1%>"><div class="pagina"><%=c%></div></a>
                        <%}%>
                        <%}%>
                        <%}%>

                    <%if (pagina + 1 != paginas) {%>
                    <a href="buscarProducto?objeto=<%=objeto%>&pagina=<%=pagina + 1%>"><div class="paginaActiva">Siguiente</div></a>
                    <%}%>
                </div>
                <%}%>
                <%} else {%>
                <div class="vacioGrande">
                    No hay productos<br>
                    <img src="images/vacioGrande.png" class="imgVacio">
                </div>
                <%}%>
            </div>
            <br><br>
        </main>

        <!---------------------------------------------------------Nuevo Producto-------------------------------------------->
        <div id="nuevoProducto" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Nuevo Producto</div>
            <form action="agregarProducto" method="post" name="formularioProducto">
                <div class="principalFlotante">
                    <label class="entradaLabel">ISBN/Serial</label><input id="isbn" placeholder="1234567890112" class="entrada" onkeypress="return validarEntradaNumero(event);" type="text" required="on" name="isbn" pattern="[0-9]+"><br>
                    <label class="entradaLabel">Text/Lit</label><select name="textlit" class="entradaSelect">
                        <option>Texto</option>
                        <option>Literario</option>
                    </select><br>

                    <label class="entradaLabel">Titulo</label><input id="titulo" placeholder="Titulo del Producto" class="entrada" type="text" required="on" name="titulo" maxlength="299"><br>
                    <label class="entradaLabel">Serie</label><input id="serie" placeholder="Serie del Producto" class="entrada" type="text" required="on" name="serie" maxlength="45"><br>
                    <label class="entradaLabel">Editorial</label><select name="editorial" class="entradaSelect">
                        <%for (Editorial eed : editoriales) {%>
                        <option value="<%=eed.getId()%>"><%=eed.getNombre()%></option>
                        <%}%>
                    </select><br>
                    <label class="entradaLabel">Autor</label><input id="autor" placeholder="Nombre del Autor" class="entrada" type="text" name="autor" maxlength="99"><br>
                    <hr>
                    <label class="entradaLabel">Nivel</label><select class="entradaSelect" name="nivel">
                        <%for (Nivel nv : niveles) {%>
                        <option value="<%=nv.getId()%>"><%=nv.getNombre()%></option>
                        <%}%>
                    </select><br>

                    <label class="entradaLabel">Precio $</label><input id="venta" class="entrada" type="text" name="pventa" onkeypress="return check(event);" placeholder="0.00" pattern="[0-9]+(\.[0-9][0-9]?)?" maxlength="8"><br>
                </div>
                <div class="botonesFlotante"><div onclick="formularioProductoNuevo();" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Agregar Producto</div><div onclick="flotante('nuevoProducto')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <!--///////////////////////////////////////////////////////////////////////////////-->
        <div id="stock" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Stock</div>
            <div class="principalFlotante">
                <div id="container-stock">

                </div> 
            </div>
            <div class="botonesFlotante"><div onclick="flotante('stock')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
        </div>
        <!--///////////////////////////////////////////////////////////////////////////////-->
        <div id="ayuda1" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Ayuda - Nuevo Pedido</div>
            <div class="principalFlotante">
                <ul>
                    <li>Escribe el <strong>Producto</strong> que buscas en la barra de busqueda.</li>
                    <li>Puedes buscar por <strong>ISBN</strong>, <strong>Titulo</strong> del producto o <strong>Autor</strong>.</li>
                    <li>Presiona el recuadro de Stock de cada <strong>Producto</strong> para ver las existencias en todas las sucursales.</li>
                </ul>
            </div>
            <div class="botonesFlotante"><div onclick="flotante('ayuda1')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div>
            </div>
        </div>
        <!--/////////////////////////////////////   MENSAJES    //////////////////////////////////////////-->

        <div class="notificacionBloque" style="<%if (notificacion == 3) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Producto Creado</div>
                El <strong>producto</strong> se creo satisfactoriamente.
            </div>
        </div>
        <div class="notificacionBloque" style="<%if (notificacion == 23) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Lista Agregada</div>
                La <strong>Lista</strong> se agrego satisfactoriamente.<br>
                <strong><%=agregados%></strong> Productos agregados de <strong><%=agregados + noAgregados%></strong>.<br>
                <strong><%=noAgregados%></strong> Productos editados de <strong><%=agregados + noAgregados%></strong>.
            </div>
        </div>
        <!--///////////////////////////////////////////////////////////////////////////////-->
        <%@ include file="menu.jsp" %>
        <script>
            function stock(idProducto) {
                $.post("verStock", {idProducto}, function (data) {
                    document.getElementById('container-stock').innerHTML = data;
                    flotante('stock');
                });
            }

            function formularioProductoNuevo() {

                var valida = 0;
//                var compra = document.getElementById('compra').value;
                var venta = document.getElementById('venta').value;
                var serial = document.getElementById('isbn').value;

                if (document.getElementById('isbn').value.length < 12) {
                    valida = 1;
                    window.alert("ISBN vacio");
                } else {
                    if (document.getElementById('autor').value.length < 3) {
                        valida = 1;
                        window.alert("Autor vacio");
                    } else {
                        if (document.getElementById('titulo').value.length < 3) {
                            valida = 1;
                            window.alert("Titulo vacio");
                        } else {
                            if (document.getElementById('venta').value.length < 1) {
                                valida = 1;
                                window.alert("Precio vacio");
                            } else {
                                $.post("verificarISBN", {serial}, function (data) {
                                    if (data == 0) {
                                        if (valida === 0) {
                                            document.formularioProducto.submit();
                                        }
                                    } else {
                                        window.alert("El ISBN ya existe");

                                    }
                                });
                            }
                        }
                    }
                }
            }

        </script>
        <script>
            function switchf(id) {
                if (document.getElementById('switchBloque' + id).className === "switchBloque switchVerde") {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchBlanco";
                    document.getElementById('switch' + id).className = "switch switchIzquierda";
                    document.getElementById('finalizadosIn').value = "0";

                } else {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchVerde";
                    document.getElementById('switch' + id).className = "switch switchDerecha";
                    document.getElementById('finalizadosIn').value = "1";
                }
            }

        </script>

    </body>
</html>
