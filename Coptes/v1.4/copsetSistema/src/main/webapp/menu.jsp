<%-- 
    Document   : menu
    Created on : 8/03/2019, 03:29:07 PM
    Author     : fernando
--%>
<%
    int trasladosNoti = ppan.traslados(sucursal.getId()).size();
    int almacenNoti = pprj.listaCompras().size();
    int comprasNoti = pprj.pedidoProveedoresCuenta();
    int pedidoNoti = p.pedidosNuevosCuenta(sucursal.getId());
    int productoNoti=pj.productosPendientesCuenta();
%>
<div id="fondoNegro" class="fondoNegro" style="display: none;"></div>
<div id="fondoNegroMenu" onclick="mostrar('menu');" class="fondoNegro" style="display: none;"></div>
<div id="menu" class="menuEmergente novisto">
    <div onclick="mostrar('menu')" class="iconoMenu margenBtn"><img class="btnMenu" src="images/menu.png"></div>
    <br>
    <%if (informacion.getPermisosList().get(0).getTpvp() == 1) {%>
    <%if(sucursal.getId()==1){%>
    <div class="opcionmenu" onclick="mostrarSucursales();"><img src="images/terminal.png" class="iconoOpcion">TPV</div>
    <div id="sucursales" class="closeTPV">
        <%for(Sucursal scf: scj.listaSucursales()){%>
        <%if(scf.getId()!=1){%>
            <a href="configurarSucursalTPV?idSucursal=<%=scf.getId()%>" class="link"><div class="opcionTPV"><%=scf.getNombre()%></div></a>
        <%}%>
        <%}%>
    </div>
    <%}else{%>
    <a href="tpv.jsp" class="link"><div class="opcionmenu"><img src="images/terminal.png" class="iconoOpcion">TPV</div></a>
    <%}%>
            <%}%>

    <%if (informacion.getPermisosList().get(0).getPedidosp() == 1) {%>
    <a href="nuevoPedido.jsp" class="link"><div class="opcionmenu"><img src="images/unidad.png" class="iconoOpcion">Nuevo Pedido</div></a>
    <div class="tituloMenu2"></div>
    <%}%>

    <a href="pedidos.jsp" class="link"><div class="opcionmenu"><img src="images/pedido.png" class="iconoOpcion">Pedidos<%if (pedidoNoti > 0) {%><div class="notificacion"><%=pedidoNoti%></div><%}%></div></a>

    <%if (informacion.getPermisosList().get(0).getComprasp() == 1) {%>
    <a href="compras.jsp" class="link"><div class="opcionmenu"><img src="images/compras.png" class="iconoOpcion">Compras<%if (comprasNoti > 0) {%><div class="notificacion"><%=comprasNoti%></div><%}%></div></a>
            <%}%>

    <%if (informacion.getPermisosList().get(0).getAlmacenp() == 1) {%>
    <a href="almacen.jsp" class="link"><div class="opcionmenu"><img src="images/devolucion.png" class="iconoOpcion">Almac�n<%if (almacenNoti > 0) {%><div class="notificacion"><%=almacenNoti%></div><%}%></div></a>
            <%}%>

    <div class="tituloMenu2"></div>

    <%if (informacion.getPermisosList().get(0).getProductosp() == 1) {%>
    <a href="buscarProducto" class="link"><div class="opcionmenu"><img src="images/producto.png" class="iconoOpcion">Productos<%if (productoNoti > 0) {%><div class="notificacion"><%=productoNoti%></div><%}%></div></a>
            <%}%>
    <%if (informacion.getPermisosList().get(0).getPedidosp() == 1||informacion.getPermisosList().get(0).getAdministracionp() == 1) {%>
    <a href="clientes.jsp" class="link"><div class="opcionmenu"><img src="images/usuario2.png" class="iconoOpcion">Clientes</div></a>
    <%}%>
    <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1) {%>
    <a href="editoriales.jsp" class="link"><div class="opcionmenu"><img src="images/descuento.png" class="iconoOpcion">Descuentos</div></a>
    <a href="proveedores.jsp" class="link"><div class="opcionmenu"><img src="images/proveedor.png" class="iconoOpcion">Proveedores</div></a>
    <a href="niveles.jsp" class="link"><div class="opcionmenu"><img src="images/nivel.png" class="iconoOpcion">Niveles</div></a>
            <%}%>



    <%if (informacion.getPermisosList().get(0).getVentap() == 1) {%>
    <a href="ventas.jsp" class="link"><div class="opcionmenu"><img src="images/ventas.png" class="iconoOpcion">Ventas</div></a>
            <%}%>

    <%if (informacion.getPermisosList().get(0).getTraslados() == 1) {%>
    <a href="traslados.jsp" class="link"><div class="opcionmenu"><img src="images/traslado.png" class="iconoOpcion">Traslados<%if (trasladosNoti > 0) {%><div class="notificacion"><%=trasladosNoti%></div><%}%></div></a>
            <%}%>

    <div class="tituloMenu2"></div>

    <%if (informacion.getPermisosList().get(0).getSuperusuario() == 1) {%>
    <a href="finanzas.jsp" class="link"><div class="opcionmenu"><img src="images/finanzas.png" class="iconoOpcion">Finanzas</div></a>
    <a href="adminTPV.jsp" class="link"><div class="opcionmenu"><img src="images/terminal.png" class="iconoOpcion">Administrar TPV</div></a>
    <a href="usuarios.jsp"class="link"><div class="opcionmenu"><img src="images/usuario2.png" class="iconoOpcion">Usuarios</div></a>
    <a href="sucursales.jsp" class="link"><div class="opcionmenu"><img src="images/sucursal.png" class="iconoOpcion">Sucursal</div></a>
            <%}%>
    <a href="ayuda.html" class="link"><div class="opcionmenu"><img src="images/ayuda.png" class="iconoOpcion">Ayuda</div></a>
    <a href="cerrarSesion" class="link"><div class="opcionmenu"><img src="images/salida.png" class="iconoOpcion">Salir</div></a>
</div>