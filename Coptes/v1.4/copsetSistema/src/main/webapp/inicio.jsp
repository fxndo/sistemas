<%-- 
    Document   : inicio
    Created on : 17/04/2018, 06:17:27 PM
    Author     : fernando
--%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="Modelo.Pedido"%>
<%@page import="ModeloJSP.DevolucionJpaController"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.VentasJpaController"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {

    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    ProductoJpaController pj = new ProductoJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);
    VentasJpaController v = new VentasJpaController(emf);
    PedidoproveedorJpaController ppj = new PedidoproveedorJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    
    Calendar fecha = new GregorianCalendar();
    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
    SimpleDateFormat formato2 = new SimpleDateFormat("dd MMMM yyyy", new Locale("es"));

    List<Sucursal> sucursales = scj.findSucursalEntities();
    List<Pedidoproveedor> compraPendiente = ppj.compraPendiente(sucursal.getId());
    List<Pedidoproveedor> devoluciones = ppj.pedidoDevolucion(sucursal.getId());
    List<Pedido> pendientes = p.pendientes(sucursal.getId());
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Inicio</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><img class="imgLogo" src="images/logo.png"></div>
                    <div class="col-md-4"><div class="letrero">Inicio<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><div class="navegacionSelect">Inicio</div></div>

            </header>

            <div class="menuPrincipal">
                <div class="row">
                    <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1 && informacion.getSucursalId().getId() == 1) {%>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="row">
                                <div class="bloqueMenu2">
                                    <h4>Sucursales</h4>
                                    <%int color = 1;
                                        for (Sucursal sc : sucursales) {
                                            if (sc.getId() != 1) {%>
                                    <div class="bloqueSuc"><div class="cuadro color<%=color%>"></div><%=sc.getNombre()%></div>
                                        <%color++;
                                                }
                                            }%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <!--/////////////////////////////////////////////////////////-->
                                <div class="bloqueMenu smd">
                                    Productos Ventas TPV<br><br>
                                    <a class="link6" href="">
                                        <div class="row">
                                            <% double maximo = 0;%>
                                            <% double maximo2 = 0;%>
                                            <%for (Sucursal sc : sucursales) {%>
                                            <%if (v.ventasMes(sc.getId()) > maximo) {%>
                                            <%maximo = v.ventasMes(sc.getId());%>
                                            <%}%>

                                            <%}%>
                                            <%if (maximo != 0) {%>
                                            <%maximo2 = 150 / maximo;%>
                                            <%}%>
                                            <div class="barraInfo">
                                                <div class="barraFuera">
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=(int) maximo%></div></div>
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=(int) (maximo / 3) * 2%></div></div>
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=(int) maximo / 3%></div></div>
                                                </div>
                                            </div>
                                            <%color = 1;%>
                                            <%for (Sucursal sc : sucursales) {%>
                                            <%if (sc.getId() != 1) {%>
                                            <div class="barraInfo"><div class="barraFuera color<%=color%>"><div class="barraDentro" style="height: <%=150 - v.ventasMes(sc.getId()) * maximo2%>px;"></div></div></div>
                                                    <%color++;%>
                                                    <%}%>
                                                    <%}%>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!--///////////////////////////////////////////////////////////////////////////-->         
                            <div class="col-md-6">
                                <div class="bloqueMenu smi">
                                    Productos Pedidos<br><br>
                                    <a class="link6" href="">
                                        <div class="row">
                                            <% maximo = 0;
                                                maximo2 = 0;
                                                for (Sucursal sc : sucursales) {
                                                    if (p.pedidosMes(sc.getId()) > maximo) {
                                                        maximo = p.pedidosMes(sc.getId());
                                                    }
                                                }
                                                if (maximo != 0) {
                                                    maximo2 = 150 / maximo;
                                                }%>
                                            <div class="barraInfo">
                                                <div class="barraFuera">
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=(int) maximo%></div></div>
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=(int) (maximo / 3) * 2%></div></div>
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=(int) maximo / 3%></div></div>
                                                </div>
                                            </div>
                                            <%color = 1;%>
                                            <%for (Sucursal sc : sucursales) {%>
                                            <%if (sc.getId() != 1) {%>
                                            <div class="barraInfo"><div class="barraFuera color<%=color%>"><div class="barraDentro" style="height: <%=150 - p.pedidosMes(sc.getId()) * maximo2%>px;"></div></div></div>
                                                    <%color++;%>
                                                    <%}%>
                                                    <%}%>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!--////////////////////////////////////////////////////////////////////////////////////-->
                            <div class="col-md-6">
                                <div class="bloqueMenu smd">
                                    Productos Ventas Generales<br><br>
                                    <a class="link6" href="">
                                        <div class="row">
                                            <% maximo = 0;
                                                maximo2 = 0;
                                                for (Sucursal sc : sucursales) {
                                                    if (p.pedidosMes(sc.getId()) + v.ventasMes(sc.getId()) > maximo) {
                                                        maximo = p.pedidosMes(sc.getId()) + v.ventasMes(sc.getId());
                                                    }
                                                }
                                                if (maximo != 0) {
                                                    maximo2 = 150 / maximo;
                                                }%>
                                            <div class="barraInfo">
                                                <div class="barraFuera">
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=(int) maximo%></div></div>
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=(int) (maximo / 3) * 2%></div></div>
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=(int) maximo / 3%></div></div>
                                                </div>
                                            </div>
                                            <%color = 1;%>
                                            <%for (Sucursal sc : sucursales) {%>
                                            <%if (sc.getId() != 1) {%>
                                            <div class="barraInfo"><div class="barraFuera color<%=color%>"><div class="barraDentro" style="height: <%=150 - (p.pedidosMes(sc.getId()) + v.ventasMes(sc.getId())) * maximo2%>px;"></div></div></div>
                                                    <%color++;%>
                                                    <%}%>
                                                    <%}%>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="bloqueMenu smi">
                                    Ganancias Generales<br><br>
                                    <a class="link6" href="">
                                        <div class="row">
                                            <% maximo = 0;
                                                maximo2 = 0;
                                                for (Sucursal sc : sucursales) {
                                                    if (sc.getId() != 1) {
                                                        if (p.gananciasPedidosMes(sc.getId()) + v.gananciasVentasMes(sc.getId()) > maximo) {
                                                            maximo = p.gananciasPedidosMes(sc.getId()) + v.gananciasVentasMes(sc.getId());
                                                        }
                                                    }
                                                }
                                                if (maximo != 0) {
                                                    maximo2 = 150 / maximo;
                                                }%>
                                            <div class="barraInfo">
                                                <div class="barraFuera">
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=nf.format(maximo)%></div></div>
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=nf.format((maximo / 3) * 2)%></div></div>
                                                    <div class="lineaGrafica"><div class="contenidoGrafica"><%=nf.format(maximo / 3)%></div></div>
                                                </div>
                                            </div>
                                            <%color = 1;%>
                                            <%for (Sucursal sc : sucursales) {%>
                                            <%if (sc.getId() != 1) {%>
                                            <div class="barraInfo"><div class="barraFuera color<%=color%>"><div class="barraDentro" style="height: <%=150 - (p.gananciasPedidosMes(sc.getId()) + v.gananciasVentasMes(sc.getId())) * maximo2%>px;"></div></div></div>
                                                    <%color++;%>
                                                    <%}%>
                                                    <%}%>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%} else {%>
                    <div class="col-md-8 imgCentral">
                        <img src="images/logoGris2.png" class="">
                    </div>
                    <%}%>
                    <div class="col-md-4 back">
                        <div class="supermenu">
                            <div class="back">
                                <!--<div class="tituloMenu">Pendientes</div>-->
                                <div class="pedidos">
                                    <div class="titulo2">Pedidos<strong> (<%=pendientes.size()%>)</strong></div>
                                    <div class="scrollApartado">
                                        <%if (pendientes.size() > 0) {%>
                                        <%for (Pedido pe : pendientes) {%>
                                        <%if((pe.getEntrega().getTime()-fecha.getTime().getTime())/86400000<5){%>
                                        <a class="link" href="pedido.jsp?id=<%=pe.getId() %>"><div class="bloqueRojo elBloque"><img src="images/unidad.png" class="btnIcono">P(<%=pe.getId()%>) <%=pe.getClienteId().getEscuela()%> <strong><%=formato2.format(pe.getEntrega())%></strong></div></a>
                                        <%}else{%>
                                        <%if((pe.getEntrega().getTime()-fecha.getTime().getTime())/86400000<10){%>
                                        <a class="link" href="pedido.jsp?id=<%=pe.getId() %>"><div class="bloqueAmarillo elBloque"><img src="images/unidad.png" class="btnIcono">P(<%=pe.getId()%>) <%=pe.getClienteId().getEscuela()%> <strong><%=formato2.format(pe.getEntrega())%></strong></div></a>
                                        <%}else{%>
                                        <a class="link" href="pedido.jsp?id=<%=pe.getId() %>"><div class="bloqueVerde elBloque"><img src="images/unidad.png" class="btnIcono">P(<%=pe.getId()%>) <%=pe.getClienteId().getEscuela()%> <strong><%=formato2.format(pe.getEntrega())%></strong></div></a>
                                        <%}%>
                                        <%}%>
                                            <%}%>
                                            <%} else {%>
                                        <div class="vacio">
                                            <img src="images/vacio.png"><br>
                                            No hay Pedidos Pendientes

                                        </div>
                                        <%}%>
                                    </div>
                                </div>

                                <div class="pedidos">
                                    <div class="titulo2">Compras<strong> (<%=compraPendiente.size()%>)</strong></div>
                                    <div class="scrollApartado">
                                        <%if (compraPendiente.size() > 0) {%>
                                        <%for (Pedidoproveedor cpr : compraPendiente) {%>
                                        <%if (cpr.getProveedorId() != null) {%>
                                        <div class="bloqueVerde"><img src="images/compras.png" class="btnIcono"> P(<%=cpr.getPedidoId().getId()%>) <%=cpr.getPedidoId().getClienteId().getEscuela()%> 
                                            <strong> - <%=cpr.getProveedorId().getNombre()%></strong>
                                        </div>
                                        <%}%>
                                        <%}%>
                                        <%} else {%>
                                        <div class="vacio">
                                            <img src="images/vacio.png"><br>
                                            No hay Compras Pendientes

                                        </div>
                                        <%}%>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <%@ include file="menu.jsp" %>
    </body>
</html>
