<%-- 
    Document   : imprimirPedido
    Created on : 31/08/2018, 11:18:53 AM
    Author     : fernando
--%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="Modelo.Pedido"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Calendar fecha = new GregorianCalendar();
    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy hh:mm a", new Locale("es"));
    PedidoJpaController p=new PedidoJpaController(emf);
    Pedido pedido=p.findPedido(Integer.parseInt(request.getParameter("id")));
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/estilo-ticket.css"/>
    </head>
    <body>
        <div class="bloqueGeneral">
            <div class="imagen">
                <img src="images/logo.jpg">
            </div><br>
            <div class="sucursal">
                Sucursal:<%=sucursal.getNombre()%><br>
                <%if (sucursal.getId() != 1) {%>
                Direccion: Calle<%=sucursal.getDireccionId().getCalle()%> Col. <%=sucursal.getDireccionId().getColonia()%>
                Delegacion <%=sucursal.getDireccionId().getDelegacion()%> C.P. <%=sucursal.getDireccionId().getCp()%>
                <%=sucursal.getDireccionId().getCiudad()%> <%=sucursal.getDireccionId().getEstado()%>
                <%}%>

            </div>
            <div class="informacion">
                <h3>Ticket de Compra</h3>
                <span class="venta">No. Pedido: PP-100<%=pedido.getId()%></span><br>
                Fecha: <%=formato.format(fecha.getTime())%>
            </div>
            <br><br>
            <div class="productos">
                <table class="table table-responsive table-bordered table-condensed">
                    <tr><td>Cliente</td><td><%=pedido.getClienteId().getNombre()%></td></tr>
                    <tr><td>Escuela</td><td><%=pedido.getClienteId().getEscuela()%></td></tr>
                    <tr><td>Total</td><td><%=nf.format(pedido.getTotal())%></td></tr>
                    <%double todo = 0;%>
                </table>
                
                <table class=" letraChica table table-responsive table-responsive table-condensed">
                    <tr>
                        <th>ISBN</th>
                        <th>Titulo</th>
                        <th>Editorial</th>
                        <th>Cantidad</th>
                    </tr>
                    <%for(Pedidoproveedor pp: pedido.getPedidoproveedorList()){%>
                    <%for(Pedidoparte pa: pp.getPedidoparteList()){%>
                    <tr>
                        <td><%=pa.getProductoId().getIsbn()%></td>
                        <td><%=pa.getProductoId().getTitulo()%></td>
                        <td><%=pa.getProductoId().getEditorialId1().getNombre()%></td>
                        <td><%=pa.getCantidad()%></td>
                    </tr>
                    <%
                        float importe=pa.getCantidad()*Float.parseFloat(pa.getProductoId().getPrecioventa());
                        todo+=importe-(importe*pa.getDescuento()/100);
                    %>
                    <%}%>
                    <%}%>
                </table>
            </div>
            <br><br>
            <div class="footer">
                Le ha atendido <%=informacion.getNombre()%>
                <hr>
                GRACIAS ELEGIR COPTES
                <hr>
            </div>

        </div>
    </body>
</html>
