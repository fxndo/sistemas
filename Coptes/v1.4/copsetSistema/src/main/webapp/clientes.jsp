<%-- 
    Document   : clientes
    Created on : 27/04/2018, 07:17:02 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="Modelo.Pago"%>
<%@page import="Modelo.Pedido"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="Modelo.Cliente"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.ClienteJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    int noProductos = 0;
    int pagina = 0;
    int elementos = 20;
    if (request.getParameter("pagina") != null) {
        pagina = Integer.parseInt(request.getParameter("pagina"));
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    ClienteJpaController c = new ClienteJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

    String objeto="";
    if(request.getParameter("objeto")!=null){
        objeto=request.getParameter("objeto");
    }
    List<Cliente> clientes = c.clienteBuscar(pagina,objeto);
    noProductos = c.clienteListaCuenta(objeto);

    
    int notificacion = 0;
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
    }


%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Clientes | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Clientes<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion">

                    <a href="inicio.jsp"><div class="navBloque">Inicio</div></a>
                    <div class="navegacionSelect">Clientes</div></div>

            </header>
            <div class="codigoBarra row">
                <div class="botonBarra col-md-2"><div onclick="flotante('nuevoCliente');" class="btn btn-sm btn-default btn-group-justified"><img src="images/suma2.png" class="btnIcono">Agregar Cliente</div></div>
                
                <form action="clientes.jsp" method="get">
                    <div class="col-md-4 barra"><input  type="search" maxlength="30" placeholder="Buscar..." value="<%=objeto%>" name="objeto"></div>
                    <div class="col-md-1"><button class="btnBuscar" type="submit" value="Buscar"><img src="images/buscar.png" class="iconoNormal"></button></div>
                    
                    <div class="col-md-4">
                        
                    </div>

                    <!--<div class="col-md-1"><div style="position: relative; top: 8px;" onclick="flotante('ayuda1');"><img src="images/ayuda.png" class="btnAyuda"><strong>Ayuda</strong></div></div>-->
                </form>
                
            </div>
                    
            <div style="padding: 30px 200px;">
                <%if(clientes.size()>0){%>
                <table class="tabla-producto table table-responsive letraChica">
                    <tr class="cabeceraTabla">
                        <th></th>
                        <th>#</th>
                        <th>Nombre</th>
                        <th style="padding-left: 15px;">Escuela</th>
                        <!--<th>Num. Tarjeta</th>-->
                        <th>Deuda</th>
                        <th style="text-align: center;">Pedidos</th>
                    </tr>
                    <%int valor = 1;%>
                    <%for (Cliente cl : clientes) {%>
                    <tr class="renglon<%=valor % 2%>">
                        <td></td>
                        <td><%=valor%></td>
                        <td><a class="link" href="cliente.jsp?id=<%=cl.getId()%>"><div class="btnSelector"><%=cl.getNombre()%></div></a></td>
                                <%if (cl.getTipo()==1) {%>
                        <td style="padding-left: 15px;"><%=cl.getEscuela()%></td>
                        <%double total = 0;%>
                        <%for (Pedido pd : p.pedidosClienteMuestra(cl.getId())) {%>
                        <%total += pd.getTotal();%>
                        <%for (Pago pg : pd.getPagoList()) {%>
                        <%total -= pg.getMonto();%>
                        <%}%>
                        <%}%>


                        <%if (total <= 0) {%>
                        <td class="saldoVerde"><div><%=nf.format(total)%></div></td>
                                <%} else {%>
                        <td class="saldoRojo"><div><%=nf.format(total)%></div></td>
                                <%}%>
                                <%} else {%>
                        <td style="padding-left: 15px;">Sucursal</td>
                        <td class="gris">---</td>
                        <%}%>
                        <td class="gris"><strong><%=cl.getPedidoList().size()%></strong></td>
                    </tr>
                    <%valor++;%>
                    <%}%>
                </table>
                <!----------------------------------------------------Paginacion---------------------------------------------->
                <%if(noProductos>20){%>
                <br>
                <div class="text-center">
                    <div class="pagina2">Paginas</div>
                    <%int paginas = noProductos / elementos;%>
                    <%if (noProductos % elementos > 0) {%>
                    <%paginas++;%>
                    <%}%>
                    <%for (int ch = 1; ch <= paginas; ch++) {%>
                    <%if (pagina == ch - 1) {%>
                    <div class="paginaActiva"><%=ch%></div>
                    <%} else {%>
                    <a href="clientes.jsp?pagina=<%=ch - 1%>&finalizados=on"><div class="pagina"><%=ch%></div></a>
                        <%}%>
                        <%}%>
                        <%if (pagina + 1 != paginas) {%>
                    <a href="clientes.jsp?pagina=<%=pagina + 1%>&finalizados=on"><div class="paginaActiva">Siguiente</div></a>
                    <%}%>
                </div>
                <br>
                <%}%>
                <%}else{%>
                    <div class="vacioGrande">
                        No hay clientes<br>
                    <img src="images/vacioGrande.png" class="imgVacio">
                </div>
                <%}%>
                <!-------------------------------------------------------------------------------------------------->
            </div>
        </main>
        <div id="fondoNegro" class="fondoNegro" style="display: none;"></div>
        <div id="nuevoCliente" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Nuevo Cliente</div>
            <form action="agregarCliente" method="post">
                <div class="principalFlotante">
                    <h5 style="margin-left: 210px;">Información</h5>
                    <label class="entradaLabel">Nombre</label><input class="entrada" type="text" name="nombre" maxlength="299" required="on"><br>
                    <label class="entradaLabel">Escuela</label><input class="entrada" type="text" name="escuela" maxlength="99" required="on"><br>
                    <label class="entradaLabel">Telefono</label><input class="entrada" type="text" name="telefono" maxlength="10" required="on"><br>
                    <!--<label>Num. Tarjeta<input class="form-control" type="text" name="tarjeta" maxlength="10" required=""></label>-->
                    <label class="entradaLabel">Celular</label><input class="entrada" type="text" name="celular" maxlength="10" required="on"><br>
                    <label class="entradaLabel">Email</label><input class="entrada" type="text" name="email" maxlength="44" required="on" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"><br>
                    <hr>
                    <h5 style="margin-left: 210px;">Dirección</h5>
                    <label class="entradaLabel">Calle</label><input class="entrada" type="text" name="calle" required="on" maxlength="45"><br>
                    <label class="entradaLabel">Colonia</label><input class="entrada" type="text" name="colonia" required="on" maxlength="45"><br>
                    <label class="entradaLabel">Delegación</label><input class="entrada" type="text" name="delegacion" required="on" maxlength="45"><br>
                    <label class="entradaLabel">Codigo Postal</label><input class="entrada" type="text" name="cp" required="on" maxlength="6"><br>
                    <label class="entradaLabel">Ciudad</label><input class="entrada" type="text" name="ciudad" required="on" maxlength="45"><br>
                    <label class="entradaLabel">Estado</label><input class="entrada" type="text" name="estado" required="on" maxlength="45"><br>
                    <br><br>
                </div>
                <div class="botonesFlotante"><button type="submit" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Agregar Cliente</button><div onclick="flotante('nuevoCliente')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <!--/////////////////////////////////////////// Notificacion    /////////////////////////////////-->
        <div class="notificacionBloque" style="<%if (notificacion == 7) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Cliente Creado</div>
                El <strong>cliente</strong> se creo satisfactoriamente.
            </div>
        </div>
        <%@ include file="menu.jsp" %>
    </body>
</html>
