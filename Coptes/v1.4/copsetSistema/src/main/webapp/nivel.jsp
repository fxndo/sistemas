<%-- 
    Document   : nivel
    Created on : 19/05/2018, 03:12:11 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Nivel"%>
<%@page import="ModeloJSP.NivelJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    NivelJpaController n = new NivelJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    Nivel nivel = n.findNivel(Integer.parseInt(request.getParameter("id")));
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Nivel | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Nivel<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="niveles.jsp"><div class="navBloque">Niveles</div></a><div class="navegacionSelect">Nivel</div></div>

            </header>
            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <div id="opciones1" onclick="listado('opciones');" class="listadoOpcion"><img src="images/colorOpciones.png" class="btnIcono2">Opciones</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">

                    <!--//////////////////////////////////////////////////////////  Informacion    //////////////////////////////////////////-->
                    <div class="listPedido" id="informacion" style="display: block;">
                        <!--<br><br>-->
                        <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                        <p class="parrafoListado">
                            Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                        </p>
                        <!--<br>-->
                        <div class="separadorListado"></div>
                        <br><br>
                        <form action="editarNivel" method="post">
                            <input type="hidden" name="idNivel" value="<%=nivel.getId()%>">
                            <label class="entradaLabel">Nivel</label><input class="entrada" required="on" maxlength="40" type="text" name="nombre" value="<%=nivel.getNombre()%>">
                            <br><br>
                            <button style="margin-left: 210px;" type="submit" class="btn btn-sm btn-default"><img src="images/editarColor.png" class="btnIcono">Editar Nivel</button>
                        </form>
                    </div>

                    <!--//////////////////////////////////////////////////////////  Opciones    //////////////////////////////////////////-->
                    <div class="listPedido" id="opciones" style="display: none;">
                        <!--<br><br>-->
                        <h4 class="tituloListado"><img src="images/colorOpciones.png" class="iconoTitulo">Opciones</h4>
                        <p class="parrafoListado">
                            Al eliminar el nivel ya no aparecera en la lista de <strong>Niveles</strong> ni al crear un nuevo producto
                        </p>
                        <!--<br>-->
                        <div class="separadorListado"></div>
                        <div onclick="flotante('eliminarNivel');" class="btn bbtn btn-sm btn-default"><img src="images/less.png" class="btnIcono">Eliminar Nivel</div>
                    </div>
                </div>
            </div>
        </main>

        <!--/////////////////////////////////////////////////////////////// Flotante    //////////////////////////////////////////////-->
        <div id="eliminarNivel" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Eliminar Nivel</div>
            <form action="eliminarNivel" method="post">
                <div class="principalFlotante">
                    <br><br>
                    <h4 class="text-center">¿Seguro que quieres eliminar el Nivel?</h4>
                    <br><br>
                    <input type="hidden" name="idNivel" value="<%=nivel.getId()%>">
                </div>
                <div class="botonesFlotante"><button type="submit" class="btn btn-danger btn-sm"><img src="images/borrarBlanco.png" class="btnIcono">Eliminar Proveedor</button><div onclick="flotante('eliminarNivel')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <%@ include file="menu.jsp" %>
        <script>
            function listado(id) {

                document.getElementById('informacion').style.display = "none";
                document.getElementById('informacion1').className = "listadoOpcion";
                document.getElementById('opciones').style.display = "none";
                document.getElementById('opciones1').className = "listadoOpcion";
                document.getElementById(id).style.display = "block";
                document.getElementById(id + "1").className = "selectL";

            }
        </script>
    </body>
</html>
