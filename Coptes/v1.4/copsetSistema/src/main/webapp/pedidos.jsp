<%-- 
    Document   : pedidos
    Created on : 28/04/2018, 04:01:22 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="java.util.ArrayList"%>
<%--<%@page import="Modelo.PedidoHasProducto"%>--%>
<%@page import="Modelo.Pago"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="Modelo.Pedido"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="Modelo.Usuario"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    String objeto = "";
    int finalizado = 0;

    if (request.getParameter("finalizados") != null) {
        finalizado = Integer.parseInt(request.getParameter("finalizados"));
    }
    if (request.getParameter("objeto") != null) {
        objeto = request.getParameter("objeto");
    }
    int addm = 0;
    if (informacion.getPermisosList().get(0).getAdministracionp() == 1) {
        addm = 1;
    }
    if (informacion.getPermisosList().get(0).getAlmacenp() == 1) {
        addm = 1;
    }

    int noProductos = 0;
    int pagina = 0;
    int elementos = 20;

    if (request.getParameter("pagina") != null) {
        pagina = Integer.parseInt(request.getParameter("pagina"));
    }

    if (request.getAttribute("noProductos") != null) {
        noProductos = Integer.parseInt(request.getParameter("noProductos"));
    }

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);

    SimpleDateFormat formato = new SimpleDateFormat("dd MMM yyyy", new Locale("es"));
    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

    List<Pedido> pedidos = p.busquedaPedido(objeto, finalizado, sucursal.getId(), addm, informacion.getId(), pagina);
    noProductos = p.numeroBusquedaPedido(objeto, finalizado, sucursal.getId(), addm, informacion.getId());

    int notificacion = 0;
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Pedidos | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Pedidos<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Pedidos</div></div>
            </header>

            <div class="codigoBarra row">
                <div class="botonBarra col-md-2">
                    <%if (informacion.getPermisosList().get(0).getPedidosp() == 1) {%>
                    <a href="nuevoPedido.jsp" class="btn btn-sm btn-default btn-group-justified"><img src="images/suma2.png" class="btnIcono">Nuevo Pedido</a>
                        <%}%>
                </div>
                <form action="pedidos.jsp" method="get">
                    <div class="col-md-4 barra"><input  type="search" maxlength="30" placeholder="Buscar..." value="<%=objeto%>" name="objeto"></div>
                    <div class="col-md-1"><button class="btnBuscar" type="submit" value="Buscar"><img src="images/buscar.png" class="iconoNormal"></button></div>
                            <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1) {%>
                            <%if (finalizado == 1) {%>

                    <div class="col-md-4">
                        <label style="position: relative;bottom: 2px;" class="labelSwitch">Cancelados y Ocultos</label>
                        <div style="position: relative;top:5px;" id="switchBloque1" onclick="switchf(1)" class="switchBloque switchVerde">
                            <div id="switch1" class="switch switchDerecha"></div>
                        </div>
                        <input id="finalizadosIn" type="hidden" value="1" name="finalizados">
                    </div>

                    <%} else {%>
                    <div class="col-md-4">
                        <label style="position: relative;bottom: 2px;" class="labelSwitch">Cancelados y Ocultos</label>
                        <div style="position: relative;top:5px;" id="switchBloque1" onclick="switchf(1)" class="switchBloque switchBlanco">
                            <div id="switch1" class="switch switchIzquierda"></div>
                        </div>
                        <input id="finalizadosIn" type="hidden" value="0" name="finalizados">
                    </div>
                    <%}%>
                    <%}%>
                    <div class="col-md-1"><div style="position: relative; top: 8px;" onclick="flotante('ayuda1');"><img src="images/ayuda.png" class="btnAyuda"><strong>Ayuda</strong></div></div>
                </form>
            </div>
            <div>
                <%if (pedidos.size() > 0) {%>
                <table class="tabla-producto table table-responsive tabla-producto letraChica">
                    <tr class="cabeceraTabla">
                        <th style="width: 50px;text-align: center;">#</th>
                        <th>Estado</th>
                        <th style="width: 80px;">Fecha</th>
                        <th style="width: 80px;">Entrega</th>
                        <th>Cliente</th>
                        <th>Escuela</th>
                        <th style="width: 80px;">Productos</th>
                        <th style="width: 100px;">Total</th>
                        <th style="width: 100px;">Restante</th>
                        <th style="padding-left: 15px;">Sucursal</th>
                    </tr>
                    <%int renglon = 1;%>
                    <%for (Pedido pd : pedidos) {%>
                    <tr class="renglon<%=renglon % 2%>">
                        <td style="text-align: center"><strong><%=pd.getId()%></strong></td>

                        <%if (pd.getEstado().equals("1")) {%>
                        <td class="estado colorE1"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Nuevo</div></a></td>
                        <%}
                            if (pd.getEstado().equals("2") || pd.getEstado().equals("3")) {%>
                        <td class="estado colorE2"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Pendiente</div></a></td>
                        <%}
                            if (pd.getEstado().equals("4") || pd.getEstado().equals("5")) {%>
                        <td class="estado colorE3"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Asignado</div></a></td>
                        <%}
                            if (pd.getEstado().equals("6")) {%>
                        <td class="estado colorE4"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Proveedor</div></a></td>
                        <%}
                            if (pd.getEstado().equals("7")) {%>
                        <td class="estado colorE5"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Faltante</div></a></td>
                        <%}
                            if (pd.getEstado().equals("8")) {%>
                        <td class="estado colorE6"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Almacen</div></a></td>
                        <%}
                            if (pd.getEstado().equals("9")) {%>
                        <td class="estado colorE7"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Traslado</div></a></td>
                        <%}
                            if (pd.getEstado().equals("10")) {%>
                        <td class="estado colorE8"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Sucursal</div></a></td>
                        <%}
                            if (pd.getEstado().equals("11")) {%>
                        <td class="estado colorE9"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Entrega</div></a></td>
                        <%}
                            if (pd.getEstado().equals("12")) {%>
                        <td class="estado colorE10"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Finalizado</div></a></td>
                        <%}
                            if (pd.getEstado().equals("13")) {%>
                        <td class="estado colorE11"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Finalizado</div></a></td>
                        <%}
                            if (pd.getEstado().equals("14")) {%>
                        <td class="estado colorE12"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Oculto</div></a></td>
                        <%}
                            if (pd.getEstado().equals("15")) {%>
                        <td class="estado colorE13"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>"><div>Cancelado</div></a></td>
                        <%}%>


                        <td class="fechaPedido"><strong><%=formato.format(pd.getFecha())%></strong></td>
                        <td class="fechaEntrega"><strong><%=formato.format(pd.getEntrega())%></strong></td>
                        <td><a class="link2" target="_blank"  href="cliente.jsp?id=<%=pd.getClienteId().getId()%>"><%=pd.getClienteId().getNombre()%></a></td>
                        <td><%=pd.getClienteId().getEscuela()%></td>

                        <%
                            int totalP = 0;
                            for (Pedidoproveedor php : pprj.listaPedidoProveedor(pd.getId())) {
                                for (Pedidoparte pa : php.getPedidoparteList()) {
                                    totalP += pa.getCantidad();
                                }
                            }
                        %>

                        <td class="gris"><strong><%=totalP%></strong></td>
                        <td class="saldoTotal"><div><%=nf.format(pd.getTotal())%></div></td>

                        <%
                            double resta = pd.getTotal();
                            for (Pago pg : pd.getPagoList()) {
                                resta -= pg.getMonto();
                            }
                            if (resta > 0) {
                        %>

                        <td class="saldoRojo"><div><%=nf.format(resta)%></div></td>
                                <%} else {%>
                        <td class="saldoVerde"><div><%=nf.format(resta)%></div></td>
                                <%}%>

                        <td style="padding-left: 15px;"><%=pd.getSucursalId().getNombre()%></td>
                    </tr>
                    <%renglon++;%>
                    <%}%>
                </table>
                <!----------------------------------------------------Paginacion---------------------------------------------->
                <%if (noProductos > 20) {%>
                <br>
                <div class="text-center">
                    <div class="pagina2">Paginas</div>
                    
                    <%
                        int paginas = noProductos / elementos;
                        if (noProductos % elementos > 0) {
                            paginas++;
                        }
                        
                        for (int c = 1; c <= paginas; c++) {
                            if (pagina == c - 1) {%>
                            
                    <div class="paginaActiva"><%=c%></div>
                    <%} else {%>
                    <%if (finalizado == 1) {%>
                    <a href="pedidos.jsp?objeto=<%=objeto%>&pagina=<%=c - 1%>&finalizados=<%=finalizado%>"><div class="pagina"><%=c%></div></a>
                        <%} else {%>
                    <a href="pedidos.jsp?objeto=<%=objeto%>&pagina=<%=c - 1%>"><div class="pagina"><%=c%></div></a>
                        <%
                                    }
                                }
                            }
                        %>

                    <%if (pagina + 1 != paginas) {%>
                    <%if (finalizado == 1) {%>
                    <a href="pedidos.jsp?objeto=<%=objeto%>&pagina=<%=pagina + 1%>&finalizados=<%=finalizado%>"><div class="paginaActiva">Siguiente</div></a>
                    <%} else {%>
                    <a href="pedidos.jsp?objeto=<%=objeto%>&pagina=<%=pagina + 1%>"><div class="paginaActiva">Siguiente</div></a>
                    <%}%>
                    <%}%>
                </div>
                <br>
                <%}%>
                <%} else {%>
                <div class="vacioGrande">
                    No hay Pedidos<br>
                    <img src="images/vacioGrande.png" class="imgVacio">
                </div>
                <!-------------------------------------------------------------------------------------------------->
                <%}%>
            </div>
        </main>
        <!--////////////////////////////////////////////    Ayuda  //////////////////////////////////////-->
        <div id="ayuda1" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Ayuda - Lista de Pedidos</div>
            <div class="principalFlotante">
                <ul>
                    <li>Puedes buscar un pedido en el campo <strong>Buscar</strong> por <strong>Cliente</strong> o <strong>Escuela</strong>.</li>
                    <li>Da clic en <strong>Estado</strong> para ver la información del pedido.</li>
                    <li>Da clic en <strong>Cancelados y finalizados</strong> para agregar a la busqueda los pedidos cancelados o finalizados.</li>
                    <li>Para generar un nuevo pedido da clic en <strong>Nuevo Pedido</strong></li>
                </ul>
            </div>
            <div class="botonesFlotante"><div onclick="flotante('ayuda1')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div>
            </div>
        </div>
        <!--////////////////////////////////////////////    Notificaciones  //////////////////////////////////////-->
        <div class="notificacionBloque" style="<%if (notificacion == 2) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Pedido Creado</div>
                El <strong>pedido</strong> se creo satisfactoriamente.
            </div>
        </div>
        <div class="notificacionBloque" style="<%if (notificacion == 19) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Pedido Finalizado</div>
                La <strong>pedido</strong> se finalizo satisfactoriamente.
            </div>
        </div>
        <div class="notificacionBloque" style="<%if (notificacion == 26) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Agregado a Bodega</div>
                El <strong>pedido</strong> se agrego satisfactoriamente.
            </div>
        </div>
        <%@ include file="menu.jsp" %>
        <script>
            function switchf(id) {
                if (document.getElementById('switchBloque' + id).className === "switchBloque switchVerde") {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchBlanco";
                    document.getElementById('switch' + id).className = "switch switchIzquierda";
                    document.getElementById('finalizadosIn').value = "0";

                } else {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchVerde";
                    document.getElementById('switch' + id).className = "switch switchDerecha";
                    document.getElementById('finalizadosIn').value = "1";
                }
            }

        </script>
    </body>
</html>
