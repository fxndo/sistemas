<%-- 
    Document   : proveedores
    Created on : 28/04/2018, 02:07:07 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Proveedor"%>
<%@page import="ModeloJSP.ProveedorJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    int noProductos = 0;
    int pagina = 0;
    int elementos = 20;
    if (request.getParameter("pagina") != null) {
        pagina = Integer.parseInt(request.getParameter("pagina"));
    }

    String objeto = "";
    if (request.getParameter("objeto") != null) {
        objeto = request.getParameter("objeto");
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    ProveedorJpaController pr = new ProveedorJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);

    List<Proveedor> proveedores = pr.buscarProveedores(objeto,pagina);
    noProductos = pr.proveedoresListaCuenta(objeto);

    int notificacion = 0;
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Proveedores | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Proveedores<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Proveedores</div></div>

            </header>
            <div class="codigoBarra row">
                <div class="col-md-4"><div onclick="flotante('nuevoProveedor');" class="btn btn-sm btn-default"><img src="images/suma2.png" class="btnIcono">Agregar Proveedor</div>
                    <a href="reporteProveedores"><div class="btn btn-sm btn-default"><img src="images/imprimirColor.png" class="btnIcono">Imprimir Proveedores</div></a></div>

                <form action="proveedores.jsp" method="get">
                    <div class="col-md-4 barra"><input  type="search" maxlength="30" placeholder="Buscar..." value="<%=objeto%>" name="objeto"></div>
                    <div class="col-md-1"><button class="btnBuscar" type="submit" value="Buscar"><img src="images/buscar.png" class="iconoNormal"></button></div>
                </form>

            </div>
            <%if (proveedores.size() > 0) {%>    
            <div style="padding: 15px 50px;">
                <table class="tabla-producto table table-responsive letraChica">
                    <tr class="cabeceraTabla">
                        <th style="width: 50px;text-align: center;">#</th>
                        <th style="width: 100px;">Clave</th>
                        <th style="padding-left: 15px;">Nom. Completo</th>
                        <th style="width: 80px;text-align: center;">Editoriales</th>
                        <th style="padding-left: 15px;">Email</th>
                        <th style="width: 100px;">Telefono</th>
                    </tr>
                    <%int valor = 1 + (pagina * 20);%>
                    <%for (Proveedor pv : proveedores) {%>
                    <tr class="renglon<%=valor % 2%>">
                        <td style="text-align: center;"><%=valor%></td>
                        <td><a class="link" href="proveedor.jsp?id=<%=pv.getId()%>"><div class="btnSelector"><%=pv.getClave()%></div></a></td>
                        <td style="padding-left: 15px;"><%=pv.getNombre()%></td>
                        <td style="text-align:center;background: #e0e0e0;border-bottom: 1px solid #d1d1d1;"><strong><%=pv.getProveedoreditorialList().size()%></strong></td>
                        <td style="padding-left: 15px;"><%=pv.getEmail()%></td>
                        <td><%=pv.getTelefono()%></td>
                    </tr>
                    <%valor++;%>
                    <%}%>
                </table>
                <!----------------------------------------------------Paginacion---------------------------------------------->
                <%if (noProductos > 20) {%>
                <br>
                <div class="text-center">
                    <div class="pagina2">Paginas</div>
                    <%int paginas = noProductos / elementos;%>
                    <%if (noProductos % elementos > 0) {%>
                    <%paginas++;%>
                    <%}%>
                    <%for (int c = 1; c <= paginas; c++) {%>
                    <%if (pagina == c - 1) {%>
                    <div class="paginaActiva"><%=c%></div>
                    <%} else {%>
                    <a href="proveedores.jsp?pagina=<%=c - 1%>&finalizados=on"><div class="pagina"><%=c%></div></a>
                        <%}%>
                        <%}%>
                        <%if (pagina + 1 != paginas) {%>
                    <a href="proveedores.jsp?pagina=<%=pagina + 1%>&finalizados=on"><div class="paginaActiva">Siguiente</div></a>
                    <%}%>
                </div>
                <%}%>
                <%} else {%>
                <div class="vacioGrande">
                    No hay Proveedores<br>
                    <img src="images/vacioGrande.png" class="imgVacio">
                </div>
                <%}%>
                <br>
                <!-------------------------------------------------------------------------------------------------->
            </div>
        </main>
        <div id="fondoNegro" class="fondoNegro" style="display: none;"></div>
        <div id="nuevoProveedor" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Nuevo Proveedor</div>
            <form action="agregarProveedor" method="post">
                <div class="principalFlotante">
                    <h5 style="margin-left: 210px;" class="tituloListado2">Información</h5>
                    <label class="entradaLabel">Clave</label><input class="entrada" type="text" onkeyup="this.value = this.value.toUpperCase()" name="clave" maxlength="5" required="on"><br>
                    <label class="entradaLabel">Nom. Completo</label><input class="entrada" type="text" name="completo" maxlength="45" required="on"><br>
                    <label class="entradaLabel">Email</label><input class="entrada" type="text" name="email" maxlength="44" required="on" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"><br>
                    <label class="entradaLabel">Telefono</label><input class="entrada" type="text" name="telefono" maxlength="12" minlength="10" required="on" pattern="[0-9]+"><br>
                    <hr>
                    <h5 style="margin-left: 210px;">Dirección</h5>
                    <label class="entradaLabel">Calle</label><input class="entrada" type="text" name="calle" maxlength="45"><br>
                    <label class="entradaLabel">Colonia</label><input class="entrada" type="text" name="colonia" maxlength="45"><br>
                    <label class="entradaLabel">Delegación</label><input class="entrada" type="text" name="delegacion" maxlength="45"><br>
                    <label class="entradaLabel">Codigo Postal</label><input class="entrada" type="text" name="cp" maxlength="45"><br>
                    <label class="entradaLabel">Ciudad</label><input class="entrada" type="text" name="ciudad" maxlength="45"><br>
                    <label class="entradaLabel">Estado</label><input class="entrada" type="text" name="estado" maxlength="45"><br>
                    <br><br>
                </div>
                <div class="botonesFlotante"><button type="submit" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Agregar Proveedor</button><div onclick="flotante('nuevoProveedor')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <!--/////////////////////////////////////// Notificaciones  ///////////////////////////////////////////-->
        <div class="notificacionBloque" style="<%if (notificacion == 4) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Proveedor Creado</div>
                El <strong>proveedor</strong> se creo satisfactoriamente.
            </div>
        </div>
        <%@ include file="menu.jsp" %>
    </body>
</html>
