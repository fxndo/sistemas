<%-- 
    Document   : usuarios
    Created on : 15/05/2018, 07:39:49 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.UsuarioJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if(sesion.getAttribute("informacion")!=null){
    }else{
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    int noProductos = 0;
    int pagina = 0;
    int elementos = 20;
    if (request.getParameter("pagina") != null) {
        pagina = Integer.parseInt(request.getParameter("pagina"));
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    UsuarioJpaController u=new UsuarioJpaController(emf);
    
    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoparteJpaController ppan=new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj=new PedidoproveedorJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);
    
    List<Usuario>usuarios=u.usuarioSucursal(sucursal.getId(),pagina);
    noProductos=u.usuarioSucursalCuenta(sucursal.getId());
    
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Usuarios | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
        <header>
            <div class="row">
                <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                <div class="col-md-4"><div class="letrero">Usuarios<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
            </div>
            <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Usuarios</div></div>

        </header>
                <div class="codigoBarra row">
                    <div class="botonBarra col-md-2"><a href="nuevoUsuario.jsp" class="btn btn-sm btn-default btn-group-justified"><img src="images/suma2.png" class="btnIcono">Agregar Usuarios</a></div>
            </div>
                <table class="tabla-producto table table-responsive tabla-producto letraChica">
                    <tr class="cabeceraTabla">
                        <th style="width: 50px; text-align: center;">#</th>
                        <th style="width: 220px;">Nombre</th>
                        <th>Cargo</th>
                        <th style="text-align: center;">Administrador</th>
                        <th style="text-align: center;">Almacen</th>
                        <th style="text-align: center;">Compras</th>
                        <th style="text-align: center;">Pagos</th>
                        <th style="text-align: center;">Pedidos</th>
                        <th style="text-align: center;">Proveedores</th>
                        <th style="text-align: center;">Remision</th>
                        <th style="text-align: center;">Super U</th>
                        <th style="text-align: center;">TPV</th>
                        <th style="text-align: center;">Traslados</th>
                        <th style="text-align: center;">Ventas</th>
                        <th>Sucursal</th>
                    </tr>
                    <%int valor=1;%>
                    <%for(Usuario us: usuarios){%>
                    <tr class="renglon<%=valor%2%>">
                        <Td style="text-align: center;"><strong><%=valor%></strong></Td>
                        <td><a class="link" href="usuario.jsp?id=<%=us.getId()%>"><div class="btnSelector"><%=us.getNombre()%></div></a></td>
                        <td title="<%=us.getTipo()%>"><%if(us.getTipo().length()<27){%>
                            <%=us.getTipo()%>
                            <%}else{%>
                            <%=us.getTipo().substring(0,27)+"..."%>
                            <%}%>
                        </td>
                        
                        <%if(us.getPermisosList().get(0).getAdministracionp()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>
                        
                        <%if(us.getPermisosList().get(0).getAlmacenp()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>
                        
                        <%if(us.getPermisosList().get(0).getComprasp()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>
                        
                        <%if(us.getPermisosList().get(0).getPagosp()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>
                        
                        <%if(us.getPermisosList().get(0).getPedidosp()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>
                        
                        <%if(us.getPermisosList().get(0).getProveedoresp()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>

                        <%if(us.getPermisosList().get(0).getRemisionp()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>

                        <%if(us.getPermisosList().get(0).getSuperusuario()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>
                        
                        <%if(us.getPermisosList().get(0).getTpvp()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>
                        
                        <%if(us.getPermisosList().get(0).getTraslados()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>
                        
                        <%if(us.getPermisosList().get(0).getVentap()==1){%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/sipermiso.png"></td>
                        <%}else{%>
                        <td style="text-align: center;width: 60px;"><img class="iconoNormalGrande" src="images/nopermiso.png"></td>
                        <%}%>
                        
                        
                        
                        <td><%=us.getSucursalId().getNombre() %></td>
                    </tr>
                    <%valor++;%>
                    <%}%>
                </table>
                <!----------------------------------------------------Paginacion---------------------------------------------->
                <%if(usuarios.size()>20){%>
                <br>
                <div class="text-center">
                    <div class="pagina2">Paginas</div>
                    <%int paginas = noProductos / elementos;%>
                    <%if (noProductos % elementos > 0) {%>
                    <%paginas++;%>
                    <%}%>
                    <%for (int c = 1; c <= paginas; c++) {%>
                    <%if (pagina == c - 1) {%>
                    <div class="paginaActiva"><%=c%></div>
                    <%} else {%>
                    <a href="usuarios.jsp?pagina=<%=c - 1%>&finalizados=on"><div class="pagina"><%=c%></div></a>
                    <%}%>
                    <%}%>
                    <%if (pagina + 1 != paginas) {%>
                    <a href="usuarios.jsp?pagina=<%=pagina + 1%>&finalizados=on"><div class="paginaActiva">Siguiente</div></a>
                    <%}%>
                </div>
                <br>
                <%}%>
                <!-------------------------------------------------------------------------------------------------->
            </main>
            <%@ include file="menu.jsp" %>
    </body>
</html>
