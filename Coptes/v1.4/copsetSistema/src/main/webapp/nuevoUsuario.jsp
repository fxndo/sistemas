<%-- 
    Document   : nuevoUsuario
    Created on : 15/05/2018, 08:19:38 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);

    List<Sucursal> sucursales = scj.findSucursalEntities();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Nuevo Usuario | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Nuevo Usuario<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="usuarios.jsp"><div class="navBloque">Usuarios</div></a><div class="navegacionSelect">Nuevo Usuario</div></div>

            </header>
            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="listadoOpcion selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <div id="permisos1" onclick="listado('permisos');" class="listadoOpcion"><img src="images/colorPermiso.png" class="btnIcono2">Permisos</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
            <form action="agregarUsuario" method="post">
                <div class="listPedido" id="informacion">
                        <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                        <p class="parrafoListado">
                            Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                        </p>

                        <div class="separadorListado"></div>
                        <br><br>
                    <%if (sucursal.getId() == 1) {%>
                    <label class="entradaLabel">Sucursal</label><select class="entrada" name="sucursal">
                        <%if (informacion.getSucursalId().getId() == 1) {%>
                        <option value="1">General</option>
                        <%}%>
                        <%for (Sucursal sc : sucursales) {%>
                        <%if (sc.getId() != 1) {%>
                        <option value="<%=sc.getId()%>"><%=sc.getNombre()%></option>
                        <%}%>
                        <%}%>
                    </select><br><br>
                    <%} else {%>
                    <input type="hidden" name="sucursal" value="<%=sucursal.getId()%>">
                    <%}%>

                    <br>
                    <label class="entradaLabel">Nombre</label><input type="text" placeholder="Nombre del usuario" class="entrada" name="nombre" maxlength="299" required="on"><br>
                    <label class="entradaLabel">Cargo</label><input type="text" placeholder="El cargo que tiene en la sucursal" class="entrada" name="cargo" maxlength="99" required="on"><br>
                    <label class="entradaLabel">Telefono</label><input type="text" placeholder="Telefono del usuario" class="entrada" name="telefono" maxlength="10" required="on"><br><br>
                    <label class="entradaLabel">Email</label><input type="text" placeholder="Email para inicio sesión" class="entrada" name="email" maxlength="44" required="on" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"><br>
                    <label class="entradaLabel">Contraseña</label><input type="text" placeholder="Contraseña para inicio de sesión" class="entrada"autocomplete="off" name="pass" maxlength="44" required="on"><br>

                    <br><br>
                    <label class="entradaLabel"> </label><div onclick="listado('permisos');" class="btn btn-sm btn-default"><img src="images/next.png" class="btnIcono">Siguiente</div>
                    <br>

                </div>

                    <div class="listPedido" id="permisos" style="display: none;">
                        <h4 class="tituloListado"><img src="images/colorPermiso.png" class="iconoTitulo">Permisos</h4>
                        <p class="parrafoListado">
                            Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                        </p>
                        <div class="separadorListado"></div>

                        <!------------------------------------------>
                        <div class="bloqueNormal">
                        <label class="tituloSwitch">Administración</label>
                        <div style="margin-left: 100px;" id="switchBloque6" onclick="switchf(6)" class="switchBloque switchBlanco">
                            <div id="switch6" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Iniciar caja TPV</span><br>
                            <span class="labelSwitch2">Realizar remisiones de sucursal a sucursal</span><br>
                            <span class="labelSwitch2">Entrada de efectivo caja TPV</span><br>
                            <span class="labelSwitch2">Salida de efectivo caja TPV</span><br>
                            <span class="labelSwitch2">Cerrar caja TPV</span><br>
                            <span class="labelSwitch2">Editar stock</span><br>
                            <span class="labelSwitch2">Visualizar, editar y crear Descuentos</span><br>
                            <span class="labelSwitch2">Visualizar, editar y crear Clientes</span><br>
                            <span class="labelSwitch2">Visualizar, editar y crear Proveedores</span><br>
                            <span class="labelSwitch2">Visualizar, editar y crear Productos</span><br>
                            <span class="labelSwitch2">Visualizar historial ventas</span><br>
                            <span class="labelSwitch2">Visualizar historial de remisiones</span><br>
                            <span class="labelSwitch2">Visualizar Pedidos ocultos y cancelados</span><br>
                        </div>
                        <input id="input6" class="form-control" type="hidden" name="administrador" value="0">
                    </div>
                        <!------------------------------------------>
                        <div class="bloqueNormal">
                        <label class="tituloSwitch">Almacen</label>
                        <div style="margin-left: 100px;" id="switchBloque3" onclick="switchf(3)" class="switchBloque switchBlanco">
                            <div id="switch3" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Agregar producto al stock</span><br>
                            <span class="labelSwitch2">Ingresar pedidos a bodega</span><br>
                            <span class="labelSwitch2">Administrar Traslados</span><br>
                            <span class="labelSwitch2">Visualizar Productos</span><br>
                            <span class="labelSwitch2">Visualizar Pedidos</span><br>
                        </div>
                        <input id="input3" class="form-control" type="hidden" name="almacen" value="0">
                    </div>
                        <!------------------------------------------>
                        <div class="bloqueNormal">
                        <label class="tituloSwitch">Compras</label>
                        <div style="margin-left: 100px;" id="switchBloque2" onclick="switchf(2)" class="switchBloque switchBlanco">
                            <div id="switch2" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Realizar compras de pedidos</span><br>
                            <span class="labelSwitch2">Visualizar historial de compras</span><br>
                        </div>
                        <input id="input2" class="form-control" type="hidden" name="compras" value="0">
                    </div>
                        <!------------------------------------------>
                        <div class="bloqueNormal">
                        <label class="tituloSwitch">Pagos</label>
                        <div style="margin-left: 100px;" id="switchBloque4" onclick="switchf(4)" class="switchBloque switchBlanco">
                            <div id="switch4" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Realizar pagos de pedidos</span><br>
                            <span class="labelSwitch2">Visualizar historial pagos</span><br>
                        </div>
                        <input id="input4" class="form-control" type="hidden" name="pagos" value="0">
                    </div>
                        <!------------------------------------------>
                        <div class="bloqueNormal">
                        <label class="tituloSwitch">Pedidos</label>
                        <div style="margin-left: 100px;" id="switchBloque10" onclick="switchf(10)" class="switchBloque switchBlanco">
                            <div id="switch10" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Realizar pedidos</span><br>
                            <span class="labelSwitch2">Visualizar historial de Pedidos</span><br>
                        </div>
                        <input id="input10" class="form-control" type="hidden" name="pedidos" value="0">
                    </div>
                        <!------------------------------------------>
                        <div class="bloqueNormal">
                        <label class="tituloSwitch">Proveedores</label>
                        <div style="margin-left: 100px;" id="switchBloque8" onclick="switchf(8)" class="switchBloque switchBlanco">
                            <div id="switch8" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Configurar proveedores de pedidos</span><br>
                            <span class="labelSwitch2">Configurar descuentos de pedidos</span><br>
                            <span class="labelSwitch2">Crear traslados de pedidos</span><br>
                        </div>
                        <input id="input8" class="form-control" type="hidden" name="proveedores" value="0">
                    </div>
                        <!------------------------------------------>
                        <div class="bloqueNormal">
                        <label class="tituloSwitch">Remisión</label>
                        <div style="margin-left: 100px;" id="switchBloque5" onclick="switchf(5)" class="switchBloque switchBlanco">
                            <div id="switch5" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Realizar remisiones de pedidos</span><br>
                            <span class="labelSwitch2">Realizar remisiones de sucursal a sucursal</span><br>
                            <span class="labelSwitch2">Visualizar lista de Productos</span><br>
                            <span class="labelSwitch2">Visualizar lista de Clientes</span><br>
                            <span class="labelSwitch2">Visualizar lista de Pedidos</span><br>
                            <span class="labelSwitch2">Visualizar historial de remisiones</span><br>
                        </div>
                        <input id="input5" class="form-control" type="hidden" name="remisiones" value="0">
                    </div>
                        <!------------------------------------------>
                    <div class="bloqueNormal">
                        <label class="tituloSwitch">Super Usuario</label>
                        <div style="margin-left: 100px;" id="switchBloque7" onclick="switchf(7)" class="switchBloque switchBlanco">
                            <div id="switch7" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Visualizar, editar y crear Sucursales</span><br>
                            <span class="labelSwitch2">Visualizar, editar y crear Usuarios</span><br>
                            <span class="labelSwitch2">Visualizar Finanzas</span><br>
                        </div>
                        <input id="input7" class="form-control" type="hidden" name="superUsuario" value="0">
                    </div>
                        <!------------------------------------------>
                        <div class="bloqueNormal">
                        <label class="tituloSwitch">Terminal Punto de Venta</label>
                        <div style="margin-left: 100px;" id="switchBloque1" onclick="switchf(1)" class="switchBloque switchBlanco">
                            <div id="switch1" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Manejar la Terminal Punto de Venta</span><br>
                            <span class="labelSwitch2">Visualizar historial de ventas</span><br>
                        </div>
                        <input id="input1" class="form-control" type="hidden" name="tpv" value="0">
                    </div>
                        <!------------------------------------------>
                    
                    <div class="bloqueNormal">
                        <label class="tituloSwitch">Traslados</label>
                        <div style="margin-left: 100px;" id="switchBloque9" onclick="switchf(9)" class="switchBloque switchBlanco">
                            <div id="switch9" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Aceptar o denegar los traslados de otras sucursales</span><br>
                        </div>
                        <input id="input9" class="form-control" type="hidden" name="traslados" value="0">
                    </div>
                        <!------------------------------------------>
                    
                    <div class="bloqueNormal">
                        <label class="tituloSwitch">Ventas</label>
                        <div style="margin-left: 100px;" id="switchBloque11" onclick="switchf(11)" class="switchBloque switchBlanco">
                            <div id="switch11" class="switch switchIzquierda"></div>
                        </div>
                        <div class="descripcionSwitch">
                            <span class="labelSwitch2">Visualizar historial de ventas</span><br>
                        </div>
                        <input id="input11" class="form-control" type="hidden" name="ventas" value="0">
                    </div>

                    
                    <br>
                    <div style="text-align: center;">
                        <button type="submit" class="btn btn-sm btn-default" value="Agregar Usuario"><img src="images/sipermiso.png" class="btnIcono">Aceptar</button>
                    </div>
                </div>
            </form>
        </div>
        </div>
        </main>
        <%@ include file="menu.jsp" %>
        <script>
            function verificarEmail() {
                var email = document.getElementById('email').value;
//                    window.alert(email);
                $.post("verificarEmail", {email}, function (data) {
                    if (data == 1) {
                        window.alert("Email ya existe");
                    }
                });
            }

            function switchf(id) {
                if (document.getElementById('switchBloque' + id).className === "switchBloque switchVerde") {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchBlanco";
                    document.getElementById('switch' + id).className = "switch switchIzquierda";
                    document.getElementById('input' + id).value = "0";
                    
                } else {
                    document.getElementById('switchBloque' + id).className = "switchBloque switchVerde";
                    document.getElementById('switch' + id).className = "switch switchDerecha";
                    document.getElementById('input' + id).value = "1";
                }
            }
        </script>
        <script>
            function listado(id) {

                document.getElementById('informacion').style.display = "none";
                document.getElementById('informacion1').className = "listadoOpcion";
                document.getElementById('permisos').style.display = "none";
                document.getElementById('permisos1').className = "listadoOpcion";
                document.getElementById(id).style.display = "block";
                document.getElementById(id + "1").className = "listadoOpcion selectL";

            }
        </script>
    </body>
</html>
