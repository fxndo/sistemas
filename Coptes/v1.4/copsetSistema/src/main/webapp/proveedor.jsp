
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.EditorialJpaController"%>
<%@page import="Modelo.Proveedoreditorial"%>
<%@page import="Modelo.Editorial"%>
<%@page import="Modelo.Proveedor"%>
<%@page import="ModeloJSP.ProveedorJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%-- 
    Document   : proveedor
    Created on : 28/04/2018, 03:09:44 PM
    Author     : fernando
--%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    ProveedorJpaController pr = new ProveedorJpaController(emf);
    EditorialJpaController ed = new EditorialJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    List<Editorial> editoriales = ed.findEditorialEntities();
    Proveedor proveedor = pr.findProveedor(Integer.parseInt(request.getParameter("id")));

    int notificacion = 0;
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Proveedor | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Proveedor<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="proveedores.jsp"><div class="navBloque">Proveedores</div></a><div class="navegacionSelect">Proveedor</div></div>

            </header>
            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <div id="editoriales1" onclick="listado('editoriales');" class="listadoOpcion"><img src="images/colorEditorial.png" class="btnIcono2">Editoriales</div>
                            <div id="opciones1" onclick="listado('opciones');" class="listadoOpcion"><img src="images/colorOpciones.png" class="btnIcono2">Opciones</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <!--/////////////////////////////////////////////////////// Informacion //////////////////////////////////////////////////////////////////7-->
                    <%if (request.getParameter("list") != null) {%>
                    <div class="listPedido" id="informacion" style="display: none;">
                        <%} else {%>
                        <div class="listPedido" id="informacion">
                            <%}%>
                            <!--<br>-->
                            <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                            <p class="parrafoListado">
                                La información del <strong>Proveedor</strong> se muestra a continuación..
                            </p>
                            <!--<br>-->
                            <div class="separadorListado"></div>
                            <br>
                            <div style="margin: 0px 180px;background: white;padding: 10px;">
                                <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                    <tr>
                                        <td class="ladoIzquierdo">Clave</td>
                                        <td class="ladoDerecho"><%=proveedor.getClave()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Nombre</td>
                                        <td class="ladoDerecho"><%=proveedor.getNombre()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Email</td>
                                        <td class="ladoDerecho"><%=proveedor.getEmail()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Telefono</td>
                                        <td class="ladoDerecho"><%=proveedor.getTelefono()%></td>
                                    </tr>
                                </table>
                                    <h5 class="tituloListado2" style="margin-left: 25px;">Dirección</h5>
                                <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                    <tr>
                                        <td class="ladoIzquierdo">Calle</td>
                                        <td class="ladoDerecho"><%=proveedor.getDireccionId().getCalle()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Colonia</td>
                                        <td class="ladoDerecho"><%=proveedor.getDireccionId().getColonia()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Delegación</td>
                                        <td class="ladoDerecho"><%=proveedor.getDireccionId().getDelegacion()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Codigo Postal</td>
                                        <td class="ladoDerecho"><%=proveedor.getDireccionId().getCp()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Ciudad</td>
                                        <td class="ladoDerecho"><%=proveedor.getDireccionId().getCiudad()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Estado</td>
                                        <td class="ladoDerecho"><%=proveedor.getDireccionId().getEstado()%></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--////////////////////////////////////////////////////////////////////////    Editoriales //////////////////////////////////////////////////////////////////-->
                        <%if (request.getParameter("list") != null && request.getParameter("list").equals("editoriales")) {%>
                        <div class="listPedido" id="editoriales">
                            <%} else {%>
                            <div class="listPedido" id="editoriales" style="display: none;">
                                <%}%>
                                <!--<br>-->
                                <h4 class="tituloListado"><img src="images/colorEditorial.png" class="iconoTitulo">Editoriales</h4>
                                <p class="parrafoListado">
                                    Lista de las editoriales que maneja el proveedor.<br>
                                </p>
                                <!--<br>-->
                                <div class="separadorListado"></div>
                                <br>
                                <h4 style="margin-left: 213px;" class="tituloListado2">Lista Editoriales</h4>
                                <div style="margin-left: 213px;">
                                    <%for (Proveedoreditorial pe : proveedor.getProveedoreditorialList()) {%>
                                    <div class="bloqueEditorial"><%=pe.getEditorialId().getNombre()%> </div> <a href="eliminarEditorial?id=<%=pe.getId()%>"><img title="Eliminar Editorial" src="images/less.png" class="btnAnimacion"></a><br>
                                        <%}%>
                                </div>
                                <br>
                                <div class="separadorListado2"></div>
                                <h4 style="margin-left: 213px;" class="tituloListado2">Agregar Editorial</h4>
                                <form action="agregarEditorial" method="post" name="formularioProveedor">
                                    <input type="hidden" name="idProveedor" value="<%=proveedor.getId()%>">
                                    <label class="entradaLabel">Nueva Editorial</label><input id="nuevaEditorial" autocomplete="off" onkeyup="verificar();" type="text" maxlength="30" name="editorial" class="entrada" placeholder="Crea una editorial que no este en la lista"><br>
                                    <label class="entradaLabel">Editorial existente</label><select id="editorialExistente" name="idEditorial" class="entradaSelect">
                                        <option value="0">Selecciona una editorial</option>
                                        <%for (Editorial ee : editoriales) {%>
                                        <option value="<%=ee.getId()%>"><%=ee.getNombre()%></option>
                                        <%}%>
                                    </select><br><br><br>
                                    <label class="entradaLabel"> </label><div onclick="verificarEditorial();" class="btn btn-sm btn-default"><img src="images/suma2.png" class="btnIcono">Agregar Editorial</div>
                                </form>
                                <p class="parrafoListado">
                                    <br><br><br>
                                    Nota: Asocia una <strong>Editorial</strong> existente al proveedor o crea una nueva si es que no existe.
                                </p>
                            </div>
                            <!--////////////////////////////////////////////////////////////////////////    Opciones    /////////////////////////////////////////////-->
                            <%if (request.getParameter("list") != null && request.getParameter("list").equals("opciones")) {%>
                            <div class="listPedido" id="opciones">
                                <%} else {%>
                                <div class="listPedido" id="opciones" style="display: none;">
                                    <%}%>
                                    <!--<br>-->
                                    <h4 class="tituloListado"><img src="images/colorOpciones.png" class="iconoTitulo">Opciones</h4>
                                    <p class="parrafoListado">
                                        Al eliminar al <strong>Proveedor</strong> no estara en la lista de proveedores.
                                    </p>
                                    <!--<br>-->
                                    <div class="separadorListado"></div>
                                    <p class="parrafoListado">
                                        Configura al <strong>Proveedor</strong> como opcional, para no este al inicio de la lista en el apartado "Nuevo Pedido".
                                    </p>
                                    <%if(proveedor.getPorcentaje().equals("0") || proveedor.getPorcentaje().equals("1")){%>
                                    <label class="apaB"></label><a href="proveedorOpcional?idProveedor=<%=proveedor.getId()%>&accion=2"><div class="btn btn-default bbtn btn-sm"><img src="images/opcional.png" class="btnIcono"> Proveedor Opcional</div></a>
                                    <%}else{%>
                                    <label class="apaB"></label><a href="proveedorOpcional?idProveedor=<%=proveedor.getId()%>&accion=1"><div class="btn btn-default bbtn btn-sm"><img src="images/opcional.png" class="btnIcono"> Quitar Proveedor Opcional</div></a>
                                    <%}%>
                                    <br><br>
                                    <div class="separadorListado2"></div>
                                    <p class="parrafoListado">
                                        Elimina al <strong>Proveedor</strong> para que no este en la lista de proveedores
                                    </p>
                                    
                                    <%if(proveedor.getProveedoreditorialList().size()==0){%>
                                    <label class="apaB"> </label><div onclick="flotante('eliminarProveedor');" class="btn btn-sm bbtn btn-default"><img src="images/less.png" class="btnIcono">Eliminar Proveedor</div>
                                    <%}else{%>
                                    <label class="apaB"> </label><div class="btn btn-sm bbtn btn-default disabled"><img src="images/less.png" class="btnIcono">Eliminar Proveedor</div><br><br>
                                    <span class="parrafoListado"><strong>Nota:</strong> Para poder eliminar al proveedor quita las editoriales relacionasas en el apartado <strong>Editoriales</strong></span>
                                    <%}%>
                                    
                                    <br><br>
                                    
                                    <div class="separadorListado2"></div>
                                    <p class="parrafoListado">
                                        Editar la información actual del <strong>Proveedor</strong>.
                                    </p>
                                    <label class="apaB"> </label><div onclick="flotante('editarProveedor');" class="btn btn-sm bbtn btn-default"><img src="images/editarColor.png" class="btnIcono">Editar Proveedor</div>
                                </div>
                                </main>
                                <!--//////////////////////////////////////////////////////////////  Flotante    //////////////////////////////////////////////////////////////////////7-->
                                <div id="eliminarProveedor" class="bloqueFlotante" style="display: none;">
                                    <div class="tituloFlotante">Eliminar Proveedor</div>
                                    <form action="eliminarProveedor" method="post">
                                        <div class="principalFlotante">
                                            <br>
                                            <h4 class="text-center">¿Seguro que quieres eliminar al Proveedor?</h4>
                                            <br><br>
                                            <input type="hidden" name="idProveedor" value="<%=proveedor.getId()%>">
                                        </div>
                                        <div class="botonesFlotante"><button type="submit" class="btn btn-danger btn-sm"><img src="images/borrarBlanco.png" class="btnIcono">Eliminar Proveedor</button><div onclick="flotante('eliminarProveedor')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="editarProveedor" class="bloqueFlotante" style="display: none;">
                            <div class="tituloFlotante">Editar Proveedor</div>
                            <form action="editarProveedor" method="post">
                                <div class="principalFlotante">
                                    <input type="hidden" name="idProveedor" value="<%=proveedor.getId()%>">

                                    <h4 style="margin-left: 213px;" class="tituloListado2">Información</h4>
                                    <label class="entradaLabel">Clave</label><input class="entrada" type="text" name="clave" maxlength="5" required="" value="<%=proveedor.getClave()%>"><br>
                                    <label class="entradaLabel">Telefono</label><input class="entrada" type="text" name="telefono" maxlength="12" minlength="10" required="" pattern="[0-9]+" value="<%=proveedor.getTelefono()%>"><br>
                                    <label class="entradaLabel">Nombre</label><input class="entrada" type="text" name="completo" maxlength="45" required="" value="<%=proveedor.getNombre()%>"><br>
                                    <label class="entradaLabel">Email</label><input class="entrada" type="text" name="email" maxlength="44" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"  value="<%=proveedor.getEmail()%>"><br>

                                    <br><br>
                                    <h4 style="margin-left: 213px;" class="tituloListado2">Dirección</h4>
                                    <label class="entradaLabel">Calle</label><input type="text" class="entrada" name="calle" maxlength="45" value="<%=proveedor.getDireccionId().getCalle()%>"><br>
                                    <label class="entradaLabel">Colonia</label><input type="text" class="entrada" name="colonia" maxlength="45" value="<%=proveedor.getDireccionId().getColonia()%>"><br>
                                    <label class="entradaLabel">Delegación</label><input type="text" class="entrada" name="delegacion" maxlength="45" value="<%=proveedor.getDireccionId().getDelegacion()%>"><br>
                                    <label class="entradaLabel">Codigo Postal</label><input type="text" class="entrada" name="cp" maxlength="45" value="<%=proveedor.getDireccionId().getCp()%>"><br>
                                    <label class="entradaLabel">Ciudad</label><input type="text" class="entrada" name="ciudad" maxlength="45" value="<%=proveedor.getDireccionId().getCiudad()%>"><br>
                                    <label class="entradaLabel">Estado</label><input type="text" class="entrada" name="estado" maxlength="45" value="<%=proveedor.getDireccionId().getEstado()%>"><br>

                                    <!--<button style="margin-left: 210px;" type="submit" class="btn btn-sm btn-default btn-sm"><img src="images/editarColor.png" class="btnIcono">Editar Proveedor</button>-->
                                </div>
                                <div class="botonesFlotante"><button type="submit" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Editar</button><div onclick="flotante('editarProveedor')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                            </form>
                        </div>
                        <!--/////////////////////////////////////////////////////////////////// Notificaciones  /////////////////////////////////////////-->
                        <div class="notificacionBloque" style="<%if (notificacion == 5) {%>display:block;<%} else {%>display:none;<%}%>">
                            <div class="notificacionImg">
                                <img src="images/notiSuccess.png" class="noti">
                            </div>
                            <div class="notificacionInfo">
                                <div class="titulo">Editorial Agregada</div>
                                La <strong>editorial</strong> se agrego satisfactoriamente.
                            </div>
                        </div>
                        <%@ include file="menu.jsp" %>
                        <script>
                            function verificarEditorial() {
                                var nuevaEditorial = document.getElementById('nuevaEditorial').value;
                                var editorialExistente = document.getElementById('editorialExistente').value;
                                
                                    if (nuevaEditorial.length <1) {
                                    if (editorialExistente == 0) {
                                        window.alert("LLena el campo editorial");
                                    } else {
                                        document.formularioProveedor.submit();
//                                        window.alert("bien");
                                    }
                                } else {
                                    document.formularioProveedor.submit();
//                                        window.alert("bien");
                                }
                            }

                            function listado(id) {

                                document.getElementById('informacion').style.display = "none";
                                document.getElementById('informacion1').className = "listadoOpcion";
                                document.getElementById('editoriales').style.display = "none";
                                document.getElementById('editoriales1').className = "listadoOpcion";
                                document.getElementById('opciones').style.display = "none";
                                document.getElementById('opciones1').className = "listadoOpcion";
                                document.getElementById(id).style.display = "block";
                                document.getElementById(id + "1").className = "selectL";

                            }
                            function verificar(){
                                var nuevaEditorial = document.getElementById('nuevaEditorial').value;
                                if(nuevaEditorial.length>0){
                                    document.getElementById('editorialExistente').disabled="true";
                                }else{
                                    document.getElementById('editorialExistente').disabled="";
                                }
                            }
                        </script>
                        </body>
                        </html>
