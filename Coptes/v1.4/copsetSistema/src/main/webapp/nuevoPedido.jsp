<%-- 
    Document   : nuevoPedido
    Created on : 2/05/2018, 02:11:40 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.UsuarioJpaController"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Editorial"%>
<%@page import="ModeloJSP.EditorialJpaController"%>
<%@page import="Modelo.Proveedor"%>
<%@page import="ModeloJSP.ProveedorJpaController"%>
<%@page import="Modelo.Nivel"%>
<%@page import="ModeloJSP.NivelJpaController"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="Modelo.Cliente"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.ClienteJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    HttpSession listaPedido = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    UsuarioJpaController us = new UsuarioJpaController(emf);
    informacion = us.findUsuario(informacion.getId());
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");

    ClienteJpaController c = new ClienteJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);
    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    EditorialJpaController edj = new EditorialJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);

    List<Pedidoparte> partes = new ArrayList<Pedidoparte>();
    List<Editorial> editoriales = edj.listaEditoriales();
    int pedidoLista = 0;
    if (listaPedido.getAttribute("partes") != null) {
        partes = (List<Pedidoparte>) listaPedido.getAttribute("partes");
        pedidoLista = 1;
        listaPedido.removeAttribute("partes");
    }

    List<Cliente> clientes = c.clienteListaCompleta();
    List<Sucursal> sucursales = scj.findSucursalEntities();

    int notificacion = 0;
    if (request.getParameter("ma") != null) {
        notificacion = Integer.parseInt(request.getParameter("ma"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Nuevo Pedido | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Nuevo Pedido<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="pedidos.jsp"><div class="navBloque">Pedidos</div></a><div class="navegacionSelect">Nuevo Pedido</div></div>
            </header>
            <form action="agregarPedido" method="post" name="pedido1">

                <!--////////////////////////////////////////////////////////////////////-->
                <div style="padding: 10px 100px; margin-top: 50px;">

                    <%if (sucursal.getId() == 1) {%>
                    <label class="entradaLabel">Sucursal</label><select id="sucursal" class="entradaSelect" name="sucursal">
                        <%for (Sucursal sc : sucursales) {%>
                        <%if (sc.getId() != 1) {%>
                        <%if (sc.getId() == sucursal.getId()) {%>
                        <option selected="" value="<%=sc.getId()%>"><%=sc.getNombre()%></option>
                        <%} else {%>
                        <option value="<%=sc.getId()%>"><%=sc.getNombre()%></option>
                        <%}%>
                        <%}%>
                        <%}%>
                    </select><br>
                    <%} else {%>
                    <input id="sucursal" type="hidden" name="sucursal" value="<%=sucursal.getId()%>">
                    <%}%>
                    <br>
                    <label class="entradaLabel">Cliente</label><select id="cliente" name="cliente" class="entradaSelect">
                        <option class="noOpcion" disabled="" selected="" value="0">---</option>
                        <option disabled="" class="noOpcion" value="0">Favoritos</option>
                        <%for (Cliente clf : informacion.getClienteList()) {%>
                        <option class="siOpcion" value="<%=clf.getId()%>"><%=clf.getEscuela()%> - <%=clf.getNombre()%></option>
                        <%}%>
                        <option disabled="" class="noOpcion" value="0">Todos los clientes</option>
                        <%for (Cliente cl : clientes) {%>
                        <option class="siOpcion" value="<%=cl.getId()%>"><%=cl.getEscuela()%> - <%=cl.getNombre()%></option>
                        <%}%>
                    </select><br>

                    <label class="entradaLabel">Fecha Entrega</label><input id="fechaEntrega" class="entrada" type="date" required=""  name="entrega" step="1">

                    <div style="text-align: right; padding-right: 25px;">
                        <div style="margin-right: 13px;" onclick="nuevo();" class="btn btn-default btn-sm"><img src="images/suma2.png" class="btnIcono">Agregar Cliente</div>
                    </div>
                    <br>
                    <div class="separadorListado"></div>
                    <div id="apartadoNuevo">

                    </div>
                </div>

                <!---------------------------------------------------------------------Productos------------------------------------------------------------------------->
                <div style="padding: 10px 25px;">
                    <!--<div style="text-align: left;">-->
                    <div onclick="flotante('agregarProducto');" style ="float: left;" class="btn btn-sm btn-default"><img src="images/buscarColor.png" class="btnIcono">Buscar Producto</div>
                    <div onclick="flotante('pedidoLista')" style="margin-left: 15px;" class="btn btn-sm btn-default"><img src="images/subir.png" class="btnIcono">Agregar Pedido desde archivo</div>
                    <!--</div>-->
                    <label style="float: right;" onclick="flotante('ayuda1');"><img src="images/ayuda.png" class="btnAyuda"><strong>Ayuda</strong></label><br><br>
                    <table id="tablaProductos" class="table  table-responsive tabla-producto">
                        <tr class="cabeceraTabla">
                            <th style="width: 50px;">#</th>
                            <th style="width: 120px;">ISBN/Serial</th>
                            <th>Titulo</th>
                            <th>Serie</th>
                            <th style="width: 120px;">Editorial</th>
                            <th style="width: 70px;">Precio</th>
                            <th style="width: 50px;">%</th>
                            <th style="width: 50px;">Cant</th>
                            <th style="width: 50px;">Elm</th>
                        </tr>

                        <%if (pedidoLista == 0) {%>
                        <tr class="renglon1">
                            <td class="numeroTabla">1</td>
                            <td><input id="isbn1" onblur="rellenar('1');" autocomplete="off" class="entradaTablad" onkeypress="return validarEntradaNumero(event);" type="text" name="isbn1" maxlength="13" pattern="[0-9]+"></td>
                            <td><input id="titulo1" class="entradaTabla" autocomplete="off" type="text" name="titulo1" maxlength="250"></td>
                            <td><input id="serie1" type="text" class="entradaTabla" name="serie1" maxlength="44"></td>
                            <td><select id="editorial1" class="entradaTabla" name="editorial1">
                                    <option value="0">---</option>
                                    <%for (Editorial ed : editoriales) {%>
                                    <option value="<%=ed.getId()%>"><%=ed.getNombre()%></option>
                                    <%}%>
                                </select></td>
                            <td><input type="number" class="entradaTablaDinero" id="precio1" name="precio1"></td>
                            <td><input id="descuento1" type="number" name="descuento1" value="0" class="entradaTablac" max="25" onkeypress="return validarEntradaNumero(event);"></td>
                            <td><input autocomplete="off" onkeypress="return validarEntradaNumero(event);" onkeyup="saltar(event, 'cantidad2');" maxlength="3" id="cantidad1" class="entradaTablac" type="text" name="cantidad1" pattern="[0-9]+"></td>
                            <td style="padding-top: 6px;" id="borrarRenglon1"><div class="btnEliminar" onclick="borrarRenglon('1')"><img src="images/less.png" class="iconoNormal"></div></td>
                        </tr>
                        <!--////////////////////////////////////////////////////////////////////////////-->

                        <tr class="renglon0">
                            <td class="numeroTabla">2</td>
                            <td><input  autocomplete="off" id="isbn2" onblur="rellenar('2');" onkeypress="return validarEntradaNumero(event);" class="entradaTablad" type="text" name="isbn2" maxlength="13" pattern="[0-9]+"></td>
                            <td><input autocomplete="off" id="titulo2" class="entradaTabla" type="text" name="titulo2" maxlength="250"></td>
                            <td><input id="serie2" type="text" class="entradaTabla" name="serie2" maxlength="44"></td>
                            <td><select id="editorial2" class="entradaTabla" name="editorial2">
                                    <option value="0">---</option>
                                    <%for (Editorial ed : editoriales) {%>
                                    <option value="<%=ed.getId()%>"><%=ed.getNombre()%></option>
                                    <%}%>
                                </select></td>
                            <td><input type="number" class="entradaTablaDinero" id="precio2" name="precio2"></td>
                            <td><input id="descuento2" type="number" name="descuento2" value="0" class="entradaTablac" max="25" onkeypress="return validarEntradaNumero(event);"></td>
                            <td><input autocomplete="off" onkeypress="return validarEntradaNumero(event);" onkeyup="saltar(event, 'cantidad3');"  maxlength="3" id="cantidad2" class="entradaTablac" type="text" name="cantidad2" pattern="[0-9]+"></td>
                            <td style="padding-top: 6px;" id="borrarRenglon2" ><div class="btnEliminar" onclick="borrarRenglon('2')"><img src="images/less.png" class="iconoNormal"></div></td>
                        </tr>
                        <!--////////////////////////////////////////////////////////////////////////////-->

                        <tr class="renglon1">
                            <td class="numeroTabla">3</td>
                            <td><input autocomplete="off" id="isbn3" onblur="rellenar('3');" onkeypress="return validarEntradaNumero(event);" class="entradaTablad" type="text" name="isbn3" maxlength="13" pattern="[0-9]+"></td>
                            <td><input autocomplete="off" id="titulo3" class="entradaTabla" type="text" name="titulo3" maxlength="250"></td>
                            <td><input id="serie3" type="text" class="entradaTabla" name="serie3" maxlength="44"></td>
                            <td><select id="editorial3" class="entradaTabla" name="editorial3">
                                    <option value="0">---</option>
                                    <%for (Editorial ed : editoriales) {%>
                                    <option value="<%=ed.getId()%>"><%=ed.getNombre()%></option>
                                    <%}%>
                                </select></td>
                            <td><input type="number" class="entradaTablaDinero" id="precio3" name="precio3"></td>
                            <td><input id="descuento3" type="number" name="descuento3" value="0" class="entradaTablac" max="25" onkeypress="return validarEntradaNumero(event);"></td>
                            <td><input autocomplete='off' onkeypress="return validarEntradaNumero(event);" onkeyup="saltar(event, 'cantidad4');" maxlength="3" id="cantidad3" class="entradaTablac" type="text" name="cantidad3" pattern="[0-9]+"></td>
                            <td style="padding-top: 6px;" id="borrarRenglon3"><div class="btnEliminar" onclick="borrarRenglon('3')"><img src="images/less.png" class="iconoNormal"></div></td>
                        </tr>
                        <!--////////////////////////////////////////////////////////////////////////////-->

                        <tr class="renglon0">
                            <td class="numeroTabla">4</td>
                            <td><input  autocomplete="off" id="isbn4" onblur="rellenar('4');" onkeypress="return validarEntradaNumero(event);" class="entradaTablad" type="text" name="isbn4" maxlength="13" pattern="[0-9]+"></td>
                            <td><input  autocomplete="off" id="titulo4" class="entradaTabla" type="text" name="titulo4" maxlength="250"></td>
                            <td><input id="serie4" type="text" class="entradaTabla" name="serie4" maxlength="44"></td>
                            <td><select id="editorial4" class="entradaTabla" name="editorial4">
                                    <option value="0">---</option>
                                    <%for (Editorial ed : editoriales) {%>
                                    <option value="<%=ed.getId()%>"><%=ed.getNombre()%></option>
                                    <%}%>
                                </select></td>
                            <td><input type="number" class="entradaTablaDinero" id="precio4" name="precio4"></td>
                            <td><input id="descuento4" type="number" name="descuento4" value="0" class="entradaTablac" max="25" onkeypress="return validarEntradaNumero(event);"></td>
                            <td><input autocomplete="off" id="cantidad4" onkeypress="return validarEntradaNumero(event);" maxlength="3" onkeyup="saltar(event, 'cantidad5');" class="entradaTablac" type="text" name="cantidad4" pattern="[0-9]+"></td>
                            <td style="padding-top: 6px;" id="borrarRenglon4"><div class="btnEliminar" onclick="borrarRenglon('4')"><img src="images/less.png" class="iconoNormal"></div></td>
                        </tr>
                        <!--////////////////////////////////////////////////////////////////////////////-->

                        <tr class="renglon1">
                            <td class="numeroTabla">5</td>
                            <td><input autocomplete="off" id="isbn5" onblur="rellenar('5');" onkeypress="return validarEntradaNumero(event);" class="entradaTablad" type="text" name="isbn5" maxlength="13" pattern="[0-9]+"></td>
                            <td><input autocomplete="off" id="titulo5" class="entradaTabla" type="text" name="titulo5" maxlength="250"></td>
                            <td><input id="serie5" type="text" class="entradaTabla" name="serie5" maxlength="44"></td>
                            <td><select id="editorial5" class="entradaTabla" name="editorial5">
                                    <option value="0">---</option>
                                    <%for (Editorial ed : editoriales) {%>
                                    <option value="<%=ed.getId()%>"><%=ed.getNombre()%></option>
                                    <%}%>
                                </select></td>
                            <td><input type="number" class="entradaTablaDinero" id="precio5" name="precio5"></td>
                            <td><input id="descuento5" type="number" name="descuento5" value="0" class="entradaTablac" max="25" onkeypress="return validarEntradaNumero(event);"></td>
                            <td><input  autocomplete="off" onkeypress="return validarEntradaNumero(event);" id="cantidad5" maxlength="3" onkeyup="saltar(event, 'cantidad6');" class="entradaTablac" type="text" name="cantidad5" pattern="[0-9]+"></td>
                            <td style="padding-top: 6px;" id="borrarRenglon5"><div class="btnEliminar" onclick="borrarRenglon('5')"><img src="images/less.png" class="iconoNormal"></div></td>
                        </tr>
                        <!--////////////////////////////////////////////////////////////////////////////-->
                        <%} else {%>
                        <%int superValor = 1;%>
                        <%for (Pedidoparte ppa : partes) {%>
                        <tr class="renglon<%=superValor % 2%>">
                            <td class="numeroTabla"><%=superValor%></td>
                            <td><input id="isbn<%=superValor%>" onblur="rellenar('<%=superValor%>');" value="<%=ppa.getProductoId().getIsbn()%>" autocomplete="off" class="entradaTablad" onkeypress="return validarEntradaNumero(event);" type="text" name="isbn<%=superValor%>" maxlength="13" pattern="[0-9]+"></td>
                            <td><input id="titulo<%=superValor%>" class="entradaTabla" autocomplete="off" value="<%=ppa.getProductoId().getTitulo()%>" type="text" name="titulo<%=superValor%>" maxlength="250"></td>
                            <td><input id="serie<%=superValor%>" type="text" class="entradaTabla" name="serie<%=superValor%>" value="<%=ppa.getProductoId().getSerie()%>" maxlength="44"></td>
                            <td><select id="editorial<%=superValor%>" type="text" class="entradaTabla" name="editorial<%=superValor%>">
                                    <%if (ppa.getProductoId().getEditorialId1() == null) {%>
                                    <option class="noOpcion" value="0">-----</option>
                                    <%for (Editorial edf : editoriales) {%>
                                    <option value="<%=edf.getId()%>"><%=edf.getNombre()%></option>
                                    <%}%>
                                    <%} else {%>

                                    <%for (Editorial edf : editoriales) {%>
                                    <%if (edf.getId() == ppa.getProductoId().getEditorialId1().getId()) {%>
                                    <option selected="" value="<%=edf.getId()%>"><%=edf.getNombre()%></option>
                                    <%} else {%>
                                    <option value="<%=edf.getId()%>"><%=edf.getNombre()%></option>
                                    <%}%>
                                    <%}%>

                                    <%}%>
                                </select></td>
                            <td><input type="number" class="entradaTablaDinero" id="precio<%=superValor%>" name="precio<%=superValor%>" value="<%=ppa.getProductoId().getPrecioventa()%>"></td>
                            <td><input id="descuento<%=superValor%>" type="number" name="descuento<%=superValor%>" value="<%=ppa.getDescuento()%>" class="entradaTablac" max="25"></td>
                            <td><input autocomplete="off" onkeypress="return validarEntradaNumero(event);" onkeyup="saltar(event, 'cantidad<%=superValor + 1%>');" maxlength="3" id="cantidad<%=superValor%>" value="<%=ppa.getCantidad()%>" class="entradaTablac" type="text" name="cantidad<%=superValor%>" pattern="[0-9]+"></td>
                            <td style="padding-top: 6px;"><div class="btnEliminar" onclick="borrarRenglon('<%=superValor%>')"><img src="images/less.png" class="iconoNormal"></div></td>
                        </tr>
                        <%superValor++;%>
                        <%}%>
                        <%}%>

                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>
                        <tr id="solo" class="renglon0"></tr>
                        <tr id="solo" class="renglon1"></tr>


                    </table>
                    <div style="text-align: right;">
                        <div id="btnAgregar"><div id="btnAdd" class="btn btn-default btn-sm" onclick="agregar('6', '5')"><img src="images/suma2.png" class="btnIcono">Agregar Filas</div></div>
                    </div>

                </div>

                <div class="apartadoBloque ">
                    <!--<div class="apartadoTitulo">Terminar Pedido</div>-->
                    <div class=" apartadoPrincipal text-center">
                        <!--<input class="btn btn-success" type="submit" value="Enviar Pedido">-->
                        <div onclick="mandarFormulario();" class="bbtn btn btn-success btn-sm"><img src="images/enviar.png" class="btnIcono">Enviar Pedido</div>
                    </div>
                </div>
        </main>
    </form>

</main>
<div id="ayuda1" class="bloqueFlotante" style="display: none;">
    <div class="tituloFlotante">Ayuda - Nuevo Pedido</div>
    <div class="principalFlotante">
        <ul>
            <li>Si el cliente no esta en la lista de clientes, puedes agregar un nuevo cliente dando clic en <strong>Agregar Cliente</strong>.</li>
            <li>Llena todos los campos de la fila para agregar un artículo al pedido.</li>
            <li>Al llenar el <strong>ISBN</strong> (+ TAB) de un artículo (Si se encuentra en el sistema) se autocompletarán los demás campos excepto Proveedor y cantidad.</li>
            <li>El campo <strong>Cantidad</strong> se pintará de color rojo o verde dependiendo si se encuentra el artículo en tienda.</li>
            <li>Puedes agregar productos dando clic en <strong>Buscar Producto</strong>.</li>
            <li>Puedes agregar una lista de productos dandio clic en <strong>Agregar Pedido Desde Archivo</strong>.</li>
            <li>Una vez agregando todos los productos del pedido y su informacion da clic en <strong>Enviar Pedido</strong> </li>
        </ul>
    </div>
    <div class="botonesFlotante"><div onclick="flotante('ayuda1')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div>
    </div>
</div>

<!--////////////////////////////////////////////////    FLOTANTE    /////////////////////////////////////////////-->
<div id="agregarProducto" class="bloqueFlotante" style="display: none;">
    <div class="tituloFlotante">Buscar</div>
    <input id="insertar" type="hidden" name="btnValor" value="6">
    <div class="principalFlotante">
        <div style="display: inline-block;"><input style="width: 400px;" onkeyup="ejecutarBusqueda(event);" id="busquedaTPV" type="text" autocomplete="off" placeholder="Buscar Producto minimimo 4 caracteres" name="objeto" class="form-control"></div> 
        <button class="btnBuscar" onclick="buscarProducto('0');"><img src="images/buscar.png" class="iconoNormal"></button>
        <div style="text-align: center;" id="espera"></div>
        <div>
            <table id="listaProductos" class="table table-responsive tabla-producto letraChica">
            </table>
        </div>
    </div>
    <div class="botonesFlotante"><div onclick="flotante('agregarProducto'); limpiarBuscar();" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div></div>
</div>
<div id="pedidoLista" class="bloqueFlotante" style="display: none;">
    <form action="agregarPedidoLista" method="post" enctype="multipart/form-data" onsubmit="envMostrar();">
        <div class="tituloFlotante">Agregar Pedido desde Archivo</div>
        <div class="principalFlotante">
            <h5 class="tituloListado">Intrucciones</h5>
            <p class="parrafoFlotante">
                Selecciona el archivo con el contenido del pedido que quieres agregar, el archivo debe tener las siguientes caracteristicas:<br>
                1- El archivo debe tener extensión xlsx.<br>
                2- El pedido debe estar en la primera hoja del archivo.<br>
                3- El pedido debe estar conformado por 4 columnas empezando en la fila 1 y columna 1.<br>
                4- No debe tener espacios u otra cosa que no sea parte del pedido entre productos.<br>
                5- Puede llevar cabecera con los nombres de las columnas.<br>
                <a target="_blank" href="images/pedidoEjemplo.png">Imagen Ejemplo</a>

            </p>
            <label class="entradaLabel">Archivo Excel</label><div style="display: inline-block;"><input type="file" name="archivo"></div>
        </div>
        <div class="botonesFlotante">
            <button class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Aceptar</button>
            <div onclick="flotante('pedidoLista');" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div>
        </div>
    </form>
</div>
<!--////////////////////////////////////////////////    NOTOFICACIONES    /////////////////////////////////////////////-->

<div id="productoAgregado" class="notificacionBloque" style="display:none">
    <div class="notificacionImg">
        <img src="images/notiSuccess.png" class="noti">
    </div>
    <div class="notificacionInfo">
        <div class="titulo">Producto Agregado</div>
        El <strong>producto</strong> fue agregado al pedido.
    </div>
</div>
<div id="productoNoAgregado" class="notificacionBloque" style="display:none;">
    <div class="notificacionImg">
        <img src="images/cancel.png" class="noti">
    </div>
    <div class="notificacionInfo">
        <div class="titulo">Producto no Agregado</div>
        El <strong>producto</strong> ya esta en el pedido.
    </div>
</div>

<!--////////////////////////////////////////////////    MENU    /////////////////////////////////////////////-->
<div id="esperaCirculo" class="espera" style="display: none;"><img class="imgLoading" src="images/loading2.gif"></div>
    <%@ include file="menu.jsp" %>

<!--///////////////////////////////////////////    NOTIFICACIONES    /////////////////////////////////////////////-->
<div class="notificacionBloque" style="<%if (notificacion == 101) {%>display:block;<%} else {%>display:none;<%}%>">
    <div class="notificacionImg">
        <img src="images/cancel.png" class="noti">
    </div>
    <div class="notificacionInfo">
        <div class="titulo">Archivo erroneo</div>
        El <strong>Archivo</strong> no tiene la extención necesaria.
    </div>
</div>

<div class="notificacionBloque" style="<%if (notificacion == 102) {%>display:block;<%} else {%>display:none;<%}%>">
    <div class="notificacionImg">
        <img src="images/cancel.png" class="noti">
    </div>
    <div class="notificacionInfo">
        <div class="titulo">ISBN erroneo</div>
        El <strong>ISBN</strong> (<%=request.getParameter("campo")%>) no es correcto.
    </div>
</div>

<div class="notificacionBloque" style="<%if (notificacion == 103) {%>display:block;<%} else {%>display:none;<%}%>">
    <div class="notificacionImg">
        <img src="images/cancel.png" class="noti">
    </div>
    <div class="notificacionInfo">
        <div class="titulo">Titulo erroneo</div>
        El <strong>Titulo</strong> (<%=request.getParameter("campo")%>) no es correcto en tamaño.
    </div>
</div>

<div class="notificacionBloque" style="<%if (notificacion == 104) {%>display:block;<%} else {%>display:none;<%}%>">
    <div class="notificacionImg">
        <img src="images/cancel.png" class="noti">
    </div>
    <div class="notificacionInfo">
        <div class="titulo">Precio erroneo</div>
        El <strong>Precio</strong> (<%=request.getParameter("campo")%>) no es correcto.
    </div>
</div>

<div class="notificacionBloque" style="<%if (notificacion == 105) {%>display:block;<%} else {%>display:none;<%}%>">
    <div class="notificacionImg">
        <img src="images/cancel.png" class="noti">
    </div>
    <div class="notificacionInfo">
        <div class="titulo">Cantidad erronea</div>
        La <strong>Cantidad</strong> (<%=request.getParameter("campo")%>) no es correcta.
    </div>
</div>
<!------------------------------------------------------Scripts--------------------------------------------------------------->
<script>
    function agregar(numero, valor) {
        for (var n = 1; n <= valor; n++) {
            document.getElementById('solo').id = "vacio";
            document.getElementById('vacio').innerHTML += "<tr class='renglon" + numero % 2 + "'>\n" +
                    "                                <td class=\"numeroTabla\">" + numero + "</td><td><input autocomplete='off' id=\"isbn" + numero + "\" onblur=\"rellenar('" + numero + "');\" onkeypress=\"return validarEntradaNumero(event);\" class=\"entradaTablad\" type=\"text\" name=\"isbn" + numero + "\" maxlength=\"13\" pattern=\"[0-9]+\"></td>\n" +
                    "                                <td><input id='titulo" + numero + "' type='text' class='entradaTabla' name='titulo" + numero + "' maxlength='250'></td>" +
                    "                                <td><input autocomplete='off' id=\"serie" + numero + "\" class=\"entradaTabla\" type=\"text\" name=\"serie" + numero + "\" maxlength=\"44\"></td>\n" +
                    "<td><select id='editorial" + numero + "' class='entradaTabla' name='editorial" + numero + "'>" +
                    "                <option value='0'>---</option>" +
    <%for (Editorial ed : editoriales) {%>
            "            <option value='<%=ed.getId()%>'><%=ed.getNombre()%></option>" +
    <%}%>
            "        </select></td><td><input type='number' class='entradaTablaDinero' name='precio" + numero + "' id='precio" + numero + "'></td><td><input id='descuento" + numero + "' type='number' name='descuento" + numero + "' value='0' class='entradaTablac' max='25'></td>" +
                    "<td><input autocomplete='off' onkeypress=\"return validarEntradaNumero(event);\"  maxlength=\"3\" onkeyup=\"saltar(event,'cantidad" + (parseInt(numero) + 1) + "');\" id=\"cantidad" + numero + "\" class=\"entradaTablac\" type=\"text\" name=\"cantidad" + numero + "\" pattern=\"[0-9]+\"></td>\n" +
                    "<td style='padding-top: 6px;' id='borrarRenglon" + numero + "'><div class='btnEliminar' onclick='borrarRenglon(" + numero + ")'><img src='images/less.png' class='iconoNormal'></div></td></tr>";
            numero++;
            document.getElementById('vacio').id = "";
            document.getElementById('btnAgregar').innerHTML = "<div class=\"btn bbtn btn-sm btn-default\" onclick=\"agregar('" + numero + "','5')\"><img src=\"images/mas.png\" class=\"btnIcono\">Agregar Filas</div>";
            document.getElementById('insertar').value = numero;

        }
    }

    function nuevo() {
        document.getElementById('apartadoNuevo').innerHTML = "<hr><h5 class=\"tituloListado2\" style=\"margin-left: 210px;\">Nuevo Cliente</h5>\n" +
                "                            <label class='entradaLabel'>Nombre</label><input id='nombreC' class=\"entrada\" type=\"text\" name=\"nombreC\" maxlength=\"299\" required=''>\n" +
                "                            <label class='entradaLabel'>Escuela</label><input id='escuelaC' class=\"entrada\" type=\"text\" name=\"escuelaC\" maxlength=\"99\" required=\"\">\n" +
                "                            <label class='entradaLabel'>Celular</label><input id='celularC' class=\"entrada\" type=\"text\" name=\"celularC\" maxlength=\"10\" required=\"\">\n" +
                "                            <label class='entradaLabel'>Email</label><input id='emailC' class=\"entrada\" type=\"text\" name=\"emailC\" maxlength=\"44\" required=\"\" pattern='[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'>\n" +
                "                            <label class='entradaLabel'>Telefono</label><input id='telefonoC' class=\"entrada\" type=\"text\" name=\"telefonoC\" maxlength=\"10\" required=\"\">\n" +
                "                        <hr>\n" +
                "                        <h5 class=\"tituloListado2\" style=\"margin-left: 210px;\">Dirección Nuevo Cliente</h5>\n" +
                "                        <label class='entradaLabel'>Calle</label><input id='calleC' class=\"entrada\" type=\"text\" name=\"calleC\" maxlength=\"45\">\n" +
                "                            <label class='entradaLabel'>Colonia</label><input id='coloniaC' class=\"entrada\" type=\"text\" name=\"coloniaC\" maxlength=\"45\">\n" +
                "                        \n" +
                "                            <label class='entradaLabel'>Delegación</label><input id='delegacionC' class=\"entrada\" type=\"text\" name=\"delegacionC\" maxlength=\"45\">\n" +
                "                            <label class='entradaLabel'>Codigo Postal</label><input id='cpC' class=\"entrada\" type=\"text\" name=\"cpC\" maxlength=\"6\">\n" +
                "                            <label class='entradaLabel'>Ciudad</label><input id='ciudadC' class=\"entrada\" type=\"text\" name=\"ciudadC\" maxlength=\"45\">\n" +
                "                            <label class='entradaLabel'>Estado</label><input id='estadoC' class=\"entrada\" type=\"text\" name=\"estadoC\" maxlength=\"45\">\n" +
                "                            <br><br><div style='text-align: right;'><div onclick='quitar();' class='btn bbtn btn-sm btn-default'><img src='images/cancelar.png' class='btnIcono'>Cancelar</div></div><br><div class='separadorListado'></div>";
        document.getElementById('cliente').value = 0;
        document.getElementById('cliente').disabled = "true";
    }

    function rellenar(id) {
        var serial = document.getElementById('isbn' + id).value;
        var idSucursal = document.getElementById('sucursal').value;
        if (serial.length > 0) {
            $.post("buscarProductoPedido", {serial, idSucursal}, function (data) {
                var data2 = data.replace("xn", "ñ");
                if (data2.length > 2) {
                    var datos = data2.split("-");
                    document.getElementById('titulo' + id).value = datos[0];
                    document.getElementById('serie' + id).value = datos[2];
                }
            });
        }
    }

    function quitar() {
        document.getElementById('cliente').disabled = "";
        document.getElementById('apartadoNuevo').innerHTML = "";
    }
    function envMostrar() {
        flotante('pedidoLista');
        document.getElementById('esperaCirculo').style.display = "block";
        document.getElementById('body').className = "desenfoque";
    }

    function mandarFormulario() {
        var valor = 0;

        if (document.getElementById('cliente').value == 0 && document.getElementById('nombreC') == null) {
            valor = 1;
            window.alert("Selecciona un cliente");
        } else {
            if (document.getElementById('fechaEntrega').value == "") {
                valor = 1;
                window.alert("Ingresa la fecha de Entrega");
            } else {
                if (document.getElementById('nombreC') != null && document.getElementById('nombreC').value.length == 0) {
                    valor = 1;
                    window.alert("Campo Nombre vacio");
                } else {
                    if (document.getElementById('escuelaC') != null && document.getElementById('escuelaC').value.length == 0) {
                        valor = 1;
                        window.alert("Campo Escuela vacio");
                    } else {
                        if (document.getElementById('telefonoC') != null && document.getElementById('telefonoC').value.length == 0) {
                            valor = 1;
                            window.alert("Campo Telefono vacio");
                        } else {
                            if (document.getElementById('tarjetaC') != null && document.getElementById('tarjetaC').value.length == 0) {
                                valor = 1;
                                window.alert("Campo Numero de Tarjeta vacio");
                            } else {
                                if (document.getElementById('celularC') != null && document.getElementById('celularC').value.length == 0) {
                                    valor = 1;
                                    window.alert("Campo Celular vacio");
                                } else {
                                    if (document.getElementById('emailC') != null && document.getElementById('emailC').value.length == 0) {
                                        valor = 1;
                                        window.alert("Campo Email vacio");
                                    } else {
                                        if (document.getElementById('calleC') != null && document.getElementById('calleC').value.length == 0) {
                                            valor = 1;
                                            window.alert("Campo Calle vacio");
                                        } else {
                                            if (document.getElementById('coloniaC') != null && document.getElementById('coloniaC').value.length == 0) {
                                                valor = 1;
                                                window.alert("Campo Colonia vacio");
                                            } else {
                                                if (document.getElementById('delegacionC') != null && document.getElementById('delegacionC').value.length == 0) {
                                                    valor = 1;
                                                    window.alert("Campo Delegacion vacio");
                                                } else {
                                                    if (document.getElementById('cpC') != null && document.getElementById('cpC').value.length == 0) {
                                                        valor = 1;
                                                        window.alert("Campo Codigo Postal vacio");
                                                    } else {
                                                        if (document.getElementById('ciudadC') != null && document.getElementById('ciudadC').value.length == 0) {
                                                            valor = 1;
                                                            window.alert("Campo Ciudad vacio");
                                                        } else {
                                                            if (document.getElementById('estadoC') != null && document.getElementById('estadoC').value.length == 0) {
                                                                valor = 1;
                                                                window.alert("Campo Estado vacio");
                                                            } else {
                                                                if (!document.getElementById('isbn1').value.length > 0) {
                                                                    window.alert("Pedido Vacio");
                                                                    valor = 1;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        for (var x = 1; x < 100; x++) {
            if (document.getElementById('isbn' + x) !== null && document.getElementById('isbn' + x).value.length > 0) {
                if (document.getElementById('titulo' + x).value.length === 0) {
                    window.alert("Campo vacio Titulo: " + x);
                    valor = 1;
                    break;
                }

                if (document.getElementById('cantidad' + x).value.length === 0) {
                    window.alert("Llene el campo de Cantidad: " + x);
                    valor = 1;
                    break;
                }

                if (document.getElementById('editorial' + x).value == 0) {
                    window.alert("Seleccione el campo en la Editorial:" + x);
                    valor = 1;
                    break;
                }

            } else {
                break;
            }
        }
        if (valor === 0) {
            document.pedido1.submit();
            document.getElementById('esperaCirculo').style.display = "block";
            document.getElementById('body').className = "desenfoque";
//            window.alert("Bien");
        }
    }

    function ponerDescuento(id) {
        var idProveedor = document.getElementById('proveedor' + id).value;
        var idEditorial = document.getElementById('editorial' + id).value;
        var descuento = document.getElementById('descuento' + id).value;
        var total = 0;
        for (var c = 1; c <= 150; c++) {
            if (document.getElementById('isbn' + c) !== null && document.getElementById('isbn' + c).value.length > 0) {
                if (document.getElementById('proveedor' + c).value === idProveedor) {
                    if (document.getElementById('editorial' + c).value === idEditorial) {
                        document.getElementById('descuento' + c).value = descuento;
                    }
                }
                total++;
            }
        }
    }

    function ponerAlmecen(id) {
        var idSucursal = document.getElementById('sucursal').value;
        var isbn = document.getElementById('isbn' + id).value;
        var cantidad = document.getElementById('cantidad' + id).value;
        $.post("verificarAlmacen", {isbn, idSucursal, cantidad}, function (data) {
            document.getElementById('origen' + id).innerHTML = data;
            if (data.length > 42) {
                document.getElementById('cantidad' + id).className = "fondoVerde entradaTablac";
            } else {
                document.getElementById('cantidad' + id).className = "fondoRojo entradaTablac";
            }
        });
    }

    function actualizarPro(id) {
        $.post("leerProveedores", {}, function (data) {
//                    window.alert(data);
            document.getElementById('proveedor' + id).innerHTML = data;
        });
    }

    function actualizarEdi(id) {
        if (document.getElementById('proveedor' + id).value != 0) {

            idProveedor = document.getElementById('proveedor' + id).value;
            $.post("leerEditorial", {idProveedor}, function (data) {
                document.getElementById('editorial' + id).innerHTML = data;
            });
        }
    }
    function ejecutarBusqueda(e) {
        (e.keyCode) ? k = e.keyCode : k = e.which;
        if (k === 13) {
            buscarProducto('0');
        }
    }

    function buscarProducto(id) {
        if (document.getElementById('busquedaTPV').value.length > 3) {
            document.getElementById('espera').innerHTML = "<img class='loadingChico' src='images/loading2.gif'>";
            document.getElementById('listaProductos').innerHTML = "";
            var objeto = document.getElementById('busquedaTPV').value;
            $.post("buscarProductoNuevoPedido", {objeto, id}, function (data) {
                if (data.length < 6) {
                    document.getElementById('espera').innerHTML = "<h5 class='tituloListado2'>No hay resultados</h5>";
                } else {
                    document.getElementById('espera').innerHTML = "";
                    document.getElementById('listaProductos').innerHTML = data;
                }
            });
        }
    }

    function insertarProducto(id) {
//        document.getElementById('productoAgregado').style.display = "none";
        var valor = document.getElementById('insertar').value;
        var acceso = 0;
        agregar(valor, 1);
        for (var c = 1; c < 100; c++) {
            if (document.getElementById('isbn' + c).value === document.getElementById('isbnS' + id).innerHTML) {
                document.getElementById('isbnS' + id).className = "brilloRojo";
                break;
            }

            if (document.getElementById('isbn' + c).value === "") {
                document.getElementById('isbn' + c).value = document.getElementById('isbnS' + id).innerHTML;
                document.getElementById('titulo' + c).value = document.getElementById('tituloS' + id).innerHTML;
                document.getElementById('serie' + c).value = document.getElementById('serieS' + id).innerHTML;
                document.getElementById('precio' + c).value = document.getElementById('precioS' + id).innerHTML;
                document.getElementById('editorial' + c).value = document.getElementById('idEditorialS' + id).value;

                acceso = 1;
                document.getElementById('isbnS' + id).className = "brilloVerde";
                break;
            }
        }

//            flotante('agregarProducto');

    }

    $(function () {
        var rx = /INPUT|SELECT|TEXTAREA/i;
        $(document).bind("keydown keypress", function (e) {
            if (e.which == 8) { // 8 == backspace
                if (!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly) {
                    e.preventDefault();
                }
            }
        });
    });

    function saltar(e, id) {
        (e.keyCode) ? k = e.keyCode : k = e.which;
        if (k === 13) {
            if (id == "submit") {
                document.forms[0].submit();
            } else {
                document.getElementById(id).focus();
            }
        }
    }

    function borrarRenglon(valor) {
        document.getElementById('borrarRenglon' + valor).innerHTML = "<div class='eliminarRojo' onclick='eliminarRenglon(" + valor + ");'>Eliminar</div>";
    }

    function eliminarRenglon(valor) {
        document.getElementById('borrarRenglon' + valor).innerHTML = "<div class='btnEliminar' onclick='borrarRenglon(" + valor + ");'><img src='images/less.png' class='iconoNormal'></div>";
        for (var f = valor; f <= 99; f++) {
            var siguiente = f + 1;
            if (document.getElementById('isbn' + siguiente).value != "") {
                document.getElementById('isbn' + f).value = document.getElementById('isbn' + siguiente).value;
                document.getElementById('titulo' + f).value = document.getElementById('titulo' + siguiente).value;
                document.getElementById('serie' + f).value = document.getElementById('serie' + siguiente).value;
                document.getElementById('editorial' + f).value = document.getElementById('editorial' + (siguiente)).value;
                document.getElementById('precio' + f).value = document.getElementById('precio' + siguiente).value;
                document.getElementById('descuento' + f).value = document.getElementById('descuento' + siguiente).value;
                document.getElementById('cantidad' + f).value = document.getElementById('cantidad' + siguiente).value;
            } else {
                document.getElementById('isbn' + f).value = "";
                document.getElementById('titulo' + f).value = "";
                document.getElementById('serie' + f).value = "";
                document.getElementById('editorial' + f).value = "";
                document.getElementById('precio' + f).value = "";
                document.getElementById('descuento' + f).value = "";
                document.getElementById('cantidad' + f).value = "";
                f = 100;
            }
        }
    }
</script>
</body>
</html>
