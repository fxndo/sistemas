<%-- 
    Document   : compras
    Created on : 3/05/2018, 09:18:50 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProveedoreditorialJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="Modelo.Pedido"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.List"%>
<%--<%@page import="Modelo.Compra"%>
<%@page import="ModeloJSP.CompraJpaController"%>--%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    int noProductos = 0;
    int pagina = 0;
    int elementos = 20;
    if (request.getParameter("pagina") != null) {
        pagina = Integer.parseInt(request.getParameter("pagina"));
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    
    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    ProveedoreditorialJpaController pvej=new ProveedoreditorialJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);
    
    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy hh:mm a", new Locale("es"));
    SimpleDateFormat formato2 = new SimpleDateFormat("dd MMMM yyyy", new Locale("es"));
    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
    
    List<Pedidoproveedor> pedido = pprj.pedidoProveedores(pagina);
    noProductos = pprj.pedidoProveedoresCuenta();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Compras | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Compras<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion">
                    <a href="inicio.jsp"><div class="navBloque">Inicio</div></a>
                    <div class="navegacionSelect">Compras</div>
                </div>

            </header>
            <%if(pedido.size()>0){%>
            <div>
                <table class="tabla-producto table table-responsive letraChica">
                    <tr class="cabeceraTabla">
                        <th style="width: 50px;">Pedido</th>
                        <th>Proveedor</th>
                        <th style="padding-left: 10px;">Cliente</th>
                        <th>Fecha Creación</th>
                        <th>Fecha Entrega</th>
                        <th style="width: 80px;">Productos</th>
                        <th>Total</th>
                        <th style="width: 130px;">Sucursal</th>
                    </tr>
                    <%int renglon=1;%>
                    <%for (Pedidoproveedor c : pedido) {%>
                    <tr class="renglon<%=renglon%2%>">
                        <td style="text-align: center;"><strong><%=c.getPedidoId().getId()%></strong></td>
                        <td><a class="link" href="pedido.jsp?id=<%=c.getPedidoId().getId()%>&nav=compras&list=compras"><div class="btnSelector"><%=c.getProveedorId().getNombre()%></div></a></td>
                        <td style="padding-left: 10px;"><%=c.getPedidoId().getClienteId().getNombre()%></td>
                        <td><%=formato.format(c.getPedidoId().getFecha())%></td>
                        <td style="color:#3590f2;"><%=formato2.format(c.getPedidoId().getEntrega())%></td>
                        <%int totalP = 0;%>
                        <%int totalP2 = 0;%>
                        <%for (Pedidoparte php : c.getPedidoparteList()) {%>
                        <%totalP += php.getCantidad();%>
                        <%totalP2 += 0; %>
                        <%}%>
                        <td class="gris"><strong><%=totalP%></strong></td>
                        <td class="saldoVerde"><div><%=nf.format(totalP2)%></div></td>
                        <td><%=c.getPedidoId().getSucursalId().getNombre()%></td>
                    </tr>
                    <%renglon++;%>
                    <%}%>
                </table>
                <!----------------------------------------------------Paginacion---------------------------------------------->
                <%if(noProductos>20){%>
                <br>
                <div class="text-center">
                    <div class="pagina2">Paginas</div>
                    <%int paginas = noProductos / elementos;%>
                    <%if (noProductos % elementos > 0) {%>
                    <%paginas++;%>
                    <%}%>
                    <%for (int c = 1; c <= paginas; c++) {%>
                    <%if (pagina == c - 1) {%>
                    <div class="paginaActiva"><%=c%></div>
                    <%} else {%>
                    <a href="compras.jsp?pagina=<%=c - 1%>&finalizados=on"><div class="pagina"><%=c%></div></a>
                    <%}%>
                    <%}%>
                    <%if (pagina + 1 != paginas) {%>
                    <a href="compras.jsp?pagina=<%=pagina + 1%>&finalizados=on"><div class="paginaActiva">Siguiente</div></a>
                    <%}%>
                </div>
                <br>
                <%}%>
                <!-------------------------------------------------------------------------------------------------->
                <%}else{%>
                <div class="vacioGrande">
                    No hay Compras<br>
                    <img src="images/vacioGrande.png" class="imgVacio">
                </div>
                <%}%>
            </div>
        </main>
        <%@ include file="menu.jsp" %>
    </body>
</html>
