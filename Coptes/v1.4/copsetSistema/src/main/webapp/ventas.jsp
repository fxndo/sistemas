<%-- 
    Document   : ventas
    Created on : 19/05/2018, 03:36:19 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="Modelo.VentasHasProducto"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="Modelo.Ventas"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.VentasJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    int pagina = 0;
    if (request.getParameter("pagina") != null) {
        pagina = Integer.parseInt(request.getParameter("pagina"));
    }
    int elementos = 20;

    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    VentasJpaController v = new VentasJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    Locale locale = new Locale("es", "MX"); // elegimos Argentina
    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy", new Locale("es"));
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

    int objeto=0;
    if(request.getParameter("objeto")!=null && request.getParameter("objeto").length()>0){
        objeto=Integer.parseInt(request.getParameter("objeto"));
    }
    List<Ventas> ventas = v.ventaSucursal(objeto,sucursal.getId(), pagina);
    int noVentas = v.numeroVentaSucursal(sucursal.getId());
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Ventas | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Ventas<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
            <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Ventas</div></div>
            </header>
                    
            <div class="codigoBarra row">
                <div class="nombreBarra col-md-2"></div>
                <div class="icono col-md-1"></div>
                <form action="ventas.jsp">
                    <div class="barra col-md-4"><div class="apartado"><input id="numeroSerial" value="<%if(objeto!=0){%><%=objeto%><%}%>" placeholder="Buscar Venta..." autofocus="true" type="text" name="objeto"></div></div>
                    <div class="botonBarra col-md-2"><button type="submit" class="btnBuscar"><img src="images/buscar.png" class="iconoNormal"></button></div>
                </form>
                <div class="botonBarra col-md-2"></div>
                <div class="col-md-1"><div style="position: relative; top: 8px;" onclick="flotante('ayuda1');"><img src="images/ayuda.png" class="btnAyuda"><strong>Ayuda</strong></div></div>
            </div>

            <div>
                <table class="tabla-producto table table-responsive letraChica">
                    <tr class="cabeceraTabla">
                        <th style="width: 50px;text-align: center;"># Venta</th>
                        <th>Fecha</th>
                        <th>Productos</th>
                        <th>Devolucion</th>
                        <th>Total</th>
                        <th>Sucursal</th>
                        <th>Usuario</th>
                    </tr>
                    <%int renglon=1;%>
                    <%for (Ventas vn : ventas) {%>
                    <tr class="renglon<%=renglon%2%>">
                        <td style="text-align: center;"><strong><%=vn.getId()%></strong></td>
                        <td><a class="link" href="venta.jsp?id=<%=vn.getId()%>"><div class="btnSelector"><%=formato.format(vn.getFecha())%></div></a></td>
                        <td class="gris"><strong><%=vn.getVentasHasProductoList().size()%></strong></td>
                        <%int devolucion=0;%>
                        <%for(VentasHasProducto vh:vn.getVentasHasProductoList()){%>
                        <%devolucion+=vh.getDevolucion();%>
                        <%}%>
                        <td class="devolucionTotal"><div><%=devolucion %></div></td>
                        <td class="saldoVerde"><div><%=nf.format(vn.getTotal())%></div></td>
                        <td><%=vn.getSucursalId().getNombre()%></td>
                        <td><%=vn.getUsuarioId().getNombre()%></td>
                    </tr>
                    <%renglon++;%>
                    <%}%>
                </table>
                <%if (ventas.size() > elementos) {%>
                <div class="text-center">
                    <div class="pagina2">Paginas</div>
                    <%int paginas = noVentas / elementos;%>
                    <%if (noVentas % elementos > 0) {%>
                    <%paginas++;%>
                    <%}%>
                    <%for (int c = 1; c <= paginas; c++) {%>
                    <%if (pagina == c - 1) {%>
                    <div class="paginaActiva"><%=c%></div>
                    <%} else {%>
                    <a href="ventas.jsp?pagina=<%=c - 1%>"><div class="pagina"><%=c%></div></a>
                        <%}%>
                        <%}%>

                    <%if (pagina + 1 != paginas) {%>
                    <a href="ventas.jsp?pagina=<%=pagina + 1%>"><div class="paginaActiva">Siguiente</div></a>
                    <%}%>
                </div>
                <%}%>
            </div>
        </main>
        <%@ include file="menu.jsp" %>
    </body>
</html>
