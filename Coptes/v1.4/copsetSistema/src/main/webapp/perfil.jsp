<%-- 
    Document   : perfil
    Created on : 31/10/2018, 10:01:22 AM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {

    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    
    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Perfil | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Perfil<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Perfil</div></div>

            </header>
            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <div id="password1" onclick="listado('password');" class="listadoOpcion"><img src="images/colorPermiso.png" class="btnIcono2">Contraseña</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <!--/////////////////////////////////////////////////////Informacion///////////////////////////////////////////////////-->
                    <div class="listPedido" id="informacion">
                        <br><br>
                        <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                        <p class="parrafoListado">
                            Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                        </p>
                        <br>
                        <div class="separadorListado"></div>

                        <form action="editarPerfil" method="post">
                            <input type="hidden" name="idUsuario" value="<%=informacion.getId()%>">
                            <label class="entradaLabel">Nombre</label><input type="text" maxlength="299" required="on" name="nombre" class="entrada" value="<%=informacion.getNombre()%>"><br>
                            <label class="entradaLabel">Cargo</label><input type="text" maxlength="99" required="on" name="cargo" class="entrada" value="<%=informacion.getTipo()%>"><br>
                            <label class="entradaLabel">Telefono</label><input type="text" maxlength="10" required="on" name="telefono" class="entrada" value="<%=informacion.getTelefono()%>"><br>
                            <label class="entradaLabel">Email</label><input type="text" maxlength="44" required="on" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" name="email" class="entrada" value="<%=informacion.getEmail()%>"><br>
                            <br><br>
                            <label class="entradaLabel"> </label><button type="submit" value="Editar" class="btn btn-sm btn-default"><img src="images/editarColor.png" class="btnIcono">Editar Información</button>
                        </form>
                    </div>
                    <!--/////////////////////////////////////////////////////Informacion///////////////////////////////////////////////////-->
                    <div class="listPedido" id="password" style="display: none;">
                        <br><br>
                        <h4 class="tituloListado"><img src="images/colorPermiso.png" class="iconoTitulo">Contraseña</h4>
                        <p class="parrafoListado">
                            Edita tu <strong>contraseña</strong> para ingresar al sistema.
                        </p>
                        <br>
                        <div class="separadorListado"></div>
                        <form action="editarPass" method="post">
                            <input type="hidden" name="idUsuario" value="<%=informacion.getId()%>">
                            <label class="entradaLabel">Nueva Contraseña</label><input id="pass1" type="password" maxlength="20" required="on" minlength="8" name="passNew1" placeholder="Crea una nueva contraseña" class="entrada" ><br>
                            <label class="entradaLabel">Repetir Contraseña</label><input id="pass2" onblur="checarpass();"  type="password" maxlength="20" required="on" minlength="8" name="passNew2" placeholder="Repite la nueva contraseña" class="entrada" ><br>
                            <label class="entradaLabel">Contraseña Actual</label><input type="password" name="password" placeholder="Contraseña que usas para Iniciar Sesión" class="entrada" ><br>
                            <br><br>
                            <label class="entradaLabel"> </label><button type="submit" value="Actualizar" class="btn btn-sm btn-default"><img src="images/editarColor.png" class="btnIcono">Actualizar</button>
                        </form>
                    </div>
                </div>
            </div>
            <!--/////////////////////////////////////////   Menu    /////////////////////////////////////////////-->
        </main> 
        <%@ include file="menu.jsp" %>
        <script>
            function checarpass() {
                var pass1 = document.getElementById('pass1').value;
                var pass2 = document.getElementById('pass2').value;
                if (pass1 !== pass2) {
                    document.getElementById('pass1').value = "";
                    document.getElementById('pass2').value = "";
                    window.alert("No coinciden");
                }
            }
        </script>
        <script>
            function listado(id) {

                document.getElementById('informacion').style.display = "none";
                document.getElementById('informacion1').className = "listadoOpcion";
                document.getElementById('password').style.display = "none";
                document.getElementById('password1').className = "listadoOpcion";
                document.getElementById(id).style.display = "block";
                document.getElementById(id + "1").className = "selectL";

            }
        </script>
    </body>
</html>
