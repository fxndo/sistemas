<%-- 
    Document   : venta
    Created on : 23/05/2018, 04:52:35 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.VentasHasProducto"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="Modelo.Ventas"%>
<%@page import="ModeloJSP.VentasJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    VentasJpaController v = new VentasJpaController(emf);
    Ventas venta = v.findVentas(Integer.parseInt(request.getParameter("id")));
    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    Locale locale = new Locale("es", "MX");
    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy", new Locale("es"));
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

    int notificacion = 0;
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
    }

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Venta | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Venta<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion">
                    <%if (request.getParameter("caja") != null) {%>
                    <a href="caja.jsp?id=<%=request.getParameter("caja")%>&list=ventas"><div class="navBloque"><img src="images/back.png" class="btnIcono">Atras</div></a>
                            <%}%>
                    <a href="inicio.jsp"><div class="navBloque">Inicio</div></a>
                    <a href="ventas.jsp"><div class="navBloque">Ventas</div></a>
                    <div class="navegacionSelect">Venta</div></div>
            </header>

            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <div id="productos1" onclick="listado('productos');" class="listadoOpcion"><img src="images/colorStock.png" class="btnIcono2">Productos</div>
                            
                            <%if (venta.getDevolucionId()==null){%>
                            <div id="devolucion1" onclick="listado('devolucion');" class="listadoOpcion"><img src="images/colorDevolucion.png" class="btnIcono2">Devolución</div>
                                <%} else {%>
                            <div id="devolucion1" onclick="listado('devolucion');" style="display: none;" class="listadoOpcion">Devolución</div>
                            <div class="listadoOpcionDes"><img src="images/colorDevolucionGris.png" class="btnIcono2">Devolución</div>
                                <%}%>
                            
                            <!--<div id="devolucion1" onclick="listado('devolucion');" class="listadoOpcion"><img src="images/colorDevolucion.png" class="btnIcono2">Devolución</div>-->
                            <div id="opciones1" onclick="listado('opciones');" class="listadoOpcion"><img src="images/colorOpciones.png" class="btnIcono2">Opciones</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <!--/////////////////////////////////////////////////// Informacion ///////////////////////////////////////////////-->
                    <%if (request.getParameter("list") != null) {%>
                    <div class="listPedido" id="informacion" style="display: none;">
                        <%} else {%>
                        <div class="listPedido" id="informacion">
                            <%}%>
                            <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                            <p class="parrafoListado">
                                Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                            </p>
                            <div class="separadorListado"></div>
                            <br>
                            <div style="margin: 0px 200px;background: white;padding: 10px;">
                                <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                    <tr>
                                        <td class="ladoIzquierdo">No. de Venta</td>
                                        <td class="ladoDerecho"><span class="numPedido"><%=venta.getId()%></span></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Fecha</td>
                                        <td class="ladoDerecho"><%=formato.format(venta.getFecha())%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Productos</td>
                                        <%int productos = 0;%>
                                        <%int devolucion = 0;%>
                                        <%for (VentasHasProducto vh : venta.getVentasHasProductoList()) {%>
                                        <%productos += vh.getCantidad();%>
                                        <%devolucion += vh.getDevolucion(); %>
                                        <%}%>
                                        <td class="ladoDerecho"><%=productos%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Sucursal</td>
                                        <td class="ladoDerecho"><span class="numPedido"><%=venta.getSucursalId().getNombre()%></span></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Usuario</td>
                                        <td class="ladoDerecho"><%=venta.getUsuarioId().getNombre()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Total</td>
                                        <td class="ladoDerecho"><strong class="total"><%=nf.format(venta.getTotal())%></strong> MXN</td>
                                    </tr>
                                </table>
                                    <%if (devolucion > 0) {%>
                                    <hr>
                                    <h4 style="color:#adadad;margin-left: 25px;">Devolución</h4>
                                    <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                    <tr>
                                        <td class="ladoIzquierdo">Fecha</td>
                                        <td class="ladoDerecho"><%=formato.format(venta.getDevolucionId().getFecha()) %></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Usuario</td>
                                        <td class="ladoDerecho"><%=venta.getDevolucionId().getUsuarioId().getNombre() %></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Productos</td>
                                        <td class="ladoDerecho"><%=devolucion %></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Cambio</td>
                                        <td class="ladoDerecho"><strong class="total"><%=nf.format(Double.parseDouble(venta.getTipo())) %></strong></td>
                                    </tr>
                                    
                                    </table>
                                    
                                <%}%>
                            </div>
                            <br>
                        </div>

                        <!--////////////////////////////////////////////////////// Productos   /////////////////////////////////////////////////-->
                        <%if (request.getParameter("list") != null && request.getParameter("list").equals("productos")) {%>
                        <div class="listPedido" id="productos">
                            <%} else {%>
                            <div class="listPedido" id="productos" style="display: none;">
                                <%}%>
                                <h4 class="tituloListado"><img src="images/colorPedido.png" class="iconoTitulo">Productos</h4>
                                <p class="parrafoListado">
                                    Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                                </p>
                                <div class="separadorListado"></div>
                                <table style="margin-left: 15px;margin-right: 15px;" class="table table-responsive tabla-producto letraChica">
                                    <tr class="cabeceraTabla">
                                        <th style="width: 40px;text-align: center;">#</th>
                                        <th style="width: 100px;">ISBN</th>
                                        <th style="width: 280px;">Titulo</th>
                                        <th>Serie</th>
                                        <th>Precio</th>
                                        <th style="width: 40px;text-align: center;">%</th>
                                        <th style="width: 40px;text-align: center; ">C</th>
                                        <th style="width: 40px;text-align: center;  ">D</th>
                                    </tr>
                                    <%int valor = 1;%>
                                    <%for (VentasHasProducto vh : venta.getVentasHasProductoList()) {%>
                                    <tr class="renglon<%=valor % 2%>">
                                        <td style="text-align: center;"><%=valor%></td>
                                        <td><a class="link2" href="producto.jsp?id=<%=vh.getProducto().getId()%>" target="_blank"><%=vh.getProducto().getIsbn()%></a></td>
                                        <td style="padding-left: 15px;"><%=vh.getProducto().getTitulo()%></td>
                                        <td><%=vh.getProducto().getSerie()%></td>
                                        <td class="saldoTotal"><div><%=nf.format(Double.parseDouble(vh.getPrecio()))%></div></td>
                                        <td class="azul"><div><strong><%=vh.getDescuento()%></strong></div></td>
                                        <td class="gris"><strong><%=vh.getCantidad()%></strong></td>
                                        <td class="devolucionTotal"><div><strong><%=vh.getDevolucion()%></strong></div></td>
                                    </tr>
                                    <%valor++;%>
                                    <%}%>
                                </table>
                            </div>

                            <!--////////////////////////////////////////////////////////    Devolucion    //////////////////////////////////////////-->
                            <%if (request.getParameter("list") != null && request.getParameter("list").equals("devolucion")) {%>
                            <div class="listPedido" id="devolucion">
                                <%} else {%>
                                <div class="listPedido" id="devolucion" style="display: none;">
                                    <%}%>
                                    
                                    <%if (venta.getDevolucionId()==null){%>
                                    <h4 class="tituloListado"><img src="images/colorDevolucion.png" class="iconoTitulo">Devolución</h4>
                                    <p class="parrafoListado">
                                        Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                                    </p>
                                    <div class="separadorListado"></div>
                                    <form action="devolucionVenta" method="post" name="devolucionFormulario">
                                        <input type="hidden" name="idVenta" value="<%=venta.getId()%>">
                                        <table class="table table-responsive tabla-producto letraChica">
                                            <tr class="cabeceraTabla">
                                                <th style="width:50px;text-align: center;">#</th>
                                                <th style="width: 100px;">Isbn</th>
                                                <th>Titulo</th>
                                                <th style="width: 200px;">Serie</th>
                                                <th style="width: 70px;">Cantidad</th>
                                                <th style="width: 50px;text-align: center;">Dev</th>
                                            </tr>
                                            <%int valorDevolucion = 1;%>
                                            <%for (VentasHasProducto vh : venta.getVentasHasProductoList()) {%>
                                            <tr class="renglon<%=valorDevolucion % 2%>">
                                                <td><%=valorDevolucion%></td>
                                                <td><a href="producto.jsp?id=<%=vh.getProducto().getId()%>" class="link2" target="_blank"><%=vh.getProducto().getIsbn()%></a></td>
                                                <td><%=vh.getProducto().getTitulo()%></td>
                                                <%if (vh.getProducto().getSerie() != null) {%>
                                                <td><%=vh.getProducto().getSerie()%></td>
                                                <%} else {%>
                                                <td><span class="noInformacion">Sin Información</span></td>
                                                <%}%>
                                                <td><%=vh.getCantidad()%></td>
                                                <td style="padding: 0px;"><input onfocus="limpiar('<%=valorDevolucion%>')" onkeyup="cambiarColor('<%=valorDevolucion%>')" id="cantidadD<%=valorDevolucion%>" onkeypress="return validarEntradaNumero(event);" name="cantidad<%=valorDevolucion%>" value="0" max="<%=vh.getCantidad()%>" required="on" autocomplete="off" title="Devolución" class="entradaTablac" type="number"></td>
                                            </tr>
                                            <%valorDevolucion++; %>
                                            <%}%>
                                        </table>
                                        <button type="submit" class="btn btn-sm btn-default" ><img src="images/sipermiso.png" class="btnIcono">Aceptar Devolución</button>
                                    </form>
                                        <%}%>

                                </div>
                                <!--////////////////////////////////////////////////////////    Opciones    //////////////////////////////////////////-->

                                <div class="listPedido" id="opciones" style="display: none;">
                                    <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Opciones</h4>
                                    <p class="parrafoListado">
                                        Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                                    </p>
                                    <div class="separadorListado"></div>
                                    <a href="imprimirVenta.jsp?venta=<%=venta.getId()%>" target="_blank"><div class="btn bbtn btn-sm btn-default"><img src="images/imprimirColor.png" class="btnIcono">Imprimir Ticket</div></a>
                                </div>
                            </div>
                        </div>
                        </main>
                        <!--/////////////////////////////////////////////////////////   Notificaciones  //////////////////////////////////////////////////-->
                        <div class="notificacionBloque" style="<%if (notificacion == 27) {%>display:block;<%} else {%>display:none;<%}%>">
                            <div class="notificacionImg">
                                <img src="images/notiSuccess.png" class="noti">
                            </div>
                            <div class="notificacionInfo">
                                <div class="titulo">Devolución Venta</div>
                                La <strong>devolución</strong> se agrego satisfactoriamente.
                            </div>
                        </div>
                        <%@ include file="menu.jsp" %>
                        <script>
                            function listado(id) {

                                document.getElementById('informacion').style.display = "none";
                                document.getElementById('informacion1').className = "listadoOpcion";
                                document.getElementById('productos').style.display = "none";
                                document.getElementById('productos1').className = "listadoOpcion";
                                document.getElementById('devolucion').style.display = "none";
                                document.getElementById('devolucion1').className = "listadoOpcion";
                                document.getElementById('opciones').style.display = "none";
                                document.getElementById('opciones1').className = "listadoOpcion";
                                document.getElementById(id).style.display = "block";
                                document.getElementById(id + "1").className = "selectL";

                            }

                            function limpiar(id) {
                                document.getElementById("cantidadD" + id).value = "";
                            }

                            function cambiarColor(id) {
                                if (parseInt(document.getElementById('cantidadD' + id).value) > 0) {
                                    document.getElementById('cantidadD' + id).className = "entradaTablaDevolucion";
                                } else {
                                    document.getElementById('cantidadD' + id).className = "entradaTablac";
                                }
                            }
                        </script>
                        </body>
                        </html>
