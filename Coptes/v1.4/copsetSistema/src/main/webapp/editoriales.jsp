<%-- 
    Document   : editoriales
    Created on : 24/04/2018, 12:29:36 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Proveedoreditorial"%>
<%@page import="ModeloJSP.ProveedoreditorialJpaController"%>
<%@page import="Modelo.Proveedor"%>
<%@page import="ModeloJSP.ProveedorJpaController"%>
<%@page import="ModeloJSP.DescuentoJpaController"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    int noProductos = 0;
    int pagina = 0;
    int elementos = 20;
    if (request.getParameter("pagina") != null) {
        pagina = Integer.parseInt(request.getParameter("pagina"));
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    ProveedoreditorialJpaController pe = new ProveedoreditorialJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy", new Locale("es"));

    List<Proveedoreditorial> proEd = pe.proveedorEditorialLista(pagina);
    noProductos = pe.proveedorEditorialListaCuenta();

    int notificacion = 0;
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Descuentos | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Descuentos<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Descuentos</div></div>

            </header>
            <div>
                <%if(proEd.size()>0){%>
                <table class="tabla-producto table table-responsive letraChica">
                    <tr class="cabeceraTabla">
                        <th style="width: 50px;text-align: center;">#</th>
                        <th>Clave</th>
                        <th style="padding-left: 10px;">Proveedor</th>
                        <th>Editorial</th>
                        <th>Descuento</th>
                        <th>Inicio</th>
                        <th>Final</th>
                    </tr>
                    <%int valor = 1+(pagina*20);%>

                    <%for (Proveedoreditorial ed : proEd) {%>
                    <tr class="renglon<%=valor % 2%>">
                        <td style="text-align: center;"><%=valor%></td>
                        <td style="width: 100px;"><a class="link" href="editorial.jsp?idEditorial=<%=ed.getId()%>"><div class="btnSelector"><%=ed.getProveedorId().getClave()%></div></a></td>
                        <td style="padding-left: 10px;"><%=ed.getProveedorId().getNombre()%></td>
                        <td><%=ed.getEditorialId().getNombre()%></td>

                        <%if (ed.getDescuentoId() != null) {%>
                        <td class="saldoNaranja"><div><strong><%=ed.getDescuentoId().getCantidad()%> %</strong></div></td>
                        <td class="saldoTotal text-center"><div><%=formato.format(ed.getDescuentoId().getInicio())%></div></td>
                        <td class="saldoRojo"><div><%=formato.format(ed.getDescuentoId().getFinal1())%></div></td>
                                <%} else {%>
                        <td class="saldoNaranja"><div><strong>0 %</strong></div></td>
                        <td class="saldoGris"><div>-</div></td>
                        <td class="saldoGris"><div>-</div></td>
                        <%}%>
                    </tr>
                    <%valor++;%>
                    <%}%>
                    <%}else{%>
                    <div class="vacioGrande">
                    No hay Proveedores o Editoriales<br>
                    <img src="images/vacioGrande.png" class="imgVacio">
                </div>
                    <%}%>
                </table>
                <!----------------------------------------------------Paginacion---------------------------------------------->
                <%if(noProductos>20){%>
                <br>
                <div class="text-center">
                    <div class="pagina2">Paginas</div>
                    <%int paginas = noProductos / elementos;%>
                    <%if (noProductos % elementos > 0) {%>
                    <%paginas++;%>
                    <%}%>
                    <%for (int c = 1; c <= paginas; c++) {%>
                    <%if (pagina == c - 1) {%>
                    <div class="paginaActiva"><%=c%></div>
                    <%} else {%>
                    <a href="editoriales.jsp?pagina=<%=c - 1%>&finalizados=on"><div class="pagina"><%=c%></div></a>
                        <%}%>
                        <%}%>
                        <%if (pagina + 1 != paginas) {%>
                    <a href="editoriales.jsp?pagina=<%=pagina + 1%>&finalizados=on"><div class="paginaActiva">Siguiente</div></a>
                    <%}%>
                </div>
                <br>
                <%}%>
                <!-------------------------------------------------------------------------------------------------->
            </div>
        </main>
        <div id="nuevaEditorial" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Nueva Editorial</div>
            <form action="agregarEditorial" method="post">
                <div class="principalFlotante">
                    <label>Nombre<input class="form-control" type="text" name="editorial"></label>
                </div>
                <div class="botonesFlotante"><button type="submit" class="btn btn-primary"><img src="images/aceptar.png" class="btnIcono">Agregar Editorial</button><div onclick="flotante('nuevaEditorial')" class="btn btn-default"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <!--//////////////////////////////////  Notificaciones  ///////////////////////////////////////-->
        <div class="notificacionBloque" style="<%if(notificacion==22){%>display:block;<%}else{%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Agregar Descuento</div>
                El <strong>descuento</strong> se agrego satisfactoriamente.
            </div>
        </div>
        <%@ include file="menu.jsp" %>
    </body>
</html>
