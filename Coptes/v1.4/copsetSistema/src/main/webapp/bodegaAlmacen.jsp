<%-- 
    Document   : bodegaAlmacen
    Created on : 12/04/2019, 11:13:14 AM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="Modelo.Pedido"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
     HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    
    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoJpaController pdj=new PedidoJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);
    
    List<Pedido> pedidos=pdj.pedidosAlmacen(); 
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Bodega Almacen | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody"  class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Bodega Almacen<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="sucursales.jsp"><div class="navBloque">Sucursales</div></a><div class="navegacionSelect">Bodega</div></div>
            </header>
            <div class="codigoBarra">
                <a href="reporteAlmacen"><div class="btn btn-default btn-sm"><img src="images/reporte.png" class="btnIcono">Reporte Almacen</div></a>
            </div>
            
            <table class="table table-responsive tabla-producto letraChica">
                <tr class="cabeceraTabla">
                    <th style="width: 50px;text-align: center;">#</th>
                    <th style="width: 100px;">ISBN</th>
                    <th style="padding-left: 15px;">Titulo</th>
                    <th>Serie</th>
                    <th style="width: 80px;">Cantidad</th>
                    <th>Pedido</th>
                </tr>
                <%int renglon=1;%>
                <%for(Pedido pf: pedidos){%>
                <%for(Pedidoproveedor pprf: pf.getPedidoproveedorList()){%>
                <%for(Pedidoparte ppaf: pprf.getPedidoparteList()){%>
                <%if(ppaf.getCantidad()!=ppaf.getFaltante()){%>
                <tr class="renglon<%=renglon%2%>">
                    <td><%=renglon%></td>
                    <td><a class="link2" href="producto.jsp?id=<%=ppaf.getProductoId().getId()%>"><%=ppaf.getProductoId().getIsbn() %></a></td>
                    <td><%=ppaf.getProductoId().getTitulo()%></td>
                    <td><%=ppaf.getProductoId().getSerie() %></td>
                    <td class="gris"><strong><%=ppaf.getCantidad()-ppaf.getFaltante() %></strong></td>
                    <td class="azul"><a href="pedido.jsp?id=<%=pf.getId()%>"><div style="color:white;"><%=pf.getClienteId().getNombre()%></div></a></td>
                </tr>
                <%renglon++;%>
                <%}%>
                <%}%>
                <%}%>
                <%}%>
            </table>
        </main>
        <%@ include file="menu.jsp" %>
    </body>
</html>
