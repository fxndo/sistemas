<%-- 
    Document   : editorial
    Created on : 24/04/2018, 02:14:33 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Proveedoreditorial"%>
<%@page import="ModeloJSP.ProveedoreditorialJpaController"%>
<%@page import="Modelo.Proveedor"%>
<%@page import="ModeloJSP.ProveedorJpaController"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    ProveedoreditorialJpaController ed = new ProveedoreditorialJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy", new Locale("es"));

    Proveedoreditorial proEd = ed.findProveedoreditorial(Integer.parseInt(request.getParameter("idEditorial")));
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Descuento | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Descuento<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="editoriales.jsp"><div class="navBloque">Descuentos</div></a><div class="navegacionSelect">Editorial</div></div>

            </header>

            <br><br>
            <div style="padding: 10px 100px;">

                <div style="margin: 0px 200px;background: white;padding: 10px;">
                    <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                        <tr>
                            <td class="ladoIzquierdo">Proveedor</td>
                            <td class="ladoDerecho"><span class="numPedido"><%=proEd.getProveedorId().getNombre()%></span></td>
                        </tr>
                        <tr>
                            <td class="ladoIzquierdo">Editorial</td>
                            <td class="ladoDerecho"><%=proEd.getEditorialId().getNombre()%></span></td>
                        </tr>
                        <%if (proEd.getDescuentoId() != null) {%>
                        <tr>
                            <td class="ladoIzquierdo">Descuento</td>
                            <td class="ladoDerecho"><strong class="numPedido"><%=proEd.getDescuentoId().getCantidad()%>%</strong></span></td>
                        </tr>
                        <tr>
                            <td class="ladoIzquierdo">Inicio</td>
                            <td class="ladoDerecho"><%=formato.format(proEd.getDescuentoId().getInicio())%></span></td>
                        </tr>
                        <tr>
                            <td class="ladoIzquierdo">Termino</td>
                            <td class="ladoDerecho"><%=formato.format(proEd.getDescuentoId().getFinal1())%></span></td>
                        </tr>
                        <%}%>
                    </table>
                </div>
                <br><br>
                <div class="separadorListado"></div> 
                <div class="row">

                    <h3 style="margin-left: 210px;" class="tituloListado2">Descuento del Proveedor</h3>
                    <form action="agregarDescuento" method="post">
                        <label class="entradaLabel">Descuento</label><input class="entrada" required="" type="number" onkeypress="return validarEntradaNumero(event);" name="cantidad" placeholder="0% - 50%" max="50" maxlength="2" pattern="[0-9]+"><br>
                        <label class="entradaLabel">Inicio</label><input id="inicio" class="entrada" type="date" placeholder="" onchange="modificar();" name ="inicio" step="1" min="" max=""><br>
                        <label class="entradaLabel">Final</label><input id="termino" class="entrada" type="date" placeholder="" name="termino" step="1" min="" max=""><br>
                        <input type="hidden" value="<%=proEd.getId()%>" name="idProEd">
                        <br>
                        <label class="entradaLabel"> </label><button type="submit" class="btn btn-sm btn-default"><img src="images/editarColor.png" class="btnIcono">Editar Descuento</button>
                    </form>
                </div>
            </div>
            <br><br>
            <!--                </div>
                        </div>-->

        </main>
        <%@ include file="menu.jsp" %>
        <script>
            function modificar() {
                document.getElementById('termino').min = document.getElementById('inicio').value;
            }
        </script>
    </body>
</html>
