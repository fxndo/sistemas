<%-- 
    Document   : cliente
    Created on : 27/04/2018, 08:18:38 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.UsuarioJpaController"%>
<%@page import="Modelo.Pago"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="ModeloJSP.DescuentoJpaController"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="Modelo.Pedido"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="Modelo.Cliente"%>
<%@page import="ModeloJSP.ClienteJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {

    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    UsuarioJpaController usj=new UsuarioJpaController(emf);
    informacion=usj.findUsuario(informacion.getId());
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");

    ProductoJpaController pj = new ProductoJpaController(emf);
    ClienteJpaController c = new ClienteJpaController(emf);
    Cliente cliente = c.findCliente(Integer.parseInt(request.getParameter("id")));
    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);

    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy", new Locale("es"));
    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

    List<Pedido> pedidos = p.pedidosClienteMuestra(cliente.getId());
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Cliente | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Cliente<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion">
                    <%if (request.getParameter("nav") != null && request.getParameter("nav").equals("finanzas")) {%>
                    <a href="finanzas.jsp"><div class="navBloque"><img src="images/back.png" class="btnIcono">Atras</div></a>
                            <%}%>
                    <a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="clientes.jsp"><div class="navBloque">Clientes</div></a><div class="navegacionSelect">Cliente</div></div>
            </header>
            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <div id="pedidos1" onclick="listado('pedidos');" class="listadoOpcion"><img src="images/colorPedido.png" class="btnIcono2">Pedidos</div>
                            <div id="opciones1" onclick="listado('opciones');" class="listadoOpcion"><img src="images/colorOpciones.png" class="btnIcono2">Opciones</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <!--//////////////////////////////////////////////////////   Informacion   //////////////////////////////////////////////////////-->
                    <%if (request.getParameter("list") != null) {%>
                    <div class="listPedido" id="informacion" style="display: none;" >
                        <%} else {%>
                        <div class="listPedido" id="informacion" >
                            <%}%>
                            <!--<br>-->
                            <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                            <p class="parrafoListado">
                                La información del <strong>Cliente</strong> se muestra a continuación.
                            </p>
                            <!--<br>-->
                            <div class="separadorListado"></div>
                            <br>
                            <div style="margin: 0px 120px;background: white;padding: 10px;">
                                <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                    <tr>
                                        <td class="ladoIzquierdo">Nombre</td>
                                        <td class="ladoDerecho"><%=cliente.getNombre()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Escuela</td>
                                        <td class="ladoDerecho"><%=cliente.getEscuela()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Telefono</td>
                                        <td class="ladoDerecho"><%=cliente.getTelefono()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Celular</td>
                                        <td class="ladoDerecho"><%=cliente.getCelular()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Email</td>
                                        <td class="ladoDerecho"><%=cliente.getEmail()%></td>
                                    </tr>
                                </table>
                                    <h5 style="margin-left: 25px;" class="tituloListado2">Dirección</h5>
                                     <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                    <tr>
                                        <td class="ladoIzquierdo">Calle</td>
                                        <td class="ladoDerecho"><%=cliente.getDireccionId().getCalle()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Colonia</td>
                                        <td class="ladoDerecho"><%=cliente.getDireccionId().getColonia()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Delegación</td>
                                        <td class="ladoDerecho"><%=cliente.getDireccionId().getDelegacion()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Codigo Postal</td>
                                        <td class="ladoDerecho"><%=cliente.getDireccionId().getCp()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Ciudad</td>
                                        <td class="ladoDerecho"><%=cliente.getDireccionId().getCiudad()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Estado</td>
                                        <td class="ladoDerecho"><%=cliente.getDireccionId().getEstado()%></td>
                                    </tr>
                                </table>
                            </div>

                            
                        </div>
                        <!--/////////////////////////////////////////////////////// Pedidos //////////////////////////////////////////////////////////////-->
                        <%if (request.getParameter("list") != null && request.getParameter("list").equals("pedidos")) {%>
                        <div class="listPedido" id="pedidos">
                            <%} else {%>
                            <div class="listPedido" id="pedidos" style="display: none;">
                                <%}%>
                                <!--<br>-->
                                <h4 class="tituloListado"><img src="images/colorPedido.png" class="iconoTitulo">Pedidos</h4>
                                <p class="parrafoListado">
                                    Lista de pedidos realizados por el <strong>Cliente</strong>.
                                </p>
                                <!--<br>-->
                                <div class="separadorListado"></div>
                                <%if (pedidos.size() > 0) {%>
                                <table style="margin-left: 15px;" class="table table-responsive tabla-producto letraChica">
                                    <tr class="cabeceraTabla">
                                        <th style="width: 50px">Pedido</th>
                                        <th>Estado</th>
                                        <th style="padding-left: 15px;">Fecha</th>
                                        <th  style="width:70px; ">Productos</th>
                                        <th>Total</th>
                                        <th>Restante</th>
                                        <th style="padding-left: 15px;">Sucursal</th>
                                        <th>Usuario</th>
                                    </tr>
                                    <%int renglon = 1;%>
                                    <%for (Pedido pd : pedidos) {%>
                                    <tr class="renglon<%=renglon % 2%>">
                                        <td style="text-align: center;"><%=pd.getId()%></td>
                                        
                                        <%if (pd.getEstado().equals("1")) {%>
                                        <td class="estado colorE1"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Nuevo</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("2") || pd.getEstado().equals("3")) {%>
                                        <td class="estado colorE2"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Pendiente</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("4") || pd.getEstado().equals("5")) {%>
                                        <td class="estado colorE3"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Asignado</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("6")) {%>
                                        <td class="estado colorE4"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Proveedor</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("7")) {%>
                                        <td class="estado colorE5"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Faltante</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("8")) {%>
                                        <td class="estado colorE6"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Almacen</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("9")) {%>
                                        <td class="estado colorE7"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Traslado</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("10")) {%>
                                        <td class="estado colorE8"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Sucursal</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("11")) {%>
                                        <td class="estado colorE9"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Entrega</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("12")) {%>
                                        <td class="estado colorE10"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Finalizado</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("13")) {%>
                                        <td class="estado colorE911"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Finalizado</div></a></td>
                                        <%}
                                            if (pd.getEstado().equals("14")) {%>
                                        <td class="estado colorE12"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Oculto</div></a></td>
                                        <%}
                                         if (pd.getEstado().equals("15")) {%>
                                        <td class="estado colorE13"><a class="link" href="pedido.jsp?id=<%=pd.getId()%>&nav=cliente&idCliente=<%=cliente.getId()%>"><div>Cancelado</div></a></td>
                                        <%}%>

                                        <td  style="padding-left: 15px;"><%=formato.format(pd.getFecha())%></td>
                                        <%int valorT = 0;
                                            for (Pedidoproveedor pv : pd.getPedidoproveedorList()) {
                                                for (Pedidoparte ppa : pv.getPedidoparteList()) {
                                                    valorT += ppa.getCantidad();
                                                }
                                            }%>
                                        <td style="text-align:center;background: #e0e0e0;border-bottom: 1px solid #d1d1d1;"><strong><%=valorT%></strong></td>
                                        <td class="saldoTotal"><div><%=nf.format(pd.getTotal())%></div></td>
                                        <%Double acumulado=0.0;%>
                                        <%for(Pago pf: pd.getPagoList()){%>
                                        <%acumulado+=pf.getMonto();%>
                                        <%}%>
                                        <%double restante=pd.getTotal()-acumulado;%>
                                        <%if(restante==0){%>
                                        <td class="saldoVerde"><div><%=nf.format(restante)%></div></td>
                                        <%}else{%>
                                        <td class="saldoRojo"><div><%=nf.format(restante)%></div></td>
                                        <%}%>

                                        <td style="padding-left: 15px;"><%=pd.getSucursalId().getNombre()%></td>
                                        <td><%=pd.getUsuarioId().getNombre()%></td>
                                    </tr>
                                    <%renglon++;%>
                                    <%}%>
                                </table> 
                                <%} else {%>
                                <div class="vacioGrande">
                                    No hay Pedidos<br>
                                    <img src="images/vacioGrande.png" class="imgVacio">
                                </div>
                                <%}%>
                                <br><br>
                            </div>

                            <!--//////////////////////////////////////////////////////   Opciones    //////////////////////////////////////////////////////////7-->
                            <%if (request.getParameter("list") != null && request.getParameter("list").equals("opciones")) {%>
                            <div class="listPedido" id="opciones">
                                <%} else {%>
                                <div class="listPedido" id="opciones" style="display: none;">
                                    <%}%>

                                    <h4 class="tituloListado"><img src="images/colorOpciones.png" class="iconoTitulo">Opciones</h4>
                                    <p class="parrafoListado">
                                        Las opciones disponibles para el <strong>Cliente</strong> se muestran a continuaciión.
                                    </p>
                                    
                                    <%int fav=0;%>
                                    <%for(Usuario usf: cliente.getUsuarioList()){%>
                                        <%if(usf.getId()==informacion.getId()){%>
                                            <%fav=1;%>
                                        <%}%>
                                    <%}%>
                                    <div class="separadorListado"></div>
                                    <%if(fav==1){%>
                                        <p class="parrafoListado">
                                            Quitar al cliente de tu lista de favoritos
                                        </p>
                                        <label class="apaB"> </label><a href="favoritos?id=<%=cliente.getId()%>&accion=2"><div class="btn bbtn btn-sm btn-default"><img src="images/favoritoAzul.png" class="btnIcono"> Quitar de Favoritos</div></a>
                                    <%}else{%>
                                        <p class="parrafoListado">
                                            Agrega al cliente a tu lista de favoritos
                                        </p>
                                        <label class="apaB"> </label><a href="favoritos?id=<%=cliente.getId()%>&accion=1"><div class="btn bbtn btn-sm btn-default"><img src="images/favoritoGris.png" class="btnIcono"> Agregar a Favoritos</div></a>
                                    <%}%>
                                    
                                    <br><br>
                                    <%if (cliente.getTipo() == 1 && informacion.getPermisosList().get(0).getAdministracionp()==1) {%>
                                    <div class="separadorListado"></div>
                                    
                                    <p class="parrafoListado">
                                        Convierte al cliente en una tienda dando clic en <strong>Cliente Tienda</strong> para poder agregar un pedido directamente al almacen de la sucursal.
                                    </p>
                                    <label class="apaB"> </label><div onclick="flotante('cambiarCliente')" class="btn bbtn btn-default btn-sm"><img src="images/almacenColor.png" class="btnIcono">Cliente Tienda</div>
                                    <br><br>
                                    <%}%>
                                    
                                    <%if (cliente.getPedidoList().size() == 0 && informacion.getPermisosList().get(0).getAdministracionp()==1) {%>
                                    <div class="separadorListado"></div>
                                    
                                    <p class="parrafoListado">
                                        Elimina el <strong>Cliente</strong> para que no este en la lista de clientes.
                                    </p>
                                    <label class="apaB"> </label><div onclick="flotante('eliminarCliente')" class="btn bbtn btn-default btn-sm"><img src="images/less.png" class="btnIcono">Eliminar Cliente</div>
                                    <br><br>
                                    <%}%>

                                    <div class="separadorListado"></div>
                                    
                                    <p class="parrafoListado">
                                        Edita los datos del <strong>Cliente</strong>
                                    </p>
                                    <label class="apaB"> </label><div onclick="flotante('editarCliente')" class="btn bbtn btn-default btn-sm"><img src="images/editarColor.png" class="btnIcono">Editar Cliente</div>

                                </div><br><br>
                            </div>
                        </div>
                        </main>
                        <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                        <div id="fondoNegro" class="fondoNegro" style="display: none;"></div>
                        <div id="cambiarCliente" class="bloqueFlotante" style="display: none;">
                            <div class="tituloFlotante">Cambiar Cliente</div>
                            <form action="cambiarCliente" method="post">
                                <input type="hidden" name="idCliente" value="<%=cliente.getId()%>">
                                <div class="principalFlotante">
                                    <h4 class="tituloListado">¿Estas seguro que quieres cambiar el cliente como cliente Interno?</h4>
                                    <p class="parrafoListado">Podras agregar stock desde un pedido</p>
                                </div>
                                <div class="botonesFlotante"><button type="submit" class="btn btn-primary"><img src="images/aceptar.png" class="btnIcono">Cambiar</button><div onclick="flotante('cambiarCliente')" class="btn btn-default"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                            </form>
                        </div>

                        <div id="eliminarCliente" class="bloqueFlotante" style="display: none;">
                            <div class="tituloFlotante">Eliminar Cliente</div>
                            <form action="eliminarCliente" method="post">
                                <input type="hidden" name="id" value="<%=cliente.getId()%>">
                                <input type="hidden" name="idCliente" value="<%=cliente.getId()%>">
                                <div class="principalFlotante">
                                    <h4 class="tituloListado">¿Estas seguro que quieres eliminar al cliente?</h4>
                                    <p class="parrafoListado">Si lo eliminas no aparecra en la lista de clientes.</p>
                                </div>
                                <div class="botonesFlotante"><button type="submit" class="btn btn-sm btn-danger"><img src="images/aceptar.png" class="btnIcono">Eliminar</button><div onclick="flotante('eliminarCliente')" class="btn btn-sm btn-default"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                            </form>
                        </div>

                        <div id="editarCliente" class="bloqueFlotante" style="display: none;">
                            <div class="tituloFlotante">Editar Cliente</div>
                            <form action="editarCliente" method="post">
                            <div class="principalFlotante">
                                    <input type="hidden" value="<%=cliente.getId()%>" name="idCliente">
                                    <input type="hidden" value="<%=cliente.getNumerotarjeta()%>" name="tarjeta">
                                    <label class="entradaLabel">Nombre</label><input type="text" class="entrada" name="nombre" maxlength="299" required="on" value="<%=cliente.getNombre()%>"><br>
                                    <label class="entradaLabel">Escuela</label><input type="text" class="entrada" name="escuela" maxlength="99" required="on" value="<%=cliente.getEscuela()%>"><br>
                                    <label class="entradaLabel">Telefono</label><input type="text" class="entrada" name="telefono" maxlength="10" required="on" value="<%=cliente.getTelefono()%>"><br>
                                    <label class="entradaLabel">Celular</label><input type="text" class="entrada" name="celular" maxlength="10" required="on" value="<%=cliente.getCelular()%>"><br>
                                    <label class="entradaLabel">Email</label><input type="text" class="entrada" name="email" maxlength="44" required="on" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<%=cliente.getEmail()%>"><br>
                                    <br>
                                    <h4 style="margin-left: 210px;" class="tituloListado2">Dirección</h4>
                                    <label class="entradaLabel">Calle</label><input type="text" class="entrada" name="calle" maxlength="45" value="<%=cliente.getDireccionId().getCalle()%>"><br>
                                    <label class="entradaLabel">Colonia</label><input type="text" class="entrada" name="colonia" maxlength="45" value="<%=cliente.getDireccionId().getColonia()%>"><br>
                                    <label class="entradaLabel">Delegación</label><input type="text" class="entrada" name="delegacion" maxlength="45" value="<%=cliente.getDireccionId().getDelegacion()%>"><br>
                                    <label class="entradaLabel">Codigo Postal</label><input type="text" class="entrada" name="cp" maxlength="45" value="<%=cliente.getDireccionId().getCp()%>"><br>
                                    <label class="entradaLabel">Ciudad</label><input type="text" class="entrada" name="ciudad" maxlength="45" value="<%=cliente.getDireccionId().getCiudad()%>"><br>
                                    <label class="entradaLabel">Estado</label><input type="text" class="entrada" name="estado" maxlength="45" value="<%=cliente.getDireccionId().getEstado()%>"><br>

                                    <!--<button type="submit" class="btn btn-default btn-sm"><img class="btnIcono" src="images/editar.png"><img src="images/editarColor.png" class="btnIcono">Editar Cliente</button>-->

                            </div>
                            <div class="botonesFlotante"><button type="submit" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Editar</button><div onclick="flotante('editarCliente')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                            </form>
                        </div>
                        <%@ include file="menu.jsp" %>
                        <script>
                            function listado(id) {

                                document.getElementById('informacion').style.display = "none";
                                document.getElementById('informacion1').className = "listadoOpcion";
                                document.getElementById('pedidos').style.display = "none";
                                document.getElementById('pedidos1').className = "listadoOpcion";
                                document.getElementById('opciones').style.display = "none";
                                document.getElementById('opciones1').className = "listadoOpcion";
                                document.getElementById(id).style.display = "block";
                                document.getElementById(id + "1").className = "selectL";

                            }
                        </script>
                        </body>
                        </html>
