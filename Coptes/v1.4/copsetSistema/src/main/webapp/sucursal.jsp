<%-- 
    Document   : sucursal
    Created on : 19/05/2018, 02:39:34 AM
    Author     : fernando
--%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);
    Sucursal suc = scj.findSucursal(Integer.parseInt(request.getParameter("id")));
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Sucursal | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Sucursal<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="sucursales.jsp"><div class="navBloque">Sucursales</div></a><div class="navegacionSelect">Sucursal</div></div>
            </header>
            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <div id="opciones1" onclick="listado('opciones');" class="listadoOpcion"><img src="images/colorOpciones.png" class="btnIcono2">Opciones</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <!--//////////////////////////////////////  Informacion //////////////////////////////////-->
                    <div class="listPedido" id="informacion">
                        <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                        <p class="parrafoListado">
                            Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                        </p>
                        <div class="separadorListado"></div>
                        <form action="editarSucursal" method="post">
                            <input type="hidden" name="idSucursal" value="<%=suc.getId()%>">

                            <label class="entradaLabel">Nombre</label><input class="entrada" type="text" required="on" name="nombre" value="<%=suc.getNombre()%>"><br>
                            <h4 class="tituloListado2" style="margin-left: 210px;">Dirección</h4>
                            <label class="entradaLabel">Calle</label><input class="entrada" type="text" name="calle" required="on" maxlength="45" value="<%=suc.getDireccionId().getCalle()%>"><br>
                            <label class="entradaLabel">Colonia</label><input class="entrada" type="text" name="colonia" maxlength="45" required="on" value="<%=suc.getDireccionId().getColonia()%>"><br>
                            <label class="entradaLabel">Delegación</label><input class="entrada" type="text" required="on" name="delegacion" maxlength="45" value="<%=suc.getDireccionId().getDelegacion()%>"><br>
                            <label class="entradaLabel">Codigo Postal</label><input class="entrada" type="text" name="cp" maxlength="45" required="on" value="<%=suc.getDireccionId().getCp()%>"><br>
                            <label class="entradaLabel">Ciudad</label><input class="entrada" type="text" name="ciudad" required="on" maxlength="45" value="<%=suc.getDireccionId().getCiudad()%>"><br>
                            <label class="entradaLabel">Estado</label><input class="entrada" type="text" name="estado" maxlength="45" required="on" value="<%=suc.getDireccionId().getEstado()%>"><br>
                            <br>
                            <label class="entradaLabel"> </label><button class="btn btn-sm btn-default" type="submit" value="Editar"><img src="images/editarColor.png" class="btnIcono">Editar Sucursal</button>
                            <br><br>
                        </form>
                    </div>
                    <!--/////////////////////////////////////   Opciones    /////////////////////////////////-->
                    <div class="listPedido" id="opciones" style="display: none;">
                        <h4 class="tituloListado"><img src="images/colorOpciones.png" class="iconoTitulo">Opciones</h4>
                        <p class="parrafoListado">
                            Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                        </p>
                        <div class="separadorListado"></div>
                            <%if (suc.getPedidoList().size() == 0 && suc.getUsuarioList().size() == 0) {%>
                            <div onclick="flotante('eliminarSucursal');" class="btn btn-sm bbtn btn-default"><img src="images/less.png" class="btnIcono">Eliminar Sucursal</div>
                        <%}%>

                    </div>
                </div>

        </main>
        <div id="eliminarSucursal" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Eliminar Sucursal</div>
            <form action="eliminarSucursal" method="post">
                <input type="hidden" name="idSucursal" value="<%=suc.getId()%>">
                <div class="principalFlotante text-center">
                    <h4>¿Estas seguro de querer eliminar la Sucursal?</h4>

                </div>
                <div class="botonesFlotante"><button type="submit" class="btn btn-sm btn-danger"><img src="images/borrarBlanco.png" class="btnIcono">Eliminar Sucursal</button><div onclick="flotante('eliminarSucursal')" class="btn btn-sm btn-default"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <%@ include file="menu.jsp" %>
        <script>
            function listado(id) {

                document.getElementById('informacion').style.display = "none";
                document.getElementById('informacion1').className = "listadoOpcion";
                document.getElementById('opciones').style.display = "none";
                document.getElementById('opciones1').className = "listadoOpcion";
                document.getElementById(id).style.display = "block";
                document.getElementById(id + "1").className = "selectL";

            }
        </script>
    </body>
</html>
