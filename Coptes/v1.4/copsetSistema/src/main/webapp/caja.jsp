<%-- 
    Document   : caja
    Created on : 9/08/2018, 12:30:50 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="Modelo.VentasHasProducto"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Ventas"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="Modelo.Caja"%>
<%@page import="ModeloJSP.CajaJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {

    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    
    
    ProductoJpaController pj = new ProductoJpaController(emf);
    CajaJpaController c = new CajaJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);
    
    int idCaja = Integer.parseInt(request.getParameter("id"));
    Caja caja = c.findCaja(idCaja);

    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy hh:mm a", new Locale("es"));

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Caja | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Caja<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><a href="adminTPV.jsp"><div class="navBloque">Administrar TPV</div></a><div class="navegacionSelect">Caja</div></div>
            </header>
            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">
                            <div id="informacion1" onclick="listado('informacion');" class="selectL"><img src="images/colorInfo.png" class="btnIcono2">Información</div>
                            <div id="ventas1" onclick="listado('ventas');" class="listadoOpcion"><img src="images/colorVentas.png" class="btnIcono2">Ventas</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <!--/////////////////////////////////////// Informacion ///////////////////////////////////////////7-->
                    <%if (request.getParameter("list") != null) {%>
                    <div class="listPedido" id="informacion" style="display: none;">
                    <%}else{%>
                    <div class="listPedido" id="informacion">
                    <%}%>
                        <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                        <p class="parrafoListado">
                            Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                        </p>
                        <div class="separadorListado"></div>
                        
                        <div style="margin: 0px 200px;background: white;padding: 10px;">
                                <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                    <tr>
                                        <td class="ladoIzquierdo">Fecha</td>
                                        <td class="ladoDerecho"><%=formato.format(caja.getFecha())%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Cerrada</td>
                                        <%if (caja.getCerrar() != null) {%>
                                        <td class="ladoDerecho"><span class="numPedido"><%=formato.format(caja.getCerrar())%></span></td>
                                        <%}else{%>
                                        <td class="ladoDerecho"><span class="">-----</span></td>
                                        <%}%>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Efectivo</td>
                                        <td class="ladoDerecho"><span class="numPedido"><%=nf.format(caja.getInicio())%></span></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Ventas</td>
                                        <td class="ladoDerecho"><span class=""><%=caja.getVentasList().size()%></span></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Sucursal</td>
                                        <td class="ladoDerecho"><span class=""><%=caja.getSucursalId().getNombre()%></span></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Usuario</td>
                                        <td class="ladoDerecho"><span class=""><%=caja.getUsuarioId().getNombre()%></span></td>
                                    </tr>
                                </table>
                        </div>
                    
                    </div>
                    <!--///////////////////////////////////////////    Ventas   ////////////////////////////////////////////////-->
                    <%if (request.getParameter("list") != null && request.getParameter("list").equals("ventas")) {%>
                    <div class="listPedido" id="ventas">
                    <%}else{%>
                    <div class="listPedido" id="ventas" style="display: none;">
                    <%}%>

                        <h4 class="tituloListado"><img src="images/colorVentas.png" class="iconoTitulo">Ventas</h4>
                        <p class="parrafoListado">
                            Para editar el producto cambia los campos a editar y presiona el boton <strong>Editar Producto</strong>.
                        </p>
                        <div class="separadorListado"></div>

                        <table style="margin-left: 15px;" class="tabla-producto table table-responsive letraChica">
                            <tr class="cabeceraTabla">
                                <th style="width: 60px;text-align: center;"># Venta</th>
                                <th>Fecha</th>
                                <th style="width: 100px; text-align: center;">Productos</th>
                                <th style="width: 200px; text-align: center;">Total</th>
                            </tr>
                            <%int valor = 1;%>
                            <%for (Ventas v : caja.getVentasList()) {%>
                            <tr class="renglon<%=valor % 2%>">
                                <td style="text-align: center;"><strong><%=v.getId()%></strong></td>
                                <td><a class="link" href="venta.jsp?id=<%=v.getId()%>&caja=<%=caja.getId()%>"><div class="btnSelector"><%=formato.format(v.getFecha())%></div></a></td>
                                <%int productos=0; %>
                                <%for(VentasHasProducto vhpf: v.getVentasHasProductoList()){ %>
                                    <%productos+=vhpf.getCantidad(); %>
                                <%}%>
                                <td style="text-align: center;"><strong><%=productos%></strong></td>
                                <td style="text-align: right;" class="saldoTotal"><div><%=nf.format(v.getTotal())%></div></td> 
                                        <%valor++;%>
                            </tr>
                            <%}%>
                        </table>
                    </div>
                    <!--///////////////////////////////////////////////7    Menu    //////////////////////////////////////-->
                </div>
            </div>
        </main>  
        <%@ include file="menu.jsp" %>
        <script>
            function listado(id) {

                document.getElementById('informacion').style.display = "none";
                document.getElementById('informacion1').className = "listadoOpcion";
                document.getElementById('ventas').style.display = "none";
                document.getElementById('ventas1').className = "listadoOpcion";
                document.getElementById(id).style.display = "block";
                document.getElementById(id + "1").className = "selectL";

            }
        </script>
    </body>
</html>
