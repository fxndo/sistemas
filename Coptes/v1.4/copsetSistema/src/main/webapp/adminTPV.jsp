<%-- 
    Document   : adminTPV
    Created on : 9/08/2018, 11:36:43 AM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="Modelo.Caja"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.CajaJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    
    ProductoJpaController pj = new ProductoJpaController(emf);
    CajaJpaController cj = new CajaJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);
    
    Locale locale = new Locale("es", "MX");
    SimpleDateFormat formato2 = new SimpleDateFormat("yyyy-MM-dd");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy hh:mm a", new Locale("es"));
    Date inicio = formato2.parse("1988-07-11 00:00:00");
    Date termino = formato2.parse("1988-07-11 00:00:00");
    
    if (request.getParameter("inicio") != null && request.getParameter("inicio").length() > 0) {
        inicio = formato2.parse(request.getParameter("inicio"));
    }
    if (request.getParameter("termino") != null && request.getParameter("termino").length() > 0) {
        termino = formato2.parse(request.getParameter("termino"));
    }
    int noProductos = 0;
    int pagina = 0;
    int elementos = 20;
    if (request.getParameter("pagina") != null) {
        pagina = Integer.parseInt(request.getParameter("pagina"));
    }

    List<Caja> cajas = cj.busquedaCaja(inicio, termino, pagina);
    noProductos = cj.busquedaCajaCuenta(inicio, termino);


%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Adm TPV | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Administrar TPV<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Administar TPV</div></div>

            </header>
            <div class="codigoBarra row">
                <div class="nombreBarra col-md-2">Periodo de Busqueda</div>
                <form action="adminTPV.jsp">
                    <div class="col-md-1">Inicio:</div>
                    <div class=" col-md-2"><input class="form-control" id="inicio" onchange="modificar();" type="date" step="1" name="inicio"></div>
                    <div class="col-md-1">Termino:</div>
                    <div class="col-md-2"><input class="form-control" id="termino" type="date" step="1" name="termino"></div>
                    <div class="botonBarra col-md-2"><button type="submit" class="btn btn-sm btn-primary btn-group-justified">Buscar</button></div>
                </form>
            </div>
            <%if (cajas.size() > 0) {%>
            <table class="tabla-producto table table-responsive letraChica">
                <tr class="cabeceraTabla">
                    <th style="width: 100px;text-align: center;"># de Caja</th>
                    <th>Fecha</th>
                    <th>Cerrada</th>
                    <th>Inicial</th>
                    <th>Ventas</th>
                    <th>Sucursal</th>
                    <th>Usuario</th>
                </tr>
                <%int renglon = 1;%>
                <%for (Caja cc : cajas) {%>
                <tr class="renglon<%=renglon % 2%>">
                    <td style="text-align: center;"><strong><%=cc.getId()%></strong></td>
                    <td><a href="caja.jsp?id=<%=cc.getId()%>" class="link"><div class="btnSelector"><%=formato.format(cc.getFecha())%></div></a></td>
                    <%if (cc.getCerrar() != null) {%>
                    <td><%=formato.format(cc.getCerrar())%></td>
                    <%} else {%>
                    <td>---</td>
                    <%}%>
                    <td class="saldoTotal"><div><%=nf.format(cc.getInicio())%></div></td>
                    <td class="gris"><strong><%=cc.getVentasList().size()%></strong></td>
                    <td><%=cc.getSucursalId().getNombre()%></td>
                    <td><%=cc.getUsuarioId().getNombre()%></td>
                </tr>
                <%renglon++;%>
                <%}%>
            </table>
            <!----------------------------------------------------Paginacion---------------------------------------------->
            <%if (noProductos > 20) {%>
            <br>
            <div class="text-center">
                <div class="pagina2">Paginas</div>
                <%int paginas = noProductos / elementos;%>
                <%if (noProductos % elementos > 0) {%>
                <%paginas++;%>
                <%}%>
                <%for (int c = 1; c <= paginas; c++) {%>
                <%if (pagina == c - 1) {%>
                <div class="paginaActiva"><%=c%></div>
                <%} else {%>
                <a href="compras.jsp?pagina=<%=c - 1%>&finalizados=on"><div class="pagina"><%=c%></div></a>
                    <%}%>
                    <%}%>
                    <%if (pagina + 1 != paginas) {%>
                <a href="compras.jsp?pagina=<%=pagina + 1%>&finalizados=on"><div class="paginaActiva">Siguiente</div></a>
                <%}%>
            </div>
            <br>
            <%}%>
            <%} else {%>
            <div class="vacioGrande">
                No hay Cajas<br>
                <img src="images/vacioGrande.png" class="imgVacio">
            </div>
            <%}%>
            <!-------------------------------------------------------------------------------------------------->
        </main>
        <%@ include file="menu.jsp" %>
        <script>
            function modificar() {
                document.getElementById('termino').min = document.getElementById('inicio').value;
            }
        </script>
    </body>
</html>
