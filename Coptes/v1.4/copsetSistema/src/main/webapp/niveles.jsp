<%-- 
    Document   : niveles
    Created on : 19/05/2018, 03:00:27 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Nivel"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.NivelJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if(sesion.getAttribute("informacion")!=null){
    }else{
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    
    ProductoJpaController pj = new ProductoJpaController(emf);
    NivelJpaController n=new NivelJpaController(emf);
    PedidoparteJpaController ppan=new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj=new PedidoproveedorJpaController(emf);
    PedidoJpaController p=new PedidoJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);
    
    List<Nivel>niveles=n.nivelLista();
    
    int notificacion=0;
    if(request.getParameter("noti")!=null){
        notificacion=Integer.parseInt(request.getParameter("noti"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Niveles | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
        <header>
            <div class="row">
                <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                <div class="col-md-4"><div class="letrero">Niveles<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
            </div>
            <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Niveles</div></div>

        </header>
                <div class="codigoBarra row">
                    <div class="botonBarra col-md-2"><div onclick="flotante('nuevoNivel');" class="btn btn-default btn-sm btn-group-justified"><img src="images/suma2.png" class="btnIcono">Agregar Nivel</div></div>
            </div>
                <div style="padding: 20px 300px;">
                <table class="tabla-producto table table-responsive letraChica">
                    <tr class="cabeceraTabla">
                        <th style="width: 50px;text-align: center;">#</th>
                        <th>Nombre</th>
                        <th style="text-align: center;">Productos</th>
                    </tr>
                    <%int valor=1;%>
                    <%for(Nivel nv: niveles){%>
                    <tr class="renglon<%=valor%2%>">
                        <td style="text-align: center;"><%=valor%></td>
                        <td><a class="link" href="nivel.jsp?id=<%=nv.getId()%>"><div class="btnSelector"><%=nv.getNombre()%></div></a></td>
                        <td style="padding-left: 15px;"><strong><%=nv.getProductoList().size()%></strong></td>
                    </tr>
                    <%valor++;%>
                    <%}%>
                </table>
                </div>
            </main>
            <div id="fondoNegro" class="fondoNegro" style="display: none;"></div>
        <div id="nuevoNivel" class="bloqueFlotante" style="display: none;">
            <div class="tituloFlotante">Nuevo Cliente</div>
            <form action="agregarNivel" method="post">
                <div class="principalFlotante">
                    <label class="entradaLabel">Nombre</label><input class="entrada" type="text" name="nombre" maxlength="35" required="on">
                    <br><br>
                </div>
                    <div class="botonesFlotante"><button type="submit" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Agregar Nivel</button><div onclick="flotante('nuevoNivel')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
            </form>
        </div>
        <!--////////////////////////////////////////////    Notificacion    ///////////////////////////////////////-->
        <div class="notificacionBloque" style="<%if (notificacion == 6) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Nivel Creado</div>
                El <strong>nivel</strong> se creo satisfactoriamente.
            </div>
        </div>
        <%@ include file="menu.jsp" %>
    </body>
</html>
