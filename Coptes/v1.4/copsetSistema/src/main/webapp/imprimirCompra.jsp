<%-- 
    Document   : imprimirCompra
    Created on : 24/05/2018, 08:59:30 PM
    Author     : fernando
--%>
<%@page import="Modelo.Pago"%>
<%@page import="ModeloJSP.PagoJpaController"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Calendar fecha = new GregorianCalendar();
    Locale locale = new Locale("es","MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy hh:mm a", new Locale("es"));
    PagoJpaController p=new PagoJpaController(emf);
    Pago pago=p.findPago(Integer.parseInt(request.getParameter("idPago")));
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/estilo-ticket.css"/>
        <!--<link rel="stylesheet" href="css/bootstrap.min.css"/>-->
    </head>
    <body>
        <div class="bloqueGeneral">
            <div class="imagen">
                <img src="images/logo.jpg">
            </div><br>
            <div class="sucursal">
                Sucursal:<%=sucursal.getNombre()%><br>
                <%if(sucursal.getId()!=1){%>
                Direccion: Calle<%=sucursal.getDireccionId().getCalle()%> Col. <%=sucursal.getDireccionId().getColonia()%>
                Delegacion <%=sucursal.getDireccionId().getDelegacion()%> C.P. <%=sucursal.getDireccionId().getCp()%>
                <%=sucursal.getDireccionId().getCiudad()%> <%=sucursal.getDireccionId().getEstado()%>
                <%}%>
                
            </div>
            <div class="informacion">
                <h3>Ticket de Compra</h3>
                <span class="venta">No. Pedido: PV-100<%=pago.getPedidoId().getId()%></span><br>
                Fecha: <%=formato.format(fecha.getTime())%>
            </div>
            <br><br>
            <div class="productos">
                <table class="table table-responsive table-bordered table-condensed">
                    <tr><td>Cliente</td><td><%=pago.getPedidoId().getClienteId().getNombre() %></td></tr>
                    <tr><td>Escuela</td><td><%=pago.getPedidoId().getClienteId().getEscuela() %></td></tr>
                    <tr><td>Total</td><td><%=pago.getPedidoId().getTotal()%></td></tr>
                    <%double todo=0;%>
                    <%for(Pago pp: pago.getPedidoId().getPagoList()){%>
                    <tr><td>Abono</td><td><%=pp.getMonto()%></td></tr>
                        <%todo+=pp.getMonto();%>
                    <%}%>
                    <tr><td>Adeudo</td><td><%=pago.getPedidoId().getTotal()-todo%></td></tr>
                        
                    
                </table>
            </div>
                    <br><br>
                    <div class="footer">
                        Le ha atendido <%=informacion.getNombre()%>
                        <hr>
                        GRACIAS ELEGIR COPTES
                        <hr>
                    </div>
            
        </div>
    </body>
</html>
