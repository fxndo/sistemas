<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    
    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    SucursalJpaController scj=new SucursalJpaController(emf);
    
    List<Pedidoproveedor> pedido = pprj.listaCompras();

    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);

    int notificacion = 0;
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Almacen | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Almacen<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">Almacen</div></div>

            </header>

            <div style="padding: 5px;">
            <%if (sucursal.getId() != 1) {%>
                <a href="bodega.jsp?id=<%=sucursal.getId()%>"><div class="btn bbtn btn-sm btn-default"><img src="images/reporte.png" class="btnIcono">Ver Bodega Sucursal</div></a>
            <%}%>
            <%if(informacion.getPermisosList().get(0).getAlmacenp()==1){%>
                <a href="bodegaAlmacen.jsp"><div class="btn btn-sm bbtn btn-default"><img src="images/almacenColor.png" class="btnIcono">Ver Almacen</div></a>
                
            <%}%>
            </div>
            <%if (pedido.size() > 0) {%>
            <div style="padding:0px 5px;">
                <table class="tabla-producto table table-responsive letraChica">
                    <tr class="cabeceraTabla">
                        <th style="width: 50px;">   Pedido</th>
                        <th>Proveedor</th>
                        <th>Nombre</th>
                        <th>Escuela</th>
                        <th style="width: 80px;text-align: center;">Productos</th>
                        <th style="width: 80px;text-align: center;">Faltante</th>
                        <th>Sucursal</th>
                    </tr>
                    <%int renglon = 1;%>
                    <%for (Pedidoproveedor c : pedido) {%>
                    <%if (c.getProveedorId() != null) {%>
                    <tr class="renglon<%=renglon % 2%>">
                        <td style="text-align: center;"><strong><%=c.getPedidoId().getId()%></strong></td>
                        <td><a class="link" href="almacenPedido.jsp?id=<%=c.getId()%>"><div class="btnSelector"><%=c.getProveedorId().getNombre()%></div></a></td>
                        <td><%=c.getPedidoId().getClienteId().getNombre()%></td>
                        <td><%=c.getPedidoId().getClienteId().getEscuela()%></td>
                        <%
                            int totalP = 0;
                            int totalP2 = 0;
                            for (Pedidoparte pp : c.getPedidoparteList()) {
                                totalP += pp.getCantidad();
                                totalP2 += pp.getFaltante();
                            }
                        %>

                        <td class="gris"><strong><%=totalP%></strong></td>
                        <td class="faltante"><div><strong><%=totalP2%></strong></div></td>
                        <td><%=c.getPedidoId().getSucursalId().getNombre()%></td>
                    </tr>
                    <%renglon++;%>
                    <%}%>
                    <%}%>
                </table>
            </div>
            <%} else {%>
            <div class="vacioGrande">
                No hay Pendientes de Almacen<br>
                <img src="images/vacioGrande.png" class="imgVacio">
            </div>
            <%}%>
        </main>
        <!--/////////////////////////////////////////////////   Notificaciones  ///////////////////////////////////-->
        <div class="notificacionBloque" style="<%if (notificacion == 12) {%>display:block;<%} else {%>display:none;<%}%>">
            <div class="notificacionImg">
                <img src="images/notiSuccess.png" class="noti">
            </div>
            <div class="notificacionInfo">
                <div class="titulo">Pedido Entregado</div>
                El <strong>Pedido</strong> fue entregado satisfactoriamente.
            </div>
        </div>
        <%@ include file="menu.jsp" %>
    </body>
</html>

