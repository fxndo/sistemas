<%-- 
    Document   : tpv
    Created on : 17/04/2018, 08:27:52 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="java.math.RoundingMode"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Caja"%>
<%@page import="Modelo.Ventas"%>
<%@page import="ModeloJSP.CajaJpaController"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="Modelo.VentasHasProducto"%>
<%@page import="ModeloJSP.DescuentoJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="ModeloJSP.AlmacenJpaController"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.Producto"%>
<%@page import="java.util.List"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    HttpSession pasada = request.getSession(true);
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursalTPV");
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    AlmacenJpaController a = new AlmacenJpaController(emf);
    List<VentasHasProducto> ventas = new ArrayList<VentasHasProducto>();
    Ventas venta = new Ventas();
    if (sesion.getAttribute("venta") != null) {
        ventas = (List<VentasHasProducto>) sesion.getAttribute("venta");
    }
    if (pasada.getAttribute("pasada") != null) {
        venta = (Ventas) sesion.getAttribute("pasada");
    }
    DescuentoJpaController d = new DescuentoJpaController(emf);

    ProductoJpaController pj = new ProductoJpaController(emf);
    SucursalJpaController scj = new SucursalJpaController(emf);
    CajaJpaController c = new CajaJpaController(emf);
    Caja caja = c.findCaja(c.cerrarCaja(sucursal.getId()));
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);

    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>TPV | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <% if (request.getParameter("imprimir") != null) {%>
    <body id="superBody" class="margenBody" onload="ventana();">
        <%} else {%>
    <body id="superBody" class="margenBody">
        <%}%>

        <%if (c.verificarCaja(sucursal.getId()) == 0) {%>
        <main id="body" class="desenfoque">
            <%} else {%>
            <main id="body">
                <%}%>
                <header>
                    <div class="row">
                        <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                        <div class="col-md-4"><div class="letrero">TPV<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%></span></div></div>
                        <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                        <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                    </div>
                    <div class="navegacion"><a href="inicio.jsp"><div class="navBloque">Inicio</div></a><div class="navegacionSelect">TPV</div></div>
                </header>
                <div class="codigoBarra row">
                    <div class="nombreBarra col-md-2">Código del Producto:</div>
                    <div class="icono col-md-1"><img src="images/teclado.png"></div>
                    <div class="barra col-md-4"><div class="apartado"><input onkeydown="checkearTecla(event);" id="numeroSerial" autocomplete="off" minlength="13" autofocus="true" type="text" name="barra"></div></div>
                    <div class="botonBarra col-md-2"><div onclick="agregarProducto();" class="btn btn-default">Agregar Producto</div></div>
                </div>
                <div class="row botones">
                    <div class="col-md-2"><button onclick="flotante('buscar');" class="btn btn-default btn-group-justified"><img class="btnIcono" src="images/lupa.png">Buscar</button></div>
                    <div class="col-md-2"><a href="borrarVenta"><button class="btn btn-default btn-group-justified"><img class="btnIcono" src="images/borrar.png">Borrar Todo</button></a></div>
                    <div class="col-md-2"><button onclick="flotante('entradaEfectivo');" class="btn btn-default btn-group-justified"><img class="btnIcono" src="images/entrada.png">Entrada Efectivo</button></div>
                    <div class="col-md-2"><button onclick="flotante('salidaEfectivo');" class="btn btn-default btn-group-justified"><img class="btnIcono" src="images/salida.png">Salida Efectivo</button></div>
                    <div class="col-md-2"><button onclick="flotante('cerrarCaja');" class="btn btn-default btn-group-justified"><img class="btnIcono" src="images/salida.png">Cerrar Caja</button></div>
                    <div class="col-md-2"><a href="ventas.jsp"><button class="btn btn-default btn-group-justified"><img class="btnIcono" src="images/salida.png">Ventas</button></a></div>
                </div>
                <div class="ticket">
                    Tiket #1
                </div>
                <div class="table-container">
                    <table id="tabla" class=" tablaTPV table table-responsive letraChica">
                        <tr class="cabeceraTabla">
                            <th>Serie</th>
                            <th>Titulo</th>
                            <th>$ Precio</th>
                            <th>Cantidad</th>
                            <th>Descuento</th>
                            <th>Importe</th>
                            <th>Existencia</th>
                            <th>Op</th>
                        </tr>
                        <%float total = 0;%>
                        <%for (VentasHasProducto pd : ventas) {%>
                        <%if (a.productosAlmacen(pd.getProducto().getId(), sucursal.getId()) > 0) {%>
                        <tr class="noEmpty2">
                            <%} else {%>
                        <tr class="empty">
                            <%}%>

                            <td><%=pd.getProducto().getIsbn()%></td>
                            <td><%=pd.getProducto().getTitulo()%></td>
                            <td class="saldoTotal"><div><%=nf.format(Double.parseDouble(pd.getProducto().getPrecioventa()))%></div></td>
                            <td><div class="btnCantidad" onclick="agregarCantidad(<%=pd.getProducto().getId()%>,<%=a.productosAlmacen(pd.getProducto().getId(), sucursal.getId())%>)"><%=pd.getCantidad()%></div></td>
                            <td><div class="btnDescuento" onclick="agregarDescuento(<%=pd.getProducto().getId()%>);"><%=pd.getDescuento()%>%</div></td>
                            <%double importe = 0;%>
                            <%double descuento = pd.getDescuento();%>
                            <%importe = Double.parseDouble(pd.getProducto().getPrecioventa()) * pd.getCantidad();%>
                            <%importe -= importe * (descuento / 100);%>
                            <td class="saldoVerde"><div><%=nf.format(new BigDecimal(importe).setScale(0, RoundingMode.UP).doubleValue())%></div></td>
                            <td  style="text-align:center;background: #e0e0e0;"><strong><%=a.productosAlmacen(pd.getProducto().getId(), sucursal.getId())%></strong></td>
                            <td><a href="restarTPV?id=<%=pd.getProducto().getId()%>"><div class="btnEliminar"><img class="iconoNormalGrande" src="images/less.png"></div></a></td>
                        </tr>
                        <%total += importe;%>
                        <%}%>


                    </table>
                </div>
                <div class="row resultado">
                    <%if (pasada.getAttribute("pasada") != null) {%>
                    <!--<div class="col-md-2"><a href="imprimirTicket" target="_blank"><div class="btn btn-default">Imprimir Ultimo Ticket</div></a></div>-->
                    <div class="col-md-2"><a href="imprimirVenta.jsp?venta=<%=venta.getId()%>" target="_blank"><div class="btn btn-default">Imprimir Ultimo Ticket</div></a></div>
                    <div class="col-md-1">Total<br><span><%=nf.format(new BigDecimal(venta.getTotal()).setScale(0, RoundingMode.UP).doubleValue())%></span></div>
                        <%
                            String valor = "";
                            String camb = "";
                            try {
                                valor = nf.format(Double.parseDouble(venta.getTipo()));
                                camb = nf.format(Double.parseDouble(venta.getTipo()) - venta.getTotal());

                            } catch (Exception e) {
                                valor = "Tarjeta";
                                camb = "Ninguno";
                            }
                        %>
                    <div class="col-md-1">Pago con<br><span><%=valor%></span></div>
                    <div class="col-md-1">Cambio<br><span><%=camb%></span></div>

                    <%} else {%>
                    <div class="col-md-5"></div>
                    <%}%>

                    <div class="col-md-2"></div>
                    <div class="col-md-2"><button onclick="verificarVenta();" class="btn btn-lg btn-group-justified btn-success"><img class="btnIcono2" src="images/carro.png"> Cobrar</button></div>
                    <div class="col-md-3"><div class="total"><span id="total" class="apartado"><%=nf.format(new BigDecimal(total).setScale(0, RoundingMode.UP).doubleValue())%></span></div></div>
                </div>


                <div id="noProducto" class="mensaje" style="display: none;">Producto no encontrado</div>
                <div id="incompleto" class="mensaje" style="display: none;">La venta no puede ser completada</div>

            </main>

            <div id="fondoNegro" class="fondoNegro" style="display: none;"></div>
            <div id="buscar" class="bloqueFlotante" style="display: none;">
                <div class="tituloFlotante">Buscar</div>
                <div class="principalFlotante">
                    <div style="display: inline-block;"><input style="width: 400px;" id="busquedaTPV" onkeyup="ejecutarBusqueda(event);" type="text" autocomplete="off" placeholder="Buscar Producto" name="objeto" class="form-control"></div> 
                    <button class="btnBuscar" onclick="buscarTPV();"><img src="images/buscar.png" class="iconoNormal"></button>
                    <div style="text-align: center;" id="espera"></div>
                    <div>
                        <table id="listaProductos" class="table table-responsive ">
                        </table>
                    </div>
                </div>
                <div class="botonesFlotante"><div onclick="flotante('buscar'); limpiarBuscar();" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div></div>
            </div>



            <!--/////////////////////////////////////////////////////////////////////////-->

            <%if (c.verificarCaja(sucursal.getId()) == 0) {%>
            <div class="fondoNegro"></div>
            <div id="descuento" class="bloqueFlotante">
                <form action="agregarCaja" method="post">
                    <div class="tituloFlotante">Iniciar Caja</div>
                    <div id="descuentoPrincipal" class="principalFlotante">
                        <label class="entradaLabel">Efectivo en Caja</label><input class="entradaDinero" type="text" required="" name="inicio" placeholder="$0.00"><br>
                        <label class="entradaLabel">Contraseña</label><input class="entrada" type="password" name="pass" required=""><div class="btnAyuda"><img src="images/ayuda.png"><div class="tooltip3">Necesitas la contraseña de un usuario con privilegios de Administrador par Iniciar la caja</div></div><br>
                    </div>
                    <div class="botonesFlotante">
                        <input type="submit" value="Iniciar Caja" class="btn btn-sm btn-primary">
                        <div class="btn btn-sm btn-default"><a href="inicio.jsp"><img src="images/cancelar.png" class="btnIcono">Cancelar</a></div></div>
                </form>
            </div>
            <%}%>
            <!--/////////////////////////////////////////////////////////////////////////-->

            <div id="cobrar" class="bloqueFlotante" style="display: none;">
                <div class="tituloFlotante">Cobrar</div>
                <form action="agregarVenta" method="post" name="formularioPagar">
                    <div class="principalFlotante">
                        
                        <label class="entradaLabel">Total:</label><label id="totalPagar" class="total"><%=nf.format(new BigDecimal(total).setScale(0, RoundingMode.UP).doubleValue())%></label><br>
                        <br><br>
                        <h5 style="margin-left: 210px;">Efectivo</h5>
                        <label class="entradaLabel">Pagó:</label><input id="entrega" autocomplete="off" onkeyup="cambio();" onkeypress="return check(event);" type="text" pattern="[0-9]+(\.[0-9][0-9]?)?" name="entradaEfectivo" class="entradaCobrar" placeholder="0.00"><br>
                        <label class="entradaLabel">Cambio: </label><label class="cambio" id="cambio">$0.00</label><br>
                        <br><br>
                        <h5 style="margin-left: 210px;">Tarjeta</h5>
                        <label class="entradaLabel">Pago: </label><label class="cambio" id="cambio2">$0.00</label><br>
                        <input id="totalTodo" type="hidden" value="<%=new BigDecimal(total).setScale(0, RoundingMode.UP).doubleValue()%>" name="total">
                        <label class="entradaLabel">Combrobante</label><input id="comprobante" type="text" name="comprobante" class="entradaCobrar" placeholder=""><br><br><br>
                    </div>
                    <div class="botonesFlotante">
                        <div onclick="pagar();" class="btn btn-success"><img src="images/carro.png" class="btnIcono">Cobrar cuenta</div>
                        <div onclick="flotante('cobrar');" class="btn btn-default"><img src="images/cancelar.png" class="btnIcono">Cancelar</div>
                    </div>
                </form>
            </div>

            <!--///////////////////////////////////////////////////////////////////////////////-->

            <div id="descuento" class="bloqueFlotante" style="display: none;">
                <div class="tituloFlotante">Agregar Descuento</div>
                <form action="agregarDescuentoTPV" method="post">
                    <div id="descuentoPrincipal" class="principalFlotante">

                    </div>
                    <div class="botonesFlotante">
                        <button type="submit" class="btn btn-success btn-sm"><img src="images/aceptar.png" class="btnIcono">Aceptar</button>
                        <div onclick="flotante('descuento');" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div>
                    </div>
                </form>
            </div>

            <div id="cantidad" class="bloqueFlotante" style="display: none;">
                <div class="tituloFlotante">Nueva Cantidad</div>
                <form action="agregarCantidadTPV" method="post">
                    <div id="cantidadPrincipal" class="principalFlotante">

                    </div>
                    <div class="botonesFlotante">
                        <button type="submit" class="btn btn-success btn-sm"><img src="images/aceptar.png" class="btnIcono">Aceptar</button>
                        <div onclick="flotante('cantidad');" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div>
                    </div>
                </form>
            </div>

            <div id="cerrarCaja" class="bloqueFlotante" style="display: none;">
                <form action="cerrarCaja" method="post">
                    <div class="tituloFlotante">Cerrar Caja</div>
                    <div id="cantidadPrincipal" class="principalFlotante">
                        <label>Efectivo en la caja:</label>
                        <label class="tituloListado2"><%=nf.format(caja.getInicio())%></label><br><br>
                        <label class="entradaLabel">Contraseña:</label><input type="password" name="pass" class="entrada">
                    </div>
                    <div class="botonesFlotante">
                        <button type="submit" class="btn btn-success btn-sm"><img src="images/aceptar.png" class="btnIcono">Aceptar</button>
                        <div onclick="flotante('cerrarCaja');" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div>
                    </div>
                </form>
            </div>

            <div id="entradaEfectivo" class="bloqueFlotante" style="display: none;">
                <form action="entradaEfectivo" method="post">
                    <div class="tituloFlotante">Entrada Efectivo</div>
                    <div id="cantidadPrincipal" class="principalFlotante">
                        <label>Efectivo en la caja:</label>
                        <label class="tituloListado2"><%=nf.format(caja.getInicio())%></label><br><br>
                        <label class="entradaLabel">Entrada:</label><input placeholder="$0.00" class="entradaDinero" type="text" name="monto" pattern="[0-9]+(\.[0-9][0-9]?)?"><br>
                        <label class="entradaLabel">Contraseña:</label><input type="password" name="pass" class="entrada"><br><br>
                        <input type="hidden" name="act" value="1">
                    </div>
                    <div class="botonesFlotante">
                        <button type="submit" class="btn btn-success btn-sm"><img src="images/aceptar.png" class="btnIcono">Aceptar</button>
                        <div onclick="flotante('entradaEfectivo');" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div>
                    </div>
                </form>
            </div>

            <div id="salidaEfectivo" class="bloqueFlotante" style="display: none;">
                <form action="entradaEfectivo" method="post">
                    <div class="tituloFlotante">Salida Efectivo</div>
                    <div id="cantidadPrincipal" class="principalFlotante">
                        <input type="hidden" name="act" value="2">
                        <label>Efectivo en la caja:</label>
                        <label class="tituloListado2"><%=nf.format(caja.getInicio())%></label><br><br>
                        <label class="entradaLabel">Salida:</label><input class="entradaDinero" placeholder="$0.00" type="text" max="<%=caja.getInicio()%>" name="monto" pattern="[0-9]+(\.[0-9][0-9]?)?"><br>
                        <label class="entradaLabel">Contraseña:</label><input type="password" name="pass" class="entrada"><br><br>
                    </div>
                    <div class="botonesFlotante">
                        <button type="submit" class="btn btn-success btn-sm"><img src="images/aceptar.png" class="btnIcono">Aceptar</button>
                        <div onclick="flotante('salidaEfectivo');" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div>
                    </div>
                </form>
            </div>
            <%@ include file="menu.jsp" %>
            <script>
                function checkearTecla(e)
                {
                    if (e.keyCode == 13)
                        agregarProducto();
                    return true;
                }

                function ejecutarBusqueda(e) {
                    (e.keyCode) ? k = e.keyCode : k = e.which;
                    if (k === 13) {
                        buscarTPV();
                    }
                }

                function agregarDescuento(id) {
                    document.getElementById('descuentoPrincipal').innerHTML = "<input required='on' type='hidden' name='id' value='" + id + "'><label class='entradaLabel'>Agregar Descuento</label><input id=\"desc\" type=\"number\" name='descuento' max='15' placeholder='0% - 15%' class=\"entradaDinero\"><br><br>";
                    flotante('descuento');
                    document.getElementById('desc').focus();
                }

                function agregarCantidad(id, max) {
                    document.getElementById('cantidadPrincipal').innerHTML = "<input type='hidden' name='id' value='" + id + "'><label class='entradaLabel'>Agregar Cantidad</label><input id=\"cant\" type=\"number\" name='cantidad'  max='" + max + "' placeholder='Maximo " + max + " Unidades' class=\"entradaDinero\"><br><br>";
                    flotante('cantidad');
                    document.getElementById('cant').focus();

                }
                function cambio() {
                    var total = document.getElementById('totalTodo').value;
                    var entrega = document.getElementById('entrega').value;
                    var cambio = entrega - total;
                    if(cambio>0){
                        document.getElementById('cambio').innerHTML = "$" + cambio;
                    }else{
                        document.getElementById('cambio').innerHTML = "$0.00";
                    }
                    if(cambio<0){
                        document.getElementById('cambio2').innerHTML = "$" + (cambio*-1);
                    }else{
                        document.getElementById('cambio2').innerHTML = "$0.00";
                        
                    }
                    
                }
                function cambioPago(id) {
                    if (id == 1) {
                        document.getElementById('tarjeta').style.display = "none";
                        document.getElementById('efectivo').style.display = "block";
                        document.getElementById('btnEfectivo').className = "select col-md-6 opcionCobrar";
                        document.getElementById('btnTarjeta').className = "col-md-6 opcionCobrar";

                    } else {
                        document.getElementById('efectivo').style.display = "none";
                        document.getElementById('tarjeta').style.display = "block";
                        document.getElementById('btnTarjeta').className = "select col-md-6 opcionCobrar";
                        document.getElementById('btnEfectivo').className = "col-md-6 opcionCobrar";

                    }
                }
            </script>
            <script>
                function pagar() {
                    var pago = parseFloat(document.getElementById('entrega').value);
                    var entrega = document.getElementById('comprobante').value;
                    var total=parseFloat(document.getElementById('totalTodo').value);
                    
                    if (pago>0) {
                        if(pago>=total){
                            document.formularioPagar.submit();
                        }else{
                            if(entrega.length>0){
                            document.formularioPagar.submit();
                                
                            }else{
                            window.alert("Comprobante vacio");
                                
                            }
                        }
                    } else {
                        window.alert("Cantidad vacia");
                    }
                }

            </script>
            <script>
                function ventana() {
                    window.open("imprimirVenta.jsp?venta=<%=venta.getId()%>", "Diseño Web", "width=400, height=600")
                }
            </script>
    </body>
</html>
