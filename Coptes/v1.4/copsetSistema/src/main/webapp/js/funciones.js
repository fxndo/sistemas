/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function mostrar(id) {
    if (document.getElementById(id).className === 'menuEmergente visto') {
        document.getElementById(id).className = 'menuEmergente novisto';
        document.getElementById('fondoNegroMenu').style.display = 'none';
        document.getElementById('body').className = 'enfoque';
        document.getElementById('superBody').className = 'margenBody';
    } else {
        document.getElementById(id).className = 'menuEmergente visto';
        document.getElementById('fondoNegroMenu').style.display = 'block';
        document.getElementById('body').className = 'desenfoque';
        document.getElementById('superBody').className = '';
    }
}
function agregarProducto() {
    if (document.getElementById('numeroSerial').value.length === 13) {
        var serial = document.getElementById('numeroSerial').value;
        var total = "";
        var total2 = 0;
        var total3 = 0;
        $.post("agregarTPV", {serial}, function (data) {
            if (data.length < 6) {
                mensaje('noProducto');
            } else {
                var datos = data.split("-");
                document.getElementById('tabla').innerHTML += datos[0];
                total = datos[1];//efectivo formato
                total2 = datos[2];//efectivo normal
                total3 = total2 + total2 * 0.3;
                document.getElementById('numeroSerial').value = "";

                document.getElementById('total').innerHTML = total;
                document.getElementById('totalPagar').innerHTML = total;
                document.getElementById('totalTodo').value = total2;
                document.getElementById('totalPagar2').innerHTML = datos[3];//tarjeta formato
                document.getElementById('totalTodo2').value = total3;
            }
        });
        document.getElementById('numeroSerial').focus();
    }
}
function flotante(id) {
    if (document.getElementById(id).style.display === 'none') {
        document.getElementById(id).style.display = 'block';
        document.getElementById('fondoNegro').style.display = 'block';
        document.getElementById('body').className = 'desenfoque';
        document.getElementById('superBody').className = '';
    } else {
        document.getElementById(id).style.display = 'none';
        document.getElementById('fondoNegro').style.display = 'none';
        document.getElementById('body').className = '';
        document.getElementById('superBody').className = 'margenBody';
    }
}

function mensaje(id) {
    document.getElementById(id).style.display = 'block';
    setTimeout("quitarMensaje('" + id + "');", 3000);
}
function quitarMensaje(id) {
    document.getElementById(id).style.display = 'none';
}

function buscarTPV() {
    if (document.getElementById('busquedaTPV').value.length > 3) {
        document.getElementById('espera').innerHTML = "<img class='loadingChico' src='images/loading2.gif'>";
        document.getElementById('listaProductos').innerHTML = "";
        var objeto = document.getElementById('busquedaTPV').value;
        $.post("buscarTPV", {objeto}, function (data) {
            if (data.length < 6) {
                document.getElementById('espera').innerHTML = "<h5 class='tituloListado2'>No hay resultados</h5>";

            } else {
                document.getElementById('espera').innerHTML = "";
                document.getElementById('listaProductos').innerHTML = data;

            }
        });
    }
}
function limpiarBuscar() {
    document.getElementById('busquedaTPV').value = "";
    document.getElementById('listaProductos').innerHTML = "";
}
function incluirProducto(isbn) {
    document.getElementById('numeroSerial').value = isbn;
    agregarProducto();
    flotante('buscar');
}
function cobrar() {

}
function verificarVenta() {
    $.post("verificarVenta", {}, function (data) {
        if (data == 1) {
            mensaje('incompleto');
        } else {
            flotante('cobrar');
        }
    });
}

function validarEntradaNumero(e) {
    tecla = e.which || e.keyCode;
    patron = /\d/; // Solo acepta números 
    te = String.fromCharCode(tecla);
    return (patron.test(te) || tecla == 9 || tecla == 8);
}

function check(e) {
//    tecla = (document.all) ? e.keyCode : e.which;
//    if (tecla == 8) {
//        return true;
//    }
//    patron = /[0-9\.]/;
//    tecla_final = String.fromCharCode(tecla);
//    return patron.test(tecla_final);
    tecla = e.which || e.keyCode;
    patron = /[0-9\.]/; // Solo acepta números 
    te = String.fromCharCode(tecla);
    return (patron.test(te) || tecla == 9 || tecla == 8);
}

function mostrarSucursales(){
    var suc=document.getElementById('sucursales');
    if(suc.className==="closeTPV"){
        suc.className="openTPV";
    }else{
        suc.className="closeTPV";
        
    }
}