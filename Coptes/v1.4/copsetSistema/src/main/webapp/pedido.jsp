<%-- 
    Document   : pedido
    Created on : 3/05/2018, 05:03:55 PM
    Author     : fernando
--%>
<%@page import="ModeloJSP.CajaJpaController"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="ModeloJSP.ProveedoreditorialJpaController"%>
<%@page import="ModeloJSP.ProductoJpaController"%>
<%@page import="ModeloJSP.AlmacenJpaController"%>
<%@page import="ModeloJSP.PedidoproveedorJpaController"%>
<%@page import="ModeloJSP.PedidoparteJpaController"%>
<%@page import="Modelo.Pedidoparte"%>
<%@page import="Modelo.Pedidoproveedor"%>
<%@page import="java.util.List"%>
<%@page import="ModeloJSP.SucursalJpaController"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="Modelo.Pago"%>
<%@page import="Modelo.Proveedor"%>
<%@page import="ModeloJSP.DescuentoJpaController"%>
<%@page import="ModeloJSP.ProveedorJpaController"%>
<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="Modelo.Pedido"%>
<%@page import="ModeloJSP.PedidoJpaController"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="Modelo.Sucursal"%>
<%@page import="Modelo.Usuario"%>
<%
    HttpSession sesion = request.getSession(true);
    if (sesion.getAttribute("informacion") != null) {
    } else {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.fernandotellez_copsetSistema_war_1.0PU");
    Usuario informacion = (Usuario) sesion.getAttribute("informacion");
    Sucursal sucursal = (Sucursal) sesion.getAttribute("sucursal");
    Sucursal sucursalTPV;
    SucursalJpaController scj = new SucursalJpaController(emf);
    if (sesion.getAttribute("sucursalTPV") != null) {
        sucursalTPV = (Sucursal) sesion.getAttribute("sucursalTPV");
    } else {
        sucursalTPV = scj.findSucursal(sucursal.getId());
    }

    ProductoJpaController pj = new ProductoJpaController(emf);
    PedidoparteJpaController ppan = new PedidoparteJpaController(emf);
    PedidoJpaController p = new PedidoJpaController(emf);
    ProveedorJpaController pvj = new ProveedorJpaController(emf);
    AlmacenJpaController a = new AlmacenJpaController(emf);
    PedidoproveedorJpaController pprj = new PedidoproveedorJpaController(emf);
    ProveedoreditorialJpaController pej = new ProveedoreditorialJpaController(emf);
    CajaJpaController cj = new CajaJpaController(emf);

    SimpleDateFormat formato = new SimpleDateFormat("dd MMMM yyyy hh:mm a", new Locale("es"));
    SimpleDateFormat formato2 = new SimpleDateFormat("dd MMMM yyyy", new Locale("es"));
    DecimalFormat limitar = new DecimalFormat("#.00");
    Locale locale = new Locale("es", "MX");
    NumberFormat nf = NumberFormat.getCurrencyInstance(locale);

    Pedido pedido = p.findPedido(Integer.parseInt(request.getParameter("id")));
    List<Sucursal> sucursales = scj.findSucursalEntities();
    String list = "informacion";
    if (request.getParameter("list") != null) {
        list = request.getParameter("list");
    }
    int notificacion = 0;
    if (request.getParameter("noti") != null) {
        notificacion = Integer.parseInt(request.getParameter("noti"));
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/favicon.ico" type="image/png">
        <title>Pedido | Coptes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/estiloinicio.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <script src="js/jquery.min.js"></script>
        <script src="js/funciones.js"></script>
    </head>
    <body id="superBody" class="margenBody">
        <main id="body">
            <header>
                <div class="row">
                    <div class="col-md-4"><a href="inicio.jsp"><img class="imgLogo" src="images/logo.png"></a></div>
                    <div class="col-md-4"><div class="letrero">Pedido<br><span class="nombreSucursal">Sucursal <%=sucursal.getNombre()%> <%=sucursalTPV.getId()%></span></div></div>
                    <div class="col-md-3"><a href="perfil.jsp" class="link"><span class="usuarioNombre"><img src="images/user.png" class="btnIcono2"><%=informacion.getNombre()%></span></a></div>
                    <div class="col-md-1"><div onclick="mostrar('menu')" class="iconoMenu"><img class="btnMenu" src="images/menu.png"></div></div>
                </div>
                <div class="navegacion">
                    <%if (request.getParameter("nav") != null && request.getParameter("nav").equals("compras")) {%>
                    <a href="compras.jsp"><div class="navBloque"><img src="images/back.png" class="btnIcono">Atras</div></a>
                            <%}%>
                            <%if (request.getParameter("nav") != null && request.getParameter("nav").equals("cliente")) {%>
                    <a href="cliente.jsp?id=<%=request.getParameter("idCliente")%>&list=pedidos"><div class="navBloque"><img src="images/back.png" class="btnIcono">Atras</div></a>
                            <%}%>
                    <a href="inicio.jsp"><div class="navBloque">Inicio</div></a>
                    <a href="pedidos.jsp"><div class="navBloque">Pedidos</div></a>
                    <div class="navegacionSelect">Pedido</div>
                </div>

            </header>

            <div class="row">
                <div class="col-md-2">
                    <div class="bloqueListado">
                        <div class="listadoTitulo">Apartado</div>
                        <div class="superListado">

                            <div id="informacionLista1" onclick="listado('informacionLista');" class="<%if (list.equals("informacion")) {%><%="selectL"%><%} else {%><%="listadoOpcion"%><%}%>"><img src="images/colorInfo.png" class="btnIcono2">Información</div>

                            <div id="productosListado1" onclick="listado('productosListado');" class="<%if (list.equals("productos")) {%><%="selectL"%><%} else {%><%="listadoOpcion"%><%}%>"><img src="images/colorPedido.png" class="btnIcono2">Productos</div>

                            <%if (informacion.getPermisosList().get(0).getProveedoresp() == 1 && ((pedido.getEstado().equals("1")) || pedido.getEstado().equals("2"))) {%> 
                            <div id="proListado1" onclick="listado('proListado');" class="<%if (list.equals("proveedores")) {%><%="selectL"%><%} else {%><%="listadoOpcion"%><%}%>"><img src="images/colorProveedor.png" class="btnIcono2">Proveedores</div>
                                <%} else {%>
                            <div id="proListado1" onclick="listado('proListado');" style="display: none;" class="listadoOpcion">Proveedores</div>
                            <div class="listadoOpcionDes"><img src="images/colorProveedorGris.png" class="btnIcono2">Proveedores</div>
                                <%}%>

                            <%if (informacion.getPermisosList().get(0).getComprasp() == 1 && (pedido.getEstado().equals("4") || pedido.getEstado().equals("5") || pedido.getEstado().equals("6") || pedido.getEstado().equals("7"))) {%> 
                            <div id="comprasListado1" onclick="listado('comprasListado');" class="<%if (list.equals("compras")) {%><%="selectL"%><%} else {%><%="listadoOpcion"%><%}%>"><img src="images/colorCompras.png" class="btnIcono2">Compras</div>
                                <%} else {%>
                            <div id="comprasListado1" onclick="listado('comprasListado');" style="display: none;" class="listadoOpcion">Compras</div>
                            <div class="listadoOpcionDes"><img src="images/colorComprasGris.png" class="btnIcono2">Compras</div>
                                <%}%>

                            <%if (informacion.getPermisosList().get(0).getRemisionp() == 1 && ((pedido.getEstado().equals("8") || pedido.getEstado().equals("10"))) && pedido.getClienteId().getNumerotarjeta().equals("1")) {%>
                            <div id="remisionListado1" onclick="listado('remisionListado');" class="<%if (list.equals("remision")) {%><%="selectL"%><%} else {%><%="listadoOpcion"%><%}%>"><img src="images/colorRemision.png" class="btnIcono2">Remisión</div>
                                <%} else {%>
                            <div style="display: none;" id="remisionListado1" onclick="listado('remisionListado');" class="listadoOpcion">Remisión</div>
                            <div class="listadoOpcionDes"><img src="images/colorRemisionGris.png" class="btnIcono2">Remisión</div>
                                <%}%>

                            <%if (informacion.getPermisosList().get(0).getAlmacenp() == 1 && (pedido.getEstado().equals("8") || pedido.getEstado().equals("9"))) {%>
                            <div id="trasladoListado1" onclick="listado('trasladoListado');" class="<%if (list.equals("salida")) {%><%="selectL"%><%} else {%><%="listadoOpcion"%><%}%>"><img src="images/colorEntrada.png" class="btnIcono2">Salida/Entrada</div>
                                <%} else {%>
                            <div style="display: none;" id="trasladoListado1" onclick="listado('trasladoListado');" class="listadoOpcion">Salida/Entrada</div>
                            <div class="listadoOpcionDes"><img src="images/colorEntradaGris.png" class="btnIcono2">Salida/Entrada</div>
                                <%}%>


                            <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1 && (pedido.getEstado().equals("11") || pedido.getEstado().equals("12"))) {%>
                            <div id="devolucionListado1" onclick="listado('devolucionListado');" class="<%if (list.equals("devolucion")) {%><%="selectL"%><%} else {%><%="listadoOpcion"%><%}%>"><img src="images/colorDevolucion.png" class="btnIcono2">Devolución</div>
                                <%} else {%>
                            <div id="devolucionListado1" onclick="listado('devolucionListado');" style="display: none;" class="listadoOpcion">Devolución</div>
                            <div class="listadoOpcionDes"><img src="images/colorDevolucionGris.png" class="btnIcono2">Devolución</div>
                                <%}%>

                            <%if (informacion.getPermisosList().get(0).getPagosp() == 1 && !pedido.getEstado().equals("15")) {%>
                            <div id="pagosListado1" onclick="listado('pagosListado');" class="<%if (list.equals("pagos")) {%><%="selectL"%><%} else {%><%="listadoOpcion"%><%}%>"><img src="images/colorPago.png" class="btnIcono2">Pagos</div>
                                <%} else {%>
                            <div id="pagosListado1" onclick="listado('pagosListado');" style="display: none;" class="listadoOpcion">Pagos</div>
                            <div class="listadoOpcionDes"><img src="images/colorPagoGris.png" class="btnIcono2">Pagos</div>
                                <%}%>
                            <div id="opcionesListado1" onclick="listado('opcionesListado');" class="<%if (list.equals("opciones")) {%><%="selectL"%><%} else {%><%="listadoOpcion"%><%}%>"><img src="images/colorOpciones.png" class="btnIcono2">Opciones</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <!--//////////////////////////////////////////////////////////Informacion//////////////////////////////////////////////////-->
                    <%if (request.getParameter("list") != null) {%>
                    <div class="listPedido" id="informacionLista" style="display: none;" >
                        <%} else {%>
                        <div class="listPedido" id="informacionLista" >
                            <%}%>

                            <!--<br>-->
                            <h4 class="tituloListado"><img src="images/colorInfo.png" class="iconoTitulo">Información</h4>
                            <p class="parrafoListado">
                                Es la información detallada del pedido.
                            </p>
                            <!--<br>-->
                            <div class="separadorListado"></div>

                            <div style="margin: 0px 120px;background: white;padding: 10px;">
                                <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                    <tr>
                                        <td class="ladoIzquierdo">No. de Pedido</td>
                                        <td class="ladoDerecho"><span class="numPedido"><%=pedido.getId()%></span></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Fecha de creación</td>
                                        <td class="ladoDerecho"><%=formato.format(pedido.getFecha())%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Fecha de Entrega</td>
                                        <td class="ladoDerecho"><span class="numPedido"><%=formato2.format(pedido.getEntrega())%></span></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Cliente</td>
                                        <td class="ladoDerecho"><%=pedido.getClienteId().getNombre()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Escuela</td>
                                        <td class="ladoDerecho"><%=pedido.getClienteId().getEscuela()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Sucursal</td>
                                        <td class="ladoDerecho"><span class="numPedido"><%=pedido.getSucursalId().getNombre()%></span></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Status</td>
                                        <%if (pedido.getEstado().equals("1")) {%>
                                        <td class="ladoDerecho">Nuevo</td>
                                        <%}
                                            if (pedido.getEstado().equals("2") || pedido.getEstado().equals("3")) {%>
                                        <td class="ladoDerecho">Pendiente</td>
                                        <%}
                                            if (pedido.getEstado().equals("4") || pedido.getEstado().equals("5")) {%>
                                        <td class="ladoDerecho">Asignado</td>
                                        <%}
                                            if (pedido.getEstado().equals("6")) {%>
                                        <td class="ladoDerecho">Proveedor</td>
                                        <%}
                                            if (pedido.getEstado().equals("7")) {%>
                                        <td class="ladoDerecho">Faltante</td>       
                                        <%}
                                            if (pedido.getEstado().equals("8")) {%>
                                        <td class="ladoDerecho">Almacen</td>
                                        <%}
                                            if (pedido.getEstado().equals("9")) {%>
                                        <td class="ladoDerecho">Traslado</td>
                                        <%}
                                            if (pedido.getEstado().equals("10")) {%>
                                        <td class="ladoDerecho">Sucursal</td>
                                        <%}
                                            if (pedido.getEstado().equals("11")) {%>
                                        <td class="ladoDerecho">Entrega</td>
                                        <%}
                                            if (pedido.getEstado().equals("12")) {%>
                                        <td class="ladoDerecho">Finalizado</td>
                                        <%}
                                            if (pedido.getEstado().equals("13")) {%>
                                        <td class="ladoDerecho">Finalizado</td>
                                        <%}
                                            if (pedido.getEstado().equals("14")) {%>
                                        <td class="ladoDerecho">Oculto</td>
                                        <%}
                                            if (pedido.getEstado().equals("15")) {%>
                                        <td class="ladoDerecho">Cancelado</td>
                                        <%}%>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Usuario</td>
                                        <td class="ladoDerecho"><%=pedido.getUsuarioId().getNombre()%></td>
                                    </tr>
                                    <tr>
                                        <td class="ladoIzquierdo">Total</td>
                                        <td class="ladoDerecho"><strong class="total"><%= nf.format(pedido.getTotal())%></strong> MXN</td>
                                    </tr>
                                </table>
                            </div>
                            <br><br><br>
                            <p class="parrafoListado">
                                <strong>Nota: </strong>El total del pedido se actualiza automaticamente, con las devoluciones realizadas.
                            </p>
                        </div>

                        <!--///////////////////////////////////////////////////////////////Productos//////////////////////////////////////////////////-->
                       
                            <%if (request.getParameter("list") != null && request.getParameter("list").equals("productos")) {%>
                            <div class="listPedido"  id="productosListado">
                                <%} else {%>
                                <div class="listPedido"  id="productosListado" style="display: none;">
                                    <%}%>
                                    <!--<br><br>-->
                                    <h4 class="tituloListado"><img src="images/colorPedido.png" class="iconoTitulo">Productos</h4>
                                    <p class="parrafoListado">
                                        Lista de productos en el pedido ordenados por Proveedor.
                                    </p>
                                    <!--<br>-->
                                    <div class="separadorListado"></div>
                                    <table style="margin-left: 15px;" class="table table-responsive tabla-producto letraChica">
                                        <tr class="cabeceraTabla">
                                            <th style="width: 40px;text-align: center;">#</th>
                                            <th style="width: 100px;">ISBN/Serial</th>
                                            <th style="width: 280px;">Titulo</th>
                                            <th>Serie</th>
                                            <th>Proveedor</th>
                                            <th>Traslado</th>
                                            <th style="width: 70px; text-align: center;">$</th>
                                            <th style="width: 40px; text-align: center;">%</th>
                                            <th style="width: 40px;text-align: center;">C</th>
                                            <th style="width: 40px;text-align: center;">F</th>
                                            <th style="width: 40px;text-align: center;">D</th>
                                                <%if (informacion.getPermisosList().get(0).getProveedoresp() == 1 && pedido.getEstado().equals("8")) {%>
                                            <th style="width: 40px;">Eliminar</th>
                                                <%}%>

                                        </tr>
                                        <%int valor2 = 1;%>
                                        <%for (Pedidoproveedor pprf : pedido.getPedidoproveedorList()) {%>
                                        <%for (Pedidoparte ppaf : pprf.getPedidoparteList()) {%>
                                        <tr class="renglon<%=valor2 % 2%>">
                                            <td style="text-align: center;"><strong><%=valor2%></strong></td> 
                                            <td><a class="link2" target="_blank" id="isbnProducto<%=ppaf.getId()%>" href="producto.jsp?id=<%=ppaf.getProductoId().getId()%>"><%=ppaf.getProductoId().getIsbn()%></a></td>
                                            <td><span id="tituloProducto<%=ppaf.getId()%>"><%=ppaf.getProductoId().getTitulo()%></span></td>
                                                <%if (ppaf.getProductoId().getSerie() != null) {%>
                                            <td><%=ppaf.getProductoId().getSerie()%></td>
                                            <%} else {%>
                                            <td><span class="noInformacion">Sin Información</span></td>
                                            <%}%>
                                            <%if (pprf.getProveedorId() != null) {%>
                                            <td class="verde"><%=pprf.getProveedorId().getClave()%></td>
                                            <%} else {%>
                                            <%if(ppaf.getSucursalId()!=null){%>
                                            <td class="azul"><%=ppaf.getSucursalId().getNombre()%> </td>
                                            <%}else{%>
                                            <td class="azul">-----</td>
                                            <%}%>
                                            <%}%>

                                            <%if (ppaf.getEstado() == 2) {%>
                                            <td class="negro">Pendiente</td>
                                            <%} else {%>
                                            <%if (ppaf.getEstado() == 3) {%>
                                            <td class="verde">Aceptado</td>
                                            <%} else {%>
                                            <%if (ppaf.getEstado() == 4) {%>
                                            <td class="rojo">Denegado</td>
                                            <%} else {%>
                                            <td class="superGris">---</td>
                                            <%}%>
                                            <%}%>
                                            <%}%>
                                            <td><strong><%=nf.format(Double.parseDouble(ppaf.getPrecio()))%></strong></td>
                                            <td class="azul" style="text-align: center;"><strong><%=ppaf.getDescuento()%></strong></td>
                                            <td class="gris" title="Cantidad"><strong id="cantidadProveedor<%=ppaf.getId()%>"><%=ppaf.getCantidad()%></strong></td>

                                            <%if (ppaf.getFaltante() > 0) {%>
                                            <td class="faltante" title="Faltante"><div><strong><%=ppaf.getFaltante()%></strong></div></td>
                                                        <%} else {%>
                                            <td class="noFaltante" title="Faltante"><div><strong><%=ppaf.getFaltante()%></strong></div></td>
                                                        <%}%>
                                            <td class="devolucionTotal" title="Devolución"><div><strong><%=ppaf.getDevolucion()%></strong></div></td>
                                                        <%if (informacion.getPermisosList().get(0).getProveedoresp() == 1 && pedido.getEstado().equals("8")) {%>
                                            <td><div onclick="editarProductoPedido('<%=ppaf.getId()%>')" class="btnEliminar"><img src="images/less.png" class="iconoNormal"></div></td>
                                                    <%}%>


                                        </tr>
                                        <%valor2++;%>
                                        <%}%>
                                        <%}%>
                                    </table>
                                </div>    
                                <!--///////////////////////////////////////////////////////////////Proveedores//////////////////////////////////////////////////-->
                                <%if (request.getParameter("list") != null && request.getParameter("list").equals("proveedores")) {%>
                                <div class="listPedido"  id="proListado">
                                    <%} else {%>
                                    <div class="listPedido"  id="proListado" style="display: none;">
                                        <%}%>
                                        <%if (informacion.getPermisosList().get(0).getProveedoresp() == 1 && ((pedido.getEstado().equals("1")) || pedido.getEstado().equals("2"))) {%> 

                                        <!--<br>-->
                                        <h4 class="tituloListado"><img src="images/colorProveedor.png" class="iconoTitulo">Proveedores</h4>
                                        <p class="parrafoListado">
                                            Configura los productos para su compra
                                        </p>
                                        <!--<br>-->
                                        <div class="separadorListado"></div>
                                        <div style="float: right">
                                            <div class="labelSwitch">Cambio descuentos y proveedores</div>
                                            <div id="switchBloque1" onclick="switchf(1)" class="switchBloque switchBlanco">
                                                <div id="switch1" class="switch switchIzquierda"></div>
                                            </div>
                                            <img onclick="flotante('ayuda1');" style="margin-left: 15px;cursor: pointer;" src="images/ayuda.png" class="btnIcono2 labelSwitch">
                                            <br><br>
                                        </div>

                                        <div style="margin-bottom: 15px;">
                                            <div style="margin-left: 15px;" onclick="flotante('agregarProducto');" class="btn btn-default btn-sm"><img src="images/suma2.png" class="btnIcono">Agregar Producto</div>
                                            <!--<img onclick="flotante('ayuda2');" style="margin-left: 15px;float: right;position: relative;top: 15px;cursor: pointer;" src="images/ayuda.png" class="btnIcono2 labelSwitch">-->
                                        </div>
                                        <form action="generarProveedores" method="post">
                                            <input type="hidden" name="idPedido" value="<%=pedido.getId()%>">
                                            <%int valor = 1;%>
                                            <%int verificar = 0;%>
                                            <%for (Pedidoproveedor pv : pedido.getPedidoproveedorList()) {%>

                                            <table style="margin-left: 15px;" class="table table-responsive tabla-producto letraChica">
                                                <tr class="cabeceraTabla">
                                                    <th style="width: 30px;text-align: center;">#</th>
                                                    <th style="width: 100px;">ISBN/Serial</th>
                                                    <th>Titulo</th>
                                                    <td style="width: 70px;">Precio</td>
                                                    <th style="width: 40px;text-align: center;">C</th>

                                                    <th style="width: 110px;text-align: center;">Proveedor</th>
                                                    <th style="width: 40px;text-align: center;">%</th>
                                                    <th style="width: 120px;">Origen</th>
                                                    <th style="width: 50px;">Eliminar</th>
                                                </tr>


                                                <%for (Pedidoparte php : pv.getPedidoparteList()) {%>
                                                <input type="hidden" id="editorial<%=valor%>" name="editorial<%=valor%>" value="<%=php.getProductoId().getEditorialId1().getId()%>">
                                                <input type="hidden" name="idPedidoParte<%=valor%>" value="<%=php.getId()%>">
                                                <tr class="renglon<%=valor % 2%>">
                                                    <%if (pvj.listaProveedoresEditorial(php.getProductoId().getEditorialId1().getId()).size() > 1) {%>
                                                    <td class="bordeRojo" style="text-align: center;"><%=valor%></td>
                                                    <%} else {%>
                                                    <td class="bordeVerde" style="text-align: center;"><%=valor%></td>
                                                    <%}%>
                                                    <td><a  href="producto.jsp?id=<%=php.getProductoId().getId()%>&pedido=<%=pedido.getId()%>"><%=php.getProductoId().getIsbn()%></a></td>
                                                        <%if (php.getProductoId().getEstado().equals("2")) {%>
                                                    <td><%=php.getProductoId().getTitulo()%></td>
                                                    <%} else {%>
                                                    <td><span class="pendienteRojo"><%=php.getProductoId().getTitulo()%></span></td>
                                                        <%verificar = 1;%>
                                                        <%}%>

                                                    <td><strong><%=nf.format(Double.parseDouble(php.getPrecio()))%></strong></td>

                                                    <%int almacen = 0;%>
                                                    <%for (Sucursal ss : scj.listaSucursales()) {
                                                            if (a.productosAlmacen(php.getProductoId().getId(), ss.getId()) > 0) {
                                                                almacen++;
                                                            }
                                                        }%>

                                                    <%if (almacen > 0) {%>
                                                    <td class="verde verdeHover" title="Cantidad"><strong onclick="editarCantidad('<%=php.getId()%>')"><%=php.getCantidad()%></strong></td>
                                                            <%} else {%>
                                                    <td class="gris grisHover" title="Cantidad"><strong onclick="editarCantidad('<%=php.getId()%>')"><%=php.getCantidad()%></strong></td>
                                                        <%}%>


                                                    <td style="padding: 0px;">
                                                        <%if (php.getPedidoProveedorid().getProveedorId() == null && pedido.getEstado().equals("2")) {%>
                                                        <select onchange="cambioProveedor(<%=valor%>);" id="proveedorSelect<%=valor%>" class="entradaTabla" disabled="on" name="idProveedor<%=valor%>">
                                                            <option id="opt<%=valor%>" value="0" selected="on"> ----- </option>
                                                            <%} else {%>
                                                            <select onchange="cambioProveedor(<%=valor%>);" id="proveedorSelect<%=valor%>" class="entradaTabla" name="idProveedor<%=valor%>">
                                                                <%}%>

                                                                <%for (Proveedor pvf : pvj.listaProveedoresEditorial(php.getProductoId().getEditorialId1().getId())) {%>
                                                                <%if (php.getPedidoProveedorid().getProveedorId() != null) {%>
                                                                <%if (pvf.getId() == php.getPedidoProveedorid().getProveedorId().getId()) {%>
                                                                <option class="opcionSelect" selected="on" value="<%=pvf.getId()%>">(<%=pej.proveedorDescuento(php.getProductoId().getEditorialId1().getId(), pvf.getId())%>%)<%=pvf.getClave()%></option>
                                                                <%} else {%>
                                                                <option class="opcionSelect" value="<%=pvf.getId()%>">(<%=pej.proveedorDescuento(php.getProductoId().getEditorialId1().getId(), pvf.getId())%>%)<%=pvf.getClave()%></option>
                                                                <%}%>
                                                                <%} else {%>
                                                                <option class="opcionSelect" value="<%=pvf.getId()%>">(<%=pej.proveedorDescuento(php.getProductoId().getEditorialId1().getId(), pvf.getId())%>%)<%=pvf.getClave()%></option>
                                                                <%}%>

                                                                <%}%>
                                                            </select></td>

                                                    <td style="padding: 0px;"><input id="descuento<%=valor%>"  style="background: #fff3d6;text-align: center;" onfocus="limpiarCantidad('<%=valor%>')" onkeyup="ponerDescuento('<%=valor%>');" title="Descuento" type="text" name="descuento<%=valor%>" class="entradaTabla" value="<%=php.getDescuento()%>" onkeypress="return validarEntradaNumero(event);" autocomplete="off" maxlength="2"></td>

                                                    <td style="padding: 0px;">
                                                        <%if ((php.getEstado() == 1 || php.getEstado() == 2) && pedido.getClienteId().getTipo() == 1) {%>
                                                        <select class="entradaTabla" name="origen<%=valor%>">
                                                            <%} else {%>
                                                            <select class="entradaTabla" disabled="true" name="origen<%=valor%>">
                                                                <%}%>

                                                                <%if (php.getSucursalId() != null) {%>
                                                                <option class="opcionSelect" onclick="liberar('<%=valor%>');" value="0">Proveedor</option>
                                                                <%} else {%>
                                                                <option class="opcionSelect" selected="on" onclick="liberar('<%=valor%>');" value="0">Proveedor</option>
                                                                <%}%>

                                                                <%for (Sucursal ss : scj.listaSucursales()) {%>
                                                                <%if (a.productosAlmacen(php.getProductoId().getId(), ss.getId()) > 0) {%>
                                                                <%if (php.getSucursalId() != null) {%>
                                                                <%if (php.getSucursalId().getId() == ss.getId()) {%>
                                                                <option class="opcionSelect" selected="on" onclick="tapar('<%=valor%>');" value="<%=ss.getId()%>">(<%=a.productosAlmacen(php.getProductoId().getId(), ss.getId())%>) - <%=ss.getNombre()%></option>
                                                                <%} else {%>
                                                                <option class="opcionSelect" onclick="tapar('<%=valor%>');" value="<%=ss.getId()%>">(<%=a.productosAlmacen(php.getProductoId().getId(), ss.getId())%>) - <%=ss.getNombre()%></option>
                                                                <%}%>
                                                                <%} else {%>
                                                                <option class="opcionSelect" onclick="tapar('<%=valor%>');" value="<%=ss.getId()%>">(<%=a.productosAlmacen(php.getProductoId().getId(), ss.getId())%>) - <%=ss.getNombre()%></option>
                                                                <%}%>


                                                                <%}%>
                                                                <%}%>

                                                            </select></td>
                                                            <td><div onclick="eliminarParte('<%=php.getId()%>')" class="btnEliminar"><img src="images/less.png" class="iconoNormal"></div></td>
                                                </tr>
                                                <%valor++;%>
                                                <%}%>
                                            </table>
                                            <%}%>
                                            <input id="cantidad" type="hidden" name="cantidad" value="<%=valor%>">
                                            <%if (verificar == 0) {%>
                                            <button style="margin-left: 15px;" class="btn btn-sm btn-default" type="submit"><img src="images/editarColor.png" class="btnIcono">Asignar Proveedores</button>
                                                <%} else {%>
                                            <span class="parrafoListado"><strong>Nota:</strong> Antes de continuar completa la información del producto pendiente. (Se muetra en color rojo)</span>
                                            <%}%>
                                            <%if (pedido.getEstado().equals("2")) {%>
                                            <a href="aceptarConfiguracion?id=<%=pedido.getId()%>"><div style="margin-left: 15px;" class="btn btn-sm btn-default"><img src="images/guardar.png" class="btnIcono">Aceptar Configuración</div></a>
                                                    <%}%>
                                        </form>
                                        <br><br><br>
                                        <%}%>
                                    </div>
                                    <!--////////////////////////////////////////////////////////Remision//////////////////////////////////////////////////////-->

                                    <%if (request.getParameter("list") != null && request.getParameter("list").equals("remision")) {%>
                                    <div class="listPedido"  id="remisionListado">
                                        <%} else {%>
                                        <div class="listPedido"  id="remisionListado" style="display: none">
                                            <%}%>
                                            <%if (informacion.getPermisosList().get(0).getRemisionp() == 1 && ((pedido.getEstado().equals("8") || pedido.getEstado().equals("10"))) && pedido.getClienteId().getNumerotarjeta().equals("1")) {%>

                                            <!--<br>-->
                                            <h4 class="tituloListado"><img src="images/colorRemision.png" class="iconoTitulo">Remisión</h4>
                                            <p class="parrafoListado">
                                                Remisiona el pedido para la entrega al cliente ó imprime el reporte de remisión para el cliente.
                                            </p>
                                            <!--<br>-->
                                            <div class="separadorListado"></div>
                                            <%if (pedido.getEstado().equals("7") || pedido.getEstado().equals("8") || pedido.getEstado().equals("10")) {%>
                                            <p class="parrafoListado">
                                                Cambia el estado del pedido a <strong>Entrega</strong> para entregar al cliente.
                                            </p>
                                            <%if (Integer.parseInt(pedido.getEstado()) != 2) {%>
                                            <label class="apaB"> </label><a href="agregarRemision?idPedido=<%=pedido.getId()%>"><div class="bbtn btn btn-success btn-sm"><img src="images/remision.png" class="btnIcono">Remisión</div></a>
                                            <!--<div onclick="flotante('traspasar');" class="bbtn btn btn-primary"><img src="images/traspasar.png" class="btnIcono">Traspasar</div>-->
                                            <%} else {%>
                                            <label class="apaB"> </label><a href="completarRemision?id=<%=pedido.getId()%>"><div class="btn btn-primary btn-sm">Completar Remision de Sucursal</div></a>
                                            <%}%>

                                            <br><br><br><br><br>
                                            <%}%>
                                            <p class="parrafoListado">
                                                Genera el reporte de la remision para imprimir en formato Excel.
                                            </p>
                                            <label class="apaB"> </label><a href="reporteRemision?tipo=remision&id=<%=pedido.getId()%>"><div class="bbtn btn btn-primary"><img src="images/imprimir.png" class="btnIcono">Imprimir Remisión</div></a>

                                            <%}%>
                                        </div>
                                        <!--////////////////////////////////////////////////Entrada y Salida////////////////////////////////////////////////////////-->
                                        <%if (request.getParameter("list") != null && request.getParameter("list").equals("entrada")) {%>
                                        <div class="listPedido"  id="trasladoListado">
                                            <%} else {%>
                                            <div class="listPedido"  id="trasladoListado" style="display: none;">
                                                <%}%>
                                                <%if (informacion.getPermisosList().get(0).getAlmacenp() == 1 && (pedido.getEstado().equals("8") || pedido.getEstado().equals("9"))) {%>

                                                <h4 class="tituloListado"><img src="images/colorEntrada.png" class="iconoTitulo">Salida/Entrada</h4>
                                                <p class="parrafoListado">
                                                    Cambia el estatus del pedido cuando salga del almacen a una sucursal y/o cuando llege a la sucursal indicada.
                                                </p>
                                                <div class="separadorListado"></div>
                                                <br><br>
                                                <!--<div class="text-center">-->
                                                <%if (pedido.getEstado().equals("8") && sucursal.getId() == 1) {%>
                                                <label class="apaB"> </label><a href="agregarTraslado?id=<%=pedido.getId()%>"><div class="btn btn-success bbtn">Iniciar Traslado</div></a>
                                                <%}
                                                    if (pedido.getEstado().equals("9") && (sucursal.getId() == pedido.getSucursalId().getId() || informacion.getSucursalId().getId() == 1)) {%>
                                                <label class="apaB"> </label><a href="terminarTraslado?id=<%=pedido.getId()%>"><div class="btn btn-warning bbtn">Terminar Traslado</div></a>
                                                <%}%>
                                                <!--</div>-->
                                                <br><br><br><br>
                                                <p class="parrafoListado">
                                                    Nota: No podras generar una remision hasta que el pedido este en la sucursal.
                                                </p>
                                                <%}%>
                                            </div>
                                            <!--///////////////////////////////////////////////////////////Compras///////////////////////////////////////////////////////////-->
                                            <%if (request.getParameter("list") != null && request.getParameter("list").equals("compras")) {%>
                                            <div class="listPedido"  id="comprasListado">
                                                <%} else {%>
                                                <div class="listPedido"  id="comprasListado" style="display: none;">
                                                    <%}%>
                                                    <%if (informacion.getPermisosList().get(0).getComprasp() == 1 && (pedido.getEstado().equals("4") || pedido.getEstado().equals("5") || pedido.getEstado().equals("6") || pedido.getEstado().equals("7"))) {%> 

                                                    <!--<br>-->
                                                    <h4 class="tituloListado"><img src="images/colorCompras.png" class="iconoTitulo">Compras</h4>
                                                    <p class="parrafoListado">
                                                        Imprime el reporte de "Compra a Proveedor" para marcarlo como impreso y quitarlo de la lista de Compras Pendientes.
                                                    </p>
                                                    <!--<br>-->
                                                    <div class="separadorListado"></div>
                                                    <table style="margin-left: 15px;" class="table table-responsive table-striped tabla-producto letraChica">
                                                        <tr class="cabeceraTabla">
                                                            <th style="padding-left: 10px;">Proveedor</th>
                                                            <th style="width: 70px;">Productos</th>
                                                            <th style="width: 70px;">Faltantes</th>
                                                            <th>Total</th>
                                                            <th style="width: 70px;">Imprimir</th>
                                                            <th style="width: 70px;">Estado</th>
                                                        </tr>
                                                        <%if (!pedido.getEstado().equals("1")) {%>
                                                        <%for (Pedidoproveedor cp : pedido.getPedidoproveedorList()) {%>
                                                        <%
                                                            int traslado = 0;
                                                            int totala = 0;
                                                            int totalb = 0;
                                                            int totalc = 0;
                                                            for (Pedidoparte pp : cp.getPedidoparteList()) {
                                                                totala += pp.getCantidad();
                                                                totalb += pp.getCantidad() * Double.parseDouble(pp.getPrecio()) - (Double.parseDouble(pp.getPrecio()) * pp.getDescuentoProveedor() / 100);
                                                                totalc += pp.getFaltante();
                                                                if (pp.getEstado() == 2) {
                                                                    traslado = 1;
                                                                }
                                                            }
                                                        %>
                                                        <tr>
                                                            <%if (cp.getProveedorId() != null) {%>
                                                            <td  style="padding-left: 10px;"><%=cp.getProveedorId().getNombre()%></td>
                                                            <%} else {%>
                                                            <td>Traslado</td>
                                                            <%}%> 
                                                            <td style="text-align:center;background: #e0e0e0;border-bottom: 1px solid #d1d1d1;"><div><strong><%=totala%></strong></div></td>
                                                            <td class="faltante"><div><strong><%=totalc%></strong></div></td>
                                                            <td class="saldoVerde"><div><%=nf.format(totalb)%></div></td>
                                                                    <%if (traslado == 0) {%>
                                                            <td style="text-align: center;"><div onclick="imprimirCompra('<%=cp.getId()%>');" class="btnImprimir"><img src="images/imprimir3.png" class="iconoNormalGrande"></div></td>
                                                                    <%} else {%>
                                                            <td class="faltante"><div>Traslado</div></td>
                                                            <%}%>
                                                            <%if (cp.getEstado() == 1 || cp.getEstado() == 3) {%>
                                                            <td style="text-align: center;" id="marca<%=cp.getId()%>"></td>
                                                            <%} else {%>
                                                            <td style="text-align: center;"><img src="images/sipermiso.png" class="iconoNormalGrande"></td>
                                                                <%}%>
                                                        </tr>
                                                        <%}%>
                                                        <%}%>
                                                    </table>
                                                    <%}%>
                                                </div>
                                                <!--/////////////////////////////////////////////////////////////Devolucion/////////////////////////////////////////////////-->
                                                <%int valorDevolucion = 1;%>
                                                <%int proP = 1;%>
                                                <%if (request.getParameter("list") != null && request.getParameter("list").equals("devolucion")) {%>
                                                <div class="listPedido"  id="devolucionListado">
                                                    <%} else {%>
                                                    <div class="listPedido"  id="devolucionListado" style="display: none;">
                                                        <%}%>
                                                        <%if (informacion.getPermisosList().get(0).getAdministracionp() == 1 && (pedido.getEstado().equals("11") || pedido.getEstado().equals("12"))) {%>

                                                        <h4 class="tituloListado"><img src="images/colorDevolucion.png" class="iconoTitulo">Devolución</h4>
                                                        <p class="parrafoListado">
                                                            Actualiza los registros de <strong>Devolción</strong> dando clic en <img src="images/opcion2.png" class="btnIcono">e imprime el reporte.
                                                        </p>
                                                        <div class="separadorListado"></div>
                                                        <p class="parrafoListado">
                                                            <strong>Nota</strong>: Si el registro del proveedor no tiene devoluciones no hace falta modificarlo.
                                                        </p><br>
                                                        <div style="padding: 10px 25px;">

                                                            <form action="devolucionPedidoProveedor" method="post" name="devolucionFormulario">
                                                                <input type="hidden" name="idPedido" value="<%=pedido.getId()%>">
                                                                <%for (Pedidoproveedor pprf : pedido.getPedidoproveedorList()) {%>

                                                                <%int totalDevo = 0;%>
                                                                <%for (Pedidoparte pp : pprf.getPedidoparteList()) {%>
                                                                <%totalDevo += Double.parseDouble(pp.getPrecio()) * (pp.getCantidad() - pp.getFaltante());%>
                                                                <%}%>

                                                                <%if (pprf.getProveedorId() != null) {%>
                                                                <div class="textoAzul"><%=pprf.getProveedorId().getNombre()%><div class="semaforo"><div class="totalDevo" id="totalDevo<%=proP%>">Total: <strong><%=nf.format(totalDevo)%></strong></div><div class="totalDevo" id="precioDevolucion">Devolucion: <strong>$0</strong></div><div id="semaforoA<%=proP%>" class="">0%</div></div></div>
                                                                <%} else {%>
                                                                <div class="textoAzul">Sucursal<div class="semaforo"><div id="semaforoA<%=proP%>" class="">0%</div></div></div>
                                                                <%}%>

                                                                <table class="table table-responsive tabla-producto letraChica">
                                                                    <tr class="cabeceraTabla">
                                                                        <th style="width:50px;text-align: center;">#</th>
                                                                        <th style="width: 100px;">Isbn</th>
                                                                        <th>Titulo</th>
                                                                        <th style="width: 100px;">Serie</th>
                                                                        <th style="width: 100px;">Precio</th>
                                                                        <th style="width: 70px;">Cantidad</th>
                                                                        <th style="width: 50px;text-align: center;">Dev</th>
                                                                    </tr>

                                                                    <%for (Pedidoparte pp : pprf.getPedidoparteList()) {%>
                                                                    <tr class="renglon<%=valorDevolucion % 2%>">
                                                                        <td style="text-align: center;"><strong><%=valorDevolucion%></strong></td>
                                                                        <td><a href="producto.jsp?id=<%=pp.getId()%>"><%=pp.getProductoId().getIsbn()%></a></td>
                                                                        <td><%=pp.getProductoId().getTitulo()%></td>
                                                                        <%if (pp.getProductoId().getSerie() != null) {%>
                                                                        <td><%=pp.getProductoId().getSerie()%></td>
                                                                        <%} else {%>
                                                                        <td><span class="noInformacion">Sin Información</span></td>
                                                                        <%}%>
                                                                    <input type="hidden" id="precioProducto<%=proP%><%=valorDevolucion%>" value="<%=pp.getPrecio()%>">
                                                                    <td class="azul"><div><%=nf.format(Double.parseDouble(pp.getPrecio()))%></div></td>
                                                                    <td style="text-align:center;background: #e0e0e0;border-bottom: 1px solid #d1d1d1;"><strong id="cantidadProducto<%=proP%><%=valorDevolucion%>"><%=pp.getCantidad()%></strong></td>
                                                                        <%if (pedido.getEstado().equals("11")) {%>
                                                                    <td style="padding: 0px;"><input onfocus="limpiar('<%=proP%><%=valorDevolucion%>')" onkeyup="cambiarColor('<%=proP%>', '<%=valorDevolucion%>')" id="cantidadD<%=proP%><%=valorDevolucion%>" name="cantidad<%=valorDevolucion%>" value="<%=pp.getDevolucion()%>" max="<%=pp.getCantidad()%>" required="on" autocomplete="off" title="Devolución" class="entradaTablac" type="number"></td>
                                                                        <%} else {%>
                                                                    <td style="padding: 0px;"><input value="<%=pp.getDevolucion()%>" disabled="true" title="Devolución" class="entradaTablac" type="number"></td>
                                                                        <%}%>
                                                                    <input type="hidden" name="idPro<%=valorDevolucion%>" value="<%=pp.getId()%>">
                                                                    </tr>
                                                                    <%valorDevolucion++;%>
                                                                    <%}%>
                                                                    <%proP++;%>
                                                                </table>
                                                                <%}%>

                                                                <%if (pedido.getEstado().equals("11")) {%>
                                                                <div style="margin-left: 0px;" onclick="checarFormulario()" class="btn btn-default btn-sm"><img src="images/sipermiso.png" class="btnIcono">Aceptar Devolución</div>
                                                                    <%}%>

                                                                <%if (pedido.getEstado().equals("12")) {%>
                                                                <a href="reportedevolucion?id=<%=pedido.getId()%>"><div class="btn btn-sm btn-default"><img src="images/devolucion.png" class="btnIcono">Reporte devolución</div></a>
                                                                        <%}%>
                                                            </form>
                                                        </div>
                                                        <br><br><br>
                                                        <%}%>
                                                    </div>

                                                    <!--////////////////////////////////////////////////////////////Pagos////////////////////////////////////////////////////////-->

                                                    <%double totalPedido = pedido.getTotal();%>
                                                    <%if (request.getParameter("list") != null && request.getParameter("list").equals("pagos")) {%>
                                                    <div class="listPedido"  id="pagosListado" style="display: block;">
                                                        <%} else {%>
                                                        <div class="listPedido"  id="pagosListado" style="display: none;">
                                                            <%}%>

                                                            <%if (informacion.getPermisosList().get(0).getPagosp() == 1 && !pedido.getEstado().equals("15")) {%>

                                                            <!--<br>-->
                                                            <h4 class="tituloListado"><img src="images/colorPago.png" class="iconoTitulo">Pagos</h4>
                                                            <p class="parrafoListado">
                                                                Escribe la cantidad a abonar al pedido.<br>
                                                            </p>
                                                            <!--<br>-->
                                                            <div class="separadorListado"></div>
                                                            <%if (pedido.getPagoList().size() > 0) {%>
                                                            <table style="margin-left: 15px;" class="table table-responsive tabla-producto letraChica">
                                                                <tr class="cabeceraTabla">
                                                                    <th style="width: 50px;text-align: center;">#</th> 
                                                                    <th>Fecha</th>
                                                                    <th>Monto</th>
                                                                    <th style="padding-left: 15px;">Usuario</th>
                                                                    <th style="width: 80px;">No. Recivo</th>
                                                                    <th style="width: 70px;">Imprimir</th>
                                                                </tr>
                                                                <%int total = 1;%>
                                                                <%for (Pago pg : pedido.getPagoList()) {%>
                                                                <%totalPedido -= pg.getMonto();%>
                                                                <tr class="renglon<%=total % 2%>">
                                                                    <td style="text-align: center;"><strong><%=total%></strong></td>
                                                                    <td><%=formato.format(pg.getFecha())%></td>
                                                                    <td class="saldoVerde"><div><%= nf.format(pg.getMonto())%></div></td>
                                                                    <td style="padding-left: 15px;"><%=pg.getUsuarioId().getNombre()%></td>
                                                                    <td class="gris"><strong><%=pg.getId()%></strong></td>
                                                                    <td style="text-align: center;"><a target="_blank" href="imprimirCompra.jsp?idPago=<%=pg.getId()%>"><div class="btnImprimir"><img src="images/imprimir3.png" class="iconoNormalGrande"></div></a></td>
                                                                </tr>
                                                                <%total++;%>
                                                                <%}%>
                                                            </table>
                                                            <%} else {%>
                                                            <div class="vacioGrande">
                                                                No hay Pagos<br>
                                                                <img src="images/vacioGrande.png" class="imgVacio">
                                                            </div>
                                                            <%}%>

                                                            <hr>
                                                            <div class="text-center">

                                                                <label>Total:<span class="sutil"> <%= nf.format(pedido.getTotal())%></span></label>&nbsp;&nbsp;&nbsp;
                                                                <label>Resta:<span class="sutil"> <%= nf.format(totalPedido)%></span></label><br><br>
                                                                <%if (totalPedido > 0) {%>
                                                                <div onclick="flotante('nuevoPago');" class="bbtn btn btn-default btn-sm"><img src="images/pagar.png" class="btnIcono2">Agregar Pago</div>
                                                                    <%}%>
                                                            </div>
                                                            <br><br><br><br>
                                                            <p class="parrafoListado">
                                                                <strong>Nota: </strong>Si la cantidad a pagar es mayor al adeudo el pago no se realizara.
                                                            </p>    
                                                            <%}%>
                                                        </div>
                                                        <!--/////////////////////////////////////////////////////////////Opciones//////////////////////////////////////////////////-->
                                                        <%if (request.getParameter("list") != null && request.getParameter("list").equals("opciones")) {%>
                                                        <div class="listPedido"  id="opcionesListado" style="display: block;">
                                                            <%} else {%>
                                                            <div class="listPedido"  id="opcionesListado" style="display: none;">
                                                                <%}%>

                                                                <!--<br>-->
                                                                <h4 class="tituloListado"><img src="images/colorOpciones.png" class="iconoTitulo">Opciones</h4>
                                                                <div class="separadorListado"></div>
                                                                <p class="parrafoListado">
                                                                    Imprime el ticket de compra del pedido
                                                                </p>
                                                                <!--<br>-->
                                                                <!--<div class="separadorListado"></div>-->
                                                                <label class="apaB"> </label><a href="imprimirPedido.jsp?id=<%=pedido.getId()%>" target="_blank"><div class="bbtn btn btn-default btn-sm"><img src="images/reporte.png" class="btnIcono">Imprimir Ticket</div></a>
                                                                <br><br>
                                                                <div class="separadorListado2"></div>
                                                                <p class="parrafoListado">
                                                                    Genera el reporte del Pedido
                                                                </p>
                                                                <!--<br>-->
                                                                <label class="apaB"> </label><a href="reporteRemision?tipo=pedido&id=<%=pedido.getId()%>"><div class="bbtn btn btn-default btn-sm"><img src="images/imprimirColor.png" class="btnIcono">Imprimir Pedido</div></a>
                                                                <br><br>
                                                                <%if (informacion.getPermisosList().get(0).getPedidosp() == 1) {%>
                                                                <%if (pedido.getEstado().equals("1") || pedido.getEstado().equals("2") || pedido.getEstado().equals("3") || pedido.getEstado().equals("4")) {%>
                                                                <div class="separadorListado2"></div>
                                                                <p class="parrafoListado">
                                                                    Al cancelar el pedido, no saldra en la lista de pedidos, y no afectara el saldo del cliente.
                                                                </p>
                                                                <label class="apaB"> </label><div onclick="flotante('cancelarPedido')" class="bbtn btn btn-default btn-sm"><img src="images/less.png" class="btnIcono"> Cancelar Pedido</div>
                                                                <br><br>
                                                                <div class="separadorListado2"></div>        
                                                                <%}%>

                                                                <%if ((pedido.getEstado().equals("11") || pedido.getEstado().equals("12") || pedido.getEstado().equals("13"))) {%>
                                                                <p class="parrafoListado">
                                                                    Oculta el pedido para que ya no aparezca en la lista de pedidos. Lo puedes volver a encontrar marcando la casilla  "Ocultos y cancelados".
                                                                </p>
                                                                <label class="apaB"> </label><a href="finalizarPedido?id=<%=pedido.getId()%>" class="bbtn btn btn-default btn-sm"><img src="images/ocultar.png" class="btnIcono">Ocultar Pedido</a>
                                                                    <%}%>

                                                                <%}%>

                                                                <%if (pedido.getEstado().equals("5") || pedido.getEstado().equals("6") || pedido.getEstado().equals("7") || pedido.getEstado().equals("8") || pedido.getEstado().equals("9") || pedido.getEstado().equals("10") || pedido.getEstado().equals("11")) {%>
                                                                <div class="separadorListado2"></div>
                                                                <p class="parrafoListado">
                                                                    Al forzar la cancelación del pedido, no aparecera el pedido en la lista de pedidos, y si el pedido esta en almacen se ira a stock.
                                                                </p>
                                                                <label class="apaB"> </label><div onclick="flotante('cancelarFuerza')" class="bbtn btn btn-default btn-sm"><img src="images/less.png" class="btnIcono">Forzar cancelación</div>
                                                                <br><br>
                                                                <div class="separadorListado2"></div>        
                                                                <%}%>

                                                                <%if ((pedido.getEstado().equals("8") || pedido.getEstado().equals("10")) && pedido.getClienteId().getTipo() == 2) {%>
                                                                <p class="parrafoListado">
                                                                    Pasa el pedido al inventario de la sucursal para que este disponible en venta en general y aparesca en TPV.
                                                                </p>
                                                                <label class="apaB"> </label><div onclick="flotante('aBodega')" class="bbtn btn btn-warning btn-sm"><img src="images/stock.png" class="btnIcono">Pasar A bodega</div>
                                                                    <%}%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </main>
                                                    <!--////////////////////////////////////Flotante//////////////////////////////////////////-->
                                                    <div id="fondoNegro" class="fondoNegro" style="display: none;"></div>
                                                    <div id="aBodega" class="bloqueFlotante" style="display: none;">
                                                        <div class="tituloFlotante">Traspaar a Bodega</div>
                                                        <form action="pasarBodega" method="post">
                                                            <div class="principalFlotante">

                                                                <input type="hidden" name="idPedido" value="<%=pedido.getId()%>">
                                                                <label class="entradaLabel">Sucursal</label>
                                                                <select class="entrada" name="idSucursal">

                                                                    <%for (Sucursal ss : sucursales) {%>
                                                                    <%if (ss.getId() != 1) {%>
                                                                    <%if (ss.getId() == pedido.getSucursalId().getId()) {%>
                                                                    <option selected="" value="<%=ss.getId()%>"><%=ss.getNombre()%></option>
                                                                    <%} else {%>
                                                                    <option value="<%=ss.getId()%>"><%=ss.getNombre()%></option>
                                                                    <%}%>
                                                                    <%}%>
                                                                    <%}%>
                                                                </select>
                                                            </div>
                                                            <div class="botonesFlotante"><button type="submit" class="btn btn-sm btn-primary"><img src="images/aceptar.png" class="btnIcono">Aceptar</button><div onclick="flotante('aBodega')" class="btn btn-sm btn-default"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                                                        </form>
                                                    </div>

                                                    <div id="nuevoPago" class="bloqueFlotante" style="display: none;">
                                                        <div class="tituloFlotante">Nuevo Pago</div>
                                                        <div class="principalFlotante">
                                                            <span class="tituloFlotante"><%= nf.format(totalPedido)%></span>
                                                            <form action="agregarPago" method="post">
                                                                <label class="entradaLabel">Monto $:</label><input class="entradaDinero" type="text" name="monto" onkeypress="return check(event);" autocomplete="off" placeholder="0.00" pattern="[0-9]+(\.[0-9][0-9]?)?">
                                                                <input type="hidden" name="idPedido" value="<%=pedido.getId()%>">
                                                                <br><br>
                                                                <%if (cj.cerrarCaja(sucursalTPV.getId()) > 0 && informacion.getPermisosList().get(0).getTpvp() == 1) {%>
                                                                <div style="margin-left: 215px;" id="switchBloque99" onclick="switchf(99)" class="switchBloque switchVerde">
                                                                    <div id="switch99" class="switch switchDerecha"></div>
                                                                </div>
                                                                <input id="input99" class="form-control" type="hidden" name="accion" value="1">
                                                                <label style="margin-left: 10px;" onclick="switchf(99);" class="labelSwitch">Agregar el pago a la caja</label>
                                                                <%}%>

                                                                <br><br>
                                                                </div>
                                                                <div class="botonesFlotante"><button type="submit" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Agregar Pago</button><div onclick="flotante('nuevoPago')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                                                            </form>   
                                                        </div>

                                                        <div id="traspasar" class="bloqueFlotante" style="display: none;">
                                                            <div class="tituloListado2 ">Traspasar Pedido</div>
                                                            <div class="principalFlotante">
                                                                <form action="traspasar" method="post">
                                                                    <input type="hidden" name="idPedido" value="<%=pedido.getId()%>">
                                                                    <label>Sucursal a traspasar<select name="idSucursal" class="form-control">
                                                                            <%for (Sucursal sc : sucursales) {%>
                                                                            <%if (sc.getId() != pedido.getSucursalId().getId() && sc.getId() != 1) {%>
                                                                            <option value="<%=sc.getId()%>"><%=sc.getNombre()%></option>
                                                                            <%}%>
                                                                            <%}%>
                                                                        </select></label>
                                                                    <br><br>
                                                                    <div class="botonesFlotante"><button type="submit" class="btn btn-primary"><img src="images/aceptar.png" class="btnIcono">Traspasar</button>
                                                                        <div onclick="flotante('traspasar')" class="btn btn-default"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                                                                </form>   
                                                            </div>
                                                        </div>

                                                        <div id="editarCantidad" class="bloqueFlotante" style="display: none;">
                                                            <div class="tituloFlotante">Editar Cantidad</div>
                                                            <form action="editarCantidad" method="post">
                                                                <div class="principalFlotante">
                                                                    <input id="idPedidoParte" type="hidden" name="idPedidoParte" value="">

                                                                    <div style="margin: 0px 20px;background: white;padding: 10px;">
                                                                        <table class="table table-responsive table-striped" style="box-shadow: rgba(0,0,0,0.5);">
                                                                            <tr>
                                                                                <td class="ladoIzquierdo">ISBN</td>
                                                                                <td class="ladoDerecho"><span id="isbnEditar"></span></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="ladoIzquierdo">Titulo</td>
                                                                                <td class="ladoDerecho"><span id="tituloEditar"></span></td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <br>
                                                                    <label class="entradaLabel">Cantidad</label><input onkeypress="return validarEntradaNumero(event);" required="on" maxlength="3" name="cantidad" type="text" class="entradaCantidad">
                                                                    <br><br>
                                                                </div>
                                                                <div class="botonesFlotante"><button type="submit" class="btn btn-primary btn-sm"><img src="images/aceptar.png" class="btnIcono">Editar</button>
                                                                    <div onclick="flotante('editarCantidad')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                                                            </form>   
                                                        </div>

                                                        <div id="eliminarParte" class="bloqueFlotante" style="display: none;">
                                                            <div class="tituloFlotante">Eliminar Producto</div>
                                                            <form action="eliminarParte" method="post">
                                                                <div class="principalFlotante">
                                                                    <input id="idPedidoParteEliminar" type="hidden" name="idPedidoParte" value="">
                                                                    <h4 class="tituloListado">¿Estas seguro de eliminar el producto?</h4>
                                                                    <p class="parrafoListado">
                                                                        Si eliminas el <strong>Producto</strong> ya no aparecera en la lista del <strong>Pedido</strong>
                                                                    </p>
                                                                    <br><br>
                                                                </div>
                                                                <div class="botonesFlotante"><button type="submit" class="btn btn-danger btn-sm"><img src="images/borrarBlanco.png" class="btnIcono">Eliminar</button>
                                                                    <div onclick="flotante('eliminarParte')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                                                            </form>   
                                                        </div>

                                                        <div id="cancelarPedido" class="bloqueFlotante" style="display: none;">
                                                            <div class="tituloFlotante">Cancelar Pedido</div>
                                                            <form action="cancelarPedido" method="post">
                                                                <div class="principalFlotante">
                                                                    <input type="hidden" name="idPedido" value="<%=pedido.getId()%>">
                                                                    <h4 class="tituloListado">¿Estas seguro de cancelar el pedido?</h4>
                                                                    <br>
                                                                    <p class="parrafoListado">
                                                                        Si cancelas el <strong>Pedido</strong> ya no aparecera en la lista del <strong>Pedidos</strong>
                                                                    </p>
                                                                    <br><br>
                                                                </div>
                                                                <div class="botonesFlotante"><button type="submit" class="btn btn-danger btn-sm"><img src="images/borrarBlanco.png" class="btnIcono">Cancelar Pedido</button>
                                                                    <div onclick="flotante('cancelarPedido')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cancelar</div></div>
                                                            </form>   
                                                        </div>

                                                        <div id="agregarProducto" class="bloqueFlotante" style="display: none;">
                                                            <div class="tituloFlotante">Buscar</div>
                                                            <div class="principalFlotante">
                                                                <div style="display: inline-block;"><input style="width: 400px;" id="busquedaTPV" onkeyup="ejecutarBusqueda(event);" type="text" autocomplete="off" placeholder="Buscar Producto" name="objeto" class="form-control"></div> 
                                                                <button class="btnBuscar" onclick="buscarProducto(<%=pedido.getId()%>);"><img src="images/buscar.png" class="iconoNormal"></button>

                                                                <div style="text-align: center;" id="espera"></div>
                                                                <div>
                                                                    <table id="listaProductos" class="table table-responsive table-striped tabla-producto letraChica">
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="botonesFlotante"><div onclick="flotante('agregarProducto'); limpiarBuscar();" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div></div>
                                                        </div>


                                                        <div id="cancelarFuerza" class="bloqueFlotante" style="display: none;">
                                                            <div class="tituloFlotante">Cancelación de Pedido</div>
                                                            <form action="forzarCancelacion" method="post">
                                                                <div class="principalFlotante">
                                                                    <h4 style="text-align: center;" class="tituloListado">
                                                                        ¿Estas seguro de cancelar el pedido?
                                                                    </h4>
                                                                    <br><br>
                                                                    <div class="separadorListado2"></div> 
                                                                    <p class="parrafoListado">
                                                                        <%if (pedido.getEstado().equals("8") || pedido.getEstado().equals("9") || pedido.getEstado().equals("10")) {%>
                                                                        <strong>Nota: </strong>Selecciona la sucursal donde se agregara el producto de almacen<br>
                                                                    </p>
                                                                    <br><br>
                                                                    <input type="hidden" name="idPedido" value="<%=pedido.getId()%>">

                                                                    <div style="margin-left: 210px;" class="labelSwitch">Agregar pedido a stock</div>
                                                                    <div id="switchBloqueP" onclick="switchPedido()" class="switchBloque switchBlanco">
                                                                        <div id="switchP" class="switch switchIzquierda"></div>
                                                                    </div>
                                                                    <input id="inputP" type="hidden" name="mandarStock" value="0">
                                                                    <br><br>

                                                                    <label class="entradaLabel">Sucursal</label><select id="selectFuerza" disabled name="idSucursal" class="entradaSelect">
                                                                        <option value="0" class="noOpcion">-----</option>
                                                                        <%for (Sucursal scf : scj.listaSucursales()) {%>
                                                                        <%if (scf.getId() != 1) {%>
                                                                        <option class="siOpcion" value="<%=scf.getId()%>"><%=scf.getNombre()%></option>
                                                                        <%}%>
                                                                        <%}%>
                                                                    </select>
                                                                    <%}%>
                                                                </div>
                                                                <div class="botonesFlotante">
                                                                    <button type="submit" class="btn btn-danger btn-sm"><img src="images/cancelarBlanco.png" class="btnIcono">Cancelar</button>
                                                                    <div onclick="flotante('cancelarFuerza'); limpiarBuscar();" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!---------------------------------------------------------------------------------->
                                                        <%if (informacion.getPermisosList().get(0).getProveedoresp() == 1) {%>
                                                        <div id="eliminarProductoProveedor" class="bloqueFlotante" style="display: none;">
                                                            <div class="tituloFlotante">Eliminar Producto</div>
                                                            <form action="editarProductoProveedor" method="post" name="formularioProveedor">
                                                                <div class="principalFlotante">
                                                                    <table class="table table-responsive table-striped">
                                                                        <tr>
                                                                            <td class="ladoIzquierdo">Titulo</td>
                                                                            <td class="ladoDerecho"><span id="tituloF" class="numPedido">Titulo del producto</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="ladoIzquierdo">Cantidad</td>
                                                                            <td class="ladoDerecho"><span id="cantidadF" class="numPedido"><strong id="superCantidad"></strong></span></td>
                                                                        </tr>
                                                                    </table>
                                                                    <div class="separadorListado2"></div>
                                                                    <div style="margin: 20px 0px;" id="cantidadProveedor"></div>
                                                                    <label class="entradaLabel">Destino</label><select id="selectProveedor" name="idSucursal" class="entradaSelect">
                                                                        <option class="noOpcion" value="0">-----</option>
                                                                        <%for (Sucursal scf : scj.listaSucursales()) {%>
                                                                        <%if (scf.getId() != 1) {%>
                                                                        <option class="siOpcion" value="<%=scf.getId()%>"><%=scf.getNombre()%></option>
                                                                        <%}%> 
                                                                        <%}%>
                                                                    </select><br><br>
                                                                    <div class="separadorListado2"></div> 
                                                                    <p style="margin: 20px 50px;" class="tituloListado2">
                                                                        <strong>Nota:</strong> Al ingresar la cantidad total, el producto se eliminara por completo. El producto Eliminado se ira al almacen general;
                                                                    </p>
                                                                </div>
                                                                <div class="botonesFlotante">
                                                                    <div onclick="enviarFormularioProveedor();" class="btn btn-sm btn-danger"><img src="images/borrarBlanco.png" class="btnIcono">Eliminar</div>
                                                                    <div onclick="flotante('eliminarProductoProveedor');" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <%}%>
                                                        <!--//////////////////////////////////////////Ayuda/////////////////////////////////////-->

                                                        <div id="ayuda1" class="bloqueFlotante" style="display: none;">
                                                            <div class="tituloFlotante">Ayuda - Cambio automatico de descuentos</div>
                                                            <div class="principalFlotante">
                                                                <ul>
                                                                    <li>Cuando el switch esta <strong>Activado</strong> se realizara el cambio automatico de descuentos.</li>
                                                                    <li>Al cambiar un producto todos los articulos que tengan el mismo <strong>Proveedor</strong> y sean de la misma <strong>editorial</strong> cambiaran automaticamente.</li>
                                                                    <li>Una vez <strong>Desactivado</strong> el switch los productos se cambiaran uno por uno.</li>
                                                                    <li>Es posible activar y desactivar el switch las veces necesarias.</li>
                                                                </ul>
                                                            </div>
                                                            <div class="botonesFlotante"><div onclick="flotante('ayuda1')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div>
                                                            </div>
                                                        </div>

                                                        <div id="ayuda2" class="bloqueFlotante" style="display: none;">
                                                            <div class="tituloFlotante">Ayuda - Edición Productos</div>
                                                            <div class="principalFlotante">
                                                                <ul>
                                                                    <li>Agrega <strong>Productos</strong> al pedido dando clic en <span class="btn btn-xs">Agregar Producto</span>.</li>
                                                                    <li>En la ventana <strong>Agregar Producto</strong> escribe el <strong>Titulo</strong> o <strong>ISBN</strong> al menos tres caracteres en la barra de busqueda.</li>
                                                                    <li>Una vez con las opciones ingresa la<strong>Cantidad</strong> y da clic en <img src="images/suma2.png" class="btnIcono"></li>
                                                                    <li>Edita la <strong>cantidad</strong> de un producto dando clic en <span class="entradaTablac">5</span> del producto que deseas.</li>
                                                                    <li>Puedes eliminar un <strong>Producto</strong> del pedido dando clic en <img src="images/less.png" class="btnIcono"></li>
                                                                </ul>
                                                            </div>
                                                            <div class="botonesFlotante"><div onclick="flotante('ayuda2')" class="btn btn-default btn-sm"><img src="images/cancelar.png" class="btnIcono">Cerrar</div>
                                                            </div>
                                                        </div>

                                                        <!--/////////////////////////////////////////////// Notificaciones  /////////////////////////////////////////////////////////////-->
                                                        <div class="notificacionBloque" style="<%if (notificacion == 8) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/notiSuccess.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Producto Eliminado</div>
                                                                El <strong>producto</strong> se quito del pedido satisfactoriamente.
                                                            </div>
                                                        </div>
                                                        <div class="notificacionBloque" style="<%if (notificacion == 9) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/notiSuccess.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Producto Agregado</div>
                                                                El <strong>producto</strong> se agrego del pedido satisfactoriamente.
                                                            </div>
                                                        </div>
                                                        <div class="notificacionBloque" style="<%if (notificacion == 10) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/notiSuccess.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Provedores Asignados</div>
                                                                Los <strong>proveedores</strong> se asignaron al pedido satisfactoriamente.
                                                            </div>
                                                        </div>
                                                        <div class="notificacionBloque" style="<%if (notificacion == 11) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/notiSuccess.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Configuración Guardada</div>
                                                                La <strong>configuración</strong> del pedido se guardo satisfactoriamente.
                                                            </div>
                                                        </div>
                                                        <div class="notificacionBloque" style="<%if (notificacion == 13) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/notiSuccess.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Traslado Iniciado</div>
                                                                El <strong>traslado</strong> del pedido inicio satisfactoriamente.
                                                            </div>
                                                        </div>
                                                        <div class="notificacionBloque" style="<%if (notificacion == 14) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/notiSuccess.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Traslado Terminado</div>
                                                                El <strong>traslado</strong> del pedido termino satisfactoriamente.
                                                            </div>
                                                        </div>
                                                        <div class="notificacionBloque" style="<%if (notificacion == 15) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/notiSuccess.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Pedido Remisionado</div>
                                                                El <strong>pedido</strong> se remisiono satisfactoriamente.
                                                            </div>
                                                        </div>
                                                        <div class="notificacionBloque" style="<%if (notificacion == 16) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/notiSuccess.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Devolucion Agregada</div>
                                                                La <strong>devolución</strong> se agrego satisfactoriamente.
                                                            </div>
                                                        </div>
                                                        <div class="notificacionBloque" style="<%if (notificacion == 17) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/notiSuccess.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Agregar Pago</div>
                                                                El <strong>pago</strong> se agrego satisfactoriamente.
                                                            </div>
                                                        </div>
                                                        <div class="notificacionBloque" style="<%if (notificacion == 18) {%>display:block;<%} else {%>display:none;<%}%>">
                                                            <div class="notificacionImg">
                                                                <img src="images/cancel.png" class="noti">
                                                            </div>
                                                            <div class="notificacionInfo">
                                                                <div class="titulo">Agregar Pago</div>
                                                                El <strong>pago</strong> no se pudo agregar al pedido.
                                                            </div>
                                                        </div>
                                                        <!--///////////////////////////////////////////////Menu/////////////////////////////////////////////////////////////-->
                                                        <%@ include file="menu.jsp" %>
                                                        <script>
                                                            function rellenar(id) {
                                                                var serial = document.getElementById('isbn' + id).value;
                                                                if (serial.length > 0) {
                                                                    $.post("buscarProductoPedido", {serial}, function (data) {
                                                                        if (data.length > 2) {
                                                                            var datos = data.split("-");
                                                                            document.getElementById('titulo' + id).innerHTML = datos[0];
                                                                            document.getElementById('autor' + id).innerHTML = datos[1];
                                                                            document.getElementById('editorial' + id).innerHTML = datos[2];
                                                                            document.getElementById('nivel' + id).innerHTML = datos[4];
                                                                            document.getElementById('pventa' + id).innerHTML = datos[6];
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        </script>
                                                        <script>
                                                            function rellenarfaltante(id) {
                                                                var serial = document.getElementById('fisbn' + id).value;
                                                                if (serial.length > 0) {
                                                                    $.post("buscarProductoPedido", {serial}, function (data) {
                                                                        if (data.length > 2) {
                                                                            var datos = data.split("-");
                                                                            document.getElementById('ftitulo' + id).innerHTML = datos[0];
                                                                            document.getElementById('fautor' + id).innerHTML = datos[1];
                                                                            document.getElementById('feditorial' + id).innerHTML = datos[2];
                                                                            document.getElementById('fnivel' + id).innerHTML = datos[4];
                                                                            document.getElementById('fpventa' + id).innerHTML = datos[6];
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        </script>
                                                        <script>
                                                            function agregar(numero) {
                                                                document.getElementById('solo').id = "vacio";
                                                                document.getElementById('vacio').innerHTML += "\n" +
                                                                        "                                <td>" + numero + "</td><td><input id=\"isbn" + numero + "\" onblur=\"rellenar('" + numero + "');\" class=\"entradaTablad\" type=\"text\" name=\"isbn" + numero + "\" maxlength=\"13\" pattern=\"[0-9]+\"></td>\n" +
                                                                        "                                <td id='titulo" + numero + "'></td>\n" +
                                                                        "                                <td id='autor" + numero + "'></td>\n" +
                                                                        "                                <td id='editorial" + numero + "'></td>\n" +
                                                                        "                                <td><input id=\"cantidad" + numero + "\" class=\"entradaTablac\" type=\"text\" name=\"cantidad" + numero + "\" pattern=\"[0-9]+\"></td>\n" +
                                                                        "                                <td id='nivel" + numero + "'></td>\n" +
                                                                        "                                <td id='pventa" + numero + "'></td>\n" +
                                                                        "                            ";
                                                                numero++;
                                                                document.getElementById('vacio').id = "";
                                                                document.getElementById('btnAgregar').innerHTML = "<div class=\"btn btn-default\" onclick=\"agregar('" + numero + "')\"><img src=\"images/mas.png\" class=\"btnIcono\">Agregar Campo</div>";
                                                            }
                                                        </script>
                                                        <script>
                                                            function agregarfaltante(numero) {
                                                                document.getElementById('solo2').id = "vacio2";
                                                                document.getElementById('vacio2').innerHTML += "\n" +
                                                                        "                                <td>" + numero + "</td><td><input id=\"fisbn" + numero + "\" onblur=\"rellenar('" + numero + "');\" class=\"entradaTablad\" type=\"text\" name=\"fisbn" + numero + "\" maxlength=\"13\" pattern=\"[0-9]+\"></td>\n" +
                                                                        "                                <td id='ftitulo" + numero + "'></td>\n" +
                                                                        "                                <td id='fautor" + numero + "'></td>\n" +
                                                                        "                                <td id='feditorial" + numero + "'></td>\n" +
                                                                        "                                <td><input id=\"fcantidad" + numero + "\" class=\"entradaTablac\" type=\"text\" name=\"fcantidad" + numero + "\" pattern=\"[0-9]+\"></td>\n" +
                                                                        "                                <td id='fnivel" + numero + "'></td>\n" +
                                                                        "                                <td id='fpventa" + numero + "'></td>\n" +
                                                                        "                            ";
                                                                numero++;
                                                                document.getElementById('vacio2').id = "";
                                                                document.getElementById('btnAgregarf').innerHTML = "<div class=\"btn btn-default\" onclick=\"agregarfaltante('" + numero + "')\"><img src=\"images/mas.png\" class=\"btnIcono\">Agregar Campo</div>";
                                                            }
                                                        </script>
                                                        <script>
                                                            function verificarDevolucion() {
                                                                var valor = 0;
                                                                if (!document.getElementById('isbn1').value.length > 0) {
                                                                    window.alert("Devolución Vacia");
                                                                    valor = 1;
                                                                }
                                                                for (var x = 1; x < 100; x++) {
                                                                    if (document.getElementById('isbn' + x) !== null && document.getElementById('isbn' + x).value.length > 0) {

                                                                        if (document.getElementById('cantidad' + x).value.length === 0) {
                                                                            window.alert("Llene el campo del Cantidad: " + x);
                                                                            valor = 1;
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        break;
                                                                    }
                                                                }
                                                                if (valor === 0) {
                                                                    document.devolucion1.submit();
                                                                    //                    window.alert("Bien");
                                                                }
                                                            }

                                                        </script>
                                                        <script>
                                                            function verificarFaltante() {
                                                                var valor = 0;
                                                                if (!document.getElementById('fisbn1').value.length > 0) {
                                                                    window.alert("Pedido Vacio");
                                                                    valor = 1;
                                                                }
                                                                for (var x = 1; x < 100; x++) {
                                                                    if (document.getElementById('fisbn' + x) !== null && document.getElementById('fisbn' + x).value.length > 0) {

                                                                        if (document.getElementById('fcantidad' + x).value.length === 0) {
                                                                            window.alert("Llene el campo del Cantidad: " + x);
                                                                            valor = 1;
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        break;
                                                                    }
                                                                }
                                                                if (valor === 0) {
                                                                    document.faltante1.submit();
                                                                    //                    window.alert("Bien");
                                                                }
                                                            }

                                                        </script>
                                                        <script>
                                                            function incompleto() {
                                                                document.getElementById('faltante').style.display = "block";
                                                                document.getElementById('btnCompleto').style.display = "none";
                                                                document.getElementById('btnIncompleto').style.display = "none";
                                                            }

                                                            function incompletoCerrar() {
                                                                document.getElementById('faltante').style.display = "none";
                                                                document.getElementById('btnCompleto').style.display = "inline-block";
                                                                document.getElementById('btnIncompleto').style.display = "inline-block";
                                                            }

                                                            function devolucion() {
                                                                document.getElementById('devo').style.display = "block";
                                                                document.getElementById('btnDevolucion').style.display = "none";
                                                            }

                                                            function imprimirCompra(id) {
                                                                if (document.getElementById("marca" + id) !== null) {
                                                                    document.getElementById("marca" + id).innerHTML = "<img src='images/sipermiso.png' class='iconoNormalGrande'>";
                                                                }
                                                                location.href = "reporteCompra?id=" + id;
                                                                document.getElementById("marca" + id).innerHTML = "<img src='images/sipermiso.png' class='iconoNormalGrande'>";
                                                            }

                                                            function listado(id) {
                                                                document.getElementById('informacionLista').style.display = "none";
                                                                document.getElementById('informacionLista1').className = "listadoOpcion";
                                                                document.getElementById('productosListado').style.display = "none";
                                                                document.getElementById('productosListado1').className = "listadoOpcion";
                                                                document.getElementById('proListado').style.display = "none";
                                                                document.getElementById('proListado1').className = "listadoOpcion";
                                                                document.getElementById('trasladoListado').style.display = "none";
                                                                document.getElementById('trasladoListado1').className = "listadoOpcion";
                                                                document.getElementById('pagosListado').style.display = "none";
                                                                document.getElementById('pagosListado1').className = "listadoOpcion";
                                                                document.getElementById('comprasListado').style.display = "none";
                                                                document.getElementById('comprasListado1').className = "listadoOpcion";
                                                                document.getElementById('devolucionListado').style.display = "none";
                                                                document.getElementById('devolucionListado1').className = "listadoOpcion";
                                                                document.getElementById('remisionListado').style.display = "none";
                                                                document.getElementById('remisionListado1').className = "listadoOpcion";
                                                                document.getElementById('opcionesListado').style.display = "none";
                                                                document.getElementById('opcionesListado1').className = "listadoOpcion";
                                                                document.getElementById(id).style.display = "block";
                                                                document.getElementById(id + "1").className = "selectL";

                                                            }

                                                            function tapar(id) {
                                                                document.getElementById('proveedorSelect' + id).innerHTML += "<option id='opt" + id + "' value='0'> ----- </option>";
                                                                document.getElementById('proveedorSelect' + id).value = 0;
                                                                document.getElementById('proveedorSelect' + id).disabled = true;
                                                            }
                                                            function liberar(id) {
                                                                document.getElementById('proveedorSelect' + id).disabled = false;
                                                                document.getElementById('proveedorSelect' + id).removeChild(document.getElementById('opt' + id));

                                                            }
                                                            function ponerDescuento(id) {
                                                                if (document.getElementById('switchBloque1').className === "switchBloque switchVerde") {
                                                                    var proveedor = document.getElementById('proveedorSelect' + id).value;
                                                                    var editorial = document.getElementById('editorial' + id).value;
                                                                    var descuento = document.getElementById('descuento' + id).value;
                                                                    for (var x = 1; x <= document.getElementById('cantidad').value; x++) {
                                                                        if (document.getElementById('proveedorSelect' + x).value === proveedor && document.getElementById('editorial' + x).value === editorial) {
                                                                            document.getElementById('descuento' + x).value = descuento;
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            function switchf(id) {
                                                                if (document.getElementById('switchBloque' + id).className === "switchBloque switchVerde") {
                                                                    document.getElementById('switchBloque' + id).className = "switchBloque switchBlanco";
                                                                    document.getElementById('switch' + id).className = "switch switchIzquierda";
                                                                    document.getElementById('input' + id).value = "0";
                                                                } else {
                                                                    document.getElementById('switchBloque' + id).className = "switchBloque switchVerde";
                                                                    document.getElementById('switch' + id).className = "switch switchDerecha";
                                                                    document.getElementById('input' + id).value = "1";
                                                                }
                                                            }

                                                            function switchPedido() {
                                                                if (document.getElementById('switchBloqueP').className === "switchBloque switchVerde") {
                                                                    document.getElementById('switchBloqueP').className = "switchBloque switchBlanco";
                                                                    document.getElementById('switchP').className = "switch switchIzquierda";
                                                                    document.getElementById('inputP').value = "0";
                                                                    document.getElementById('selectFuerza').disabled = true;
                                                                    document.getElementById('selectFuerza').value = 0;
                                                                } else {
                                                                    document.getElementById('switchBloqueP').className = "switchBloque switchVerde";
                                                                    document.getElementById('switchP').className = "switch switchDerecha";
                                                                    document.getElementById('inputP').value = "1";
                                                                    document.getElementById('selectFuerza').disabled = false;
                                                                }

                                                            }

                                                            function editarCantidad(id) {
                                                                document.getElementById('isbnEditar').innerHTML = document.getElementById('isbnProducto' + id).innerHTML;
                                                                document.getElementById('tituloEditar').innerHTML = document.getElementById('tituloProducto' + id).innerHTML;
                                                                document.getElementById('idPedidoParte').value = id;

                                                                flotante('editarCantidad');
                                                            }

                                                            function eliminarParte(id) {
                                                                document.getElementById('idPedidoParteEliminar').value = id;
                                                                flotante('eliminarParte');
                                                            }

                                                            function ejecutarBusqueda(e) {
                                                                (e.keyCode) ? k = e.keyCode : k = e.which;
                                                                if (k === 13) {
                                                                    buscarProducto(<%=pedido.getId()%>);
                                                                }
                                                            }

                                                            function buscarProducto(id) {
                                                                if (document.getElementById('busquedaTPV').value.length > 3) {
                                                                    document.getElementById('espera').innerHTML = "<img class='loadingChico' src='images/loading2.gif'>";
                                                                    document.getElementById('listaProductos').innerHTML = "";
                                                                    var objeto = document.getElementById('busquedaTPV').value;
                                                                    $.post("buscarParte", {objeto, id}, function (data) {
                                                                        if (data.length < 6) {
                                                                            document.getElementById('espera').innerHTML = "<h5 class='tituloListado2'>No hay resultados</h5>";
                                                                        } else {
                                                                            document.getElementById('espera').innerHTML = "";
                                                                            document.getElementById('listaProductos').innerHTML = data;
                                                                        }
                                                                    });
                                                                }
                                                            }

                                                            function limpiar(id) {
                                                                document.getElementById("cantidadD" + id).value = "";
                                                            }
                                                            function limpiarCantidad(id) {
                                                                document.getElementById("descuento" + id).value = "";
                                                            }

                                                            function checarFormulario() {
                                                                var check = 0;
                                                                for (var f = 1; f <<%=proP%>; f++) {
                                                                    for (var z = 1; z <<%=valorDevolucion%>; z++) {
                                                                        if (document.getElementById('cantidadD' + f + z) && document.getElementById('cantidadD' + f + z).value.length === 0) {
                                                                            check = 1;
                                                                        }
                                                                    }
                                                                }
                                                                if (check === 0) {
                                                                    document.devolucionFormulario.submit();
                                                                } else {
                                                                    window.alert("Llena todos los campos");

                                                                }
                                                            }

                                                            function cambiarColor(pro, id) {
                                                                if (parseInt(document.getElementById('cantidadD' + pro + id).value) > 0) {
                                                                    document.getElementById('cantidadD' + pro + id).className = "entradaTablaDevolucion";
                                                                } else {
                                                                    document.getElementById('cantidadD' + pro + id).className = "entradaTablac";
                                                                }
                                                                var cuenta = 0;
                                                                var cantidad = 0;

                                                                for (var x = 1; x < 50; x++) {
                                                                    if (document.getElementById('cantidadD' + pro + x)) {
                                                                        cuenta += parseInt(document.getElementById('cantidadD' + pro + x).value);
                                                                        cantidad += parseInt(document.getElementById('cantidadProducto' + pro + x).innerHTML);
                                                                    }
                                                                }

                                                                var numero = cuenta / cantidad * 100;
                                                                document.getElementById('semaforoA' + pro).innerHTML = numero.toFixed(1) + "%";
                                                                if (numero < 20) {
                                                                    if (numero < 15) {
                                                                        if (numero < 10) {
                                                                            document.getElementById('semaforoA' + pro).className = "luzNormal";
                                                                        } else {
                                                                            document.getElementById('semaforoA' + pro).className = "luzAmarilla";
                                                                        }
                                                                    } else {
                                                                        document.getElementById('semaforoA' + pro).className = "luzAmarilla";
                                                                    }
                                                                } else {
                                                                    document.getElementById('semaforoA' + pro).className = "luzRoja";
                                                                }

                                                            }


                                                            function editarProductoPedido(id) {
                                                                var cantidad = document.getElementById('cantidadProveedor' + id).innerHTML;
                                                                var titulo = document.getElementById('tituloProducto' + id).innerHTML;

                                                                document.getElementById('tituloF').innerHTML = titulo;
                                                                document.getElementById('superCantidad').innerHTML = cantidad;

                                                                document.getElementById('cantidadProveedor').innerHTML = "<label class='entradaLabel'>Cantidad a eliminar</label><input type='number' max='" + cantidad + "' name='nuevaCantidad' class='entradaCantidad' id='nuevaCantidadProveedor'><input type='hidden' name='ppa' value='" + id + "'><input type='hidden' name='total' id='totalProveedor' value='" + cantidad + "'>"
                                                                flotante('eliminarProductoProveedor');
                                                            }

                                                            function enviarFormularioProveedor() {
                                                                var select = document.getElementById("selectProveedor").value;
                                                                var cantidad = parseInt(document.getElementById("nuevaCantidadProveedor").value);
                                                                var total = parseInt(document.getElementById("totalProveedor").value);
                                                                if (select == 0) {
                                                                    window.alert("Seleccionar Sucursal");
                                                                } else {
                                                                    if (cantidad.length === 0) {
                                                                        window.alert("Ingresa cantidad");
                                                                    } else {
                                                                        if (cantidad > total) {
                                                                            window.alert("La cantidad es mayor - cantidad:"+cantidad+"> total:"+total);
                                                                        } else {
                                                                            document.formularioProveedor.submit();
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            function cambioProveedor(id) {
                                                                if (document.getElementById('switchBloque1').className === "switchBloque switchVerde") {
                                                                    var valor = document.getElementById("proveedorSelect" + id).value;
                                                                    var editorial = document.getElementById('editorial' + id).value;
                                                                    for (var x = id; x <= document.getElementById('cantidad').value; x++) {
                                                                        if (document.getElementById('editorial' + x).value == editorial) {
                                                                            document.getElementById("proveedorSelect" + x).value = valor;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        </script>
                                                        </body>
                                                        </html>
